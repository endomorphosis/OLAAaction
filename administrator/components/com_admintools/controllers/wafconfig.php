<?php
/**
 *  @package AdminTools
 *  @copyright Copyright (c)2010 Nicholas K. Dionysopoulos
 *  @license GNU General Public License version 3, or later
 *  @version $Id: wafconfig.php 44 2010-10-01 11:39:31Z nikosdion $
 */

// Protect from unauthorized access
defined('_JEXEC') or die('Restricted Access');

jimport('joomla.application.component.controller');

require_once JPATH_COMPONENT_ADMINISTRATOR.DS.'controllers'.DS.'default.php';

class AdmintoolsControllerWafconfig extends AdmintoolsControllerDefault
{
	public function save()
	{
		$model = $this->getThisModel();
		$data = JRequest::get('post');
		$model->saveConfig($data);

		$this->setRedirect('index.php?option=com_admintools&view=waf',JText::_('ATOOLS_LBL_WAF_CONFIGSAVED'));
	}
}
