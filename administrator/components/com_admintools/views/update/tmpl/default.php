<?php
/**
 *  @package AdminTools
 *  @copyright Copyright (c)2010 Nicholas K. Dionysopoulos
 *  @license GNU General Public License version 3, or later
 *  @version $Id: default.php 44 2010-10-01 11:39:31Z nikosdion $
 */

defined('_JEXEC') or die('Restricted access');

if(!$this->updates->supported)
{
	$overview_class = 'notok';
	$mode = 'unsupported';
}
elseif( $this->updates->update_available )
{
	$overview_class = 'update';
	$mode = 'update';
}
else
{
	$overview_class = 'ok';
	$mode = 'ok';
}

$isPro = JFile::exists(JPATH_COMPONENT_ADMINISTRATOR.DS.'tables'.DS.'redirs.php');

?>

<?php if( ($isPro) && empty($this->updates->package_url_suffix) && ($this->updates->update_available) ): ?>
<div class="admintools-warning">
	<?php echo JText::_('ATOOLS_ERR_UPDATE_USERNAMEPASSWORDREQ'); ?>
</div>
<?php endif; ?>

<div class="disclaimer <?php echo $overview_class; ?>">
	<h3>
	<?php switch($mode):
			case 'ok': ?>
		<?php echo JText::_('ATOOLS_LBL_UPDATE_NOUPGRADESFOUND') ?>
	<?php	break;
			case 'update': ?>
		<?php echo JText::_('ATOOLS_LBL_UPDATE_UPGRADEFOUND') ?>
	<?php	break;
			default: ?>
		<?php echo JText::_('ATOOLS_LBL_UPDATE_NOTAVAILABLE') ?>
	<?php endswitch; ?>
	</h3>
</div>

<?php if($mode != 'unsupported'): ?>
	<table id="version_info_table" class="ui-corner-all">
		<tr>
			<td class="label"><?php echo JText::_('ATOOLS_LBL_UPDATE_EDITION') ?></td>
			<td colspan="3">
				<?php if($isPro): ?>
				<strong>Admin Tools Professional</strong>
				<?php else: ?>
					Admin Tools Core
				<?php endif; ?>
			</td>
		</tr>
		<tr>
			<td class="label"><?php echo JText::_('ATOOLS_LBL_UPDATE_YOURVERSION') ?></td>
			<td>
				<span class="version"><?php echo $this->updates->current_version ?></span>
				<span class="version-status">
					(<?php echo JText::_('ATOOLS_LBL_UPDATE_STATUS_'.strtoupper($this->updates->current_status)); ?>)
				</span>
			</td>
			<td colspan="2">
				<?php echo JText::_('ATOOLS_LBL_UPDATE_RELEASEDON') ?>
				<span class="reldate"><?php echo $this->updates->current_date ?></span>
			</td>
		</tr>
		<tr>
			<td class="label"><?php echo JText::_('ATOOLS_LBL_UPDATE_LATESTVERSION') ?></td>
			<td>
				<span class="version"><?php echo $this->updates->latest_version ?></span>
				<span class="version-status">
					(<?php echo JText::_('ATOOLS_LBL_UPDATE_STATUS_'.strtoupper($this->updates->status)); ?>)
				</span>
			</td>
			<td colspan="2">
				<?php echo JText::_('ATOOLS_LBL_UPDATE_RELEASEDON') ?>
				<span class="reldate"><?php echo $this->updates->latest_date ?></span>
			</td>
		</tr>
		<tr>
			<td class="label"><?php echo JText::_('ATOOLS_LBL_UPDATE_PACKAGELOCATION') ?></td>
			<td colspan="3">
				<a href="<?php echo htmlentities($this->updates->package_url.$this->updates->package_url_suffix) ?>">
					<?php echo htmlentities($this->updates->package_url); ?>
				</a>
			</td>
		</tr>
	</table>

	<div id="updater-buttons">

	<?php if($mode == 'update'): ?>
	<form enctype="multipart/form-data" action="index.php" method="post" name="adminForm" id="updateform">
		<input type="hidden" name="option" value="com_admintools" />
		<input type="hidden" name="view" value="update" />
		<input type="hidden" name="task" value="update" />
		<input type="submit" value="<?php echo JText::_('ATOOLS_LBL_UPDATE_UPDATENOW'); ?>" />
	</form>
	<br/>
	<?php endif; ?>

	<form enctype="multipart/form-data" action="index.php" method="post" name="adminForm" id="requeryform">
		<input type="hidden" name="option" value="com_admintools" />
		<input type="hidden" name="view" value="update" />
		<input type="hidden" name="task" value="force" />
		<input type="submit" value="<?php echo JText::_('ATOOLS_LBL_UPDATE_FORCE'); ?>" />
	</form>

	</div>

<?php endif; ?>