<?php
/**
 *  @package AdminTools
 *  @copyright Copyright (c)2010 Nicholas K. Dionysopoulos
 *  @license GNU General Public License version 3, or later
 *  @version $Id: view.html.php 44 2010-10-01 11:39:31Z nikosdion $
 */

// Protect from unauthorized access
defined('_JEXEC') or die('Restricted Access');

// Load framework base classes
jimport('joomla.application.component.view');

/**
 * MVC View for Live Update
 *
 */
class AdmintoolsViewUpdate extends JView
{
	function display()
	{
		$task = JRequest::getCmd('task');
		$force = ($task == 'force');

		// Set the toolbar title; add a help button
		JToolBarHelper::title(JText::_('ADMINTOOLS_TITLE_UPDATE'),'admintools');
		JToolBarHelper::back('Back', 'index.php?option='.JRequest::getCmd('option'));

		// Load the model
		$model =& $this->getModel();
		$updates =& $model->getUpdates($force);
		$this->assignRef('updates', $updates);

		$document = JFactory::getDocument();
		$document->addStyleSheet('../media/com_admintools/css/backend.css');

		parent::display();
	}
}