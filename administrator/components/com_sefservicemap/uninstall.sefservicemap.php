<?php
//*******************************************************
//* SEF Service Map Component
//* http://www.sefservicemap.com
//* (C) Radoslaw Kubera
//* license http://www.gnu.org/copyleft/gpl.html GNU/GPL
//*******************************************************

jimport( 'joomla.installer.installer' );
jimport('joomla.installer.helper');
jimport('joomla.client.ftp');

jimport('joomla.filesystem.file');
jimport('joomla.filesystem.folder');
jimport('joomla.filesystem.archive');
jimport('joomla.filesystem.path');

function remove_integrator($folder,$integrator) {
	$result = true;
	$db = JFactory::getDBO();

	$installer = new JInstaller;
	$installer->setOverwrite(false);

	$installer->setPath('extension_root', JPATH_SITE.DS.'plugins'.DS.$folder);
	$manifestFile = JPATH_SITE.DS.'plugins'.DS.$folder.DS.$integrator.'.xml';

	if (file_exists($manifestFile))
	{
		$xml =& JFactory::getXMLParser('Simple');
			// If we cannot load the xml file return null
		if (!$xml->loadFile($manifestFile)) {
			return false;
		}

		$root =& $xml->document;
		if ($root->name() != 'install' && $root->name() != 'mosinstall') {
			return false;
		}
			
		$files = $root->getElementByPath('files');
		$installer->removeFiles($files,-1);

		$images = $root->getElementByPath('images');
		$installer->removeFiles($images,-1);

		$languages = $root->getElementByPath('languages');
		$installer->removeFiles($languages,1);

		$media = $root->getElementByPath('media');
		$installer->removeFiles($media);

		JFile::delete($manifestFile);

	} else {
		return false;
	}
	return $result;
}

function update_robots_txt() {
	$robots = array();
	$robots = file (JPATH_SITE.DS.'robots.txt');

	$new_robots = array();
	if ($robots) {
		$is = 0;
		foreach ($robots as $line) {
			if (!eregi('com_sefservicemap',$line)) {
				$new_robots[]=$line;
			} 
			else {
				$is = 1;
			}
		}

	}

	if ($is==1) {
		$file = JPATH_SITE.DS.'robots.txt';

		if (@$rob=fopen($file,'w+')) {
			foreach ($new_robots as $line) {
				fwrite($rob, trim($line)."\n");
			}
			fclose ($rob);
		} 
		else {
			JError::raiseWarning(-1, JText::_("Cannot change robots.txt. Please change robots.txt manualy."));
		}
	}
}

function com_uninstall() 
{
	update_robots_txt();

	remove_integrator('system','sefservicemap');
	remove_integrator('com_sefservicemap','com_contact_bot');
	remove_integrator('com_sefservicemap','com_content_bot');
	remove_integrator('com_sefservicemap','com_newsfeeds_bot');
	remove_integrator('com_sefservicemap','com_weblinks_bot');

	$db = JFactory::getDBO();

	$query = "delete from #__plugins where element='sefservicemap'";
	$db->setQuery($query);
	$db->query();

	$query = "delete from #__plugins where element='com_contact_bot' and folder='com_sefservicemap'";
	$db->setQuery($query);
	$db->query();

	$query = "delete from #__plugins where element='com_content_bot' and folder='com_sefservicemap'";
	$db->setQuery($query);
	$db->query();

	$query = "delete from #__plugins where element='com_newsfeeds_bot' and folder='com_sefservicemap'";
	$db->setQuery($query);
	$db->query();

	$query = "delete from #__plugins where element='com_weblinks_bot' and folder='com_sefservicemap'";
	$db->setQuery($query);
	$db->query();

	$query = "select value from #__sef_sm_settings where variable='keep_settings'";
	$db->setQuery($query);
	$keep_settings=$db->loadResult();
	if (!$keep_settings) {
		$db->setQuery("DROP TABLE #__sef_sm_cache");
		$db->query();
		$db->setQuery("DROP TABLE #__sef_sm_cache_items");
		$db->query();
		$db->setQuery("DROP TABLE #__sef_sm_menu");
		$db->query();
		$db->setQuery("DROP TABLE #__sef_sm_menus");
		$db->query();
		$db->setQuery("DROP TABLE #__sef_sm_plugins");
		$db->query();
		$db->setQuery("DROP TABLE #__sef_sm_pingback");
		$db->query();
		$db->setQuery("DROP TABLE #__sef_sm_pingback_log");
		$db->query();
		$db->setQuery("DROP TABLE #__sef_sm_pingback_menu");
		$db->query();
		$db->setQuery("DROP TABLE #__sef_sm_pingback_stack");
		$db->query();
		$db->setQuery("DROP TABLE #__sef_sm_pingback_plugins");
		$db->query();
		$db->setQuery("DROP TABLE #__sef_sm_settings");
		$db->query();
		echo JText::_('Settings removed');
	}
}
?>