<?php
//*******************************************************
//* SEF Service Map Component
//* http://www.sefservicemap.com
//* (C) Radoslaw Kubera
//* license http://www.gnu.org/copyleft/gpl.html GNU/GPL
//*******************************************************


/** ensure this file is being included by a parent file */
defined( '_JEXEC' ) or die( 'Restricted access' );

class HTML_sefservicemap_admin {
	var $component_params;
	var $utils;
	function HTML_sefservicemap_admin() {
		global $mainframe;
		$this->component_params=$mainframe->ParamsArray;
	}

	function about() {
		$lang	=& JFactory::getLanguage();
		$mylang = $lang->_lang;
		$mylang = JRequest::getVar('lang',$mylang);
		?>
		<form action="index2.php" method="post" name="adminForm">
			<table class="admintable" width="100%">
				<tr>
					<td>
						<fieldset class="adminform">
							<legend><?php echo JText::_( 'Help' ); ?></legend>
							<?php

							if (file_exists(JPATH_COMPONENT.DS."config".DS.$mylang.".help.php")) { 
								$f = file (JPATH_COMPONENT.DS."config".DS.$mylang.".help.php");
							} 
							else {
								$f = file (JPATH_COMPONENT.DS."config".DS."help.php");
							} 
							$txt = implode ("<br/>",$f); 
							echo $txt;
							?>
						</fieldset>
					</td>
				</tr>
			</table>
			<input type="hidden" name="task" value="">
			<input type="hidden" name="option" value="com_sefservicemap" />
		</form>
		<?php
	}

	function edit_config ($params,$langs,$robots) { 

		$user =& JFactory::getUser();
		JHTML::_('behavior.tooltip');

		$version = ' v.'._SEF_SM_VERSION;
		?>
		<form action="index2.php" method="post" name="adminForm">
			<table class="admintable" width="100%">
				<tr>
					<td>
					<?php if (isset($robots->to_add)) {
						?>
						<fieldset class="adminform">
							<legend><?php echo JText::_( 'robots.txt' ); ?></legend>
								<table width="100%" class="paramlist admintable" cellspacing="1">
									<?php
									if ($robots->to_add) {
										?>
										<tr>
											<td width="40%" class="paramlist_key"><span style="color:red"><?php echo JText::_('Recommended to add') ?></span></td>
											<td class="paramlist_value">
											<?php
											foreach ($robots->to_add as $line) {
												echo 'Sitemap: '.htmlentities($line).'<br/>';
											}
											?>
											</td>
										</tr>
										<?php
									}
									?>
								</table>
						</fieldset>
					<?php } ?>
					<?php if (isset($robots->to_delete)) {
						?>
						<fieldset class="adminform">
							<legend><?php echo JText::_( 'robots.txt' ); ?></legend>
								<table width="100%" class="paramlist admintable" cellspacing="1">
									<?php
									if ($robots->to_delete) {
										$new_robots = array();
										$i = 0;
										foreach ($robots->new_robots as $line) {
											$new_robots[$i] = $line;
											$i++;
										}
										?>
										<tr>
											<td width="40%" class="paramlist_key"><span style="color:red"><?php echo JText::_('Recommended to delete') ?></span></td>
											<td class="paramlist_value">
											<?php
											foreach ($robots->to_delete as $index) {
												$index;
												echo htmlentities($new_robots[intval($index)]).'<br/>';
											}
											?>
											</td>
										</tr>
										<?php
									}
									?>
								</table>
						</fieldset>
					<?php } ?>

						<fieldset class="adminform">
							<legend><?php echo JText::_( 'Sitemaps' ); ?></legend>
								<table width="100%" class="paramlist admintable" cellspacing="1">
									<?php
									if ($langs)
									foreach ($langs as $lang) {
										if (isset($lang->name_xml)) {
											?>
											<tr>
												<td width="40%" class="paramlist_key"><?php echo $lang->name_xml ?></td>
												<td class="paramlist_value"><?php echo $lang->link_xml ?></td>
											</tr>
											<?php
										} 
										if (isset($lang->name_txt)) {
											?>
											<tr>
												<td width="40%" class="paramlist_key"><?php echo $lang->name_txt ?></td>
												<td class="paramlist_value"><?php echo $lang->link_txt ?></td>
											</tr>
											<?php
										}
									}
									if (!isset($robots->to_add)) {
									?>
										<tr>
											<td width="40%" class="paramlist_key">robots.txt</td>
											<td class="paramlist_value"><span style="color:green;">OK!</span></td>
										</tr>
									<?php } ?>
								</table>
						</fieldset>
						
						<fieldset class="adminform">
							<legend><?php echo JText::_( 'Config' ); ?></legend>
								<?php echo $params->render('params','global');?>
						</fieldset>
						<fieldset class="adminform">
							<legend><?php echo JText::_( 'Default integrators settings' ); ?></legend>
								<?php echo $params->render('params','integrators');?>
						</fieldset>
						<fieldset class="adminform">
							<legend><?php echo JText::_( 'Pingback' ); ?></legend>
								<?php echo $params->render('params','pingback');?>
						</fieldset>
						<fieldset class="adminform">
							<legend><?php echo JText::_( 'Cache' ); ?></legend>
								<?php echo $params->render('params','cache');?>
						</fieldset>
						<fieldset class="adminform">
							<legend><?php echo JText::_( 'CSS' ); ?></legend>
								<?php echo $params->render('params','css');?>
						</fieldset>
					</td>
				</tr>
				<tr align="center"><td align="center"><div class="small"><a href="<?php echo _SEF_SM_HOME_PAGE ?>">SEF Service Map Component<?php echo $version?></a></div></td></tr><tr align="center"><td align="center"><div class="small">by Radoslaw Kubera</div></td></tr>        
			</table>
			<input type="hidden" name="task" value="config">
			<input type="hidden" name="option" value="com_sefservicemap" />
		</form>
		<?php
	}

	function edit_config_css ($params) { 
		$user =& JFactory::getUser();
		JHTML::_('behavior.tooltip');

		$version = ' v.'._SEF_SM_VERSION;

		?>
		<form action="index2.php" method="post" name="adminForm">	         
			<?php 	      
			$k=0;
			?>
			<table width="100%" class="adminform">
				<tr align="center">
					<td align="center" width="100%">
						<div align="center">
							<textarea name="css" rows="30" cols="100"><?php include JPATH_SITE.DS.'components'.DS.'com_sefservicemap'.DS.'css'.DS.'template_css.css' ?></textarea> 
						</div>
					</td>	
				</tr>
				<tr align="center"><td align="center"><div class="small"><hr/><a href="<?php echo _SEF_SM_HOME_PAGE ?>">SEF Service Map Component<?php echo $version?></a></div></td></tr><tr align="center"><td align="center"><div class="small">by Radoslaw Kubera</div></td></tr>
			</table>
			<input type="hidden" name="task" value="config">
			<input type="hidden" name="option" value="com_sefservicemap" />
		</form>
		<?php
	}

	function edit_integratos ($rows,$params,$comp,$i) {
		$db = JFactory::getDBO();		

		if ($comp==0) 
			$title = JText::_( 'SEF Service Map Integrators' );
			else $title = JText::_( 'Compatible Integrators') .' (Xmap, Joomap)';
		$k=0;
		?>
		<fieldset class="adminform">
			<legend><?php echo $title; ?></legend>

			<table width="100%" class="adminlist">
				<tr>
					<th class="title" width="10" align="center">#</th>		
					<th class="title" width="30%" align="center"><?php echo JText::_('Global Settings') ?></th>
					<th class="title" width="5%" align="center"><?php echo JText::_('Published') ?></th>
					<th class="title" width="10%" align="center"><?php echo JText::_('Access') ?></th>
					<th class="title" width="20%" align="center"><?php echo JText::_('Component') ?></th>
					<th class="title" width="20%" align="center"><?php echo JText::_('Author') ?></th> 
					<th class="title" width="10%" align="center"><?php echo JText::_('Version') ?></th> 
					<th class="title" width="5%" align="center"><?php echo JText::_( 'Compatibility' ); ?></th> 
				</tr>      
				<?php

				$config = &JFactory::getConfig();

				if ($rows)
				foreach ($rows as $row) {
					$k++; if ($k>1) $k=0;       

					$row->checked_out = 0;
					$row->id = $row->id.'-'.$comp;
					$checked = JHTML::_('grid.checkedout',  $row , $i);

					$legacy = 0;
					if ($comp==1) {
						$row->folder= $row->type;
						$publink = 'index2.php?option=com_sefservicemap&amp;task=chintpub&amp;comp=1&amp;id='.$row->id;
						if ($row->published==0) 
							$publink = '<a href="'.$publink.'"><img src="images/publish_x.png"></a>';
						else $publink = '<a href="'.$publink.'"><img src="images/tick.png"></a>';

						$acceslink = 'index2.php?option=com_sefservicemap&amp;task=chintaccess&amp;comp=1&amp;id='.$row->id;
					}
					else {
						$publink = 'index2.php?option=com_sefservicemap&amp;task=chintpub&amp;comp=0&amp;id='.$row->id;
						if ($row->published==0) 
							$publink = '<a href="'.$publink.'"><img src="images/publish_x.png"></a>';
						else $publink = '<a href="'.$publink.'"><img src="images/tick.png"></a>';

						$acceslink = 'index2.php?option=com_sefservicemap&amp;task=chintaccess&amp;comp=0&amp;id='.$row->id;
					}
					
					if (!$config->getValue('config.legacy')) {
						if ($row->legacy==1) {
							$comp_icon="publish_r.png";
							$comp_desc = JText::_( 'Not Compatible Extension');
						}
						else {
							$comp_icon="publish_g.png";
							$comp_desc = JText::_( 'Compatible Extension');

						}
					}
					else {
						if ($row->legacy==1) {
							$comp_icon="publish_y.png";
							$comp_desc = JText::_( 'Extension Compatible in Legacy Mode');

						}
						else {
							$comp_icon="publish_g.png";
							$comp_desc = JText::_( 'Compatible Extension');
						}
					}

					?>
					<tr class="<?php echo "row$k"; ?>">
						<td>
							<?php echo $checked; ?>
						</td>
						<td><a href='index2.php?option=com_sefservicemap&amp;task=integrator&amp;comp=<?php echo $comp ?>&amp;settings=global&id=<?php echo $row->id?>'><?php echo $row->plugname?></a></td>
						<td align="center">
							<?php echo $publink; ?>
						</td>
						<td  align="center">
							<?php
							$db->setQuery("select name from #__groups where id='$row->access'");
							$group = $db->loadResult();
							echo '<a href="'.$acceslink.'">'.$group .'</a>';
							?>
						</td>
						<td>
							<?php 
							if ($comp==0) $row->element=str_replace('_bot','',$row->element);
							echo $row->element; ?>
						</td>
						<td><?php echo $row->author?></td>
						<td><?php echo $row->version?></td>
						<td  align="center">
							<span class="editlinktip hasTip" title="<?php echo $comp_desc ?>">
							<img src="images/<?php echo $comp_icon ?>"/>
							</span>
						</td>
					</tr>
					<?php
					$i++;
				}
				?>    
			</table>
		</fieldset>
		<?php
	}


	function edit_config_integrators ($rows,$comp_rows,$params) { 

		$user =& JFactory::getUser();
		JHTML::_('behavior.tooltip');

		$db = JFactory::getDBO();		

		$version = ' v.'._SEF_SM_VERSION;

		?>
		<form enctype="multipart/form-data" action="index2.php" method="post" name="adminForm">
			<fieldset class="adminform">
				<legend><?php echo JText::_('New integrator').' (SEF Service Map, Xmap, Joomap)'; ?></legend>
				<input class="input_box" id="install_package" name="install_package" type="file" size="57" />
			</fieldset>


			<?php
			$this-> edit_integratos ($rows,$params,0,0);
			$this-> edit_integratos ($comp_rows,$params,1,count($rows));

			?>
			<table align="center">
				<tr align="center"><td colspan="5" align="center"><div class="small"><a href="<?php echo _SEF_SM_HOME_PAGE ?>">SEF Service Map Component<?php echo $version?></a></div></td></tr><tr align="center"><td colspan="5" align="center"><div class="small">by Radoslaw Kubera</div></td></tr>        
			</table>
			<input type="hidden" name="task" value="config_integrators">
			<input type="hidden" name="boxchecked" value="">
			<input type="hidden" name="option" value="com_sefservicemap" />
		</form>
		<?php
	}

	function edit_config_cache ($rows,$params,$cachetime) { 

		$user =& JFactory::getUser();
		JHTML::_('behavior.tooltip');

		$k=1;

		$version = ' v.'._SEF_SM_VERSION;

		?>
		<form action="index2.php" method="post" name="adminForm"> 
			<table width="100%" class="adminlist">
				<tr>
					<th class="title"><?php echo JText::_('Integrator') ?></th>
					<th class="title"><?php echo JText::_('Cache time') ?></th>
				</tr>
				<?php

				if ($rows)
				foreach ($rows as $row) {
					echo '<tr class="row'.$k.'">';
					$k++; if ($k>1) $k=0;     
					echo '<td>'.$row->plugname.'</td>';
					$enabled = $params->get($row->id.'-'.$row->comp.'_cache',0);
					$value = $params->get($row->id.'-'.$row->comp.'_time',0);
					if (!$value) $value = $cachetime;
					?>
					<td> 
						<input size=10" type="text" class = "inputbox" name = "params[<?php echo $row->id.'-'.$row->comp?>_time]" value="<?php echo $value?>"><?php echo JText::_('Seconds')?>
					</td>
					<?php
					echo '</tr>';
				}
				?>
			</table>
			<table align="center">
				<tr align="center"><td colspan="3" align="center"><div class="small"><a href="<?php echo _SEF_SM_HOME_PAGE ?>">SEF Service Map Component<?php echo $version?></a></div></td></tr><tr align="center"><td colspan="3" align="center"><div class="small">by Radoslaw Kubera</div></td></tr>
			</table>
			<input type="hidden" name="task" value="config_cache">
			<input type="hidden" name="option" value="com_sefservicemap" />
		</form>
		<?php
	}

	function edit_integrator ($id,$menu_id,$title,$name,$row,$params,$settings,$compatibile) {
		$version = ' v.'._SEF_SM_VERSION;

		$user =& JFactory::getUser();
		JHTML::_('behavior.tooltip');

		?>	
		<form action="index2.php" method="post" name="adminForm">
			<table  width="100%" class="adminform">
				<tr>
					<th class = "title"><?php echo JText::_('Parameters') ?></th>
					<th class = "title"><?php echo JText::_('Integrator Info')?></th>
				</tr>
				<tr>
					<td width = "50%" valign = "top" align = "left">
						<table class="adminform">
							<tr>
								<td>
									<?php echo $params->render();?>
								</td>
							</tr>
						</table>
					</td>
					<td valign = "top">
						<table class="adminform">
							<tr>
								<td width="100" align="left">
									<?php echo JText::_('Name') ?>:
								</td>
								<td>
									<?php echo $row->plugname; ?>
								</td>
							</tr>
							<tr>
								<td valign="top" align="left">
									<?php echo JText::_('Folder') ?>:
								</td>
								<td>
									<?php echo $row->folder; ?>
								</td>
							</tr>
							<tr>
								<td valign="top" align="left">
									<?php echo JText::_('File') ?>:
								</td>
								<td>
									  <?php echo $row->element; ?>.php
								</td>
							</tr>
							<tr>
								<td valign="top" align="left">
									<?php echo JText::_('Access') ?>:
								</td>
								<td>
									<?php echo $row->group ?>
								</td>
							</tr>
							<tr>
								<td valign="top">
									<?php echo JText::_('Published') ?>:
								</td>
								<td>
									<?php if ($row->published == 1) echo JText::_('Yes'); else echo JText::_('No') ?>
								</td>
							</tr>
							<tr>
								<td valign="top">
			                                        <?php echo JText::_('Author') ?>:
								</td>
								<td>
			                                        <?php echo $row->author ?>
								</td>
							</tr>
							<tr>
								<td valign="top">
									<?php echo JText::_('Version') ?>:
								</td>
								<td>
									<?php echo $row->version; ?>
								</td>
							</tr>
				            </table>   
			          	</td>
		        	</tr>
			        <tr>
					<th class = "title" colspan="2"></th>
				</tr>
				<tr><td colspan="2" valign="bottom" align="center"><div class="small"><a href="<?php echo _SEF_SM_HOME_PAGE ?>">SEF Service Map Component<?php echo $version?></a></div></td></tr><tr><td colspan="2" align="center"><div class="small">by Radoslaw Kubera</div></td></tr>
			</table>   
			<input type="hidden" name="task" value="saveint">
			<input type="hidden" name="menu_id" value="<?php echo $menu_id?>">
			<input type="hidden" name="option" value="com_sefservicemap" />
			<input type="hidden" name="id" value="<?php echo $id?>" /> 
			<input type="hidden" name="compatibile" value="<?php echo $compatibile ?>" /> 
			<input type="hidden" name="settings" value="<?php echo $settings ?>" />  
		</form>
		<?php
	}

	function up_down($id,$count,$number,$header=false) {
		if ($number!=1) {
			?>
	  
			<a href='index2.php?option=com_sefservicemap&task=upmenu&id=<?php echo $id?>'><img src='images/uparrow.png' width="12" height="12" border="0" alt="Move Up"></a>
			<?php 
		}
		if ($header) echo '</th><th bgcolor="#E4E4E4">'; else echo '</td><td>';
		if ($number!=$count) {
			?>
			<a href='index2.php?option=com_sefservicemap&task=downmenu&id=<?php echo $id?>'><img src='images/downarrow.png' width="12" height="12" border="0" alt="Move Up"></a>
			<?php 
		}
	}

	function editPingBack($pingback) {
		JHTML::_('behavior.tooltip');
		$version = ' v.'._SEF_SM_VERSION;

		$countm = count($pingback->stack);

		?>
		<form action="index2.php" method="post" name="adminForm">
			<fieldset class="adminform">
				<legend><?php echo JText::_('Queue') ?></legend>

				<table class="adminlist">
					<tr>
						<th width = "10px" class="title">
							<input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo $countm; ?>);" />
						</th>
						<th class="title">
							<?php echo JText::_('Name') ?>
						</th>
						<th width = "15%" class="title" align="center">
							<?php echo JText::_('Last modified date of item') ?>
						</th>
						<th width = "15%" class="title" align="center">
							<?php echo JText::_('Added to queue') ?>
						</th>
					</tr>
					<?php
					if ($pingback->stack) {
						$k=0;
						$i =0;
						foreach ($pingback->stack as $stack) {
							$stack->id = $stack->url_md5;
							$stack->checked_out='0';
							$checked = JHTML::_('grid.checkedout',  $stack, $i);

							?>
							<tr class="<?php echo "row$k"; ?>">
								<td>
									<?php echo $checked; ?>
								</td>
								<td>
									<a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'.$stack->url ?>"><?php echo $stack->name; ?></a>
								</td>
								<td align="center">
									<?php echo $stack->modified; ?>
								</td>
								<td align="center">
									<?php echo $stack->added; ?>
								</td>
							</tr>
							<?php
							$i++;
							$k++; if ($k>1) $k=0;     
						}
					}
					?>
				</table>
			</fieldset>

			<fieldset class="adminform">
				<legend><?php echo JText::_('Log (last 30 pings)') ?></legend>
				<table class="adminlist">
					<tr>
						<th width = "10px" class="title">
						 #
						</th>
						<th class="title">
							<?php echo JText::_('Name') ?>
						</th>
						<th width = "15%" class="title" align="center">
							<?php echo JText::_('Status') ?>
						</th>
						<th class="title">
							<?php echo JText::_('Message') ?>
						</th>
						<th width = "10%" class="title" align="center">
							<?php echo JText::_('Session log') ?>
						</th>
						<th width = "15%" class="title" align="center">
							<?php echo JText::_('Ping date') ?>
						</th>
					</tr>
					<?php
					if ($pingback->log) {
						$k=0;
						$i=1;
						foreach ($pingback->log as $log) {
							?>
							<tr class="<?php echo "row$k"; ?>">
								<td>
									<?php echo $i ?>
								</td>
								<td>
									<a href="<?php echo $log->url ?>"><?php echo $log->name; ?></a>
								</td>
								<td align="center">
									<?php
									 if ($log->flerror==0) echo 'OK'; else echo 'ERROR'; 
									?>
								</td>
								<td align="center">
									<?php echo $log->message; ?>
								</td>
								<td align="center">
									<?php 
									echo '<a href="index3.php?option=com_sefservicemap&amp;task=show_log&amp;id='.$log->id.'&amp;pid='.$log->pingback_id.'" target="_blank"><img src ="components/com_sefservicemap/images/edit.png"></a>';
									?>
								</td>
								<td align="center">
									<?php echo $log->ping_date; ?>
								</td>
							</tr>
							<?php
							$i++;
							$k++; if ($k>1) $k=0;     
						}
					}
						?>
				</table>
			</fieldset>

			<table align="center">
				<tr><td valign="bottom" align="center"><div class="small"><a href="<?php echo _SEF_SM_HOME_PAGE ?>">SEF Service Map Component<?php echo $version?></a></div></td></tr><tr><td align="center"><div class="small">by Radoslaw Kubera</div></td></tr>
			</table>

			<input type="hidden" name="option" value="com_sefservicemap" />
			<input type="hidden" name="id" value="<?php echo JRequest::getVar('id'); ?>">
			<input type="hidden" name="task" value="" />
			<input type="hidden" name="boxchecked" value="0" />
			<?php echo JHTML::_( 'form.token' ); ?>
		</form>
		<?php
	}

	function configPignback ($pingbacks,$state) {
		JHTML::_('behavior.tooltip');
		$version = ' v.'._SEF_SM_VERSION;

		?>

		<script language="javascript" type="text/javascript">
		function submitbutton(pressbutton) {
			if (pressbutton == 'add_new_host') {
				if (document.adminForm.host.value == '') {
					alert("<?php echo JText::_( 'Enter host address', true ); ?>");
				} else {
					submitform(pressbutton);
				}
			}  else 
			{
				submitform(pressbutton);
			}
		}
		</script>

		<form action="index2.php" method="post" name="adminForm">
			<?php
			if ($state=='0') {
				JError::raiseWarning(1, 'Config: '.JText::_('Ping must be enabled in configuration'));
			}
			else {
				$countm = count($pingbacks);
				?>
				<fieldset class="adminform">
				<legend><?php echo JText::_('Add new host') ?></legend>
					<?php echo JText::_('Enter host address') ?>: &nbsp;
					<input type = "text" size="60" class="textarea" name="host">
				</fieldset>
				<fieldset class="adminform">
				<legend><?php echo JText::_('Defined Hosts List') ?></legend>
					<table class="adminlist">
						<tr>
							<th width = "10px" class="title">
								<?php echo JText::_('ID') ?>
							</th>
							<th width="5" class="title">
								<input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo $countm; ?>);" />
							</th>		
							<th width = "30%" class="title">
								<?php echo JText::_('Host') ?>
							</th>
							<th width = "30%" class="title">
								<?php echo JText::_('Queue length') ?>
							</th>
							<th class="title">
								<?php echo JText::_('Last ping date') ?>
							</th>
						</tr>
						<?php
						if ($pingbacks) {
							$k=0;
							$i =0;
							foreach ($pingbacks as $pingback) {
								$link = "index2.php?option=com_sefservicemap&amp;task=edit_pingback&amp;id=".$pingback->id;
								$pingback->checked_out=0;
								$checked = JHTML::_('grid.checkedout',  $pingback, $i);
								?>
								<tr class="<?php echo "row$k"; ?>">
									<td>
										<?php echo $pingback->id; ?>
									</td>
									<td>
										<?php echo $checked; ?>
									</td>
									<td>
										<a href="<?php echo $link ?>"><?php echo $pingback->ping_host;?></a>
									</td>
									<td>
										<?php echo $pingback->stack;?>
									</td>
									<td>
										<?php echo $pingback->last_ping_date;?>
									</td>
								</tr>
								<?php
								$k++; if ($k>1) $k=0;     
								$i++;
							}
						}
						?>
					</table>
					<table align="center">
						<tr><td valign="bottom" align="center"><div class="small"><a href="<?php echo _SEF_SM_HOME_PAGE ?>">SEF Service Map Component<?php echo $version?></a></div></td></tr><tr><td align="center"><div class="small">by Radoslaw Kubera</div></td></tr>

					</table>
				</fieldset>
				<?php
			}
			?>
			<input type="hidden" name="option" value="com_sefservicemap" />
			<input type="hidden" name="task" value="" />
			<input type="hidden" name="boxchecked" value="0" />
			<?php echo JHTML::_( 'form.token' ); ?>
		</form>

		<?php
	}

	function show_structure ($struct,$cache) {
		$user =& JFactory::getUser();
		JHTML::_('behavior.tooltip');

		$countm = 0;
		if ($struct)
		foreach ($struct as $menu) {
			if (is_array($menu->submenus)) {
				$countm = $countm + count ($menu->submenus);
			}
		}
		$pingback = $this->component_params->get('ping_enabled',1);
		$k = 0;
		?>
		<form action="index2.php" method="post" name="adminForm">
			<table class="adminlist">
				<tr>
					<th width = "10px" class="title">
						<?php echo JText::_('ID') ?>
					</th>
					<th width="10" class="title">
						<input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo $countm; ?>);" />
					</th>		
					<th width = "30%" class="title">
						<?php echo JText::_('Menu name') ?>
					</th>
					<th colspan="2" width="1%" class="title" nowrap="true">
						<?php echo JText::_('Reorder') ?>
					</th>
					<th width="5%" class="title" nowrap="true">
						<?php echo JText::_('Published') ?>
					</th>
					<th width="20%" class="title" nowrap="true" align="center">
						<?php echo JText::_('Integrator') ?>
					</th>
					<th width="2%" colspan="2" class="title" nowrap="true">
						<?php echo JText::_('Integrator Settings') ?>
					</th>
					<th width = "30%" class="title" nowrap="true">
						<?php echo JText::_('Link') ?>
					</th>
					<th nowrap="true" align="center">
						<?php echo JText::_('Ping enabled') ?>
					</th>
					<th nowrap="true" align="center">
						<?php echo JText::_('Cache') ?>
					</th>
				</tr>
				<?php
				$numberm=1;
				$i = 0;

				if ($struct)
				foreach ($struct as $menu) {
					$pars = new JParameter($menu->params->params);
					$title = $menu->params->title;
               
					if ($menu->params->published==1) {
						$img='tick.png';
						$task='unpublishmenu';
						$alt = "Published";
						$proc =1;
					}
					else {
						$img = 'publish_x.png';
						$task = "publishmenu";
						$alt = "Unpublished";
						$proc=1;
					}  
					?>
					<tr bgcolor="#E4E4E4">
						<th bgcolor="#E4E4E4">
							<?php echo $menu->params->id ?>
						</th>
						<th bgcolor="#E4E4E4">
						</th>		
						<th bgcolor="#E4E4E4">
							<?php if ($proc==1) echo "<a href='index2.php?option=com_menus&amp;task=view&amp;menutype=".$menu->params->title."'>".$menu->params->title."</a>"; else echo "<a href='index2.php?option=com_menus&menutype=".$menu->title."'><i>".$menu->title."</i></a>";?>
						</th>
						<th bgcolor="#E4E4E4">
							<?php $this->up_down ($menu->params->id, count($struct),$numberm,1);$numberm++;?> 
						</th>
						<th bgcolor="#E4E4E4" align="center">
							<?php if ($proc==1) {?><a href='index2.php?option=com_sefservicemap&amp;task=<?php echo $task?>&id=<?php echo $menu->params->id?>'><img src='images/<?php echo $img ?>' hspace='3' border='0' alt="<?php echo $alt ?>"></a><?php }?>
							<?php if ($proc==0) {echo '<img src="images/'.$img.'" />';}?>
						</td>
						<th bgcolor="#E4E4E4">
						</th>
						<th colspan="2" bgcolor="#E4E4E4">
						</th>
						<th bgcolor="#E4E4E4">
						</th>
						<th bgcolor="#E4E4E4">
						</th>
						<th bgcolor="#E4E4E4">
						</th>
					</tr>
					<?php
					$k = 0;
//					$i = 0;
					if ($menu->submenus)
					foreach ($menu->submenus as $submenu) {
						$submenu->submenu->checked_out = 0;
						if ($submenu->submenu->menu_published==1) $pub = '<a href="index.php?option=com_sefservicemap&amp;task=unpublish&id='.$submenu->submenu->id.'"><img hspace="3" border="0" src="images/tick.png"></a>';
						else $pub = '<a href="index.php?option=com_sefservicemap&amp;task=publish&id='.$submenu->submenu->id.'"><img hspace="3" border="0" src="images/publish_x.png"></a>';
				
						$ping_pub = '<a href="index.php?option=com_sefservicemap&amp;task=enable_ping&cid[]='.$submenu->submenu->id.'"><img hspace="3" border="0" src="images/publish_x.png"></a>';
						global $mainframe;
						if ($mainframe->ParamsArray->get ('ping_enabled',1)==1) {
							if ($submenu->submenu->ping_enabled==1) $ping_pub = '<a href="index.php?option=com_sefservicemap&amp;task=disable_ping&cid[]='.$submenu->submenu->id.'"><img hspace="3" border="0" src="images/tick.png"></a>';
						}
						

						if (!isset($submenu->integrator->name)) $int_name=''; else $int_name=$submenu->integrator->name;
						if ($submenu->path) $submenu->path .= '<sup>|_</sup>';
						if ($submenu->submenu->published==1) $menu_name = $submenu->path.$submenu->submenu->name; else  $menu_name='<i>'.$submenu->path.$submenu->submenu->name.'</i>';
						if ($submenu->integrators_count>1) $integrator_name = '<a href="index.php?option=com_sefservicemap&amp;task=nextprocessor&id='.$submenu->submenu->id.'">'.$int_name.'</a>';
						else $integrator_name = $int_name;
						$edit_link='';
						switch ($submenu->settings) {
							case 'disabled': $palt = JText::_('Disabled');
								break;
							case 'global': $palt = JText::_('Global');
								break;
							case 'local': $palt = JText::_('Custom');
								$edit_link='<a href="index2.php?option=com_sefservicemap&amp;task=integrator&settings=local&amp;id='.$submenu->integrator->id.'&amp;menu_id='.$submenu->submenu->id.'&title='.$submenu->submenu->name.'"><img src ="components/com_sefservicemap/images/edit.png"></a>';
								break;
						}
						if (isset($palt)) $settings_link = '<a href="index2.php?option=com_sefservicemap&task=changeprocessorstate&id='.$submenu->submenu->id.'">'.$palt.'</a>';
						else $settings_link='';

						if (isset ($submeny->submenu->menu_publishes) && $submeny->submenu->menu_publishes==1) $pub= '';
						$checked = JHTML::_('grid.checkedout',  $submenu->submenu, $i);
						$i++;	
						?>
						<tr class="<?php echo "row$k"; ?>">
							<td>
								<?php echo $submenu->submenu->id; ?>
							</td>
							<td>
								<?php echo $checked; ?>
							</td>
							<td>
								<?php echo $menu_name;?>
							</td>
							<td colspan="2">
							</td>
							<td align="center">
								<?php echo $pub ?>
							</td>                       
							<td align="center">
								<?php 
								echo $integrator_name; 
								?>
							</td>
							<td align="center">
								<?php if (isset($submenu->integrator->settings)) echo $settings_link; ?>
							</td>
							<td align="center" width="20">
								<?php echo $edit_link ?>
							</td>
							<td>
								<?php echo $submenu->submenu->link; ?>
							</td>
							<td align="center">
								<?php echo $ping_pub ?>
							</td>                       
							<td align="center">
								<?php 
								@$enabled = $this->component_params->get($submenu->integrator->id.'-'.$submenu->integrator->comp.'_cache',1); 
								if ($enabled) 
								{ 
									$cache_type = $this->component_params->get( 'cache_type','0');
									switch ($cache_type) {
										case _SEF_SM_FILESYSTEM:
											$cache_dir = JPATH_SITE.DS.'components'.DS.'com_sefservicemap'.DS.'cache'.DS.$submenu->submenu->id.DS;
											if (is_dir($cache_dir)) {
												echo '<input type="submit" '.$enabled.' class="button" name="clear_'.$submenu->submenu->id.'" value ="'.JText::_('Clear').'">';
											}
											break;

										case _SEF_SM_DATABASE:
											$db = JFactory::getDBO();
											$cache_id = $submenu->submenu->id; 
											$query = "select sum(items) from #__sef_sm_cache where items>='0' and menu_id='$cache_id'";
											$db->setQuery($query);
											$items = 0;
											$items = $db->loadResult();
											if ($items>0) { 
												$cache = 1;
												echo '<input type="submit" '.$enabled.' class="button" name="clear_'.$submenu->submenu->id.'" value ="'.JText::_('Clear').'">';
											}
											break;

									}

								} 
								?>
							</td>
						</tr>
						<?php
						$k = 1 - $k;
					}
				}
				$version = ' v.'._SEF_SM_VERSION; 
				?>
			</table>
			<table align="center" width="100%">
				<tr>
					<th colspan="8"></th>
					<th align="right">
            					<?php if ($cache) { 
							?>
							<input type="submit" class="button" name="remove_all" value ="<?php echo JText::_('Global Cache Clear') ?>">
							<?php 
						} 
						?>
					</th>
				</tr>
				<tr>
					<td colspan="9" valign="bottom" align="center"><div class="small"><a href="<?php echo _SEF_SM_HOME_PAGE ?>">SEF Service Map Component<?php echo $version ?></a></div></td>
					</tr><tr><td colspan="9" align="center"><div class="small">by Radoslaw Kubera</div></td></tr>
			</table>
			<input type="hidden" name="option" value="com_sefservicemap" />
			<input type="hidden" name="task" value="" />
			<input type="hidden" name="boxchecked" value="0" />
			<input type="hidden" name="filter_order" value="<?php echo $lists['order']; ?>" />
			<input type="hidden" name="filter_order_Dir" value="<?php echo $lists['order_Dir']; ?>" />
			<?php echo JHTML::_( 'form.token' ); ?>
		</form>
		<?php
	}

}

?>