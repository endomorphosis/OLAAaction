<?php

//*******************************************************
//* SEF Service Map Component
//* http://www.sefservicemap.com
//* (C) Radoslaw Kubera
//* license http://www.gnu.org/copyleft/gpl.html GNU/GPL
//*******************************************************

/** ensure this file is being included by a parent file */
defined( '_JEXEC' ) or die( 'Restricted access' );

if (!function_exists("stripos")) {
  function stripos($str,$needle,$offset=0)
  {
      return strpos(strtolower($str),strtolower($needle),$offset);
  }
}

jimport('joomla.application.component.controller');

$user = & JFactory::getUser();
if (!$user->authorize( 'com_contact', 'manage' )) {
	$mainframe->redirect( 'index.php', JText::_('ALERTNOTAUTH') );
}

include_once (JPATH_COMPONENT.DS."include".DS."sefservicemap.util.php");
include_once ( JPATH_LIBRARIES.DS.'domit'.DS.'xml_domit_lite_include.php' );

include_once (JPATH_COMPONENT.DS."admin.sefservicemap.html.php");
require_once (JPATH_SITE.DS.'components'.DS.'com_sefservicemap'.DS.'version.php');

$SEF_SM_Admin = new SEF_SM_Admin_class();

class SEF_SM_Admin_class {
	var $utils;
	var $HTML;
	function SEF_SM_Admin_class()
	{
		$this->utils = new SEF_SM_Utils_class;
		$this->utils->smGetComponentParams();

		$this->utils->smSynchronize();
		$this->utils->cache_clear_Check();

		$this->HTML = new HTML_sefservicemap_admin;
		$this->HTML->utils = $this->utils;

		$task	= JRequest::getVar('task'); 

		$id = JRequest::getVar('id', 0, 'request', 'int');
		$menu_id = JRequest::getVar('menu_id', 0, 'request', 'int');
		$title = JRequest::getVar('title', 0, 'request', 'int');

		$settings = JRequest::getVar('settings', 0, 'request', '');

		$request = JRequest::get( 'request' );

		if ($request)
		foreach ($request as $key=>$value) {
			if (eregi('clear_',$key)) {
				$task = 'clearcache';
				$id = str_replace('clear_','',$key);
			}

			if (eregi('ping_',$key)) {
				$task = 'manual_ping';
				$id = str_replace('ping_','',$key);
			}


		}

		if (isset($request['remove_all'])) {
			$param = 'menu'; 
			$task = 'removecache';
		}
		switch ($task) {

			case "show_log" : $this->show_log();
				break;
			case "delete_from_queue" : $this->deleteFromQueue();
				break;
			case "edit_pingback" : $this->editPingBack ();
				break;

			case "delete_queue" : $this->delete_queue();
				break;

			case "delete_hosts" : $this->delete_hosts();
				break;

			case "add_new_host" : $this->add_new_host();
				break;

			case "config_pingback" : $this->configPignback();
				break;

			case "list_send_ping_all" : $this->listSendPing(1);
				break;

			case "list_send_ping_new" : $this->listSendPing(0);
				break;

			case "list_cache_clear": $this->listCacheClear();
				break;

			case "sendping" : $this->sentping ($id);
				break;

			case "uploadintegrator" : $this->upload_integrator();
				$this->edit_config_integrators();
				break;

			case "remove": $this->uninstall_integrators();
				$this->edit_config_integrators();
				break;				

			case "enable_ping" : $this->enable_ping(1);
				break;

			case "disable_ping" : $this->enable_ping(0);
				break;

			case "publishitems" : $this->publishitems(1);
				break;

			case "unpublishitems" : $this->publishitems(0);
				break;

			case "chintpub" : $this->change_integrator_publish();
				break;

			case "publishintegrators"  : $this->publish_integrators(1);
				break;

			case "unpublishintegrators"  : $this->publish_integrators(0);
				break;

			case "chintaccess" : $this->change_integrator_access();
				break;

			case "removecache" : $this->remove_cache($param);
				break;

			case "clearcache" : $this->clear_cache($id);
				break;

			case "integrator" : $this->edit_integrator($id,$menu_id,$title,$settings);
				break;

			case "cancelint" : $this->cancelint($menu_id,$id,$settings);
				break;

			case "saveint" : $this->saveint($menu_id,$id,$settings);
				break;

			case "cancelcfg" : 
			case "config_all": $this->edit_config();
				break;

			case "config_integrators": $this->edit_config_integrators();
				break;

			case 'cancelcache' :
			case "config_cache": $this->edit_config_cache();
				break;

			case "cancelcss" :
			case "config_css": $this->edit_config_css();
				break;
 
			case "save" : $this->save_config('config_all',$task);
				break;

			case "savecache" : $this->save_config('config_cache');
				break;

			case "savecss" : $this->save_css();
				break;
  
			case "menu" : $this->menu();
				break;

			case 'publish': $this->changePubl( $id, 1);
				break;

			case 'unpublish': $this->changePubl( $id, 0);
				break;

			case 'publishmenu': $this->changeMenuPubl( $id, 1);
				break;

			case 'unpublishmenu': $this->changeMenuPubl( $id, 0);
				break;

			case 'nextprocessor': $this->NextProcessor($id);
				break;

			case 'changeprocessorstate': $this->ChangeProcessorState($id);
				break;

			case 'downmenu': $this->down($id);
				break;

			case 'upmenu': $this->up ($id);
				break;

			case 'about': $this->about ();
				break;

			default: $this->menu();
		}
	}

	function show_log() {
		$db = JFactory::getDBO();
		$id = JREQUEST::getVar('id',0);
		$pid = JREQUEST::getVar('pid',0);
		$query = "select session_log from #__sef_sm_pingback_log where id='$id' and pingback_id='$pid'";
		$db->setQuery($query);
		$log = $db->loadResult();
		$log = htmlentities($log);
		$log = explode ("\n",$log);
		echo implode ('<br/>',$log);


	}
	function editPingBack() {
		$db = JFactory::getDBO();
		$id = intval(JRequest::getVar('id'));
		$query= "select * from #__sef_sm_pingback where id='".$id."'";
		$db->setQuery($query);
		$pingback = $db->loadObject();
		
		JToolBarHelper::title( 'SEF Service Map - '.$pingback->ping_host, 'sections.png' );

		$query = "select * from #__sef_sm_pingback_stack where id='".$id."' order by modified desc";
		$db->setQuery($query);
		$pingback->stack = $db->loadObjectList();

		$query = "select * from #__sef_sm_pingback_log where pingback_id='".$id."' order by ping_date desc";
		$db->setQuery($query);
		$pingback->log = $db->loadObjectList();
		$this->HTML->editPingBack($pingback);
		
	}
	function deleteFromQueue() {
		$cids = JREQUEST::GetVar('cid',array());
		$id = JREQUEST::getVar('id',0);
		global $mainframe;
		$db = JFactory::getDBO();
		if ($cids) {
			$query = "DELETE from #__sef_sm_pingback_stack where id='".$id."' and ( ";
			
			$i=0;
			if ($cids)
			foreach ($cids as $cid) {
				if ($i==0) $i++; 
                           else {
					$query.=" OR ";
				}
				$query .=" url_md5='".$cid."'";
			}
			$query .= ') ';
			$db->setQuery($query);
			$db->query();
			$mainframe->redirect( "index2.php?option=com_sefservicemap&task=edit_pingback&id=".$id, JText::_('Deleted from queue successfully'),'message' );
		} else  $mainframe->redirect( "index2.php?option=com_sefservicemap&task=edit_pingback&id=".$id);

	}
	function delete_hosts () {
		$cids = JREQUEST::GetVar('cid',array());
		global $mainframe;
		$db = JFactory::getDBO();
		if ($cids) {
			$query = "DELETE from #__sef_sm_pingback_stack where ";
			$hosts_query = "DELETE from #__sef_sm_pingback where ";
			$log_query = "DELETE from #__sef_sm_pingback_log where ";
			
			$i=0;
			if ($cids)
			foreach ($cids as $cid) {
				if ($i==0) $i++; 
                           else {
					$query.=" OR ";
					$hosts_query.=" OR ";
					$log_query.=" OR ";
				}
				$query .=" id='".$cid."'";
				$hosts_query .=" id='".$cid."'";
				$log_query .=" pingback_id='".$cid."'";
			}
			$db->setQuery($query);
			$db->query();

			$db->setQuery($hosts_query);
			$db->query();

			$db->setQuery($log_query);
			$db->query();


			$mainframe->redirect( "index2.php?option=com_sefservicemap&task=config_pingback", JText::_('Hosts deleted successfully'),'message' );
		} else  $mainframe->redirect( "index2.php?option=com_sefservicemap&task=config_pingback");
	}

	function delete_queue () {
		$cids = JREQUEST::GetVar('cid',array());
		global $mainframe;
		$db = JFactory::getDBO();
		if ($cids) {
			$query = "DELETE from #__sef_sm_pingback_stack where ";
			$i=0;
			if ($cids)
			foreach ($cids as $cid) {
				if ($i==0) $i++; else $query.=" OR ";
				$query .=" id='".$cid."'";
			}
			$db->setQuery($query);
			$db->query();

			$mainframe->redirect( "index2.php?option=com_sefservicemap&task=config_pingback", JText::_('Queues deleted successfully'),'message' );
		} else  $mainframe->redirect( "index2.php?option=com_sefservicemap&task=config_pingback");
	}

	function add_new_host() {
		global $mainframe;
		$db = JFactory::getDBO();
		$host = addslashes(trim(JRequest::getVar('host','')));
		if (!eregi ('http://',strtolower($host))) $host = 'http://'.$host;

		$datenow =& JFactory::getDate();
		$now = $datenow->toMysql();

		$query = "select count(id) from #__sef_sm_pingback where ping_host like '%$host%'";
		$db->setQuery($query);
		$result = $db->loadResult();
		if ($result==0) {
			$pingback = new PingBackClass;
			$result = $pingback->checkHost($host);
			if (!$result || $result->flerror==-1) {
				JError::raiseWarning(-1, JText::_("Bad host address or host don't support XML-RPC pingback protocol"));
				$this->configPignback ();
			}
			else {
				$message = $result->message;
				$query = "insert into #__sef_sm_pingback set ping_host='$host', last_ping_date = '$now'";
				$db->setQuery($query);
				$db->query();
				$id =$db->Insertid();
				$pingback->logSession($id);
				$mainframe->redirect( "index2.php?option=com_sefservicemap&task=config_pingback", JText::_('Host added successfully. Ping message').': '.$message,'message' );
			}
		}
		else {
			JError::raiseWarning(-1, JText::_("Host Exists"));
			$this->configPignback ();
		}
	}
	function configPignback () {
		global $mainframe;
		$params=$mainframe->ParamsArray;
		if ($params->get('ping_enabled',1)==0) {
			$this->HTML->configPignback ($pingbacks,0);
		}
		else {
			$db = JFactory::getDBO();
			$query = "select * from #__sef_sm_pingback order by last_ping_date DESC";
			$db->setQuery($query);
			$pingbacks = $db->loadObjectList();
			for ($i=0; $i<count($pingbacks); $i++) {
				$id = $pingbacks[$i]->id;
				$query="select count(id) from #__sef_sm_pingback_stack where id='$id'";
				$db->setQuery($query);
				$pingbacks[$i]->stack = $db->loadResult();
			}
			$this->HTML->configPignback ($pingbacks,1);
		}
	}

	function publishitems($state) {
		global $mainframe;

		$db = JFactory::getDBO();
		$cids = JREQUEST::GetVar('cid',array(0));
		if ($cids)
		foreach ($cids as $cid) {

			$query = "UPDATE #__sef_sm_menu"
			. "\n SET published = '". intval ($state) ."'"
			. "\n WHERE menu_id='$cid'";
	
			$db->setQuery( $query );
			$db->query();
		}
		$mainframe->redirect( "index2.php?option=com_sefservicemap&task=menu" );
	}

	function enable_ping($state) {
		global $mainframe;
		$params=$mainframe->ParamsArray;

		$db = JFactory::getDBO();
		$cids = JREQUEST::GetVar('cid',array(0));
		if ($params->get('ping_enabled',1)==0) {
			JError::raiseWarning(1, 'Config: '.JText::_('Ping must be enabled in configuration'));
			$this->menu();
		}
		else {
			if ($cids)
			foreach ($cids as $cid) {

				$query = "UPDATE #__sef_sm_menu"
				. "\n SET ping_enabled = '". intval ($state) ."'"
				. "\n WHERE menu_id='$cid'";
	
				$db->setQuery( $query );
				$db->query();

				$db->setQuery("delete from #__sef_sm_pingback_stack where menu_id='$cid'");
				$db->query();
			}
			$mainframe->redirect( "index2.php?option=com_sefservicemap&task=menu" );
		}
	}

	function listSendPing($all) {

		global $mainframe;
		$mainframe->queue_elements = 0;
		$mainframe->elements_count = 0;
		$db = JFactory::getDBO();
		$db->setQuery("select count(id) from #__sef_sm_pingback");
		$count = $db->loadResult();

		$mainframe->maptype = 'xml';
		$params=$mainframe->ParamsArray;
		if ($params->get('ping_enabled',1)==0) {
			JError::raiseWarning(1, 'Config: '.JText::_('Ping must be enabled in configuration'));
			$this->menu();
		}
		else {
			$cids = JREQUEST::GetVar('cid',array());
			if ($cids) {
				foreach ($cids as $cid) {
					$ret = $this->sentping ($cid,$all);
				}
				if ($count) {
					$qelements = intval($mainframe->queue_elements/$count);
				} else $qelements = $mainframe->queue_elements;
				$mainframe->redirect( "index2.php?option=com_sefservicemap&task=menu", JText::_('Added to queue').': '.$qelements.' '.JText::_('items') ,'message' );
			}
			$mainframe->redirect( "index2.php?option=com_sefservicemap&task=menu");
		}
	}

	function sentping ($id,$all=0) {
		$db = JFactory::getDBO();
		$query = "SELECT a.*,b.ping_enabled, b.published AS menu_published,b.integrator AS menu_integrator, b.integrator_id as integrator_id, b.ordering AS menu_ordering, b.params AS menu_params"
		."\n FROM #__menu AS a LEFT JOIN #__sef_sm_menu as b ON b.menu_id = a.id "
		."\n WHERE a.id = '$id'";
		$db->setQuery($query);
		$menuitem = $db->loadObject();
		$integrator = $this->utils->get_menuitem_integtator($menuitem);
		if ($integrator) {
			global $mainframe;
			if ($all==1) $mainframe->pingall='1';
			$this->utils->get_pingback();
			$this->utils->integrator_call($integrator,$menuitem,0,0);
			$mainframe->pingall='';
			return 1;
		} 
		else {
			return 0;
		}

	}

	function remove_cache($param='') {
		global $mainframe;

		$db = JFactory::getDBO();
		$query = "delete from #__sef_sm_cache_items";
		$db->setQuery($query);
		$db->query();

		$query = "update #__sef_sm_cache set items='-1'";
		$db->setQuery($query);
		$db->query();
		
		$cache_dir = JPATH_SITE.DS.'components'.DS.'com_sefservicemap'.DS.'cache'.DS;
		$this->utils->DeleteDirectory ($cache_dir);
		@mkdir ($cache_dir);

		if ($param) $mainframe->redirect("index2.php?option=com_sefservicemap&task=".$param, JText::_('Cache is clear now'),'message');
	}

	function listCacheClear () {
		global $mainframe;
		$cids = JREQUEST::GetVar('cid',array());
		if ($cids) {
			foreach ($cids as $cid) {
				$this->clear_cache($cid,0);
			}
			$mainframe->redirect("index2.php?option=com_sefservicemap&task=menu", JText::_('Cache is clear now'),'message');
		}
		$mainframe->redirect("index2.php?option=com_sefservicemap&task=menu");
	}

	function clear_cache($id,$redirect = 1) {
		global $mainframe;
		$db = JFactory::getDBO();
		if ($id) {
			$cache_dir = JPATH_SITE.DS.'components'.DS.'com_sefservicemap'.DS.'cache'.DS.$id;
			$this->utils->DeleteDirectory ($cache_dir);

			$db->setQuery("select * from #__sef_sm_cache where menu_id='$id'");
			$caches = $db->loadObjectList();

			$query = "delete from #__sef_sm_cache_items where ";
			$updatequery = "update #__sef_sm_cache set items='-1' where ";
			$i = 0;
			if ($caches) {
				foreach ($caches as $cache) {
					if ($i!=0) {
						$query .=' OR '; 
						$updatequery .=' OR '; 
					} else $i++;
					$query .=" id='$cache->id' ";
					$updatequery .=" id='$cache->id' ";
				}

				$db->setQuery($query);
				$db->query();

				$db->setQuery($updatequery);
				$db->query();
			}
		}
		if ($redirect ==1) $mainframe->redirect("index2.php?option=com_sefservicemap&task=menu", JText::_('Cache is clear now'),'message');

	}
	
	function about() {
		$this->HTML->about();
	}
	
	function check_robots_txt ($xmlmap) {
		$robots = array();
		$robots = file (JPATH_SITE.DS.'robots.txt');

		$to_delete = array();
		$to_add = array();
		$is_ok = array();

		$root = $_SERVER['DOCUMENT_ROOT'];
		$rootarr = explode ('/',$root);
		$rootarray = array();
		if ($rootarr) {
			foreach ($rootarr as $r) {
				if ($r!='') $rootarray[]=$r;
			}
		}

		$path = JPATH_SITE;
		$patharr = explode ('/',$path);
		$patharray = array();
		if ($patharr) {
			foreach ($patharr as $r) {
				if ($r!='') $patharray[]=$r;
			}
		}

		$segments = count($patharray) - count($rootarray);
		if ($segments !=0) {
			$folder = '/';
			for ($i = count($rootarray); $i<count($patharray); $i++) {
				$folder .= $patharray[$i].'/';
			}
		} else $folder = '/';

		$link_xml = SEFSMRoute::_('index.php?option=com_sefservicemap&amp;task=xmlmapindex&amp;no_html=1');
		$unsets = array();
		if ($robots) {
			$is_ok = 0;
			$i = 0;
			$found =0;
			foreach ($robots as $line) {
				if (eregi('sitemap:',$line) && stripos($line,$xmlmap)) {
					$line = trim(substr($line,8,strlen($line)-8));
					if ($is_ok==0) $is_ok = 1;
					$found = 1;
				}
				else {
					if (eregi('sitemap:',$line)) {
						$to_delete[] = $i;
						$is_ok=-1;
					}
				}
				$i++;
			}

		}

		if ($is_ok==-1) $is_ok=0;

		if ($found==0) {
			$robots[] = 'Sitemap: '.$xmlmap;
		}


		$new_robots = $robots;
		$robots = '';
		$robots->new_robots = $new_robots;
		$robots->to_add[] = $xmlmap;
		$robots->to_delete = $to_delete;
		$robots->is_ok = $is_ok;
		return $robots;
	}

	function update_robots ($robots) {
		$new_robots = array();
		$i = 0;
		foreach ($robots->new_robots as $line) {
			$new_robots[$i] = $line;
			$i++;
		}

		if ($robots->to_delete) {
			$arr = array();
			$arr = $robots->to_delete;
			sort($arr);
			$count = count($arr);
			for ($i=0; $i<$count;$i++) {
				$index = $arr[$count-$i-1].' ';
				unset ($new_robots[intval($index)]);
			}
		}
		$file = JPATH_SITE.DS.'robots.txt';
		if (@$rob=fopen($file,'w+')) {
			foreach ($new_robots as $line) {
				fwrite($rob, trim($line)."\r\n");
			}
			fclose ($rob);
			unset($robots->to_add);
			unset($robots->to_delete);

		} else
              {
			JError::raiseWarning(-1, JText::_("Cannot change robots.txt. Please change robots.txt manualy. See description below."));
		}
		return $robots;
	}
	function edit_config() {
		global $mainframe;
		$params=$mainframe->ParamsArray;

		$root = $_SERVER['DOCUMENT_ROOT'];
		$rootarr = explode ('/',$root);
		$rootarray = array();
		if ($rootarr) {
			foreach ($rootarr as $r) {
				if ($r!='') $rootarray[]=$r;
			}
		}

		$path = JPATH_SITE;
		$patharr = explode ('/',$path);
		$patharray = array();
		if ($patharr) {
			foreach ($patharr as $r) {
				if ($r!='') $patharray[]=$r;
			}
		}

		$segments = count($patharray) - count($rootarray);
		if ($segments !=0) {
			$folder = '/';
			for ($i = count($rootarray); $i<count($patharray); $i++) {
				$folder .= $patharray[$i].'/';
			}
		} else $folder = '/';

		$db = JFactory::getDBO();
		$query = "select * from #__languages where active='1'";
		$db->setQuery($query);
		$languages = $db->loadObjectList();
		$langs = array();
		$xmlmap = SEFSMRoute::_('index.php?option=com_sefservicemap&amp;task=xmlmapindex&amp;no_html=1');

		$lang = '';
		$lang->name_xml = JText::_('XML link').': ';
		$lang->link_xml = '<a href="'.SEFSMRoute::_('index.php?option=com_sefservicemap&amp;task=xmlmapindex&amp;no_html=1').'">'.SEFSMRoute::_('index.php?option=com_sefservicemap&amp;task=xmlmapindex&amp;no_html=1').'</a>';
		$langs[] = $lang;

		if ($languages) {
			foreach ($languages as $language) {
				$lang = '';
				$lang->name_txt = JText::_('TXT link').' ('.$language->code.'): ';
				$lang->link_txt = '<a href="'.SEFSMRoute::_('index.php?option=com_sefservicemap&amp;task=txtmap&amp;no_html=1&amp;lang='.$language->shortcode).'">'.SEFSMRoute::_('index.php?option=com_sefservicemap&amp;task=txtmap&amp;no_html=1&amp;lang='.$language->shortcode).'</a>';

				$langs[] = $lang;
			}
		}
		else {
			$lang =& JFactory::getLanguage();
			$mylang = $lang->_lang;
			$lang = '';
			$lang->name_txt = JText::_('TXT link').': ';
			$lang->link_txt = '<a href="'.SEFSMRoute::_('index.php?option=com_sefservicemap&amp;task=txtmap&amp;no_html=1&amp;lang='.$mylang).'">'.SEFSMRoute::_('index.php?option=com_sefservicemap&amp;task=xmlmap&amp;no_html=1&amp;lang='.$mylang).'</a>';
			$langs[] = $lang;
		}
		
		$robots = $this->check_robots_txt($xmlmap);
		if ($robots->is_ok==0) {
			$robots = $this->update_robots($robots);
//			$robots = $this->check_robots_txt($xmlmap);
		} else unset ($robots->to_add);
		$this->HTML->edit_config($params,$langs,$robots);
	}

	function edit_config_css() {
		global $mainframe;
		$params=$mainframe->ParamsArray;
		$this->HTML->edit_config_css($params);
	}

	function edit_config_cache() {
		global $mainframe;

		$config = new JConfig();

		$params=$mainframe->ParamsArray;

		$rows = $this->utils->get_integrators_list(-1,'',1);
		$this->HTML->edit_config_cache($rows,$params,$config->cachetime);
	}

	function edit_config_integrators() {
		global $mainframe;
		$params=$mainframe->ParamsArray;

		$rows = $this->utils->get_integrators_list(0,'',1);
		$comp_rows = $this->utils->get_integrators_list(1,'',1);

		$this->HTML->edit_config_integrators($rows,$comp_rows,$params);
	}

	function menu() {

		$db =& JFactory::getDBO();
		$db->setQuery("select *,name as title from #__sef_sm_menus order by ordering");
		$menus = $db->loadObjectList();

		$struct = array();
		if ($menus)
		foreach ($menus as $menu) {
			$actmenu = '';
			$actmenu->params = $menu;
			$actmenu->submenus = $this->find_menu (0,$menu->title,'',$menu->title);
			$struct[] = $actmenu;
		}

		$cache=0;
		$cache_path = JPATH_SITE.DS.'components'.DS.'com_sefservicemap'.DS.'cache'.DS;
		if (is_dir($cache_path)) { 
			$dir=opendir($cache_path);
			while ($file=readdir($dir)) {
				if ($file!='.' && $file!='..') {
					$cache = 1;
				}
			}  
			closedir($dir);
		}
		$this->HTML->show_structure($struct,$cache);
	}
	
	function find_menu($parent,$menutype,$path,$title) {

		$db = JFactory::GetDBO();
		$query = "SELECT a.*,b.ping_enabled, b.published AS menu_published,b.integrator AS menu_integrator,b.integrator_id as integrator_id, b.ordering AS menu_ordering, b.params AS menu_params"
		."\n FROM #__menu AS a LEFT JOIN #__sef_sm_menu as b ON b.menu_id = a.id "
		."\n WHERE a.menutype='$menutype' and a.published >='0' and a.parent='$parent' ORDER BY `ordering` ASC";
		
		$db->setQuery($query);
		$submenus = $db->loadObjectList();
	
		$subret = array();
		if ($submenus)
		foreach ($submenus as $submenu) {
			if ($submenu->published == 1) {	                      	
				$integrator='';
				$integrator = $this->utils->get_menuitem_integtator($submenu);

				if ($integrator) {
					$integrators_count = count($this->utils->get_menuitem_integrators_list($submenu));

				}
				else {
						$integrators_count = 0;
				} 
	
				$sub = '';
				$sub->path = $path;
				$sub->submenu = $submenu;
				if (isset($integrator->settings)) $sub->settings = $integrator->settings; else $sub->settings='';
				$sub->integrator = $integrator;
//				$sub->settings = $integrator->settings;
				$sub->integrators_count = $integrators_count;
			
				$subret[] = $sub;
			
				$subs = $this->find_menu($sub->submenu->id,$menutype,$path.'.&nbsp;&nbsp;&nbsp;',$title);
				if ($subs) {
					foreach ($subs as $s)
						$subret[] = $s;
				}
			}
		}
		return $subret;

	}
	
	function edit_integrator ($id,$menu_id,$title,$settings) {
		global $mainframe;
		$db = JFactory::getDBO();
		$comp = intval(JRequest::getVar('comp',0));

		$integrators = $this->utils->get_integrators_list(-1,'',1);

		if ($integrators)
		foreach ($integrators as $integrator) {
			if ($comp == $integrator->comp && $id == $integrator->id) {
				$row = $integrator;
				break;
			}
		}

		if ($settings=='local') {
			$query = "SELECT params FROM #__sef_sm_menu where menu_id='$menu_id'";
			$db->setQuery($query);
			$pars = $db->loadResult();
			$query = "SELECT name FROM #__menu where id='$menu_id'";
			$db->setQuery($query);
			$name = $db->loadResult();
			$title= $name;
		}
		else {
			if ($comp==0) $query = "SELECT name,params from #__plugins where id='$id'";
			else $query = "SELECT name,params from #__sef_sm_plugins where id='$id'";
			$db->setQuery($query);
			$result = $db->loadObject();
			$title= $result->name;
			$pars = $result->params;
			$name= JText::_('Global Settings');
		}

		if ($comp==1) {
			$row->folder = $row->type;
			$xmlfile = JPATH_SITE.DS.'components'.DS.'com_sefservicemap'.DS.'plugins'.DS.$row->type.DS.$row->element.'.xml';	
			if (!file_exists($xmlfile)) $xmlfile = JPATH_SITE.DS.'components'.DS.'com_sefservicemap'.DS.'plugins'.DS.'generic_plugin.xml';
		}
		else {                          
			$xmlfile = JPATH_SITE.DS.'plugins'.DS.$row->folder . DS . $row->element .".xml";
		}

		if ($settings!='local') $title = $row->name;
		$params = new JParameter( $pars, $xmlfile, 'plugin');

		$db->setQuery("select name from #__groups where id='$row->access'");
		$row->group = $db->loadResult();

		JToolBarHelper::title( 'SEF Service Map - '.$title, 'sections.png' );

		$this->HTML->edit_integrator ($id,$menu_id,$title,$name,$row,$params,$settings,$comp);
	
	}

	function down($id) {
		global $mainframe;
		$db = JFactory::GetDBO();

		$db->setQuery("SELECT id,ordering FROM #__sef_sm_menus WHERE id='$id'");
		$act = $db->loadObject();
	
		$db->setQuery("SELECT id,ordering FROM #__sef_sm_menus WHERE ordering>$act->ordering order by ordering");
		$next = $db->loadObject();
	  
		$db->setQuery("UPDATE #__sef_sm_menus set ordering='$next->ordering' where id='$act->id'");
		$db->query();
	
		$db->setQuery("UPDATE #__sef_sm_menus set ordering='$act->ordering' where id='$next->id'");
		$db->query();
	
		$mainframe->redirect( "index2.php?option=com_sefservicemap&task=menu" );
	}
	
	function up($id) {
		global $mainframe;
		$db = JFactory::GetDBO();
	
		$db->setQuery("SELECT id,ordering FROM #__sef_sm_menus WHERE id='$id'");
		$act = $db->loadObject();
	
		$db->setQuery("SELECT id,ordering FROM #__sef_sm_menus WHERE ordering<$act->ordering order by ordering desc");
		$next = $db->loadObject();
	  
		$db->setQuery("UPDATE #__sef_sm_menus set ordering='$next->ordering' where id='$act->id'");
		$db->query();
	
		$db->setQuery("UPDATE #__sef_sm_menus set ordering='$act->ordering' where id='$next->id'");
		$db->query();
	
		$mainframe->redirect( "index2.php?option=com_sefservicemap&task=menu" );
	}
	
	function changePubl( $id=null, $state=0) {
		global $mainframe;
		$db = JFactory::GetDBO();
	
		$query = "UPDATE #__sef_sm_menu"
		. "\n SET published = " . intval( $state )
		. "\n WHERE menu_id='$id'";
	
		$db->setQuery( $query );
		if (!$db->query()) {
			echo "<script> alert('".$database->getErrorMsg()."'); window.history.go(-1); </script>\n";
			exit();
		}
		$mainframe->redirect( "index2.php?option=com_sefservicemap&task=menu" );
	}
	
	function changeMenuPubl( $id=null, $state=0) {
		global $mainframe;
		$db = JFactory::GetDBO();
	
		$query = "UPDATE #__sef_sm_menus"
		. "\n SET published = " . intval( $state )
		. "\n WHERE id='$id'";
	
		$db->setQuery( $query );
		if (!$db->query()) {
			echo "<script> alert('".$database->getErrorMsg()."'); window.history.go(-1); </script>\n";
			exit();
		}
	
		$mainframe->redirect( "index2.php?option=com_sefservicemap&task=menu" );
	}
	
	function NextProcessor ($id=null) {
		global $mainframe;
		$db = JFactory::GetDBO();
		$query = "SELECT a.*,b.published AS menu_published,b.integrator AS menu_integrator,b.integrator_id as integrator_id, b.ordering AS menu_ordering, b.params AS menu_params"
		."\n FROM #__menu AS a LEFT JOIN #__sef_sm_menu as b ON b.menu_id = a.id "
		."\n WHERE b.menu_id='".$id."'";
		
		$db->setQuery($query);
		$menuitem = $db->loadObject();

		if ($menuitem->menu_integrator==0) $new_state = 0; else $new_state = 1;
		$integrator = $this->utils->get_menuitem_integtator($menuitem);

		$list = $this->utils->get_menuitem_integrators_list($menuitem);

		for ($i=0; $i<count($list); $i++) {
			$fromlist = $list[$i];
			if ($integrator->id == $fromlist->id && $integrator->comp == $fromlist->comp) {
				$next = $i+1;
				break;
			}
		}
		if ($next>=count($list)) $next = 0;
		$integrator = $list[$next];

		$query = "UPDATE #__sef_sm_menu"
		. "\n SET integrator = '".$new_state."'"
		. "\n , integrator_id = '" . $integrator->id .'-'.$integrator->comp."'"
		. "\n , params = '' "
		. "\n WHERE menu_id='$id'";
		
		$db->setQuery( $query );
		$db->query();
		if ($id) $this->clear_cache($id,0);
		$mainframe->redirect( "index2.php?option=com_sefservicemap&task=menu" );


	}
	function changeProcessorState($id=null)	{	     
		global $mainframe;
		$db = JFactory::GetDBO();
		$query = "SELECT a.*,b.published AS menu_published,b.integrator AS menu_integrator,b.integrator_id as integrator_id, b.ordering AS menu_ordering, b.params AS menu_params"
		."\n FROM #__menu AS a LEFT JOIN #__sef_sm_menu as b ON b.menu_id = a.id "
		."\n WHERE b.menu_id='".$id."'";
		
		$db->setQuery($query);
		$menuitem = $db->loadObject();

		$integrator = $this->utils->get_menuitem_integtator($menuitem);
				
		$state = $integrator->settings;			       
		switch ($state) {
			case 'disabled': $newstate=1;
				break; //use global settings
			case 'global': $newstate=2;
				break; //use custom settings
			case 'local': $newstate=0;
				break; //integrator disabled
		}
		$query = "UPDATE #__sef_sm_menu"
		. "\n SET integrator = '" . intval( $newstate ) . "'"
		. "\n , integrator_id = '" . $integrator->id .'-'.$integrator->comp."'"
		. "\n , params = '' "
		. "\n WHERE menu_id='$id'";
		
		$db->setQuery( $query );
		$db->query();

		if ($id) $this->clear_cache($id,0);

		$mainframe->redirect( "index2.php?option=com_sefservicemap&task=menu" );

	}
	
	function save_config($from) { 
		$db = JFactory::getDBO();

		global $mainframe;
		$params = JREQUEST::getVar('params',array());

		if (isset($params['ping_enabled'])) {
			$ping_enabled = $params['ping_enabled'];
			unset ($params['ping_enabled']);

			$db->setQuery("update #__plugins set published = '$ping_enabled' where element='sefservicemap' and folder='system'");
			$db->query(); 
		}

		$config = array();
		$i=1;
		if (is_array( $params )) {
			$txt = array();
			if ($params)
			foreach ( $params as $k=>$v) {
                        $config[$i]->variable=$k;
                        $config[$i]->value=$v; 
                        $i++;
               
			}
		}
		for ($j=1; $j<$i;$j++) {
			$query = ( "select count(variable) from #__sef_sm_settings where variable='".$config[$j]->variable."'");
			$db->setQuery($query);
			$cnt = $db->loadResult();
			if ($cnt==0) {
				$query = ( "insert into #__sef_sm_settings set variable='".$config[$j]->variable."'");
				$db->setQuery($query);
				$db->query();
			}
			$query = ( "update #__sef_sm_settings set value='".$config[$j]->value."' where variable='".$config[$j]->variable."'");
			$db->setQuery($query);
			$db->query();
		}
		$mainframe->redirect( "index2.php?option=com_sefservicemap&task=".$from,JText::_('Saved'),'message');
	}

	function save_css() {
		global $mainframe;
		$css = JREQUEST::getVar('css','');
		$file = JPATH_SITE.DS.'components'.DS.'com_sefservicemap'.DS.'css'.DS.'template_css.css';
		$config = @fopen($file,'wb');
		@fwrite($config,$css);
		@fclose($config); 
		$mainframe->redirect("index2.php?option=com_sefservicemap&task=config_css",JText::_('Saved'),'message');
	}

	function cancelint($menu_id,$id,$settings) {
		global $mainframe;
		if ($menu_id) $mainframe->redirect( "index2.php?option=com_sefservicemap&task=menu" );
		else  $mainframe->redirect("index2.php?option=com_sefservicemap&task=config_integrators");

	}
	function saveint($menu_id,$id,$settings) {
		global $mainframe;

		$params = JRequest::getVar('params', array());
		$comp = intval(JRequest::getVar('compatibile', 0));

		if (is_array( $params )) {
			$txt = array();
			if ($params)
			foreach ( $params as $k=>$v) {
				$txt[] = "$k=$v";
			}
			$row = implode( "\n", $txt );
		}
		$db = JFactory::getDBO();

  		if ($settings=='local') {
			$query = "UPDATE #__sef_sm_menu SET params='$row' where menu_id='$menu_id'";
		}
		else {
			if ($comp==0)
				$query = "UPDATE #__plugins SET params='$row' where id='$id'";
			else
				$query = "UPDATE #__sef_sm_plugins SET params='$row' where id='$id'";
		}
		$db->setQuery($query);
		$db->query();
		$this->remove_cache();
		if ($menu_id) $mainframe->redirect( "index2.php?option=com_sefservicemap&task=menu",JText::_('Saved'),'message' );
		else  $mainframe->redirect("index2.php?option=com_sefservicemap&task=config_integrators" ,JText::_('Saved'),'message');
	}

	function change_integrator_access() {
		global $mainframe;
		$db = JFactory::getDBO();   

		$comp = intval(JRequest::getVar('comp', 0));
		$id = intval(JRequest::getVar('id', 0));

		if ($comp==0) {
			$query = "select access from #__plugins"
			. "\n WHERE id='$id'";
		}
		else {
			$query = "select access from #__sef_sm_plugins"
			. "\n WHERE id='$id'";
		}
		
		$db->setQuery($query);
		$now = $db->loadResult();
	       
		switch ($now) {
			case 0: $state=1;
				break; //registered only
			case 1: $state=2;
				break; //special access
			case 2: $state=0;
				break; //public access
		}
	
		if ($comp==0) {
			$query = "UPDATE #__plugins"
			. "\n SET access = " . intval( $state )
			. "\n WHERE id='$id'";
		}
		else {
			$query = "UPDATE #__sef_sm_plugins"
			. "\n SET access = " . intval( $state )
			. "\n WHERE id='$id'";
		}
	
		$db->setQuery( $query );
		$db->query();
		$this->remove_cache();
		$mainframe->redirect( "index2.php?option=com_sefservicemap&task=config_integrators" );
	}

	function change_integrator_publish() {	     
		global $mainframe;

		$comp = intval(JRequest::getVar('comp', 0));
		$id = intval(JRequest::getVar('id', 0));

		$db = JFactory::getDBO();   

		if ($comp ==0)  {
			$query = "select published from #__plugins"
			. "\n WHERE id='$id'";
		}
		else {
			$query = "select published from #__sef_sm_plugins"
			. "\n WHERE id='$id'";
		}

		$db->setQuery($query);
		$published = $db->loadResult();

		if ($published == 1) $published = 0; else $published = 1;

		if ($comp ==0)  {	
			$query = "UPDATE #__plugins"
			. "\n SET published = " . intval( $published )
			. "\n WHERE id='$id'";
		}
		else {
			$query = "UPDATE #__sef_sm_plugins"
			. "\n SET published = " . intval( $published )
			. "\n WHERE id='$id'";
		}
	
		$db->setQuery( $query );
		$db->query();

		$this->remove_cache();

		$mainframe->redirect( "index2.php?option=com_sefservicemap&task=config_integrators" );

	}
	
	function publish_integrators($published) {

		global $mainframe;

		$db = JFactory::getDBO();   

		$cids = JREQUEST::GetVar('cid',array());
		if ($cids) {
			foreach ($cids as $cid) {
				$val = explode ('-',$cid);
				$comp =  $val[1];
				$id = $val[0];

				if ($comp ==0)  {	
					$query = "UPDATE #__plugins"
					. "\n SET published = " . intval( $published )
					. "\n WHERE id='$id'";
				}
				else {
					$query = "UPDATE #__sef_sm_plugins"
					. "\n SET published = " . intval( $published )
					. "\n WHERE id='$id'";
				}
			
				$db->setQuery( $query );
				$db->query();
			}
			$this->remove_cache();
		}

		$mainframe->redirect( "index2.php?option=com_sefservicemap&task=config_integrators" );
	}

	function detectType($p_dir,$package)
	{
		global $mainframe;

		$package['error']=false;
		// Search the install dir for an xml file
		$files = JFolder::files($p_dir, '\.xml$', 1, true);

		if (count($files) > 0)
		{
			if ($files)
			foreach ($files as $file)
			{
				$xmlDoc = & JFactory::getXMLParser();
				$xmlDoc->resolveErrors(true);

				if (!$xmlDoc->loadXML($file, false, true))
				{
					// Free up memory from DOMIT parser
					unset ($xmlDoc);
					continue;
				}
				$root = & $xmlDoc->documentElement;
				if (!is_object($root) || ($root->getTagName() != "install" && $root->getTagName() != 'mosinstall'))
				{
					unset($xmlDoc);
					continue;
				}

				$element = &$root->getElementsByPath('name', 1);
				$package['name'] = $element ? $element->getText() : '';

				$type = $root->getAttribute('type');
				$group = $root->getAttribute('group');

				switch ($type) {
					case 'mambot': 
						$config = &JFactory::getConfig();
						if ((version_compare($version, '1.5', '<') || $rootName == 'mosinstall') && !$config->getValue('config.legacy')) {
							JError::raiseWarning(1, 'Installer::install: '.JText::_('MUSTENABLELEGACY'));
							$package['type']='legacy';
							$package['class']='';
							$package['error']=true;
						}
						if ($group=='com_sefservicemap') {
							$package['type']='sef_sm_plugin';
							$package['class']='';
						}

						break;

					case 'plugin': 
						if ($group=='com_sefservicemap') {
							$package['type']='sef_sm_plugin';
							$package['class']='';
						}
						break;

					case 'xmap_ext':
						$package['type']='xmap';
						$package['class']='';
						break;

					default:
						$package['type']='';
						$package['class']='';

				}
				// Free up memory from DOMIT parser
				unset ($xmlDoc);
				return $package;
			}
		} 
		else {
			$config = &JFactory::getConfig();
			if ((version_compare($version, '1.5', '<') || $rootName == 'mosinstall') && !$config->getValue('config.legacy')) {
				JError::raiseWarning(1, 'Installer::install: '.JText::_('MUSTENABLELEGACY'));
				$package['type'] = 'legacy';
				$package['class'] = '';
				$package['error']=true;
				return $package;
			}

			$files = JFolder::files($p_dir, '\.php$', 1, true);

			// Generic integrators for xmap or joomap
			if ($files)
			foreach ($files as $file)
			{
				$mainframe->compatibile_plugin = '';
				include $file;
				$path = explode (DS,$file);
				$filename = $path[count($path)-1];
				$filearr = explode(".", $filename);
				unset($filearr[count($filearr)-1]);
				$file = trim(implode('.',$filearr));
				$package['type'] = $mainframe->compatibile_plugin->type;
				$package['class'] = $mainframe->compatibile_plugin->class;
				$package['file'] = $file;
				return $package;
			}
		}

	}

	function unpack_integrator($p_filename)
	{
		// Path to the archive
		$archivename = $p_filename;

		// Temporary folder to extract the archive into
		$tmpdir = uniqid('install_');

		// Clean the paths to use for archive extraction
		$extractdir = JPath::clean(dirname($p_filename).DS.$tmpdir);
		$archivename = JPath::clean($archivename);

		// do the unpacking of the archive
		$result = JArchive::extract( $archivename, $extractdir);
		if ( $result === false ) {
			return false;
		}

		/*
		 * Lets set the extraction directory and package file in the result array so we can
		 * cleanup everything properly later on.
		 */
		$retval['extractdir'] = $extractdir;
		$retval['packagefile'] = $archivename;

		/*
		 * Try to find the correct install directory.  In case the package is inside a
		 * subdirectory detect this and set the install directory to the correct path.
		 *
		 * List all the items in the installation directory.  If there is only one, and
		 * it is a folder, then we will set that folder to be the installation folder.
		 */
		$dirList = array_merge(JFolder::files($extractdir, ''), JFolder::folders($extractdir, ''));

		if (count($dirList) == 1)
		{
			if (JFolder::exists($extractdir.DS.$dirList[0]))
			{
				$extractdir = JPath::clean($extractdir.DS.$dirList[0]);
			}
		}

		/*
		 * We have found the install directory so lets set it and then move on
		 * to detecting the extension type.
		 */
		$retval['dir'] = $extractdir;
		$retval = $this->detectType($extractdir,$retval);

		return $retval;
	}

	function cleanupInstall($package, $resultdir)
	{
		$config =& JFactory::getConfig();

		// Does the unpacked extension directory exist?
		if (is_dir($resultdir)) {
			JFolder::delete($resultdir);
		}

		// Is the package file a valid file?
		if (is_file($package)) {
			JFile::delete($package);
		} elseif (is_file(JPath::clean($config->getValue('config.tmp_path').DS.$package))) {
			// It might also be just a base filename
			JFile::delete(JPath::clean($config->getValue('config.tmp_path').DS.$package));
		}
	}

	function copyGenericIntegratorFiles($src, $dest, $path='', $force = false) {
		// Initialize variables
		jimport('joomla.client.helper');
		$FTPOptions = JClientHelper::getCredentials('ftp');

		if ($path) {
			$src = JPath::clean($path.DS.$src);
			$dest = JPath::clean($path.DS.$dest);
		}
		
		// Eliminate trailing directory separators, if any
		$src = rtrim($src, DS); 
		$dest = rtrim($dest, DS);

		if (!JFolder::exists($src)) {
			return JError::raiseWarning(-1, JText::_('Cannot find source folder'));
		}

		// Make sure the destination exists
		if (! JFolder::create($dest)) {
			return JError::raiseWarning(-1, JText::_('Unable to create target folder'));
		}

		if ($FTPOptions['enabled'] == 1)
		{
			// Connect the FTP client
			jimport('joomla.client.ftp');
			$ftp = & JFTP::getInstance($FTPOptions['host'], $FTPOptions['port'], null, $FTPOptions['user'], $FTPOptions['pass']);

			if(! ($dh = @opendir($src))) {
				return JError::raiseWarning(-1, JText::_('Unable to open source folder'));
			}
			// Walk through the directory copying files and recursing into folders.
			while (($file = readdir($dh)) !== false) {
				$sfid = $src . DS . $file;
				$dfid = $dest . DS . $file;
				if (filetype($sfid)=='file') {
					$dfid = JPath::clean(str_replace(JPATH_ROOT, $FTPOptions['root'], $dfid), '/');
					if (! $ftp->store($sfid, $dfid)) {
						return JError::raiseWarning(1, JText::_('Copy failed'));
					}
				}
			}
		} else {
			if(! ($dh = @opendir($src))) {
				return JError::raiseWarning(-1, JText::_('Unable to open source folder'));
			}
			// Walk through the directory copying files and recursing into folders.
			while (($file = readdir($dh)) !== false) {
				$sfid = $src.DS.$file;
				$dfid = $dest.DS.$file;
				if (filetype($sfid)=='file') {
					IF (file_exists($dfid)) {
						JError::raiseWarning(1, JText::_('WARNSAME'));
						return $false;
					}
					if (!@ copy($sfid, $dfid)) {
						return JError::raiseWarning(1, JText::_('Copy failed'));
						return $false;
					}
				}
			}
		}
		return true;
	}

	function copyIntegratorFiles($package) { 
		$files = JFolder::files($package['extractdir'], '\.xml$', 1, true);
		$plugfile = false;
		if (count($files) > 0)
		{
			if ($files)
			foreach ($files as $manifestfile)
			{
				$installer = new JInstaller;
				$installer->setOverwrite(false);

				$xmlDoc = & JFactory::getXMLParser('Simple');
				if (!$xmlDoc->loadFile($manifestfile)) {
					JError::raiseWarning(100, JText::_('Install').': '.JText::_('Manifest File invalid or not found'));
					return false;
				}					
				$root =& $xmlDoc->document;
				$element =& $root->getElementByPath('files');

				if (is_a($element, 'JSimpleXMLElement') && count($element->children())) {
					$files =& $element->children();
					if ($files)
					foreach ($files as $file) {
						if ($file->attributes($type)) {
							$plugfile = $file->attributes($type); 
							break;
						}
					}
				}
				if ( !empty ($plugfile) ) {
					$installer->setPath('source', $package['extractdir']);
					$installer->setPath('manifest', $manifestfile);
					if ($package['type']=='sef_sm_plugin') $installer->setPath('extension_root', JPATH_SITE.DS.'plugins'.DS.'com_sefservicemap');
					else
					if ($package['type']=='xmap') $installer->setPath('extension_root', JPATH_SITE.DS.'components'.DS.'com_sefservicemap'.DS.'plugins'.DS.'xmap');
				} 
				else {
					$installer->abort( JText::_('Install').': '.JText::_('No extension file specified'));
					return false;
				}

				if ($installer->parseFiles($element, -1) === false) {
					$installer->abort();
					return false;
				}
				else {
					//parse images
					$element =& $root->getElementByPath('images');

					if (is_a($element, 'JSimpleXMLElement') && count($element->children())) {
						$installer->parseFiles($element,-1);
					}

					//parse media
					$element =& $root->getElementByPath('media');

					if (is_a($element, 'JSimpleXMLElement') && count($element->children())) {
						$installer->parseMedia($element);
					}

					//parse languages
					$element =& $root->getElementByPath('languages');

					if (is_a($element, 'JSimpleXMLElement') && count($element->children())) {
						$installer->parseLanguages($element,1);
					}
				}
				if (!$installer->copyManifest(-1)) {
					$installer->abort(JText::_('Install').': '.JText::_('Could not copy Manifest file'));
					return false;
				}
			}
		}
		return $plugfile;
	}

	function uninstall_integrator($integrator,$comp) {

		$result = true;
		$db = JFactory::getDBO();

		$installer = new JInstaller;
		$installer->setOverwrite(false);

		if ($comp==1) {
			$installer->setPath('extension_root', JPATH_SITE.DS.'components'.DS.'com_sefservicemap'.DS.'plugins'.DS.'xmap');
			$manifestFile =  JPATH_SITE.DS.'components'.DS.'com_sefservicemap'.DS.'plugins'.DS.'xmap'.DS.$integrator->element.'.xml';
		}
		else {
			$installer->setPath('extension_root', JPATH_SITE.DS.'plugins'.DS.'com_sefservicemap');
			$manifestFile = JPATH_SITE.DS.'plugins'.DS.'com_sefservicemap'.DS.$integrator->element.'.xml';
		}

		if (file_exists($manifestFile))
		{
			$xml =& JFactory::getXMLParser('Simple');

			// If we cannot load the xml file return null
			if (!$xml->loadFile($manifestFile)) {
				JError::raiseWarning(1, JText::_('Uninstall').': '.JText::_('Manifest File invalid or not found'));
				return false;
			}

			$root =& $xml->document;
			if ($root->name() != 'install' && $root->name() != 'mosinstall') {
				JError::raiseWarning(1, JText::_('Uninstall').': '.JText::_('Invalid manifest file'));
				return false;
			}
			
			$files = $root->getElementByPath('files');
			$installer->removeFiles($files,-1);

			$images = $root->getElementByPath('images');
			$installer->removeFiles($images,-1);

			$languages = $root->getElementByPath('languages');
			$installer->removeFiles($languages,1);

			$media = $root->getElementByPath('media');
			$installer->removeFiles($media);

			JFile::delete($manifestFile);

		} else {
			JError::raiseWarning(1, JText::_('Uninstall').': '.JText::_('Manifest File invalid or not found'));
			return false;
		}

		return $result;

	}

	function uninstall_generic_integrator($integrator) {
		if ($integrator->type == 'xmap') {
			$plug_file = JPATH_SITE.DS.'components'.DS.'com_sefservicemap'.DS.'plugins'.DS.'xmap'.DS.$integrator->element.'.php';
		}
		else {
			$plug_file = JPATH_SITE.DS.'components'.DS.'com_sefservicemap'.DS.'plugins'.DS.'joomap'.DS.$integrator->element.'.php';
		}
		$result = JFile::delete($plug_file);
		return $result;

	}

	function uninstall_integrators() {
		global $mainframe;

		jimport( 'joomla.installer.installer' );
		jimport('joomla.installer.helper');

		jimport('joomla.filesystem.file');
		jimport('joomla.filesystem.folder');
		jimport('joomla.filesystem.archive');
		jimport('joomla.filesystem.path');

		$db = JFactory::getDBO();
		$cids = JREQUEST::GetVar('cid',array(0));
		if ($cids)
		foreach ($cids as $cid) {
			$what = explode ('-',$cid);
			$comp = $what[1];
			$id = $what[0];

			if ($comp==1) {
				$query = "select * from #__sef_sm_plugins where id='".$id."'";
			}
			else {
				$query = "select * from #__plugins where id='".$id."'";
			}
			$db->setQuery($query);
			$integrator = $db->loadObject();

			if ($comp==0) $result = $this->uninstall_integrator($integrator,$comp);
			else {
				if ($integrator->class) $result = $this->uninstall_generic_integrator($integrator);
				else $result = $this->uninstall_integrator($integrator,$comp);
			}

			if ($result) {
				if ($comp==1) {
					$query = "DELETE from #__sef_sm_plugins where id='".$id."'";
				}
				else {
					$query = "DELETE from #__plugins where id='".$id."'";
				}
				$db->setQuery($query);
				$db->query();
				global $mainframe;
				$this->remove_cache();
				$mainframe->redirect( "index2.php?option=com_sefservicemap&task=config_integrators",JText::_('Uninstall').': '.JText::_('Successful'),'message' );
			}
		}
	}

	function upload_integrator() {
		jimport( 'joomla.installer.installer' );
		jimport('joomla.installer.helper');

		jimport('joomla.filesystem.file');
		jimport('joomla.filesystem.folder');
		jimport('joomla.filesystem.archive');
		jimport('joomla.filesystem.path');

		global $mainframe;
		$userfile = JRequest::getVar('install_package', null, 'files', 'array' );

		// Make sure that file uploads are enabled in php
		if (!(bool) ini_get('file_uploads')) {
			JError::raiseWarning('SOME_ERROR_CODE', JText::_('WARNINSTALLFILE'));
			return false;
		}

		// Make sure that zlib is loaded so that the package can be unpacked
		if (!extension_loaded('zlib')) {
			JError::raiseWarning('SOME_ERROR_CODE', JText::_('WARNINSTALLZLIB'));
			return false;
		}

		// If there is no uploaded file, we have a problem...
		if (!is_array($userfile) ) {
			JError::raiseWarning('SOME_ERROR_CODE', JText::_('No file selected'));
			return false;
		}

		// Check if there was a problem uploading the file.
		if ( $userfile['error'] || $userfile['size'] < 1 )
		{
			JError::raiseWarning('SOME_ERROR_CODE', JText::_('WARNINSTALLUPLOADERROR'));
			return false;
		}

		// Build the appropriate paths
		$config =& JFactory::getConfig();
		$tmp_dest 	= $config->getValue('config.tmp_path').DS.$userfile['name'];
		$tmp_src	= $userfile['tmp_name'];

		// Move uploaded file
		jimport('joomla.filesystem.file');
		$uploaded = JFile::upload($tmp_src, $tmp_dest);

		// Unpack the downloaded package file
		$package = $this->unpack_integrator($tmp_dest);


		$error = $package['error'];

		if (!$error)
		{
			echo $package['type'];
			// Copy files to destination
			switch ($package['type']) {
				case 'xmap' : 
					if ($package['class']) {
						$result = $this->copyGenericIntegratorFiles($package['extractdir'],JPATH_SITE.DS.'components'.DS.'com_sefservicemap'.DS.'plugins'.DS.'xmap'.DS);
						if ($result) $result = $package['file'];
					}
					else $result = $this->copyIntegratorFiles($package);
					break;
				case 'joomap' : $result = $this->copyGenericIntegratorFiles($package['extractdir'],JPATH_SITE.DS.'components'.DS.'com_sefservicemap'.DS.'plugins'.DS.'joomap'.DS);
					if ($result) $result = $package['file'];
					break;
				case 'sef_sm_plugin' : 
					@mkdir(JPATH_SITE.DS.'plugins'.DS.'com_sefservicemap');
					$result = $this->copyIntegratorFiles($package);
					break;
			}
	
			if ($result) {
				if (is_array($result)) {
					if ($result)
					foreach ($result as $key=>$value);
					$result = $value;
				}
				$db = JFactory::getDBO();
				switch ($package['type']) {
					case 'sef_sm_plugin' :
						$query = "select id from #__plugins where element='".$result."' and folder='com_sefservicemap'";

						$db->setQuery("select max(ordering) from #__plugins where folder='com_sefservicemap'");
						$max = $db->loadResult()+1;
						
						$addquery = "INSERT INTO #__plugins SET name='".$package['name']."', element='".$result."', folder='com_sefservicemap', ordering='".$max."', published='1'";
						break;
					default :
						$query = "select id from #__sef_sm_plugins where element='".$result."' and type='".$package['type']."'";
						if ($package['class']) {
							$addquery = "INSERT INTO #__sef_sm_plugins SET name='".$package['class']."', class='".$package['class']."', element='".$result."', type='".$package['type']."', published='1'";
						} 
						else {
							$addquery = "INSERT INTO #__sef_sm_plugins SET name='".$package['name']."', element='".$result."', type='".$package['type']."', published='1'";
						}

				}
				$db->setQuery($query);
				$id = $db->loadResult();
				if (!$id) {
					$db->setQuery($addquery);
					$db->query();
					$this->cleanupInstall ($package['packagefile'],$package['extractdir']);
					$mainframe->redirect( "index2.php?option=com_sefservicemap&task=config_integrators",JText::_('Install').': '.JText::_('Successful'),'message' );

				} 
				else
				{
					JError::raiseWarning(1, JText::_('Integrator Exists'));
				}
			} 

		} else	JError::raiseWarning(1, JText::_('Invalid Package'));


		// Clanup
		$this->cleanupInstall ($package['packagefile'],$package['extractdir']);
	}
}
?>