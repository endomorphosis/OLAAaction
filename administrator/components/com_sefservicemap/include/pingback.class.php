<?php
//*******************************************************
//* SEF Service Map Component
//* http://www.sefservicemap.com
//* (C) Radoslaw Kubera
//* license http://www.gnu.org/copyleft/gpl.html GNU/GPL
//*******************************************************

class PingBackClass {
	var $timeout = 1;
	var $request;
	var $answer;
	var $name;
	var $url;
	var $host;
	var $flerror;
	var $message;
	var $method;

	function PingBackClass (){
		$db = JFactory::getDBO();
		$db->setQuery("select value from #__sef_sm_settings where variable='ping_timeout'");
		$this->timeout= intval($db->loadResult()); if ($this->timeout<1) $this->timeout=1;
	}

	function checkMethod () {
		if (function_exists('fsockopen')) {
			return 'fsc';
		} elseif (function_exists('curl_init')) { 
			return 'crl'; 
		} else return 0;
	}

	function call ($url,$frame) {
		$request = '';
		$this->host = $url;
		$return = false;
		$useragent = "SEF SM 2.0";
		$framelength = strlen($frame);
		$address='http://'.str_replace('http://', '', $url);
		$url=parse_url($address);
		$site_url=$url['path'];
		if (isset($url['query'])) $site_url.='?'.$url['query'];
		$connection_type=$this->checkMethod ();
		$this->method = $connection_type;
		switch ($connection_type){
			case 'crl':
				$header[] = "POST ".$site_url." HTTP/1.1";
				$header[] = "Host: ".$url['host'];
				$header[] = "Content-type: text/xml";
				$header[] = "User-Agent: ".$useragent;
				$header[] = "Content-length: ".$framelength . "\r\n";
				$header[] = $frame;
				$request = implode ("\r\n",$header);
				$ch = curl_init();
				@curl_setopt( $ch, CURLOPT_URL, $url['host'].$site_url);
				@curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 ); # return into a variable
				@curl_setopt( $ch, CURLOPT_HTTPHEADER, $header ); # custom headers, see above
				@curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'POST' ); # This POST is special, and uses its specified Content-type
				$return = curl_exec( $ch );
				curl_close($ch); 
				break;

			case 'fsc':
				$request = "POST ".$site_url." HTTP/1.1\r\n";
				$request .= "Host: ".$url['host']."\r\n";
				$request .= "User-Agent: ".$useragent."\r\n";
				$request .= "Content-Type: text/xml\r\n";
				$request .= "Content-length: $framelength\r\n\r\n";
				$request .= "$frame\r\n";
				$data = @fsockopen($url['host'], 80, $er1, $er2, $this->timeout);
				if ($data){
					@fwrite($data, $request);
					while (!@feof($data)){
						$return .= @fgets($data, 128);
					}
					@fclose($data);
				}
				break;
			default : $return = false;
		}
		$this->request = $request;
		$this->answer = $return;
		return $return;
	}
	function checkHost($url) {
		$record = '';
		$config = JFactory::getConfig();
		$record->name = $config->getValue('sitename');
		$record->url = 'http://'.$_SERVER['SERVER_NAME'].'/';
		$return = $this->ping ($url,$record);
		return $return;
	}

	function get_result($result) {
		$return = false;
		$result = explode ("\n",$result);
		$result = implode ('',$result);
		
		$ret = array();
		if (eregi('<boolean>',$result)) {
			$ret = explode ('<boolean>',$result);
			if ($ret) {
				$flarr = explode ('</boolean>',$ret[1]);
				$return->flerror = SEF_SM_HTML_To_Plaintext($flarr[0]);
	
				if (eregi('message',$result)) {
					$ret = explode ('<value>',$result);
					if ($ret) {
						$messagearr = explode ('</value>',$ret[3]);
						$return->message = SEF_SM_HTML_To_Plaintext($messagearr[0]);
					}
				}
			}
		}
		else {
			$return->flerror = -1;
			$return->message = JText::_('Ping return unknown error. Check session log');
		}

		$this->flerror = $return->flerror;
		$this->message = $return->message;
		return $return;
	}

	function logSession ($id) {
		$db = JFactory::getDBO();
		$query = "select * from #__sef_sm_pingback where id='$id'";
		$db->setQuery($query);
		$pingback = $db->loadObject();
		if ($pingback) {
			$position = $pingback->log_position;
			$position++;
			if ($position>29) $position=0;
			$db->setQuery("select id from #__sef_sm_pingback_log where pingback_id='$id' and id='$position'");
			$exists = $db->loadResult();

			$datenow =& JFactory::getDate();
			$now = $datenow->toMysql();

			$log = '';
			$log .="Pinging host: ".$this->host."\r\n";
			$log .="Medhod: ".$this->method."\r\n";
			$log .="Request: ".$this->host."\r\n";
			$log .=$this->request."\r\n";
			$log .="\r\n";
			$log .="Answer:\r\n";
			$log .=$this->answer."\r\n";

			if ($exists) {
				$query  = "update #__sef_sm_pingback_log set ";
				$query .= " ping_date = '".$now."'";
				$query .= ", name = '".addslashes($this->name)."'";
				$query .= ", url = '".addslashes($this->url)."'";
				$query .= ", flerror = '".addslashes($this->flerror)."'";
				$query .= ", message = '".addslashes($this->message)."'";
				$query .= ", session_log = '".addslashes($log)."'";
				$query .= " where pingback_id='$id' and id='$position'";
			}
			else {
				$query  = "insert into #__sef_sm_pingback_log set ";
				$query .= " ping_date = '".$now."'";
				$query .= ", name = '".addslashes($this->name)."'";
				$query .= ", url = '".addslashes($this->url)."'";
				$query .= ", flerror = '".addslashes($this->flerror)."'";
				$query .= ", message = '".addslashes($this->message)."'";
				$query .= ", session_log = '".addslashes($log)."'";
				$query .= ", pingback_id='$id', id='$position'";
			}
			$db->setQuery($query);
			$db->query();
			$db->setQuery("update #__sef_sm_pingback set log_position='$position', last_ping_date='$now' where id='$id'");
			$db->query();
		}
	}
	function ping ($url,$record) {

		$link= utf8_encode($record->url);
		$link = str_replace('&amp;','&',$link);
		$link = str_replace('&apos;',"'",$link);
		$link = str_replace('&quot;','"',$link);
		$link = str_replace('&gt;','>',$link);
		$link = str_replace('&lt;','<',$link);
		$link = str_replace('&','&amp;',$link);
		$link = str_replace("'",'&apos;',$link);
		$link = str_replace('"','&quot;',$link);
		$link = str_replace('>','&gt;',$link);
		$link = str_replace('<','&lt;',$link);

		$name= utf8_encode($record->name);
		$name = str_replace('&amp;','&',$name);
		$name = str_replace('&apos;',"'",$name);
		$name = str_replace('&quot;','"',$name);
		$name = str_replace('&gt;','>',$name);
		$name = str_replace('&lt;','<',$name);
		$name = str_replace('&','&amp;',$name);
		$name = str_replace("'",'&apos;',$name);
		$name = str_replace('"','&quot;',$name);
		$name = str_replace('>','&gt;',$name);
		$name = str_replace('<','&lt;',$name);


		$this->name = $name;
		$this->url = $link;

		$frame  = "<"."?xml version=\"1.0\"?".">\r\n";
		$frame .= "<methodCall>\r\n";
		$frame .= "	<methodName>weblogUpdates.ping</methodName>\r\n";
		$frame .= "	<params>\r\n";
		$frame .= "		<param>\r\n";
		$frame .= "			<value>".$name."</value>\r\n";
		$frame .= "		</param>\r\n";
		$frame .= "		<param>\r\n";
		$frame .= "			<value>".$link."</value>\r\n";
		$frame .= "		</param>\r\n";
		$frame .= "	</params>\r\n";
		$frame .= "</methodCall>\r\n";
		$result = $this->call($url,$frame);
		return $this->get_result($result);
	}
}

function SEF_SM_HTML_To_Plaintext($html_text) {
	$search = array ('@<script[^>]*?>.*?</script>@si', // Strip out javascript
                 '@<[\/\!]*?[^<>]*?>@si',          // Strip out HTML tags
                 '@([\r\n])[\s]+@',                // Strip out white space
                 '@&(quot|#34);@i',                // Replace HTML entities
                 '@&(amp|#38);@i',
                 '@&(lt|#60);@i',
                 '@&(gt|#62);@i',
                 '@&(nbsp|#160);@i',
                 '@&(iexcl|#161);@i',
                 '@&(cent|#162);@i',
                 '@&(pound|#163);@i',
                 '@&(copy|#169);@i',
                 '@&#(\d+);@e');                    // evaluate as php

	$replace = array ('',
                 '',
                 '\1',
                 '"',
                 '&',
                 '<',
                 '>',
                 ' ',
                 chr(161),
                 chr(162),
                 chr(163),
                 chr(169),
                 'chr(\1)');

	$text = preg_replace($search, $replace, $html_text);
	$text = preg_replace( '/{.*?\}/', ' ', $text );

	return $text;
}


?>