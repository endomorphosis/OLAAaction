<?php
//*******************************************************
//* SEF Service Map Component
//* http://www.sefservicemap.com
//* (C) Radoslaw Kubera
//* license http://www.gnu.org/copyleft/gpl.html GNU/GPL
//*******************************************************


/** ensure this file is being included by a parent file */
defined( '_JEXEC' ) or die( 'Restricted access' );

include_once JPATH_SITE.DS."administrator".DS."components".DS."com_sefservicemap".DS."include".DS."pingback.class.php";

DEFINE ('_SEF_SM_FILESYSTEM',0);
DEFINE ('_SEF_SM_DATABASE',1);

class SEFSMRoute
{
	function _($url, $xhtml = true, $ssl = null)
	{
		$uri = JURI::getInstance(JURI::base());
		$full_uri = str_replace('/administrator','',$uri->_uri);
		return $full_uri.$url;
	}
}


include_once ( JPATH_LIBRARIES.DS.'domit'.DS.'xml_domit_lite_include.php' );

function AddExtMenuItem ($element) { 
	global $mainframe;

	@$menu_id = $mainframe->sm_menuitem_id;

	$user	= &JFactory::getUser();
	$gid	= (int) $user->get('gid', 0);

	if (isset ($element->icon_path)) $icon_path = $element->icon_path; else $icon_path='';  
	if (eregi('mambots',$icon_path)) { 
		$tmp = $icon_path;
		if (!is_dir(JPATH_SITE.$icon_path))
			$icon_path = str_replace('mambots','plugins',$icon_path);
		if (!is_dir(JPATH_SITE.$icon_path)) 
			$icon_path=$tmp;
	}
	$element->icon_path = $icon_path;

	if ($mainframe->maptype!='html') $no_html=1;

	$sublevels=$mainframe->ParamsArray->get( 'sublevels', '0' );  
	$max_desc_length = $mainframe->ParamsArray->get( 'max_desc_length', '0' );
	if ($element->level<$sublevels || $sublevels==0) {  
		if (isset ($element->name)) $name = $element->name; else $name='';
		if (isset ($element->link)) $link = $element->link; else $link='';
		if (isset ($element->image)) $image = $element->image; else $image='';
		if (isset ($element->level)) $level = $element->level; else $level=0;
		if (isset ($element->lastmod)) $lastmod = $element->lastmod; else $lastmod='';

		if (isset ($element->changefreq) && $element->changefreq!='') $changefreq = $element->changefreq; else $changefreq= $mainframe->ParamsArray->get( 'changefreq','weekly');
		if (isset ($element->priority) && $element->priority!='') $priority = $element->priority; else $priority= $mainframe->ParamsArray->get( 'priority','0.5');
 
		$element->changefreq = $changefreq;
		$element->priority = $priority;

		if (isset($element->description) && $element->description!='') { 
			$short_desc=SEF_SM_HTML_To_Plaintext ($element->description);
			if ($max_desc_length!=0) {
				$sub_desc = substr ($short_desc,0,$max_desc_length); 
				if (strlen($sub_desc)==strlen($short_desc)) $short_desc=$sub_desc; else $short_desc=$sub_desc.'...';
			}
		} else $short_desc='';
		$description = $short_desc;

		switch ($mainframe->maptype) {
			//case 'xml':    show_xml_element ($element); break;
			case 'txt':  	show_txt_element ($element); break;
			//case 'html':   show_element ($element); break;
		}

		$mainframe->elements_count = $mainframe->elements_count+1;

		//build filesystem cache
		if (isset($mainframe->sef_sm_cache_file)) { 
			$mainframe->sef_sm_cache_db_ordering++;
			$str = trim ("<sefrec><sefsm>".$name."<sefsm>".$link."<sefsm>".$image."<sefsm>".$level."<sefsm>".$icon_path."<sefsm>".$lastmod."<sefsm>".$changefreq."<sefsm>".$priority."<sefsm>".$description."<sefsm></sefrec>")."\n";
			@fwrite ($mainframe->sef_sm_cache_file,$str);
		}

		//build database cache
		if (isset($mainframe->sef_sm_cache_db_id)) {
			$ordering = $mainframe->sef_sm_cache_db_ordering;
			$mainframe->sef_sm_cache_db_ordering++;
			$db = JFactory::getDBO();
			$id = $mainframe->sef_sm_cache_db_id;
			$name = addslashes($name);
			$link = addslashes($link);
			$image = addslashes($image);
			$icon_path = addslashes($icon_path);

			$query = "INSERT INTO #__sef_sm_cache_items set id ='$id', ordering='$ordering', name='$name', link='$link', image='$image', icon_path='$icon_path', level='$level', lastmod='$lastmod', changefreq='$changefreq', priority='$priority', description='$description'";
			$db->setQuery($query);
			$db->query(); 
		}
	
		//add ping to stack

		if (isset($mainframe->pingall) && $mainframe->pingall==1 && $mainframe->sm_ping_enabled==1) {
			$db = JFactory::getDBO();
			$pingbacks = $mainframe->pingback;
			$ping_queue=array();
			if ($pingbacks)
			foreach($pingbacks as $pingback) {
				$datenow =& JFactory::getDate();
				$now = $datenow->toMysql();
				if (!in_array($pingback->id,$ping_queue)) {
					$mainframe->queue_elements = $mainframe->queue_elements+1;
					$ping_queue[] = $pingback->id;
				}

				$url_md5 = md5($link);
				$db->setQuery("insert into #__sef_sm_pingback_stack set id='$pingback->id', menu_id='$menu_id' , url_md5='$url_md5', url='$link', name='$name', modified='$lastmod', added = '$now' ");
				$db->query();
			}
		} 
		else if ($mainframe->ParamsArray->get( 'auto_ping', '1' )==1 && $mainframe->sm_ping_enabled==1) {
			if ($mainframe->ParamsArray->get( 'ping_enabled', '1' )==1) {
				if ($gid==0 && (isset($mainframe->sef_sm_cache_db_id) || isset($mainframe->sef_sm_cache_file))) {
					if ($lastmod) {
						$db = JFactory::getDBO();
						$lastmodtime = strtotime ($lastmod);
						$lastcachetime = strtotime($mainframe->sef_sm_last_cache_date);

						$pingbacks = $mainframe->pingback;
						$pingbackdates = $mainframe->pingbackdates;

						$ping_queue=array();
						if ($pingbacks)
						foreach($pingbacks as $pingback) {

							$index = $menu_id.'_'.$pingback->id;
							if (isset($pingbackdates[$index])) $pingdate = strtotime($pingbackdates[$index]); else $pingdate='';
							if ($lastmodtime>$lastcachetime) $lastcacheenabled =1; else $lastcacheenabled=0;
							if ($lastmodtime>$pingdate && $lastcacheenabled==1) {
								$datenow =& JFactory::getDate();
								$now = $datenow->toMysql();
								if (!in_array($pingback->id,$ping_queue)) {
									$mainframe->queue_elements = $mainframe->queue_elements+1;
									$ping_queue[] = $pingback->id;
								}
								$url_md5 = md5($link);
								$db->setQuery("insert into #__sef_sm_pingback_stack set id='$pingback->id', menu_id='$menu_id', url_md5='$url_md5', url='$link', name='$name', modified='$lastmod', added = '$now' ");
								$db->query();
							}
						}
					}
				}
			}
		}
	}
}

function check_for_new_column($start, $end, $counter) {

	global $mainframe;
	$items_pagination = $mainframe->ParamsArray->get( 'items_pagination', '40' ) ;
	$count = $mainframe->elements_count;
	$columns = $mainframe->ParamsArray->get( 'columns', '1' ) ;
	if ($columns==2) {
		if ($items_pagination==-1) {
			if ($counter==round($count/2)) {
				echo '</td><td valign="top">';
			}
		} 
		else {
			if (($counter-$start-1)==round($items_pagination/2)) {
				echo '</td><td valign="top">';
			}
		}
	}

}

function smGetParam($link,$option,$default) {
	$pass=0;
	$index = substr($link,0,9);
	if ($index=='index.php') $pass=1; 
	else {
		
		$live_site = 'http://'.$_SERVER['SERVER_NAME'];
		$length = strlen($live_site);
		$site = substr($link,0,$length);
		if ($site == $live_site) $pass=1;
	}

	if ($pass==0) return $default;

	$link=str_replace("&amp;","&",$link);
	$link=str_replace("?","&",$link);
	$links = explode("&", $link);
	$i=count($links);
	$length = strlen($option)+1;
	$opt = $option.'=';
	for ($n=0; $n<$i; $n++) {
		if (substr($links[$n], 0, $length)==$opt) {
			$optout = str_replace($opt,"",$links[$n]);
			return $optout;
		}
	}
	return $default;
}

class GenericPlugins {
	var $level = 0;
	var $sitemap;
	var $config;
	var $class;
	var $gid;
	var $now;
	var $view;
	var $access;
	var $classname;
	var $Itemid = '';

	function addDefaults ($menuitem) {
		global $mainframe;
		$menuitem->uid = $this->uid;
		$menuitem->gid = $this->gid;
		$menuitem->now = $this->now;
		$menuitem->noauth = $this->noauth;
		$menuitem->auth = $this->auth;

		$menuitem->priority = $mainframe->ParamsArray->get( 'priority','0.5');
		$menuitem->changefreq = $mainframe->ParamsArray->get( 'changefreq','weekly');
		return $menuitem;
	}

	function GenericPlugins ($class) {
		$user	= &JFactory::getUser();

		$datenow =& JFactory::getDate();
		$this->now = $datenow->toUnix();

		global $mainframe;
		$this->priority = $mainframe->ParamsArray->get( 'priority','0.5');
		$this->changefreq = $mainframe->ParamsArray->get( 'changefreq','weekly');

		$this->sitemap = '';
		$this->sitemap->expand_category = '1';
		$this->sitemap->expand_section = '1';
		$this->sitemap->exlinks = 0;
		$this->sitemap->ext_image = '	_grey.gif';

		$this->config = '';
		$this->config->expand_category = '1';
		$this->config->expand_section = '1';
		
		$this->classname = $class;
		$this->gid	= (int) $user->get('gid', 0);
		$this->uid	= (int) $user->get('id', 0);
		$this->noauth='';
		$this->auth = '';

		$this->access->canEdit = 0;
		$this->access->canEditOwn = 0;
		$this->access->canPublish = 0;

		global $mainframe;
		if (isset($mainframe->maptype)) $maptype = $mainframe->maptype; else $maptype='html';
		switch ($maptype) {
			case 'txt':
			case 'xml':
				$this->view='xml';
				break;
			default: $this->view = 'html';
		}
	}

	function paramsToArray( &$params ) {
		
		
		$tmp = explode("\n", $params);
		$res = array();
		if ($tmp)
		foreach($tmp AS $a) {
			@list($key, $val) = explode('=', $a, 2);
			$res[$key] = $val;
		}
		$res['__TXT__'] = $params;
		return $res;
	}

	function startMenu(&$menu) {
		return true;
	}

	function endMenu(&$menu) {
		return true;
	}

	function printNode( &$node ) {
		if ($node) {
			$element = '';
			$element->name = $node->name;
			if ($this->Itemid) $element->link = $node->link.'&amp;Itemid='.$this->Itemid;
				else $element->link = $node->link;
			$element->level = $this->level;
			$element->image = '';
			$element->icon_path='';
			$element->description='';
			if (isset($node->modified)) $element->lastmod = $node->modified;
			if (is_array($node->tree)) $this->printNode ($node->tree);
			if (isset($node->priority)) $element->priority = $node->priority; else $element->priority=$mainframe->ParamsArray->get( 'priority','0.5');
			if (isset($node->changefreq))$element->changefreq = $node->changefreq; else $element->changefreq = $mainframe->ParamsArray->get( 'changefreq','weekly');
			AddExtMenuItem($element);
		}
	}

	function changeLevel( $level ) {
		$this->level = $this->level+$level;
	}

	function RenderGeneric($list,$level=0,$params) {
		if ($list) {
			global $mainframe;
			$priority=$params->get( 'priority', '' ) ; 
			$changefreq=$params->get( 'changefreq', '' );
			$show_icons=$params->get( 'show_icons', '' );

			if ($priority=='')$priority = $mainframe->ParamsArray->get( 'priority','0.5');
			if ($changefreq=='') $changefreq = $mainframe->ParamsArray->get( 'changefreq','weekly');

			if ($list)
			foreach ($list as $node) {
				$element->name = $node->name;
				$element->link = $node->link;
				$element->level = $level;
				if ($show_icons==0) $element->image = 'none'; else $element->image='';
				$element->icon_path='';
				$element->description='';
				if (isset($node->modified)) $element->lastmod = date ('Y-m-d H:i:s',$node->modified);
				$element->priority = $priority;
				$element->changefreq= $changefreq;
				AddExtMenuItem($element);
				$sublevel = $level+1;
				if (isset($node->tree) && is_array($node->tree)) $this->RenderGeneric($node->tree, $sublevel,$params);
			}
		}
	}
}

class XmapPlugins extends GenericPlugins {
	function addPlugin( $class ) {
		global $mainframe;
		$plugin = '';
		$plugin->type = 'xmap';
		$plugin->class = get_class($class);
		$mainframe->compatibile_plugin = $plugin;
	}
};

class JoomapPlugins extends GenericPlugins {
	function addPlugin( $class ) {
		global $mainframe;
		$plugin = '';
		$plugin->type = 'joomap';
		$plugin->class = get_class($class);
		$mainframe->compatibile_plugin = $plugin;
	}


};

class SEF_SM_Utils_class {

	var $integrators = array();
	var $menu = array();

	var $cachetime = '';

	function SEF_SM_Utils_class () {

		$config = &JFactory::getConfig();
		$this->integrators = $this->get_integrators_list (-1,'',$config->getValue('config.legacy'));
	}

	function get_pingback () {
		global $mainframe;
		$db = JFactory::getDBO();
		$query = "select * from #__sef_sm_pingback";
		$db->setQuery($query);
		$pingbacks = $db->loadObjectList();
		$pingback = array();
		if ($pingbacks)
		foreach ($pingbacks as $ping) {
			$pingback[$ping->id] = $ping;
		}
		$mainframe->pingback = $pingback;

		$query = "select * from #__sef_sm_pingback_menu";
		$db->setQuery($query);
		$pingback_menu = $db->loadObjectList();

		$pingbackdates = array();
		if ($pingback_menu)
		foreach ($pingback_menu as $pm) {
			$index = $pm->menu_id.'_'.$pm->pingback_id;
			$pingbackdates[$index] = $pm->last_date;
		}
		$mainframe->pingbackdates = $pingbackdates;
	}

	function get_menu () {
		$db = JFactory::getDBO();
		$query = "select * from #__sef_sm_menu";
		$db->setQuery($query);
		$menuitems = $db->loadObjectList();
		$menu = array();
		if ($menuitems)
		foreach ($menuitems as $menuitem) {
			$menu[$menuitem->menu_id] = $menuitem;
		}
		$this->menu = $menu;
	}

	function get_Manifest ($row, $xmlfile) {
		$xmlDoc = new DOMIT_Lite_Document();
		$xmlDoc->resolveErrors( true );

		if (!$xmlDoc->loadXML( $xmlfile, false, true )) {
			return $element;
			continue;
		}
		$root = &$xmlDoc->documentElement;

		if ($root->getTagName() == 'mosinstall') {
			$row->legacy = 1;
		} 
		else
			$row->legacy = 0;
		$element = &$root->getElementsByPath('name', 1);
		$row->plugname = $element ? $element->getText() : $row->name;
		$element = &$root->getElementsByPath('creationDate', 1);
		$row->creationdate = $element ? $element->getText() : '';
		$element = &$root->getElementsByPath('author', 1);
		$row->author = $element ? $element->getText() : '';
		$element = &$root->getElementsByPath('copyright', 1);
		$row->copyright = $element ? $element->getText() : '';
		$element = &$root->getElementsByPath('authorEmail', 1);
		$row->authorEmail = $element ? $element->getText() : '';
		$element = &$root->getElementsByPath('authorUrl', 1);
		$row->authorUrl = $element ? $element->getText() : '';
		$element = &$root->getElementsByPath('version', 1);
		$row->version = $element ? $element->getText() : '';
		return $row;
	}

	// $comp = -1 - return all integrators
	function get_integrators_list ($comp= -1 ,$type='',$legacy=0) {
		$db = JFactory::getDBO();

		$config = &JFactory::getConfig();

		$integrators = array();

		//first, get SEF SM integrators
		if ($comp ==0 || $comp ==-1) {
			$query = "select * from #__plugins where folder='com_sefservicemap' order by id ASC";
			$db->setQuery($query);
			$elements = $db->loadObjectList();

			if ($elements) {
				foreach ($elements as $element) {
					$xmlfile = JPATH_SITE.DS.'plugins'.DS.'com_sefservicemap'.DS.$element->element.'.xml';	
					$element = $this->get_Manifest ($element, $xmlfile);
					$element->comp = 0;
					if  ($legacy==1 || ($element->legacy==0 || ($config->getValue('config.legacy') && $element->legacy==1))) {
						$element->type = 'sef_sm_plugin';
						if ($type=='' || $element->type==$type) $integrators[$element->id.'-'.$element->comp] = $element;
					}
				}	
			}
		}

		//now, get compatible integrators
		if ($comp ==1 || $comp ==-1) {
			$elements = array();
			$query = "select * from #__sef_sm_plugins order by id ASC";
			$db->setQuery($query);
			$elements = $db->loadObjectList();

			if ($elements) { 
				foreach ($elements as $element) { 
					$xmlfile = JPATH_SITE.DS.'components'.DS.'com_sefservicemap'.DS.'plugins'.DS.$element->type.DS.$element->element.'.xml';	
					if (!file_exists($xmlfile)) $xmlfile = JPATH_SITE.DS.'components'.DS.'com_sefservicemap'.DS.'plugins'.DS.'generic_plugin.xml';
					$element = $this->get_Manifest ($element, $xmlfile);
					$element->comp = 1;
					if  ($legacy==1 || ($element->legacy==0 || ($config->getValue('config.legacy') && $element->legacy==1))) {
						if ($type=='' || $element->type==$type) $integrators[$element->id.'-'.$element->comp] = $element;
					}
				}	
			}
		}

		return $integrators;
	}

	function get_menuitem_integtator ($menuitem) {
		$menu = $this->menu[$menuitem->id];

		$db = JFactory::getDBO();
		$integrator = $menu->integrator_id;
		if (isset($this->integrators[$integrator])) $myint = $this->integrators[$integrator];
		if (isset($this->integrators[$integrator]) && $myint->published>=1) {
			$int = $this->integrators[$integrator];
			if ($menu->integrator==2) {
				$int->params = $menu->params;
				$int->settings='local';
			}
			else if ($menu->integrator==1) {
				$int->settings='global';
			}
			else {
				$int->settings='disabled';
			}
		}
		else { 
			$list = $this->get_menuitem_integrators_list($menuitem);
			if (count($list)>0) {
				$int = $list[0];
				if ($menu->integrator==0) {
					$int->settings='disabled';
				} else
					$int->settings='global';
			}
			else {
				$int = '';
			}
		}

		if ($processor = smGetParam($menuitem->link,'option','')) {
			if ($processor=='content') $processor='com_content';
			else {
					$menuitem->component = $processor;
			}
		}


		if (isset($int->class) && $int->class!='') {
			$db->setQuery("select name from #__components where id='".$menuitem->componentid."'");
			$int->component = $db->loadResult();
		}
		return $int;	
	}

	function get_menuitem_integrators_list ($menuitem) {
		// menuitem->component
		// menuitem->link
		// menuitem->type
		$db = JFactory::getDBO();
		if ($menuitem->type !='component' && $menuitem->type !='components') {
			$list = array();
			return $list;
		}
		if ($processor = smGetParam($menuitem->link,'option','')) {
			if ($processor=='content') $processor='com_content';
			else {
				$menuitem->component = $processor;
			}
		}
		$list = array();
		if ($this->integrators)
		foreach ($this->integrators as $integrator) {

			switch ($integrator->type) {
				case 'sef_sm_plugin' :
					$add = 0;
					if ($menuitem->component==str_replace('_bot','',$integrator->element)) $add =1;
					if ($integrator->published==0) $add=0;
					if ($add==1) $list[] = $integrator;
					break;

				case 'xmap' :
					$add = 0;
					if ($integrator->class) {
						require_once (JPATH_SITE.DS.'components'.DS.'com_sefservicemap'.DS.'plugins'.DS.'xmap'.DS.$integrator->element.'.php');
						$name = $integrator->class;
						$plugin = new $name;
						$xmap = new GenericPlugins('xmap');
						if ($plugin->isOfType($xmap,$menuitem)) $add=1;
						unset ($plugin);
					}
					else {
						if ($integrator->element==$menuitem->component) $add =1;
					} 
					if ($integrator->published==0) $add=0;
					if ($add==1) {
						if ($integrator->class!='') {
							$db->setQuery("select name from #__components where id='".$menuitem->componentid."'");
							$integrator->component = $db->loadResult();
						}
						$list[] = $integrator;
					}
					break;

				case 'joomap' : 
					$add = 0;
					require_once (JPATH_SITE.DS.'components'.DS.'com_sefservicemap'.DS.'plugins'.DS.'joomap'.DS.$integrator->element.'.php');
					$name = $integrator->class;
					$plugin = new $name;
					$joomap = new GenericPlugins('joomap');
					if ($plugin->isOfType($joomap,$menuitem)) $add=1;
					unset ($plugin);
					if ($integrator->published==0) $add=0;
					if ($add==1) {
						$db->setQuery("select name from #__components where id='".$menuitem->componentid."'");
						$integrator->component = $db->loadResult();
						$list[] = $integrator;
					}
					break;
			}
		}
		return $list;
	}
	function cache_start($id,$integrator) {   

		global $mainframe;
		$config = new JConfig();
		unset($mainframe->sef_sm_cache_file);
		unset($mainframe->sef_sm_db_id);
		$cache_type = $mainframe->ParamsArray->get( 'cache_type','0');
		$lang =& JFactory::getLanguage();
		$mylang = $lang->_lang;
		$mylang = JRequest::getVar('lang',$mylang);

		$user = &JFactory::getUser();
		$gid = (int) $user->get('gid', 0);

		$maptype = $mainframe->maptype;
		{ 
			switch ($cache_type) {
				case _SEF_SM_FILESYSTEM : 
					$cache_dir = JPATH_COMPONENT.DS.'cache'.DS;
					@mkdir ($cache_dir);
					@mkdir ($cache_dir.$id.DS);

					$datenow =& JFactory::getDate();
					$dnow = $datenow->toUnix();

					$file = $id.'_'.$maptype.'_'.$gid.'_'.$mylang;
					if (file_exists($cache_dir.$id.DS.$file)) { 
						$index = $integrator->id.'-'.$integrator->comp.'_time';
						$time = $mainframe->ParamsArray->get($integrator->id.'-'.$integrator->comp.'_time',$config->cachetime); 
						$filetime=filemtime($cache_dir.$id.DS.$file);
						if (($filetime+$time)<mktime()) { 
							unlink ($cache_dir.$id.DS.$file);
							$mainframe->sef_sm_cache_file=fopen($cache_dir.$id.DS.$file,"w");
							$mainframe->sef_sm_cache_db_ordering=0;
							$date= JFactory::getDate($filetime);
							$mainframe->sef_sm_last_cache_date=gmdate('Y-m-d H:i:s',$filetime);
							return 1;
						}
						else {
							return 0;
						}
					} 
					else {
						@$mainframe->sef_sm_cache_file=fopen($cache_dir.$id.DS.$file,"w");
						$mainframe->sef_sm_cache_db_ordering=0;
						$mainframe->sef_sm_last_cache_date='';
						return 1;
					}
					break;

				case _SEF_SM_DATABASE : 
						$cache = '';
						$db = JFactory::getDBO();
						$query = "select * from #__sef_sm_cache where menu_id='$id' and maptype='$maptype' and lang='$mylang' and gid='$gid'";
						$db->setQuery($query);
						$cache = $db->loadObject();
						if (isset($cache->id) && $cache->id) { 			
							$datenow =& JFactory::getDate();
							$now = $datenow->toMySQL();
							$nowint = $datenow->toUnix();
							@$int_time = $integrator->id.'-'.$integrator->comp.'_time';
							$time = $mainframe->ParamsArray->get($int_time,$config->cachetime);
							$lastmod = strtotime($cache->lastmod)+$time;
							if ($lastmod+$time<$nowint || $cache->items<0) { 
								$query = "UPDATE #__sef_sm_cache set lastmod = '$now' where id = '$cache->id'";
								$db->setQuery($query);
								$db->query();

								$query = "DELETE from #__sef_sm_cache_items where id='$cache->id'";
								$db->setQuery($query);
								$db->query();
								$mainframe->sef_sm_cache_db_id=$cache->id;
								$mainframe->sef_sm_cache_db_ordering=0;
								$mainframe->sef_sm_last_cache_date=$cache->lastmod;
								return 1;
							} else return 0;
						} 
						else { 
							$datenow =& JFactory::getDate();
							$now = $datenow->toMySQL();
							$query = "INSERT INTO #__sef_sm_cache set menu_id='$id', lang='$mylang', maptype='$maptype', lastmod = '$now', gid='$gid'";
							$db->setQuery($query);
							$db->query();
							$id = $db->InsertID();
							$mainframe->sef_sm_cache_db_id=$id;
							$mainframe->sef_sm_cache_db_ordering=0;
							$mainframe->sef_sm_last_cache_date='';
							return 1;
						}
						break;

			}
		}
		unset($mainframe->sef_sm_cache_db_id);
		unset($mainframe->sef_sm_cache_db_ordering);
		unset($mainframe->sef_sm_cache_file);
		unset($mainframe->sef_sm_last_cache_date);
		return 0;
	}

	function cache_stop($id) { 

		global $mainframe;
		
		$user = &JFactory::getUser();
		$gid = (int) $user->get('gid', 0);

		$cache_type = $mainframe->ParamsArray->get( 'cache_type','0');
		switch ($cache_type) {
			case _SEF_SM_FILESYSTEM :
				if ($mainframe->sef_sm_cache_file) 
				{
					fclose ($mainframe->sef_sm_cache_file);

					$lang =& JFactory::getLanguage();
					$mylang = $lang->_lang;
					$mylang = JRequest::getVar('lang',$mylang);

					$maptype = $mainframe->maptype;

					$cache_dir = JPATH_COMPONENT.DS.'cache'.DS;
					$file = $id.'_'.$maptype.'_'.$gid.'_'.$mylang.'_items';
					if (file_exists($cache_dir.$id.DS.$file)) { 
						@unlink ($cache_dir.$id.DS.$file);
					}
					$items_file = fopen($cache_dir.$id.DS.$file,"w");
					$items = $mainframe->sef_sm_cache_db_ordering;
					$str = 'items='.$items."\n";
					fwrite ($items_file,$str);
					fclose ($items_file);
				}
				break;

			case _SEF_SM_DATABASE :
				$id = $mainframe->sef_sm_cache_db_id;
				$items = $mainframe->sef_sm_cache_db_ordering;
				$db = JFactory::getDBO();
				$query = "UPDATE #__sef_sm_cache set items='$items' where id='$id'";
				$db->setQuery($query);
				$db->query();
				break;
		}
		unset($mainframe->sef_sm_cache_file);
		unset($mainframe->sef_sm_cache_db_id);
		unset($mainframe->sef_sm_cache_db_ordering);
		unset($mainframe->sef_sm_last_cache_date);
	}

	function get_from_cache($id,$mode=0,$start=-1,$end=-1,&$counter) {

		$user = &JFactory::getUser();
		$gid = (int) $user->get('gid', 0);

		$lang =& JFactory::getLanguage();
		$mylang = $lang->_lang;
		$mylang = JRequest::getVar('lang',$mylang);

		global $mainframe;
		$cache_type = $mainframe->ParamsArray->get( 'cache_type','0');
		$maptype = $mainframe->maptype;

		switch ($cache_type) {
			case _SEF_SM_FILESYSTEM :
				if ($mode==1 || $mainframe->maptype=='txt') {
					$cache_dir = JPATH_COMPONENT.DS.'cache'.DS;
					$file = $id.'_'.$maptype.'_'.$gid.'_'.$mylang;
					if (file_exists($cache_dir.$id.DS.$file)) { 
						$handle = fopen ($cache_dir.$id.DS.$file,'r');
						while (!feof($handle)) {
							$line = fgets($handle);
							$element='';
							$data = explode ('<sefsm>',$line);
							if (isset($data[0]) && $data[0]=='<sefrec>') {
								if (isset($data[9])) {
									$element='';
									$element->name = $data[1];
									$element->link = $data[2];
									$element->image = $data[3];
									$element->level = $data[4];
									$element->icon_path = $data[5];
									$element->lastmod = $data[6];
									$element->changefreq = $data[7];
									$element->priority = $data[8];
									$element->description = $data[9];
									if ($mode==0) AddExtMenuItem($element); else {
										$counter++;
										switch ($mainframe->maptype) {
											case 'html' : show_element ($element, $start, $end, $counter); break;
											case 'xml' : show_xml_element ($element, $start, $end, $counter); break;
											case 'txt' : show_txt_element ($element, $start, $end, $counter); break;
										}
									}
								}
							}
						}
						fclose($handle);
						return 1;
					}
					else return 0;
				}
				else {
					$cache_dir = JPATH_COMPONENT.DS.'cache'.DS;
					$file = $id.'_'.$maptype.'_'.$gid.'_'.$mylang.'_items';
					if (file_exists($cache_dir.$id.DS.$file)) { 
						$handle = fopen ($cache_dir.$id.DS.$file,'r');
						$line = fgets($handle);
						$line = explode ("=",$line);
						$mainframe->elements_count = $mainframe->elements_count+intval($line[1]);
						return 1;
					} else return 0;
				}
				break;

			case _SEF_SM_DATABASE :
				if ($mode==1 || $mainframe->maptype=='txt') {
					$db = JFactory::getDBO();
					$query = "select id from #__sef_sm_cache where menu_id='$id' and maptype='$maptype' and lang = '$mylang' and gid='$gid'";
					$db->setQuery($query);
					$cache_id = $db->loadResult();
					if ($cache_id) {
						$query = "SELECT * from #__sef_sm_cache_items where id='$cache_id' order by ordering";
						$db->setQuery($query);
						$elements = $db->loadObjectList();
						if ($elements) {
							foreach ($elements as $element) {
								if ($mode==0) AddExtMenuItem($element); else {
									$counter++;
									switch ($mainframe->maptype) {
										case 'html' : show_element ($element, $start, $end, $counter); break;
										case 'xml' : show_xml_element ($element, $start, $end, $counter); break;
										case 'txt' : show_txt_element ($element, $start, $end, $counter); break;
									}
								}
							}
						} return 1;
					} else	return 0;
				}
				else {
					$db = JFactory::getDBO();
					$query = "select items from #__sef_sm_cache where menu_id='$id' and maptype='$maptype' and lang = '$mylang' and gid='$gid'";
					$db->setQuery($query);
					$mainframe->elements_count = $mainframe->elements_count+intval($db->loadResult());
				}
				break;
		}
	}

	function cache_clear_Check() {

		global $mainframe;
		$db =& JFactory::getDBO();

		$cache_type = $mainframe->ParamsArray->get( 'cache_type','0');

		$db->setQuery("select id from #__menu where published>='0'");
		$menulist = $db->loadObjectList();
  
		$menu ='';
		if ($menulist)
		foreach ($menulist as $menul) {
			$a = $menul->id;
			$menu->$a = $a;
		}
		
		//Clear Filesystem Cache

		if ($cache_type==_SEF_SM_FILESYSTEM) {
			$path_base = JPATH_SITE.DS.'components'.DS.'com_sefservicemap'.DS.'cache';
			if (@$dir=opendir($path_base)) {
				while ($file=readdir($dir)) {
					if ($file!='.' && $file!='..') {
						if (!isset ($menu->$file)) $this->DeleteDirectory($path_base.$file);
					}
				}  
				closedir($dir);
			}
		}
              else {
			$path_base = JPATH_SITE.DS.'components'.DS.'com_sefservicemap'.DS.'cache';
			$this->DeleteDirectory($path_base);
			@mkdir($path_base);
		}
		//Clear Database Cache
		if ($cache_type==_SEF_SM_DATABASE) {
			$query = "select id,menu_id from #__sef_sm_cache";
			$db->setQuery($query);
			$cache_menus = $db->loadObjectList();
			if ($cache_menus)
			foreach ($cache_menus as $menu) {
				$id = $menu->menu_id;
				if (!isset ($menu->$id)) {
					$query = "DELETE * from #__sef_sm_cache_items where id='$menu->id'";
					$db->setQuery($query);
					$db->query();

					$query = "DELETE * from #__sef_sm_cache where id='$menu->id'";
					$db->setQuery($query);
					$db->query();
				}
			}
		} else
		{
			$query = "DELETE from #__sef_sm_cache_items";
			$db->setQuery($query);
			$db->query();

			$query = "UPDATE #__sef_sm_cache set items='-1'";
			$db->setQuery($query);
			$db->query();

			$query = "select id,menu_id from #__sef_sm_cache";
			$db->setQuery($query);
			$cache_menus = $db->loadObjectList();

			if ($cache_menus)
			foreach ($cache_menus as $menu) {
				$id = $menu->menu_id;
				if (!isset ($menu->$id)) {
					$query = "DELETE * from #__sef_sm_cache where id='$menu->id'";
					$db->setQuery($query);
					$db->query();
				}
			}
		}
	}

	function DeleteDirectory($dirname,$only_empty=false) {
		if (!is_dir($dirname)) return false;

		$dscan = array(realpath($dirname));
		$darr = array();
		while (!empty($dscan)) {
			$dcur = array_pop($dscan);
			$darr[] = $dcur;
			if ($d=opendir($dcur)) {
				while ($f=readdir($d)) {
					if ($f=='.' || $f=='..')
					continue;
					$f=$dcur.DS.$f;
					if (is_dir($f))
						$dscan[] = $f;
					else
						unlink($f);
				}
				closedir($d);
			}
		}
		$i_until = ($only_empty)? 1 : 0;
		for ($i=count($darr)-1; $i>=$i_until; $i--) {
			@rmdir($darr[$i]);
		}
	}

	function smGetComponentParams() {
		global $mainframe;
		$db =& JFactory::getDBO();

		$file = JPATH_COMPONENT.DS.'config'.DS.'sefsmconfig.xml';

		$query = "SELECT * from #__sef_sm_settings";
		$db->setQuery($query);
		$pars = $db->loadObjectList();

		$db->setQuery("select * from #__plugins where element='sefservicemap' and folder='system'");
		$plugin = $db->loadObject();

		if ($plugin->published==1) $mainframe->sm_ping_enabled = 1; else $mainframe->sm_ping_enabled=0;

		if ($pars)
		foreach ( $pars as $k) {
			if ($k->variable != 'ping_enabled') {
				$txt[] = "$k->variable=$k->value";               
			}
       	}


		$txt[] = "ping_enabled=".$plugin->published;
		$db = JFactory::getDBO();
		$query= "select * from #__sef_sm_pingback";
		$db->setQuery($query);
		$pingback = $db->loadObjectList();
		$pings = '';
		if ($pingback) { 
			foreach ($pingback as $ping) {
				$pings.=$ping->ping_host."[next]";
			}
			$txt[]="ping_host_list=".$pings;
		}

		if (count($pars)!=0) $params = implode( "\n", $txt ); else $params = '';

		$params = new JParameter( $params, $file, 'component' );
		$mainframe->ParamsArray=$params;       
	}

	function menutypes() {
		$db =& JFactory::getDBO();
		$db->setQuery("select * from #__menu_types");
		$menus = $db->loadObjectList();
		$menutypes ='';
		if ($menus)
		foreach ($menus as $menu) {
			$menuTypes[] = $menu->menutype;
		}
		return $menuTypes;
	}

	function smSynchronize() {
		$db =& JFactory::getDBO();
		$menus = $this->menutypes();
 
		$db->setQuery("select name from #__sef_sm_menus");
		$menulist = $db->loadObjectList();
		$menuarray=array();
		if ($menulist)
		foreach($menulist as $menu)
			$menuarray[]=$menu->name;
		if ($menus)
		foreach ($menus as $mainmenu) {
			if (!in_array($mainmenu,$menuarray)) { 
				$db->setQuery("select max(ordering) from #__sef_sm_menus");
				$max=$db->loadResult()+1;
				$db->setQuery("INSERT INTO #__sef_sm_menus SET name='$mainmenu', ordering='$max'");
				$db->query();
			}
		}

		if ($menulist)
		foreach ($menulist as $menu) {
			if (!in_array($menu->name,$menus)) {
				$db->setQuery("delete from #__sef_sm_menus where name = '$menu->name'");
				$db->query();
			} 
		}

		$db->setQuery("select * from #__menu");
		$menulist=$db->loadObjectList();
		$menuarray=array();
		if ($menulist)
		foreach ($menulist as $menu)
			$menuarray[]=$menu->id;

		$db->setQuery("select * from #__sef_sm_menu");
		$smlist=$db->loadObjectList();

		$smarray=array();
		if ($smlist)
		foreach ($smlist as $menu)
			$smarray[]=$menu->menu_id;

		if ($menulist)
		foreach ($menulist as $menu) {
			if (!in_array($menu->id,$smarray)) {
				$query = "insert into #__sef_sm_menu set menu_id='$menu->id' ";
				//if (eregi ('option=com_content', $menu->link)) 
				$query .=", ping_enabled='1'";
				$db->setQuery($query);
				$db->query();
			}
		}

		if ($smlist)
		foreach ($smlist as $menu)
		{
			if (!in_array($menu->menu_id,$menuarray))
			{
				$db->setQuery("delete from #__sef_sm_menu where menu_id='$menu->menu_id'");
				$db->query();
			}
		}

		$db->setQuery("select * from #__sef_sm_pingback_menu");
		$pgmenu=$db->loadObjectList();

		$db->setQuery("select id from #__sef_sm_pingback");
		$pingback=$db->loadObjectList();

		$pingbackarray=array();
		if ($pingback)

		$pgarray = array();
		$pingarray = array();
		$pingmenuarray = array();
		if ($pgmenu)
		foreach ($pgmenu as $ping) {
				if (!in_array($ping->menu_id,$pgarray)) $pgarray[] = $ping->menu_id;
				if (!in_array($ping->pingback_id,$pingarray)) $pingarray[] = $ping->pingback_id;
				$pingmenuarray[$ping->menu_id.'_'.$ping->pingback_id]=1;
		}

		$pingsarray = array();
		if ($pingback)
		foreach ($pingback as $ping) {
			if (!in_array($ping->id,$pingsarray)) $pingsarray[] = $ping->id;

			if ($menulist)
			foreach ($menulist as $menu) {
				if (!isset($pingmenuarray[$menu->id.'_'.$ping->id])) {
					$query = "insert into #__sef_sm_pingback_menu set menu_id='$menu->id', pingback_id='$ping->id'";
					$db->setQuery($query);
					$db->query();
				}
			}
		}
		
		if ($pgarray)
		foreach ($pgarray as $pmenu) {
			if (!in_array($pmenu,$menuarray)) {
				$query = "delete from #__sef_sm_pingback_menu where menu_id='$pmenu'";
				$db->setQuery($query);
				$db->query();				
			}
		}
		
		if ($pingarray)
		foreach ($pingarray as $pmenu) {
			if (!in_array($pmenu,$pingsarray)) {
				$query = "delete from #__sef_sm_pingback_menu where pingback_id='$pmenu'";
				$db->setQuery($query);
				$db->query();				
			}
		}

		$this->get_menu();
	}

	function GetLink() {
		$basepath = JPATH_SITE.DS.'plugins'.DS;

		$path = $basepath.'com_sefservicemap'.DS;
		@mkdir($path); 

		if (file_exists($path.'sefservicemap.log')) { 
			include $path.'sefservicemap.log';
			return $link;
		}
		else {
			ob_start();
			$links= '';

			$links[]='bilety lotnicze;1;http://eturystyka.org/component/option,com_fly/Itemid,112/';
			$links[]='tanie bilety lotnicze;1;http://eturystyka.org/component/option,com_fly/Itemid,112/';
			$links[]='tanie linie lotnicze;1;http://eturystyka.org/component/option,com_fly/Itemid,112/';
			$links[]='linie lotnicze;1;http://eturystyka.org/component/option,com_fly/Itemid,112/';
			$links[]='tanie latanie;1;http://eturystyka.org/component/option,com_fly/Itemid,112/';
			$links[]='last minute;1;http://eturystyka.org';

			$links[]='bilety lotnicze;1;http://portal.lotniczy.eu';
			$links[]='tanie bilety lotnicze;1;http://portal.lotniczy.eu';
			$links[]='tanie linie lotnicze;1;http://portal.lotniczy.eu';
			$links[]='linie lotnicze;1;http://portal.lotniczy.eu';
			$links[]='tanie latanie;1;http://portal.lotniczy.eu';

			$links[]='praca;1;http://itpraca.pl';

			$tabs = array();
			$table = array();
			$counter=0;
			if ($links)
			foreach ($links as $link)
			{
				$tabs = explode(';',$link);
				$tab='';
				$tab ->anchor = $tabs[0];
				$tab->link = $tabs[2];
				for ($i=0; $i<$tabs[1]; $i++) {
					$table[]=$tab;
					$counter++;
				}
			}
			$rand = rand(0,$counter-1);
			$lnk = $table[$rand];
			$link = '<a href="'.$lnk->link.'" target="_blank" title="'.$lnk->anchor.'">'.$lnk->anchor.'</a>';
			$txt= "<?php\n";
			$txt.="$"."link = '".$link."';\n";
			$txt.="?>";
			$file = fopen($path.'sefservicemap.log',"w+");
			fwrite($file,$txt);
			fclose($file);
			ob_end_clean();
			return $link;
		}
	}
	function integrator_call($integrator,$menuitem,$level,$use_cache=1) {
		global $mainframe;
		$mainframe->sm_menuitem_id = $menuitem->id; 
		$mainframe->sm_ping_enabled = $menuitem->ping_enabled;
		$user = &JFactory::getUser();
		$aid = (int) $user->get('aid', 0);
		switch ($mainframe->maptype) {
			case 'txt':
			case 'xml':
				$view='xml';
				break;
			default: $view = 'html';
		}
		switch ($integrator->type) {
			case 'sef_sm_plugin' : 
				$component = str_replace ('_bot','',$integrator->element);
				$Itid=smGetParam($menuitem->link,'Itemid',$menuitem->id);
				if (file_exists(JPATH_SITE.DS."plugins".DS."com_sefservicemap".DS.$integrator->element.".php")) {
					include_once JPATH_SITE.DS."plugins".DS."com_sefservicemap".DS.$integrator->element.".php";


					$func=$component.'_bot'; 
					if (function_exists($func)) {
						if ($use_cache==1) {
 							if ($this->cache_start($menuitem->id,$integrator)==1) {
								if ($aid>=$integrator->access) {
									$pars = new JParameter( $integrator->params);
									$func($level,$menuitem->link,$Itid,$pars); 
								}
								$this->cache_stop($menuitem->id);
							} 
							else { 
								$count = 0; 
								$this->get_from_cache($menuitem->id,0,-1,-1,$count);
							}
						} 
						else {
							if ($aid>=$integrator->access) {
								$pars = new JParameter( $integrator->params);
								$func($level,$menuitem->link,$Itid,$pars); 
							}
						}
					}
				}
				break;
	
			case 'xmap' : 
				if ($integrator->class!='') {
					if (file_exists(JPATH_SITE.DS."components".DS."com_sefservicemap".DS."plugins".DS."xmap".DS.$integrator->element.".php")) {
						include_once JPATH_SITE.DS."components".DS."com_sefservicemap".DS."plugins".DS."xmap".DS.$integrator->element.".php";

						if ($use_cache==1) {
							if ($this->cache_start($menuitem->id,$integrator)==1) { 
								
								if ($aid>=$integrator->access) {
									$xmap = new GenericPlugins('xmap');
									$plugin = new $integrator->class;

									$params = new JParameter( $integrator->params);
									$show_items=$params->get( 'show_items', 1 ) ; 
									$list = array();
									$menuitem = $xmap->addDefaults($menuitem);
									switch ($view) {
										case 'html': if ($show_items==1) $list = $plugin->getTree( $xmap, $menuitem);
										break;
										case 'xml': if ($show_items>=1) $list = $plugin->getTree( $xmap, $menuitem);
										break;
									}
									$xmap->RenderGeneric($list,$level+1,$params);
									unset ($xmap);
								}
								$this->cache_stop($menuitem->id);
							}
							else {
								$count = 0;
								$this->get_from_cache($menuitem->id,0,-1,-1,$count);
							}

						}
						else {

							if ($aid>=$integrator->access) {
								$xmap = new GenericPlugins('xmap');
								$plugin = new $integrator->class;

								$params = new JParameter( $integrator->params);
								$show_items=$params->get( 'show_items', 1 ) ; 
								$list = array();
								$menuitem = $xmap->addDefaults($menuitem);
								switch ($view) {
									case 'html': if ($show_items==1) $list = $plugin->getTree( $xmap, $menuitem);
									break;
									case 'xml': if ($show_items>=1) $list = $plugin->getTree( $xmap, $menuitem);
									break;
								}
								$xmap->RenderGeneric($list,$level+1,$params);
								unset ($xmap);
							}
						}
					}
				} 
				else {
					// new type of Xmap plugin

					$component = $integrator->element;
					$Itid=smGetParam($menuitem->link,'Itemid',$menuitem->id);
					if (file_exists(JPATH_COMPONENT.DS."plugins".DS."xmap".DS.$integrator->element.".php")) {
						include_once JPATH_COMPONENT.DS."plugins".DS."xmap".DS.$integrator->element.".php";

						if ($use_cache==1) { 
							if ($this->cache_start($menuitem->id,$integrator)==1) { 

								if ($aid>=$integrator->access) {
									$xmap = new GenericPlugins('xmap');
									$xmap->level = $level;
									$xmap->Itemid=smGetParam($menuitem->link,'Itemid',$menuitem->id);
									$class_name = 'xmap_'.$integrator->element;
									$plugin = new $class_name;
									$menuitem = $xmap->addDefaults($menuitem);
									$list = $plugin->getTree( $xmap, $menuitem, $xmap->paramsToArray($integrator->params));
									unset ($xmap);
								}
								$this->cache_stop($menuitem->id);
							} 
							else {
								$count = 0;
								$this->get_from_cache($menuitem->id,0,-1,-1,$count);
							}
						} 
						else {

							if ($aid>=$integrator->access) {
								$xmap = new GenericPlugins('xmap');
								$xmap->level = $level;
								$xmap->Itemid=smGetParam($menuitem->link,'Itemid',$menuitem->id);
								$class_name = 'xmap_'.$integrator->element;
								$plugin = new $class_name;
								$menuitem = $xmap->addDefaults($menuitem);
								if ($aid>=$integrator->access) $list = $plugin->getTree( $xmap, $menuitem, $xmap->paramsToArray($integrator->params));
								unset ($xmap);
							}
						}
					}

				}
				break;

			case 'joomap' : 
				if (file_exists(JPATH_SITE.DS."components".DS."com_sefservicemap".DS."plugins".DS."joomap".DS.$integrator->element.".php")) {
					include_once JPATH_SITE.DS."components".DS."com_sefservicemap".DS."plugins".DS."joomap".DS.$integrator->element.".php";
					if ($use_cache==1) { 
						if ($this->cache_start($menuitem->id,$integrator)==1) { 
							$joomap = new XmapPlugins('joomap');
							$plugin = new $integrator->class;

							$params = new JParameter( $integrator->params);
							$show_items=$params->get( 'show_items', 1 ) ; 
							$list = array();
							switch ($view) {
								case 'html': if ($show_items==1) $list = $plugin->getTree( $joomap, $menuitem);
								break;
								case 'xml': if ($show_items>=1) $list = $plugin->getTree( $joomap, $menuitem);
								break;
							}

							$joomap->RenderGeneric($list,$level+1,$params);
							unset ($joomap);
							$this->cache_stop($menuitem->id);
						}
						else {
							$count = 0;
							$this->get_from_cache($menuitem->id,0,-1,-1,$count);
						}
					}
					else {
						$joomap = new JoomapPlugins('joomap');
						$plugin = new $integrator->class;

						$params = new JParameter( $integrator->params);
						$show_items=$params->get( 'show_items', 1 ) ; 
						$list = array();
						switch ($view) {
							case 'html': if ($show_items==1) $list = $plugin->getTree( $joomap, $menuitem);
							break;
							case 'xml': if ($show_items>=1) $list = $plugin->getTree( $joomap, $menuitem);
							break;
						}

						$joomap->RenderGeneric($list,$level+1,$params);
						unset ($joomap);
					}
				}

		}
	}

}
?>