<?php
//*******************************************************
//* SEF Service Map Component
//* http://www.sefservicemap.com
//* (C) Radoslaw Kubera
//* license http://www.gnu.org/copyleft/gpl.html GNU/GPL
//*******************************************************

/** ensure this file is being included by a parent file */
defined( '_JEXEC' ) or die( 'Restricted access' );

$adminPath = JPATH_SITE.DS.'administrator'.DS;
 
include_once (JPATH_SITE.DS.'components'.DS.'com_sefservicemap'.DS.'version.php');
include_once (JPATH_SITE.DS.'administrator'.DS.'components'.DS.'com_sefservicemap'.DS.'include'.DS.'sefservicemap.util.php');


function checkMethod () {
	if (function_exists('fsockopen')) {
		return 'fsc';
	} elseif (function_exists('curl_init')) { 
		return 'crl'; 
	} else return 0;
}

function ping_map($address) {
	$url=parse_url($address);
	$site_url=$url['path'];
	if ($url['query']) $site_url.='?'.$url['query'];
	$timeout = 3;
	$connection_type=checkMethod();
	$return = '';
	switch ($connection_type){
		case 'crl':
			$curl_conn = @curl_init();
			@curl_setopt($curl_conn, CURLOPT_URL, $url['host'].$site_url);
			@curl_setopt($curl_conn, CURLOPT_RETURNTRANSFER, 1);
			@curl_setopt($curl_conn, CURLOPT_TIMEOUT, $timeout);
			@curl_setopt($curl_conn, CURLOPT_CONNECTTIMEOUT, $timeout);
			$return = @curl_exec ($curl_conn);
			@curl_close ($curl_conn);
			break;
		case 'fsc':
			$request = "GET $site_url HTTP/1.1\r\n";
			$request .= "Host: ".$url['host']."\r\n";
			$request .= "Connection: Close\r\n\r\n";
			$data = @fsockopen($url['host'], 80, $er1, $er2, $timeout);
			if ($data){
				@fwrite($data, $request);
				while (!@feof($data)){
					$return .= @fgets($data, 8192);
				}
				@fclose($data);
			}
			break;
	}
	if ($connection_type) {
		echo $address."<br/>";
	}
	return;
}
function ping_maps() {
	$map = urlencode(SEFSMRoute::_('index.php?option=com_sefservicemap&amp;task=xmlmapindex&amp;no_html=1'));

	$googleping = 'http://www.google.com/webmasters/tools/ping?sitemap='.$map;

	ping_map($googleping);

	$liveping = 'http://webmaster.live.com/ping.aspx??sitemap='.$map;

	ping_map($liveping);

	$yahooping = 'http://search.yahooapis.com/SiteExplorerService/V1/ping?sitemap='.$map;

	ping_map($yahooping);

}


function update_robots_txt () {
	$robots = array();
	$robots = file (JPATH_SITE.DS.'robots.txt');

	$xmlmap = 'http://'.$_SERVER['SERVER_NAME'].'/index.php?option=com_sefservicemap&amp;task=xmlmapindex&amp;no_html=1';
	$is_ok = array();

	if ($robots) {
		$is_ok = 0;
		foreach ($robots as $line) {
			if (eregi('com_sefservicemap',$line)) {
				$is_ok = 1;
			}
		}

	}

	if ($is_ok==0) {
		$robots[] = 'Sitemap: '.$xmlmap;

		$new_robots = $robots;

		$file = JPATH_SITE.DS.'robots.txt';

		if (@$rob=fopen($file,'w+')) {
			foreach ($new_robots as $line) {
				fwrite($rob, trim($line)."\n");
			}
			fclose ($rob);
		} 
		else {
			JError::raiseWarning(-1, JText::_("Cannot change robots.txt. Please change robots.txt manualy."));
		}
	}
}

function copyIntegratorFiles($src, $dest) {
	// Initialize variables
	jimport('joomla.client.helper');
	$FTPOptions = JClientHelper::getCredentials('ftp');
		
	// Eliminate trailing directory separators, if any
	$src = rtrim($src, DS); 
	$dest = rtrim($dest, DS);

	if (!JFolder::exists($src)) {
		return JError::raiseWarning(-1, JText::_('Cannot find source folder'));
	}

	// Make sure the destination exists
	if (! JFolder::create($dest)) {
		return JError::raiseWarning(-1, JText::_('Unable to create target folder'));
	}

	if ($FTPOptions['enabled'] == 1)
	{
		// Connect the FTP client
		jimport('joomla.client.ftp');
		$ftp = & JFTP::getInstance($FTPOptions['host'], $FTPOptions['port'], null, $FTPOptions['user'], $FTPOptions['pass']);

			if(! ($dh = @opendir($src))) {
			return JError::raiseWarning(-1, JText::_('Unable to open source folder'));
		}
		// Walk through the directory copying files and recursing into folders.
		while (($file = readdir($dh)) !== false) {
			$sfid = $src . DS . $file;
			$dfid = $dest . DS . $file;
			if (filetype($sfid)=='file') {
				$dfid = JPath::clean(str_replace(JPATH_ROOT, $FTPOptions['root'], $dfid), '/');
				if (! $ftp->store($sfid, $dfid)) {
					return JError::raiseWarning(1, JText::_('Copy failed'));
				}
			}
		}
	} else {
		if(! ($dh = @opendir($src))) {
			return JError::raiseWarning(-1, JText::_('Unable to open source folder'));
		}
		// Walk through the directory copying files and recursing into folders.
		while (($file = readdir($dh)) !== false) {
			$sfid = $src.DS.$file;
			$dfid = $dest.DS.$file;
			if (filetype($sfid)=='file') {
				if (!@ copy($sfid, $dfid)) {
					return JError::raiseWarning(1, JText::_('Copy failed'));
					return $false;
				}
			}
		}
	}
	return true;
}

function create_tables() {
	$db = JFactory::getDBO();
	$db->setQuery("
			CREATE TABLE IF NOT EXISTS `#__sef_sm_cache` (
			  `id` int(11) NOT NULL auto_increment,
			  `menu_id` int(11) NOT NULL,
			  `maptype` varchar(30) NOT NULL,
			  `lang` varchar(30) NOT NULL,
			  `gid` int(11) NOT NULL,
			  `lastmod` datetime NOT NULL,
			  `items` int(11) NOT NULL,
			  PRIMARY KEY  (`id`),
			  KEY `maptype` (`maptype`),
			  KEY `lang` (`lang`),
			  KEY `lastmod` (`lastmod`),
			  KEY `menu_id` (`menu_id`),
			  KEY `gid` (`gid`)
			) ENGINE=MyISAM;
	");
	$db->query();
	$db->setQuery("
			CREATE TABLE IF NOT EXISTS `#__sef_sm_cache_items` (
			  `id` int(11) NOT NULL,
			  `ordering` int(11) NOT NULL,
			  `name` varchar(100) NOT NULL,
			  `link` varchar(255) NOT NULL,
			  `image` varchar(50) NOT NULL,
			  `icon_path` varchar(100) NOT NULL,
			  `level` int(11) NOT NULL,
			  `lastmod` varchar(20) NOT NULL,
			  `changefreq` varchar(10) NOT NULL,
			  `priority` varchar(3) NOT NULL,
			  `description` text NOT NULL,
			  KEY `name` (`name`),
			  KEY `level` (`level`),
			  KEY `id` (`id`),
			  KEY `ordering` (`ordering`),
			  KEY `lastmod` (`lastmod`)
			) ENGINE=MyISAM;
	");
	$db->query();
	$db->setQuery("
			CREATE TABLE IF NOT EXISTS `#__sef_sm_menu` (
			  `menu_id` int(11) NOT NULL default '0',
			  `published` int(11) NOT NULL default '1',
			  `integrator` int(11) NOT NULL default '1',
			  `integrator_id` varchar(10) NOT NULL,
			  `ordering` int(11) NOT NULL default '0',
			  `params` text NOT NULL,
			  `ping_enabled` int(11) NOT NULL,
			  KEY `menu_id` (`menu_id`),
			  KEY `published` (`published`),
			  KEY `integrator` (`integrator`),
			  KEY `ordering` (`ordering`)
			) ENGINE=MyISAM;
	");
	$db->query();
	$db->setQuery("
		ALTER TABLE `#__sef_sm_cache_items` CHANGE `changefreq` `changefreq` VARCHAR( 10 ) NOT NULL;
	");
	$db->query();
	$db->setQuery("
		ALTER TABLE `#__sef_sm_menu` ADD `ping_enabled` INT DEFAULT '1' NOT NULL ;
	");
	$db->query();
	$db->setQuery("
		ALTER TABLE `#__sef_sm_menu` ADD `integrator_id` VARCHAR( 10 ) NOT NULL AFTER `integrator` ;
	");
	$db->query();
	$db->setQuery("
			CREATE TABLE IF NOT EXISTS `#__sef_sm_menus` (
			  `id` int(10) NOT NULL auto_increment,
			  `name` varchar(100) NOT NULL default '',
			  `ordering` int(11) NOT NULL default '0',
			  `published` int(11) NOT NULL default '1',
			  `params` text NOT NULL,
			  PRIMARY KEY  (`id`)
			) ENGINE=MyISAM;
	");
	$db->query();
	$db->setQuery("
			CREATE TABLE IF NOT EXISTS `#__sef_sm_pingback` (
			  `id` int(11) NOT NULL auto_increment,
			  `ping_host` varchar(100) NOT NULL,
			  `last_ping_date` datetime NOT NULL,
			  `log_position` int(11) NOT NULL,
			  PRIMARY KEY  (`id`),
			  UNIQUE KEY `ping_host` (`ping_host`),
			  KEY `last_ping_date` (`last_ping_date`)
			) ENGINE=MyISAM;
	");
	$db->query();
	$db->setQuery("
			INSERT INTO `#__sef_sm_pingback` VALUES (1, 'http://rpc.pingomatic.com/', '0000-00-00 00:00:00', 0);
	");
	$db->query();
	$db->setQuery("
			CREATE TABLE IF NOT EXISTS `#__sef_sm_pingback_log` (
			  `id` int(11) NOT NULL,
			  `pingback_id` int(11) NOT NULL,
			  `ping_date` datetime NOT NULL,
			  `name` varchar(100) NOT NULL,
			  `url` varchar(255) NOT NULL,
			  `flerror` varchar(10) NOT NULL,
			  `message` varchar(100) NOT NULL,
			  `session_log` text NOT NULL,
			  PRIMARY KEY  (`id`,`pingback_id`),
			  KEY `date` (`ping_date`)
			) ENGINE=MyISAM;
	");
	$db->query();
	$db->setQuery("
			CREATE TABLE IF NOT EXISTS `#__sef_sm_pingback_menu` (
			  `menu_id` int(11) NOT NULL,
			  `pingback_id` int(11) NOT NULL,
			  `last_date` datetime NOT NULL,
			  KEY `menu_id` (`menu_id`),
			  KEY `last_date` (`last_date`),
			  KEY `pingback_id` (`pingback_id`)
			) ENGINE=MyISAM;
	");
	$db->query();
	$db->setQuery("
			CREATE TABLE IF NOT EXISTS `#__sef_sm_pingback_stack` (
			  `id` int(11) NOT NULL,
			  `menu_id` int(11) NOT NULL,
			  `name` varchar(100) NOT NULL,
			  `url` varchar(255) NOT NULL,
			  `url_md5` varchar(20) NOT NULL,
			  `modified` datetime NOT NULL,
			  `added` datetime NOT NULL,
			  PRIMARY KEY  (`id`,`url_md5`),
			  KEY `modified` (`modified`)
			) ENGINE=MyISAM;
	");
	$db->query();
	$db->setQuery("
			CREATE TABLE IF NOT EXISTS `#__sef_sm_plugins` (
			  `id` int(11) NOT NULL auto_increment,
			  `name` varchar(100) NOT NULL default '',
			  `element` varchar(100) NOT NULL,
			  `class` varchar(100) NOT NULL,
			  `type` varchar(100) NOT NULL,
			  `access` tinyint(3) unsigned NOT NULL default '0',
			  `published` tinyint(3) NOT NULL default '0',
			  `params` text NOT NULL,
			  PRIMARY KEY  (`id`),
			  KEY `idx_folder` (`published`,`access`,`type`)
			) ENGINE=MyISAM;
	");
	$db->query();
	$db->setQuery("
			CREATE TABLE IF NOT EXISTS `#__sef_sm_settings` (
			  `variable` varchar(100) NOT NULL default '',
			  `value` tinytext,
			  `description` tinytext,
			  PRIMARY KEY  (`variable`)
			) ENGINE=MyISAM DEFAULT CHARSET=utf8;
	");
	$db->query();
	$db->setQuery("INSERT INTO `#__sef_sm_settings` VALUES ('menu_titles', '1', NULL) ");
	$db->query();
	$db->setQuery("INSERT INTO `#__sef_sm_settings` VALUES ('show_icons', '0', NULL) ");
	$db->query();
	$db->setQuery("INSERT INTO `#__sef_sm_settings` VALUES ('show_module_icons', '1', NULL) ");
	$db->query();
	$db->setQuery("INSERT INTO `#__sef_sm_settings` VALUES ('show_menu_icons', '0', NULL); ");
	$db->query();
	$db->setQuery("INSERT INTO `#__sef_sm_settings` VALUES ('icon_path', '', NULL) ");
	$db->query();
	$db->setQuery("INSERT INTO `#__sef_sm_settings` VALUES ('show_desc', '0', NULL) ");
	$db->query();
	$db->setQuery("INSERT INTO `#__sef_sm_settings` VALUES ('max_length', '0', NULL) ");
	$db->query();
	$db->setQuery("INSERT INTO `#__sef_sm_settings` VALUES ('max_desc_length', '50', NULL) ");
	$db->query();
	$db->setQuery("INSERT INTO `#__sef_sm_settings` VALUES ('sublevels', '0', NULL) ");
	$db->query();
	$db->setQuery("INSERT INTO `#__sef_sm_settings` VALUES ('keep_settings', '1', NULL) ");
	$db->query();
	$db->setQuery("INSERT INTO `#__sef_sm_settings` VALUES ('ping_enabled', '1', NULL) ");
	$db->query();
	$db->setQuery("INSERT INTO `#__sef_sm_settings` VALUES ('auto_ping', '1', NULL) ");
	$db->query();
	$db->setQuery("INSERT INTO `#__sef_sm_settings` VALUES ('ping_interval', '5', NULL) ");
	$db->query();
	$db->setQuery("INSERT INTO `#__sef_sm_settings` VALUES ('bot_visit', '1', NULL) ");
	$db->query();
	$db->setQuery("INSERT INTO `#__sef_sm_settings` VALUES ('cache_type', '1', NULL) ");
	$db->query();
	$db->setQuery("INSERT INTO `#__sef_sm_settings` VALUES ('level0', 'level0', NULL) 	");
	$db->query();
	$db->setQuery("INSERT INTO `#__sef_sm_settings` VALUES ('level1', 'level1', NULL) 	");
	$db->query();
	$db->setQuery("INSERT INTO `#__sef_sm_settings` VALUES ('level2', 'level2', NULL) 	");
	$db->query();
	$db->setQuery("INSERT INTO `#__sef_sm_settings` VALUES ('level3', 'level3', NULL) 	");
	$db->query();
	$db->setQuery("INSERT INTO `#__sef_sm_settings` VALUES ('level4', 'level4', NULL) 	");
	$db->query();
	$db->setQuery("INSERT INTO `#__sef_sm_settings` VALUES ('level5', 'level5', NULL) 	");
	$db->query();
	$db->setQuery("INSERT INTO `#__sef_sm_settings` VALUES ('level6', 'level6', NULL) 	");
	$db->query();
	$db->setQuery("INSERT INTO `#__sef_sm_settings` VALUES ('level7', 'level7', NULL) 	");
	$db->query();
	$db->setQuery("INSERT INTO `#__sef_sm_settings` VALUES ('level8', 'level8', NULL) 	");
	$db->query();
	$db->setQuery("INSERT INTO `#__sef_sm_settings` VALUES ('level9', 'level9', NULL) 	");
	$db->query();
	$db->setQuery("INSERT INTO `#__sef_sm_settings` VALUES ('levelx', 'levelx', NULL) 	");
	$db->query();
	$db->setQuery("INSERT INTO `#__sef_sm_settings` VALUES ('desc', 'description', NULL) ");
	$db->query();
	$db->setQuery("INSERT INTO `#__sef_sm_settings` VALUES ('items_pagination', '40', NULL) ");
	$db->query();
	$db->setQuery("INSERT INTO `#__sef_sm_settings` VALUES ('ping_timeout', '1', NULL) ");
	$db->query();
	$db->setQuery("INSERT INTO `#__sef_sm_settings` VALUES ('priority', '0.5', NULL) ");
	$db->query();
	$db->setQuery("INSERT INTO `#__sef_sm_settings` VALUES ('changefreq', 'weekly', NULL) ");
	$db->query();
	$db->setQuery("INSERT INTO `#__sef_sm_settings` VALUES ('columns', '2', NULL) ");
	$db->query();
}

function install_plugins() {
	$src = JPATH_SITE.DS.'administrator'.DS.'components'.DS.'com_sefservicemap'.DS.'plugins'.DS.'com_sefservicemap';
	$dst = JPATH_SITE.DS.'plugins'.DS.'com_sefservicemap';

	copyIntegratorFiles($src,$dst);

	$db = JFactory::getDBO();

	$db->setQuery("select max(ordering) from #__plugins where folder='com_sefservicemap'");
	$max = $db->loadResult()+1;

	$delquery= "DELETE FROM #__plugins where (element='com_weblinks_bot' AND folder='com_sefservicemap') "
			." OR (element='com_content_bot' AND folder='com_sefservicemap') "
			." OR (element='com_newsfeeds_bot' AND folder='com_sefservicemap') "
			." OR (element='com_contact_bot' AND folder='com_sefservicemap') "
			." OR (element='sefservicemap' AND folder='system') ";

	$db->setQuery($delquery);
	$db->query();
		
	$addquery = "INSERT INTO #__plugins SET name='SEF SM Content Integrator for Joomla 1.5.x', element='com_content_bot', folder='com_sefservicemap', ordering='".$max."', published='1'";
	$max++;

	$db->setQuery($addquery);
	$db->query();

	$addquery = "INSERT INTO #__plugins SET name='SEF SM Newsfeeds Integrator for Joomla 1.5.x', element='com_newsfeeds_bot', folder='com_sefservicemap', ordering='".$max."', published='1'";
	$max++;

	$db->setQuery($addquery);
	$db->query();

	$addquery = "INSERT INTO #__plugins SET name='SEF SM Contacts Integrator for Joomla 1.5.x', element='com_contact_bot', folder='com_sefservicemap', ordering='".$max."', published='1'";
	$max++;

	$db->setQuery($addquery);
	$db->query();

	$addquery = "INSERT INTO #__plugins SET name='SEF SM Weblinks Integrator for Joomla 1.5.x', element='com_weblinks_bot', folder='com_sefservicemap', ordering='".$max."', published='1'";
	$max++;

	$db->setQuery($addquery);
	$db->query();

	$addquery = "INSERT INTO #__plugins SET name='System - SEF Service Map PingBack plugin', element='sefservicemap', folder='system', ordering='".$max."', published='1'";
	$max++;

	$db->setQuery($addquery);
	$db->query();

	$src = JPATH_SITE.DS.'administrator'.DS.'components'.DS.'com_sefservicemap'.DS.'plugins'.DS.'system';
	$dst = JPATH_SITE.DS.'plugins'.DS.'system';
	copyIntegratorFiles($src,$dst);

}

function com_install() {
	echo JText::_('Creating tables').'...........<br/>';
	create_tables();
	echo JText::_('Installing plugins').'...........<br/>';
	install_plugins();
	echo JText::_('Updating robots.txt').'...........<br/>';	
	update_robots_txt ();
	echo JText::_('Adding xml map to Google, Yahoo and Live.com').'...........<br/>';	
	ping_maps();
	$adminPath = JPATH_SITE.DS.'administrator'.DS;
	$lang =& JFactory::getLanguage();
	$mylang = $lang->_lang;

?>
  <center>
  <table width="100%" border="0">
    <tr>
      <td>
        <strong>SEF Service Map Component v.<?php echo _SEF_SM_VERSION ?></strong><br/>
        <font class="small">by Radoslaw Kubera.</font>
      </td>
    </tr>
    <tr>
      <td>
      <?php

        if (file_exists($adminPath."components".DS."com_sefservicemap".DS."config".DS.$mylang.".help.php")) 
        { 
          $f = file ($adminPath."components".DS."com_sefservicemap".DS."config".DS.$mylang.".help.php");
        } 
        else 
        {
          $f = file ($adminPath."components".DS."com_sefservicemap".DS."config".DS."help.php");
        } 
        $txt = implode ("<br/>",$f); 
        echo $txt;
      ?>
      </td>
    </tr>
    <tr> 
      <td>
        <font color="green"><b>Installation finished.</b></font>
      </td>
    </tr>
   </table>
   </center>
<?php
  global $mosConfig_absolute_path;
  $path = JPATH_SITE.DS.'components'.DS.'com_sefservicemap'.DS;
  @mkdir ($path.'cache/');
}

?>