<?php
//*******************************************************
//* SEF Service Map Component
//* http://www.sefservicemap.com
//* (C) Radoslaw Kubera
//* license http://www.gnu.org/copyleft/gpl.html GNU/GPL
//*******************************************************


/** ensure this file is being included by a parent file */
defined( '_JEXEC' ) or die( 'Restricted access' );

class TOOLBAR_sefservicemap {
	function MENU() {
		$task = JREQUEST::GetVar('task','menu');


		switch ($task) {
			case 'cancelcfg' :
			case 'save' :
			case 'config_all' : 
				JToolBarHelper::title( 'SEF Service Map - '.JText::_( 'Configuration' ), 'sections.png' );
				JToolBarHelper::save( 'save', JText::_('Save') );
				break;

			case 'integrator' :
				JToolBarHelper::save( 'saveint', JText::_('Save') );
				JToolBarHelper::cancel( 'cancelint', JText::_('Cancel') );
				break;

			case 'cancelcache' :
			case 'config_cache' :
				JToolBarHelper::title( 'SEF Service Map - '.JText::_( 'Cache' ), 'sections.png' );
				JToolBarHelper::save( 'savecache', JText::_('Save') );
				break;

			case 'config_integrators' :
			case 'uploadintegrator' :
			case 'remove':
				JToolBarHelper::title( 'SEF Service Map - '.JText::_( 'Integrators' ), 'sections.png' );
				JToolBarHelper::publishList('publishintegrators');
				JToolBarHelper::unpublishList('unpublishintegrators');
				JToolBarHelper::deleteList(JText::_('Delete Integrators?'));
				JToolBarHelper::custom( 'uploadintegrator','upload.png','upload_f2.png', JText::_( 'Upload' ), false );
				break;

			case 'menu' :
			case 'list_send_ping_new' :
			case 'list_send_ping_all' :
			case 'enable_ping' :
				JToolBarHelper::title( 'SEF Service Map - '.JText::_( 'Structure' ), 'sections.png' );
				JToolBarHelper::publishList('publishitems');
				JToolBarHelper::unpublishList('unpublishitems');
				JToolBarHelper::custom('list_send_ping_new','send.png','send_f2.png','Ping last modified',true);
				JToolBarHelper::custom('list_send_ping_all','send.png','send_f2.png','Ping all items',true);
				JToolBarHelper::custom('enable_ping','publish.png','publish_f2.png','Enable ping',true);
				JToolBarHelper::custom('disable_ping','unpublish.png','unpublish_f2.png','Disable ping',true);
				JToolBarHelper::custom('list_cache_clear','delete.png','delete_f2.png','Clear cache',true);
				break;

			case 'cancelcss' :
			case 'config_css' :
				JToolBarHelper::title( 'SEF Service Map - '.JText::_( 'CSS Template' ), 'sections.png' );
				JToolBarHelper::save( 'savecss', JText::_('Save') );
				break;

			case 'config_pingback' :
			case 'add_new_host' :
			case 'delete_hosts' :
				JToolBarHelper::title( 'SEF Service Map - '.JText::_( 'PingBack' ), 'sections.png' );
				JToolBarHelper::custom('delete_queue','trash.png','trash_f2.png','Delete queue',true);	
				JToolBarHelper::custom('add_new_host','new.png','new_f2.png','Add new host',false);	
				JToolBarHelper::custom('delete_hosts','delete.png','delete_f2.png','Delete hosts',true);	
				break;

			case 'edit_pingback' :
				JToolBarHelper::custom('delete_from_queue','trash.png','trash_f2.png','Delete from queue',true);	
				break;
		}
		JToolBarHelper::custom('about','help.png','help_f2.png','Help',false);	

	}
}

?>
