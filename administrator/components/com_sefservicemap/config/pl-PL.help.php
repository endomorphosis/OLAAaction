<u>1. Inforamcje ogólne</u>

SEF Service Map 2 jest komponentem, którego zadaniem jest stworzenie kompletnej mapy serwisu. Oto podstawowe możliwości:

- SEF Service map tworzy trzy rodzaje map serwisu: HTTP, XML i TXT;

- SEF Service Map jest jedyną aktywną mapą serwisu. Dzięki wdudowanemu mechanizmowi PingBack (<a href="http://en.wikipedia.org/wiki/Pingback">http://en.wikipedia.org/wiki/Pingback</a>) mapa serwisu potrafi aktywnie zgłaszać (pingować) nowości na stronie do google.com, yahoo.com i innych serwisów. Dzięki temu roboty indeksujące będą odwiedzać Twoją witrynę dużo częściej, a strona potrafi zaindeksować się sama baz dodatkowych zabiegów. Gdy Twoja witryna będzie zaufaną witryną dla wyszukiwarek, Twoje nowości na stronie będą potrafiły znaleźć się w indeksie już po kilku godzinach;

- SEF Service Map poza integracją ze standardowymi komponentami Joomla! (artykuły, nagłówki RSS, linki, kontakty) jako pierwsza umożliwia integrację z innymi komponentami  przy użyciu mechanizmu integratorów, co czyni SEF Service Map komponentem otwartym. Jako jedyna integruje się też z dodatkami do innych map serwisów takich jak Joomap lub Xmap (<a href="http://joomla.vargas.co.cr/">http://joomla.vargas.co.cr/</a>) dzięki czemu może zostać zintegrowana z naprawdę dużą liczbą komponentów;

- SEF Service map jest zgodna z integratorami przystosowanymi do porzednich wersji Joomla 1.0.x. W momencie wyłączenia trybu zgodności (Legacy) omija niezgodne integratory, co pozwala na dalszą poprawną pracę bez komunikatu typu " Direct access is not allowed";

- SEF Service Map pozwala zainstalować nawet dwa integratory dla tego samego komponentu. W strukturze mapy serwisu można wtedy wybierać, którego integratora się chce używać.

- SEF Service Map jako jedyna pozwala na tworzenie mapy dla serwisów które posiadają więcej niż 50 000 podstron. Jest to możliwe dzięki strumieniowemu tworzeniu mapy (zabezpiecza to przed przepełnieniem pamięci) oraz podziałowi generownej mapy XML serwisu na pomniejsze mapy. Do przeglądarek natomiast zostaje zgłoszony indeks tych map, zgodzie ze spracyfikacją <a href="http://sitemaps.org">http://sitemaps.org</a>;

- SEF Service Map w momencie instalacji sam zgłasza mapę serwisu do przeglądarek oraz dodaje odpowiednie wpisy w pliku robots.txt - dzięki temu nie trzeba już w żaden dodatkowy sposób zgłaszać mapy strony do przeglądarek. Mimo to zalecane jest dodanie mapy serwisu XML w Google Webmaster Tools (www.google.com/webmasters/tools/) w celu okresowego monitorowania, czy przebieg pobierania mapy jest prawidłowy.

- SEF Service Map pozwala na paginację mapy serwisu prezentowanej w postaci WWW, czyli podziału na podstrony. Dzięki temu zwiększa się moc linkowania wewnętrznego serwisu. Mapa posiada również możliwość dodawania opisów do każdego wygenerowanego linku. Dzięki tym mechanizmom nie staje się jedynie famą linków (co korzystnie wpływa na przeglądarki), ale także mapą dla ludzi - daje możliwość zojentowania się, co się kryje pod danym linkiem oraz prostej nawigacji po mapie. Dla dużych map podział na podstrony dodatkowo drastycznie zmniejasza zużycie transferu.

- SEF Service Map posiada dwa rodzaje pamięci podręcznej (Cache): opartą o bazę danych lub system plików. Cache oparty o bazę danych zaleca się serwisom, które nie mają możliwości zapisu danych bezpośrednio na dysku (brak uprawnień); Oba rozwiązanie zmniejszają obciążenie serwera jednak w różny sposób. W przypadku pamięci podręcznej bazującej na systemie plików zmniejsza sie obciążenie zapytań do bazy danych, w przypadku pamięci podręcznej opartej o bazę danych - zużycie procesora (przy równoczesnym zmniejszeniu zapytań do bazy).

- SEF Service Map automatycznie wykrywa strony wielojęzykowe i generuje mapy serwisów dla każdego języka odzielnie. Nie wymaga to od Ciebie żadnych dodatkowych czynności.

<u>2. Instalacja</u>
Po zainstalowaniu komponentu powinny równocześnie zainstalować się 4 integratory(pluginy) do standardowych komponentów Joomla:
- Artykuły (com_content_bot);
- Linki (com_weblinks_bot);
- Nagłówki RSS (com_newsfeeds_bot);
- Kontakty (com_contacts_bot);

oraz jeden plugin systemowy:
- sefservicemap - którego zadaniem jest wysyłanie w tle pingów do zdefiniowanych serwisów.

Dodatkowo system automatycznie dodaje wspis o mapie serwisu do pliku robots.txt. W przypadku niepowodzenia, proszę wejść w konfigurację komponentu i postępować zgodnie ze wskazówkami.

Oto gdzie można szukać dodatkowych integratorów do innych komponentów:
- <a href="http://extensions.joomla.org/index.php?option=com_mtree&task=listcats&cat_id=1858&Itemid=35">Joomla Extensions Directory</a>
- <a href="http://www.sefservicemap.com/">SEF Service Map</a>
- <a href="http://joomla.vargas.co.cr/">Xmap</a>

<u>3.Upgrade</u>
Przed dokonaniem upgrade z wersji 1.0.x.  należy na początku poprawnie odinstalować starą wersję. Przed odinstalowaniem należy w konfiguracji komponentu zaznaczyć opcję "Remove settings after uninstall" (Usuń ustawienia po odinstalowaniu). Dopiero wtedy usunąć komponent. Jeżeli komponent został wyinstlowany bez tej opcji - nie szkodzi. Można zainstalować nową wersję, zaznaczyć opcję i komponent odinstalować. W wyniku tego zabiegu wszystkie wcześniejsze ustawienia znikną. Teraz należy ponownie zainstalować komponent. 

<u>4. PingBack</u>
Uwaga. Aby pingowanie działało prawidłowo, serwer WWW musi otwartą obsługę fsocksopen lub curl. 

Pingowanie serwisów zawsze odbywa się w tle, podczas odwiedzin strony. Ponieważ dla niektórych serwisów odpowiedź na ping (od strony pingowanego serwisu) może być wydłużona, może to momentami robić wrażenie dłuższego czasu ładowania się strony. Dlatego system ma możliwość pracy w dwóch trybach:
- pinguje, gdy stronę odwiedza robot indeksujący;
- pinguje przy wszystkich odwiedzinach;
Pingowanie w tle przy odwiedzinach robotów indeksujących poleca się ustawić dla witryn zaindeksowanych już w wyszukiwarkach. Dla nowych, niezaindeksowanych witryn zaleca się ustawienie pingowania dla wszystkich odwiedzin, a po zaindeksowaniu przejście w tryb pingowania tylko przy odwiedzinach robotów indeksujących.

Dodatkowo, aby pingowanie nie obciążało nadmiernie serwera oraz serwisów pingowanych, można ustawić interwał, co ile minut ma być wysyłany następny ping do danego serwisu. Aby obsłużyć dużą ilość pingów, w systemie tworzona jest kolejka komunikatów.

Można ustawić autoamtyczne pingowanie nowości i ostatnio zmodyfikowanych elementów (opcja domyślna i zalecana). Jednak aby ten machanizm działał poprawnie, integrator dla danego komponentu musi zwracać datę ostatniej modyfikacji i jest to związane bezpośrednio z możliwościami integratora. W przypadku wątpliwosci (czy zwraca datę modyfikacji), najlepiej pytać autora integratora. W przypadku standardowo instalowanych integratorów taką możliwość posiada integrator do artykułów (treści - com_content_bot). Pozostałe nie posiadają takiej możliwości, a to ze względu na fakt, że Joomla nie przechowuje daty dodania i modyfikacji elementów typu newsfeeds, kontaktów i linków.


<u>5. Licencja</u>
GNU/GPL

<u>6. Podziękowania</u>
- Jarosław Gawłowski - <a href="http://www.festivalinfo.pl">http://www.festivalinfo.pl</a> (small bug fixes in version 1.x);


<u>7. SEF Service Map 2 Team</u>
Radoslaw Kubera (developer)
Bartosz Kubera (translator)

<u>8. Tłumaczenia</u>
- english translation: Bartosz Kubera
- turkish translation: Denis Dulici