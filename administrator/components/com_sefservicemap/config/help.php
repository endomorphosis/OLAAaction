<u>1. General Information</u>

SEF Service Map 2 is a Joomla 1.5.x component, which aim is to create a complete map of the site. Here are the basic possibilities:

- SEF Service map creates three kinds of site maps: HTTP, XML i TXT;

- SEF Service Map is the only active site map available. Thanks to a built-in PingBack mechanism (<a href="http://en.wikipedia.org/wiki/Pingback">http://en.wikipedia.org/wiki/Pingback</a>), the site map can actively report (ping) new or modified items to google.com, yahoo.com and other services. Thanks to that, indexing robots (spiders) will visit your site much more often and the site can index itself without your further activity. Once your site becomes a trusted site for search engines, your new or modified items will be able to appear in the index after a few hours;

- SEF Service Map is the first site map to integrate not only the standard Joomla! components (content, RSS newsfeeds, weblinks and contacts) but also other components, using an integrator mechanism. This makes SEF Service Map an open component. Moreover, it's the only site map to integrate add-ons to other site maps such as Joomap and Xmap (<a href="http://joomla.vargas.co.cr/">http://joomla.vargas.co.cr/</a>), which allows the integration of a large number of components;

- SEF Service map is compatible with integrators for previous Joomla versions (1.0.x). The moment you turn off the compatibility mode (Legacy), it avoids the use of incompatible integrators, which allows further work without the message "Direct access is not allowed";

- SEF Service Map allows the installation of even two (or more) integrators for the same component. In the site map structure, you will be able to choose which integrator to use;

- SEF Service Map is the only component available to create site maps for services consisting of more than 50.000 pages. It is possible thanks to streaming creation of the site map (which disallows memory overload) and division of the main XML site map into smaller maps. Index of these maps is then reported to search engines, in compatibility with <a href="http://sitemaps.org">http://sitemaps.org</a> specification;

- SEF Service Map in the moment of installation, by itself reports the site map to search engines and adds relevant entries in the robots.txt file - thanks to that, there is no need to additionally report the site map to search engines. However, adding the XML site map to Google Webmaster Tools (<a href="http://www.festivalinfo.pl">http://www.festivalinfo.pl</a>) is still advised in order to enable monitoring whether the site map import process is valid.

- SEF Service Map allows site map pagination presented in WWW form. Thanks to that the power of internal linking is increased. The map allows adding item descriptions to each generated link. Thanks to these mechanisms, it does not become a mere link farm (which positively affects search engine spiders), but also becomes user-friendly - it allows simple map navigation and provides link descriptions. For large maps, the division into smaller maps drastically decreases the use of transfer.

- SEF Service Map has two kinds of butli-in cache: one based on database or file system. Database cache is recommended for services, which disallow write access permission on file system. Both solutions decrease server load in different ways. In case of file system cache, the number of queries to database is decreased and in case of database cache - processor load (simultaneously with the number of queries to database).

- SEF Service Map automatically detects multi-language sites and generates site maps for each language version. It does not require and other actions from the user.

<u>2. Installation</u>
Simultaneously with the installation of the component, 4 integrators (plugins) for standard Joomla! components are installed:
- Content (com_content_bot);
- Links (com_weblinks_bot);
- RSS newsfeeds (com_newsfeeds_bot);
- Contacts (com_contacts_bot);

And one additional system plugin:
- sefservicemap - which aim is to send pings in the background to defined services.

Additionally, the system automatically adds an entry regarding the map to robots.txt file. In case of failure, please enter the component configuration panel and proceed as advised there.
This is where you can find integrators to other components:
- <a href="http://extensions.joomla.org/index.php?option=com_mtree&task=listcats&cat_id=1858&Itemid=35">Joomla Extensions Directory</a>
- <a href="http://www.sefservicemap.com/">SEF Service Map</a>
- <a href="http://joomla.vargas.co.cr/">Xmap</a>


<u>3.Upgrade</u>
If you are using the previous version of SEF Service Map (1.x), before installation of this new component (2.x), you should first correctly uninstall the old version. Remember to check the "Remove settings after uninstall" option before uninstalling.
If you forget to check the above option, do not worry - you can always install the new version, check the "Remove settings after uninstall" option and then uninstall the new component to clear all previous settings. Afterwards, you are ready to correctly install the new SEF Service Map (2.x).
 
<u>4. PingBack</u>
Attention - the PingBack will function properly only if the WWW server has the fsocksopen or curl function open.
Pinging services is always performed in the background during visits. Because in case of some sites the ping responces (from the site being pinged) might be longer, it may increase page load time. That's why the system has two work modes:
- Pinging during the visit of an indexing robot (spider);
- Pinging during all visits;

It is advised to set pinging in the background during visits of indexing bots (spiders) in case of sites already indexed in search engines. For new, not indexed sites, it is advised to set pinging for all visits, and to change the settings to pinging in the background during visits of indexing bots after indexation. 
Additionally, to ensure that the pinging does not overload the server or sites being pinged, you can set a few minutes interval between pings sent to a given site. In order to manage a large number of pings, the system creates messages queue.
It is possible to set automatic pinging of new and modified items (this option is default and recommended for most users). To ensure the mechanism works properly, the integrator for each component should return the last modification date, which is directly dependant on the possibilities of the given integrator. In case of any questions or queries regarding whether the integrator returns the last modification date or not, please consult the author of the integrator. In case of the integrators being installed along with SEF Service Map, such functionality is implemented in content integrator (com_content_bot). The others cannot return the last modification date, as Joomla! does not store dates of addition or modification of items such as newsfeeds, contacts and links.

<u>5. Licence</u>
GNU/GPL

<u>6. Special thanks</u>
- Jaroslaw Gawlowski - <a href="http://www.festivalinfo.pl">http://www.festivalinfo.pl</a> (small bug fixes);

<u>7. SEF Service Map 2 Team</u>

- Radoslaw Kubera (developer)
- Bartosz Kubera (translator)

<u>8. Translations</u>
- english translation: Bartosz Kubera
- turkish translation: Denis Dulici