<?php
//*******************************************************
//* SEF Service Map Component
//* http://www.sefservicemap.com
//* (C) Radoslaw Kubera
//* license http://www.gnu.org/copyleft/gpl.html GNU/GPL
//*******************************************************


// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.plugin.plugin');
class plgSystemSefservicemap extends JPlugin
{
	function plgSystemSefservicemap(&$subject, $config)  {
		parent::__construct($subject, $config);
	}

	function onAfterRoute() {
		$db = JFactory::getDBO();		
		if (file_exists(JPATH_SITE.DS."administrator".DS."components".DS."com_sefservicemap".DS."include".DS."pingback.class.php")) {
			include_once (JPATH_SITE.DS."administrator".DS."components".DS."com_sefservicemap".DS."include".DS."pingback.class.php");

			//Unpublish plugin if fsocksopen and curl not available
			$pingback = new PingBackClass;
			$test = $pingback->checkMethod();
			if (!$test || $test=='0') {
				JError::raiseWarning(-1, JText::_("fsocksopen and curl functions are unavailable. Check Your server configuration. Plugin cannot be published."));

				$db->setQuery("select * from #__plugins where element='sefservicemap' and folder='system'");
				$plugin = $db->loadObject();
				if ($plugin->published='1') {
					$db->setQuery("update #__plugins set published='0' where id='$plugin->id'");
					$db->query();
					return true;
				}
			}
		}
	}
	function onAfterRender() {
		if (substr($_SERVER['REQUEST_URI'],1,13)!='administrator') 
		{
			$db = JFactory::getDBO();		
			if (file_exists(JPATH_SITE.DS."administrator".DS."components".DS."com_sefservicemap".DS."include".DS."pingback.class.php")) 
			{
				$db->setQuery("select value from #__sef_sm_settings where variable='bot_visit'");
				$bot_visit = intval($db->loadResult());
				if ($bot_visit==1) {
					$add=0;
					$agent = strtolower($_SERVER['HTTP_USER_AGENT']);
					if (eregi('bot',$agent) || eregi('crawler',$agent) || eregi('slurp',$agent) || eregi('ia_archiver',$agent) || eregi('msnbot',$agent) || eregi('live',$agent) || eregi('yahoo',$agent))
						$add=1;
				} else $add=1;
				if ($add==1) {
					$pingback = new PingBackClass;
					$query = "select count(a.id) as 'stack',b.* from #__sef_sm_pingback_stack AS a LEFT JOIN #__sef_sm_pingback AS b ON a.id = b.id GROUP BY a.id ORDER BY b. last_ping_date ASC LIMIT 1";
					$db->setQuery($query);
					$stack = $db->loadObject();
					if ($stack) { 
						$db->setQuery("select value from #__sef_sm_settings where variable='ping_interval'");
						$interval = intval($db->loadResult());
						if ($interval<1) $interval = 1;
						$lastdate = strtotime($stack->last_ping_date);
						$id = $stack->id;
						$nowdate = JFactory::getDate();
						$now = $nowdate->toUnix();
						$nowmysql = $nowdate->toMysql();
						$ready_time = ($interval*60)+$lastdate;
						if ($now>$ready_time) {
							$query = "select * from #__sef_sm_pingback_stack where id='$id' order by modified ASC LIMIT 1";
							$db->setQuery($query);
							$record = $db->loadObject();

							$query = "select last_date from #__sef_sm_pingback_menu where menu_id='$record->menu_id' and pingback_id='$id'";
							$db->setQuery($query);
							$stack->last_date = $db->loadResult();
							$url = JRoute::_($record->url);
							if (substr($url,0,1)=='/') $record->url = 'http://'.$_SERVER['SERVER_NAME'].$url; else
								$record->url = 'http://'.$_SERVER['SERVER_NAME'].'/'.JRoute::_($record->url);
							$url = $stack->ping_host;
							$result = $pingback->ping ($url,$record);

							if (strtotime($record->modified)>strtotime($stack->last_date)) $lastdate=" last_date='".$record->modified."'"; else $lastdate='';
	
							$db->setQuery("update #__sef_sm_pingback set last_ping_date='$nowmysql' ".$lastdate." where id='$id'");
							$db->query();

							if ($lastdate) {
								$query = "update #__sef_sm_pingback_menu set ".$lastdate." where pingback_id='$id' and menu_id='$record->menu_id'";
								$db->setQuery($query);
								$db->query();
							}
	
							if ($result) {
								$db->setQuery("delete from #__sef_sm_pingback_stack where id='$id' and url_md5='$record->url_md5'");
								$db->query();
								$pingback->logSession($id);
							}
						}
					}
				}
			}
		}
		return true;
	}
}
