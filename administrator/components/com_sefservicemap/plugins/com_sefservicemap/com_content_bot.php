<?php
//*******************************************************
//* SEF Service Map Component
//* http://www.sefservicemap.com
//* (C) Radoslaw Kubera
//* license http://www.gnu.org/copyleft/gpl.html GNU/GPL
//*******************************************************

/** ensure this file is being included by a parent file */
defined( '_JEXEC' ) or die( 'Restricted access' );

function Content_categories ($id, $level,$Itemid,$params,$link) {
	$priority=$params->get( 'priority', '' ) ;
	$changefreq=$params->get( 'changefreq', '' );
	$show_content_desc=intval($params->get( 'show_content_desc', '1' )) ;
	$show_item_icons=intval($params->get( 'show_item_icons', '1' )) ;

	$user	= &JFactory::getUser();
	$aid	= (int) $user->get('aid', 0);
	$db = JFactory::getDBO();

	$now = JFactory::getDate();
	$now = $now->toMysql();

	$lev=$level;
	$lev++;

	$xwhere = "\n AND a.state='1'"
		. "\n AND ( a.publish_up = '0000-00-00 00:00:00' OR a.publish_up <= '$now' )"
		. "\n AND ( a.publish_down = '0000-00-00 00:00:00' OR a.publish_down >= '$now' )"
		;

	$db->setQuery( "SELECT a.title,a.id,a.sectionid,a.catid,a.ordering,a.modified,a.created,a.introtext,a.alias, b.alias as'catalias' "
	 	. "\nFROM #__content AS a LEFT JOIN #__categories AS b ON a.catid = b.id "
		. "\n where a.catid=$id".$xwhere
                . " and a.access<=$aid order by ordering"
		);
	$contes = $db->loadObjectList();
	if (count($contes)!=0) {
		foreach ($contes as $cont) { 
			$description = '';
			if ($show_content_desc) $description = $cont->introtext;
			$element->description = $description;
      
			$linkcont ="index.php?option=com_content&amp;view=article&amp;id=".$cont->id.":".$cont->alias."&amp;catid=".$cont->catid.":".$cont->catalias."&amp;Itemid=".$Itemid;
			$element->name = $cont->title;
			$element->link = $linkcont;
			$element->level = $lev;
			$element->priority = $priority;
			$element->changefreq= $changefreq;
			if ($cont->modified> $cont->created) $element->lastmod = $cont->modified; else $element->lastmod = $cont->created;
			if ($show_item_icons) $element->image = 'content_article.gif'; else $element->image='none';
			$element->icon_path='/plugins/com_sefservicemap/';
			AddExtMenuItem ($element);
		} 
	}
}


function Content_sections($id, $level,$Itemid,$params,$link) {

	$layout = smGetParam($link,'layout','default');

	$priority=$params->get( 'priority', '' ) ;
	$changefreq=$params->get( 'changefreq', '' );
	$show_cat_icons=intval($params->get( 'show_cat_icons', '1' )) ;
	$show_cat_desc=intval($params->get( 'show_cat_desc', '1' )) ;
	$show_mode=intval($params->get( 'show_mode', '1' )) ;

	$user	= &JFactory::getUser();
	$aid	= (int) $user->get('aid', 0);
	$db = JFactory::getDBO();

	$lev=$level;
	if ($layout!='blog') $lev++;
	$db->setQuery( "SELECT title,id,section,ordering,description,alias"
	 	. "\nFROM #__categories"
		. "\n where section='$id' and access<=$aid and published='1' order by ordering"
		);
  
	$section=$db->loadObjectList();
	if (count($section)!=0) {
		foreach ($section as $sec) { 
			$description='';
			if ($show_cat_desc) $description = $sec->description;

			$db->setQuery("select count(id) FROM #__content"
			. "\n where catid=$sec->id and access<=$aid and state = 1 order by ordering");
			$count=$db->LoadResult();
			if ($count!=0) {
				if ($layout!='blog') {
					$linkcont ="index.php?option=com_content&amp;view=category&amp;id=".$sec->id.":".$sec->alias."&amp;layout=".$layout."&amp;Itemid=".$Itemid;
					$element->name = $sec->title;
					$element->link = $linkcont;
					$element->level = $lev;
					if ($show_cat_icons) $element->image = 'content_folder.gif'; else $element->image='none';
					$element->icon_path='/plugins/com_sefservicemap/';
					$element->description=$description;
					$element->priority = $priority;
					$element->changefreq= $changefreq;
					AddExtMenuItem ($element);
				}
				if ($show_mode || $layout=='blog') Content_categories($sec->id,$lev,$Itemid,$params,$link);
			}
		}
	} 
}

function Content_archive ($id, $level,$Itemid,$params,$link) {
	$priority=$params->get( 'priority', '' ) ;
	$changefreq=$params->get( 'changefreq', '' );

	$show_content_desc=intval($params->get( 'show_content_desc', '1' )) ;

	$user	= &JFactory::getUser();
	$aid	= (int) $user->get('aid', 0);
	$db = JFactory::getDBO();

	$now = JFactory::getDate();
	$now = $now->toMysql();

	$show_cat_desc=intval($params->get( 'show_content_desc', '0' )) ;
	$show_item_icons=intval($params->get( 'show_item_icons', '1' )) ;
	$lev=$level+1;
	$query = "SELECT a.*"
	. "\n FROM #__content AS a"
	. "\n INNER JOIN #__categories AS cc ON cc.id = a.catid"
	. "\n LEFT JOIN #__users AS u ON u.id = a.created_by"
	. "\n LEFT JOIN #__content_rating AS v ON a.id = v.content_id"
	. "\n LEFT JOIN #__sections AS s ON a.sectionid = s.id"
	. "\n LEFT JOIN #__groups AS g ON a.access = g.id"
	. "\n WHERE s.access<=$aid AND a.access<=$aid AND cc.access<=$aid "
	. "\n AND a.state =-1";
	$db->setQuery( $query );
	$contes=$db->loadObjectList();

	if (count($contes)!=0) {
		foreach ($contes as $cont) { 
			$description = '';
			if ($show_content_desc) $description = $cont->introtext;

			$element->description = $description;

      			$linkcont ="index.php?option=com_content&amp;view=article&amp;id=".$cont->id.":".$cont->alias."&amp;Itemid=".$Itemid;
			$element->name = $cont->title;
			$element->link = $linkcont;
			$element->level= $lev;
			$element->priority = $priority;
			$element->changefreq= $changefreq;
			if ($cont->modified> $cont->created) $element->lastmod = $cont->modified; else $element->lastmod = $cont->created;
			if ($show_item_icons) $element->image = 'content_article.gif'; else $element->image='none';
			$element->icon_path='/plugins/com_sefservicemap/';
			AddExtMenuItem ($element);
		} 
	}
}


function com_content_bot ($level,$link,$Itemid,$params) {
	$view = smGetParam($link,'view','');
	$sectionid = smGetParam($link,'sectionid','');
	$id = smGetParam($link,'id','');
	switch ($view) {    
		case 'view': break;
		case 'category': Content_categories($id,$level,$Itemid,$params,$link); break;
		case 'section': Content_sections($id,$level,$Itemid,$params,$link);break;
		case 'archivesection':Content_archivesection ($id, $level,$Itemid,$params,$link); break;
		case 'archivecategory':Content_archivecategory ($id, $level,$Itemid,$params,$link); break;
		case 'archive':Content_archive ($id, $level,$Itemid,$params,$link); break;

	}
}
?>