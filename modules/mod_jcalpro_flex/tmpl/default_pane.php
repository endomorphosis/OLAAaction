<?php
/*
 **********************************************
 Copyright (c) 2006-2009 Anything-Digital.com
 **********************************************
 JCal Pro is a fork of the existing Extcalendar component for Joomla!
 (com_extcal_0_9_2_RC4.zip from mamboguru.com).
 Extcal (http://sourceforge.net/projects/extcal) was renamed
 and adapted to become a Mambo/Joomla! component by
 Matthew Friedman, and further modified by David McKinnis
 (mamboguru.com) to repair some security holes.

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This header must not be removed. Additional contributions/changes
 may be added to this header as long as no information is deleted.
 **********************************************

 $Id: default_pane.php 434 2009-07-26 11:17:38Z shumisha $

 **********************************************
 Get the latest version of JCal Pro at:
 http://dev.anything-digital.com//
 **********************************************
 */

// No direct access
defined('_JEXEC') or die('=;)');

?>
<div class="jcalpro_flex_panel"	id="jcalpro_flex_panel_<?php echo $currentPane; ?> ">
  <?php

  $paneTitle = $panelsParams[$currentPane]['title' . $currentPane];
  $paneTitle = empty( $paneTitle) ? 'Panel #' . $currentPane : $paneTitle;
  echo $pane->startPanel(JText :: _($paneTitle), 'panel-' . $currentPane);
  echo "<ul>\n";
  if (!empty($panelsData[$currentPane])) {
    foreach($panelsData[$currentPane] as $event) {
      $nl = '';
      $html = '';
      // title and link to event
      $title = htmlspecialchars($event[3]->title);
      $formattedTitle = $pane->output( $title, array( 'type' => 'summary'));
      $html .= '<li><a href="' . $event[3]->linkToEvent . '" title="' .$title . '" >' . $formattedTitle . '</a></li>';

      // date/time
      if (!empty($event[3]->dateDisplay)) {
        $html .= $pane->output( $event[3]->dateDisplay, array('type' => 'dtstart', 'startDate' => $event[3]->start_date));
        $nl = '<br />';
      }

      // calendar display
      if (!empty($event[3]->calDisplay)) {
        $html .= $nl . '<small>(' . $pane->output( $event[3]->calDisplay, array( 'type' => 'category')) . ')</small>';
        $nl = '<br />';
      }

      // category display
      if (!empty($event[3]->catDisplay)) {
        $space = empty($event[3]->calDisplay) ? '' : '&nbsp;';
        $html .= $space . '<small>(' . $pane->output( $event[3]->catDisplay, array( 'type' => 'category')) . ')</small>';
        $nl = '<br />';
      }

      // event description display
      if (!empty($event[3]->descriptionDisplay)) {
        $html .= $nl . $pane->output( $event[3]->descriptionDisplay, array( 'type' => 'description')) ;
        $nl = '<br />';
      }

      // contact display
      if (!empty($event[3]->contactDisplay)) {
        $html .= $nl . '<small>' . $event[3]->contactDisplay . '</small>';
      }

      // wrap in microformat
      echo $pane->output( $html, array( 'type' => 'vevent'));

    }
  } else {
    echo $panelsParams['commonParams']['no_events_text'];
  }
  echo "</ul>\n";

  echo $pane->endPanel();

  ?>
</div>
