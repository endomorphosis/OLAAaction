<?php
/*
 **********************************************
 Copyright (c) 2006-2009 Anything-Digital.com
 **********************************************
 JCal Pro is a fork of the existing Extcalendar component for Joomla!
 (com_extcal_0_9_2_RC4.zip from mamboguru.com).
 Extcal (http://sourceforge.net/projects/extcal) was renamed
 and adapted to become a Mambo/Joomla! component by
 Matthew Friedman, and further modified by David McKinnis
 (mamboguru.com) to repair some security holes.

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This header must not be removed. Additional contributions/changes
 may be added to this header as long as no information is deleted.
 **********************************************

 $Id: minical_pane.php 661 2010-08-17 16:49:47Z shumisha $

 **********************************************
 Get the latest version of JCal Pro at:
 http://dev.anything-digital.com//
 **********************************************
 */

// No direct access
defined('_JEXEC') or die('=;)');

?>
<div class="jcalpro_flex_panel"
	id="jcalpro_flex_panel_<?php echo $currentPane; ?> "><?php

	$paneTitle = $panelsParams[$currentPane]['title' . $currentPane];
	$paneTitle = empty( $paneTitle) ? 'Panel #' . $currentPane : $paneTitle;
	echo $pane->startPanel(JText :: _($paneTitle), 'panel-' . $currentPane);

	// echo a minical
	if( is_readable(JPATH_ROOT. DS. 'components'.DS.'com_jcalpro'.DS.'include'.DS.'minical.inc.php') ) {
	  include( JPATH_ROOT. DS. 'components'.DS.'com_jcalpro'.DS.'include'.DS.'minical.inc.php' );
	}

	// set some params for this specific usage
	$params->set( 'navigation_controls', 0);
	$dateRange = $panelsParams[$currentPane]['date_range' . $currentPane];
	switch ($dateRange) {
	  case JCL_LIST_LAST_MONTH_EVENTS :
	    $dateRange = -1;
	  break;
	  case JCL_LIST_NEXT_MONTH_EVENTS :
      $dateRange = 1;
    break;
	  default:
	    $dateRange = 0;
	  break;  
	}
	$params->set( 'month_to_display', $dateRange);
	
	// remove the Add new event link in minical, Flex module has its own
	$params->set( 'show_minical_add_event_button', 0);

	// get the module output
	$moduleContent = jclMinical( $params, $module->id);

	// echo it
	echo $moduleContent;

	// echo end of panel
	echo $pane->endPanel();

	?></div>
