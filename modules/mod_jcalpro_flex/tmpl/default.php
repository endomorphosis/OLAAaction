<?php
/*
 **********************************************
 Copyright (c) 2006-2009 Anything-Digital.com
 **********************************************
 JCal Pro is a fork of the existing Extcalendar component for Joomla!
 (com_extcal_0_9_2_RC4.zip from mamboguru.com).
 Extcal (http://sourceforge.net/projects/extcal) was renamed
 and adapted to become a Mambo/Joomla! component by
 Matthew Friedman, and further modified by David McKinnis
 (mamboguru.com) to repair some security holes.

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This header must not be removed. Additional contributions/changes
 may be added to this header as long as no information is deleted.
 **********************************************

 $Id: default.php 531 2009-11-23 21:44:12Z shumisha $

 **********************************************
 Get the latest version of JCal Pro at:
 http://dev.anything-digital.com//
 **********************************************
 */

// No direct access
defined('_JEXEC') or die('=;)');

?>
<div class="jcalpro_flex" id="jcalpro_flex_<?php echo $module->id; ?>">

<?php
// start slider
echo $pane->startPane('jcalpro_flex_' . $module->id);

// iterate over all possible panes
for($currentPane = 1; $currentPane <= JCL_FLEX_MAX_PANES; $currentPane++) {
  if ($panelsParams[$currentPane]['enabled' . $currentPane] && $panelsParams[$currentPane]['number_of_events' . $currentPane] > 0) :
  // user wants to display this pane, use sub-template
  switch ($panelsParams[$currentPane]['display_content' . $currentPane]) {
    case JCL_FLEX_DISPLAY_CONTENT_MINICAL : // minical
      require( JModuleHelper::getLayoutPath( 'mod_jcalpro_flex', 'minical_pane'));
    break;
    default :   
      require( JModuleHelper::getLayoutPath( 'mod_jcalpro_flex', 'default_pane'));
    break;  
  }
  endif;
}

echo $pane->endPane();

##-------------------Extra Links to Full Calendar and Add New Event:

if ( !empty($footerData['show_full_calendar_link']) ) {
  echo '<a href="'.$footerData['show_full_calendar_link']->url.'">' . $footerData['show_full_calendar_link']->anchor . '</a><br />';
}
if ( !empty($footerData['show_add_event_link']) && has_priv( 'add') ) {
  echo '<a href="'.$footerData['show_add_event_link']->url.'">' . $footerData['show_add_event_link']->anchor . '</a><br />';
}

?></div>
