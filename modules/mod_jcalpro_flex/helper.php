<?php
/*
 **********************************************
 Copyright (c) 2006-2009 Anything-Digital.com
 **********************************************
 JCal Pro is a fork of the existing Extcalendar component for Joomla!
 (com_extcal_0_9_2_RC4.zip from mamboguru.com).
 Extcal (http://sourceforge.net/projects/extcal) was renamed
 and adapted to become a Mambo/Joomla! component by
 Matthew Friedman, and further modified by David McKinnis
 (mamboguru.com) to repair some security holes.

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This header must not be removed. Additional contributions/changes
 may be added to this header as long as no information is deleted.
 **********************************************

 $Id: helper.php 720 2011-05-12 08:36:01Z jeffchannell $

 **********************************************
 Get the latest version of JCal Pro at:
 http://dev.anything-digital.com//
 **********************************************
 */

//--No direct access
defined('_JEXEC') or die('=;)');

/**
 * Helper class for jcalpro_flex
 */
class Modjcalpro_flexHelper {

  // private copy of the current module instance parameters
  private static $_params = null;

  /**
   * Inject private properties into the class
   *
   * @param properties an array holding the names/values pairs
   * @return none
   */
  public static function setParams( $params) {

    self::$_params = $params;
  }

  /**
   * Returns data for all panes, as an array of arrays of objects
   */
  public static function getPanelsData( & $panelsParams) {

    // global array holding configuration
    global $CONFIG_EXT;

    // Require the events model, and instantiate it
    require_once(JPATH_ROOT.DS.'components'.DS.'com_jcalpro'.DS.'models'.DS.'events.php');
    $model = & JModel::getInstance( 'events', 'JcalproModel');

    // array to hold returned output
    $events = array();
    // variables for finding panel to open initially
    $initialStateFound = false;
    $now = jcUTCDateToFormatNoOffset( extcal_get_local_time(), '%Y-%m-%d %H:%M:%S');
    $panelsCount = 0;

    // iterate over panels
    for($currentPanel = 1; $currentPanel <= JCL_FLEX_MAX_PANES; $currentPanel++) {
      $isEnabled = 'enabled' . $currentPanel;
      if ($panelsParams[$currentPanel][$isEnabled]) {
        // count opened panels
        $panelsCount++;
        // this pane is enabled, let's fetch events for it
        // first find start and end date according to this panel parameters
        $boundariesProp = 'date_range' . $currentPanel;
        $range = jclFindDateRangeBoundaries( $panelsParams[$currentPanel][$boundariesProp]);
        // then inject those params into the model
        $properties = array(
            'calId' => $panelsParams[$currentPanel]['cal_id' . $currentPanel],
            'catId' => $panelsParams[$currentPanel]['cat_id' . $currentPanel],
            'calList' => $panelsParams[$currentPanel]['cal_list' . $currentPanel],
            'catList' => $panelsParams[$currentPanel]['cat_list' . $currentPanel],
            'dateBoundaries' => $range,
            'maxNumberOfEvents' => $panelsParams[$currentPanel]['number_of_events' . $currentPanel],
            'privateEventsMode' => $panelsParams['commonParams']['private_events_mode'],
            'showRecurringEvents' => $panelsParams['commonParams']['show_recurring_events'],
            'lastUpdated' => false,
            'multiDayEvents' => $CONFIG_EXT['multi_day_events'],
            'orderDir' => Modjcalpro_flexHelper::_getOrderDir( $panelsParams[$currentPanel], $currentPanel)
        );
        $model->injectProperties( $properties);

        // and ask the model for events
        $events[$currentPanel] = $model->getEvents( $rangeStart = null, $rangeEnd = null, $dispatchByDay = false);

        // as a side task, retrieve which panel to open at start-up, if user has asked that to be automatic
        if (!$initialStateFound && $panelsParams['commonParams']['initial_state'] == 'automatic') {
          // we open panel if it contains 'today', or is the "What's up" panel
          if ($panelsParams[$currentPanel][$boundariesProp] == JCL_LIST_ACTIVE_EVENTS || ($range->start < $now && $range->end >= $now)) {
            $panelsParams['commonParams']['startOffset'] = $panelsCount - 1;
            $initialStateFound = true;
          }
        }
        // attach elements needed for display
        if (!empty($events[$currentPanel])) {
          Modjcalpro_flexHelper::_prepareForDisplay( $events[$currentPanel]);
        }
      }
    }

    // apply correction to $panelsParams['commonParams']['initial_state'] :
    // if not all elements are opened, we need to
    // compensate, as Mootools accordion uses startOffset as an index in the
    // opened panels list
    /*    if ($initialStateFound && $panelsCount != JCL_FLEX_MAX_PANES) {
     $panelsParams['commonParams']['startOffset'] = $panelsCount;
     }*/

    return $events;
  } //end getItems

  /**
   * Add elements to each event, used when displaying it in the flex module
   *
   * @param $events the events for the current panel
   * @return none
   */
  private function _prepareForDisplay( &$events) {

    if (empty ($events)) {
      return;
    }

    // find the target Itemid for the main calendar
    $calendarItemid = jclGetItemid( 'com_jcalpro', $published = true);
    $calendarItemidString = empty( $calendarItemid) ? '' : '&amp;Itemid=' . $calendarItemid;

    // iterate over each event
    foreach( $events as $event) {

      // add a link to the event
      if (@property_exists($event[3], 'isIllbethere')) {
	      $event[3]->linkToEvent = JRoute::_( 'index.php?option=com_illbethere&amp;controller=events&amp;task=view&amp;id='
	      . $event[3]->extid . getIllBeThereItemid());
      }
      else if (@property_exists($event[3], 'isCommunity')) {
	      $event[3]->linkToEvent = JRoute::_( 'index.php?option=com_community&amp;view=events&amp;task=viewevent&amp;eventid='
	      . $event[3]->extid . getJomSocialItemid());
      }
      else {
	      $event[3]->linkToEvent = JRoute::_( 'index.php?option=com_jcalpro&amp;extmode=view&amp;extid='
	      . $event[3]->extid . $calendarItemidString);
      }

      // add date/time display
      if( self::$_params->get('show_dates', 1) || self::$_params->get('show_times', 1)) {
        $event[3]->dateDisplay = Modjcalpro_flexHelper::_getDateTimeString($event[3]);
      }

      // add calendar/category display
      $event[3]->calDisplay = self::$_params->get('show_calendar', 1) ? stripslashes( $event[3]->cal_name) : '';
      $event[3]->catDisplay = self::$_params->get('show_category', 1) ? stripslashes( $event[3]->cat_name) : '';

      // add description
      $desc = sub_string($event[3]->description, self::$_params->get('description_max_length', 256),'...');
      $desc = jclProcessReadmore( $desc, self::$_params->get('show_readmore', 0), $event[3]->linkToEvent );
      $event[3]->descriptionDisplay = self::$_params->get('show_description', 1) && !empty($desc) ? $desc : '';
      $event[3]->descriptionDisplay = preg_replace( '#<[\s]*meta#isU', '', $event[3]->descriptionDisplay);

      // add contact information
      $event[3]->contactDisplay = self::$_params->get('show_contact', 1) && !empty( $event[3]->contact) ? stripslashes( $event[3]->contact) : '';

    }
  }

  /**
   * Builds the display string for a givern date/time value
   * according to current parameters set
   *
   * @param $event
   * @return string, the formatted stringf ready for display
   */
  private function _getDateTimeString($event) {

    global $CONFIG_EXT, $EXTCAL_CONFIG, $lang_latest_events;

    $params = self::$_params;
    // do not show anything for end date if all day event or no end date event
    $no_end_specified = ( jclIsNoEndDate($event->end_date) || jclIsAllDay($event->end_date) || self::$_params->get('show_times') == 3 ) ? true : false;

    // find about details of date, for easier comparisons later
    $start_month = jcUTCDateToFormat($event->start_date, '%m');
    $start_daynumber = jcUTCDateToFormat($event->start_date, '%d');
    $start_year = jcUTCDateToFormat($event->start_date, '%Y');
    $end_month = jcUTCDateToFormat($event->end_date, '%m');
    $end_daynumber = jcUTCDateToFormat($event->end_date, '%d');
    $end_year = jcUTCDateToFormat($event->end_date, '%Y');

    // is it an all-day event
    if ( jclIsAllDay( $event->end_date) ) { // This event is an "All Day" event
      $start_time = EXTCAL_TEXT_ALL_DAY;
    }
    else {
      // else prepare event start time data
      $hour = jcUTCDateToFormat( $event->start_date, '%H');
      $minute = jcUTCDateToFormat( $event->start_date, '%M');
      $start_time = jcHourToDisplayString( $hour, $minute,!self::$_params->get('time_format_12_or_24', @$EXTCAL_CONFIG['time_format_12_or_24']));
      
      // and event end time data
      $hour = jcUTCDateToFormat( $event->end_date, '%H');
      $minute = jcUTCDateToFormat( $event->end_date, '%M');
      $end_time = jcHourToDisplayString( $hour, $minute,!self::$_params->get('time_format_12_or_24', @$EXTCAL_CONFIG['time_format_12_or_24']));
      
    }


    // If months are the same, return January 6-7, 2005. If not, return January 6 - February 7, 2005, if same year.
    if ( (($start_daynumber == $end_daynumber) && ($start_month == $end_month) && ($start_year == $end_year)) || $no_end_specified ) {
      // January 30, 2007 (08:00 - 10:00)
      $returnstring = self::$_params->get('show_dates') ? jcUTCDateToFormat( $event->start_date, self::$_params->get('date_format')) : '';
      $returnstring .=  self::$_params->get('show_times')  ? ' (' . $start_time . ( (self::$_params->get('show_times') == 3 || $no_end_specified) ? '' : ' - ' . $end_time ) . ')' : '';
    } else {
      // events are not on the same day
      if (self::$_params->get('show_dates')) {
        $temp_start = jcUTCDateToFormat( $event->start_date, self::$_params->get('date_format'));
        $temp_end   = jcUTCDateToFormat(  $event->end_date, self::$_params->get('date_format')) ;
      } else {
        $temp_start = '';
        $temp_end = '';
      }
      if ( self::$_params->get('show_times') ) {
        $returnstring = $temp_start . '(' . $start_time . ')' . ' - ' . $temp_end . ( (self::$_params->get('show_times') == 3 || $no_end_specified) ? '' : ' (' . $end_time . ')' ) ;
      } else {
        $returnstring = $temp_start . ' - ' . $temp_end;
      }
    }

    if ( self::$_params->get('days_view') ) {
      $difference = jcUTCDateToTs( $event->start_date) - TSserverToUTC( extcal_get_local_time());
      $days = $difference / 24 / 60 / 60;
      if ( $days >= 0 ) {
        $returnstring = round ( $days ) . $lang_latest_events['more_days'];
      } else {
        $returnstring = round ( $days * -1 ) . $lang_latest_events['days_ago'];
      }
    }

    return $returnstring;
  }


  /**
   * Get and prepare individual panes parameters from Joomla module param set
   *
   * @param object $params the param object as passed by Joomla
   * @return array[1..JCL_FLEX_MAX_PANES] of pane parameters object
   */
  public static function getPanelsParams( $module) {

    $paramList = array( 'enabled', 'cal_list', 'cat_list', 'title',  'date_range', 'number_of_events', 'display_content');

    $commonParamsList = array( 'initial_state', 'private_events_mode', 'show_recurring_events', 'no_events_text', 'show_full_calendar_link', 'full_calendar_link_text', 'show_add_event_link', 'add_event_text', 'moduleclass_sfx');

    // result table
    $panelsParams = array();

    // for each pane
    $startOffset = null;
    for($i = 1; $i <= JCL_FLEX_MAX_PANES; $i++) {
      // get all parameters needed from the module parameters provided by Joomla
      foreach($paramList as $paramItem) {
        // parameters are named using the param name followed by pane number, ie : enabled1
        $paramItemName = $paramItem . $i;
        $panelsParams[$i][$paramItemName] = self::$_params->get( $paramItemName);
      }
    }

    // add params common to all panels
    foreach( $commonParamsList as $paramItemName) {
      $panelsParams['commonParams'][$paramItemName] = self::$_params->get( $paramItemName);
    }

    // add a few more things
    $panelsParams['commonParams']['transition'] = null;
    $panelsParams['commonParams']['duration'] = 300;
    $panelsParams['commonParams']['show'] = null;
    $panelsParams['commonParams']['startTransition'] = null;
    $panelsParams['commonParams']['allowAllClose'] = true;
    $panelsParams['commonParams']['moduleId'] = $module->id;
    Modjcalpro_flexHelper::_getPanelToOpenInitially($panelsParams);

    // return completed array of object
    return $panelsParams;
  }

  /**
   * Returns data for module footer, as an array of arrays of objects
   */
  public static function getFooterData() {


    // global array holding configuration
    global $lang_latest_events;

    // array to hold returned output
    $footerData = array();

    // find the target Itemid for the main calendar
    $calendarItemid = jclGetItemid( 'com_jcalpro', $published = true);

    // should we add a link to main calendar
    if ( self::$_params->def('show_full_calendar_link',1)) {
      $footerData['show_full_calendar_link']->url = JRoute::_( 'index.php?option=com_jcalpro&amp;view=calendar'
      . (empty( $calendarItemid) ? '' : '&amp;Itemid=' . $calendarItemid ));
      $footerData['show_full_calendar_link']->anchor = htmlspecialchars(self::$_params->def('full_calendar_link_text',$lang_latest_events['view_full_cal']));
    }
    if ( self::$_params->def('show_add_event_link',1) && has_priv( 'add') ) {
      $footerData['show_add_event_link']->url = JRoute::_( 'index.php?option=com_jcalpro&amp;extmode=event&amp;event_mode=add'
      . (empty( $calendarItemid) ? '' : '&amp;Itemid=' . $calendarItemid ));
      $footerData['show_add_event_link']->anchor = htmlspecialchars(self::$_params->def('add_event_text',$lang_latest_events['add_new_event']));
    }

    return $footerData;

  } //end getFooterData

  public static function loadLibs() {
	
		jimport( 'joomla.filesystem.file');
		
		// check if jcal is installed
		$configFileName = JPATH_ROOT."/components/com_jcalpro/config.inc.php";
		if (!JFile::exists( $configFileName)) {
		  return false;
		}
    // load common libraries
    require_once( $configFileName );

    // include JPaneSliders class
    // joomla jimport does not work because JPaneSliders is in the same file as JPane
    require_once( JPATH_ROOT . DS . 'libraries' . DS . 'joomla' . DS . 'html' . DS . 'pane.php');

    // load common libraries
    require_once( JPATH_ROOT.'/components/com_jcalpro/include/display/jclslider.php' );
    require_once( JPATH_ROOT.'/components/com_jcalpro/include/format/jcloutputfilter.php' );

		return true;
  }

  /**
   * Decides on the panel number to be opened at start-up
   * based on backend params
   *
   * @param $panesParams backend parameters
   * @return integer the panel id, starting at 0, to be opened, null if all must be closed
   */
  private function _getPanelToOpenInitially( & $panelsParams) {

    // default value : let mootools decide
    $result =  null;

    // various possible options
    switch ($panelsParams['commonParams']['initial_state']) {

      // automatic : we look for the first panel which contains today's date
      // if today is sunday, and the first panel displays 'this week', then first panel will be opened
      // if today is sunday, first panel is 'Last week', second panel is 'this week', then second panel will be opened
      case 'automatic':
        // special case : as we need all details about events to do this calculation, the starting pane will be retrieved
        // when we search for the events to display
        break;
        // easy, close them all
      case 'close_all':
        $result = false;
        $panelsParams['commonParams']['allowAllClose'] = true;
        break;
        // user has selected to open a specific panel
      default:
        // param follows : 'open_n' where n is between 1 and 9
        $panelId = intval( substr($panelsParams['commonParams']['initial_state'], -1));
        if (isset($panelsParams[$panelId]) && isset($panelsParams[$panelId]['enabled'.$panelId])
        && $panelsParams[$panelId]['enabled'.$panelId]) {
          // apply correction to $startOffset :
          // if not all elements are opened, we need to
          // compensate, as Mootools accordion uses startOffset as an index in the
          // opened panels list
          $panelsCount = 0;
          for($currentPanel = 1; $currentPanel <= JCL_FLEX_MAX_PANES; $currentPanel++) {
            if( $panelsParams[$currentPanel]['enabled'.$currentPanel] ) {
              $panelsCount++;
            }
            if ($panelId == $currentPanel) {
              $panelId = $panelsCount;
              break;
            }
          }
          // user asked for panel, and it is not enabled ! if not enabled, we will eventually return 0
          $result = $panelId - 1;
        }
        break;
    }

    // set value in panes parameters array
    $panelsParams['commonParams']['startOffset'] = $result;
  }

  /**
   * Decide of event sort order, based on type of display
   * requested by user in backend
   *
   * @param $panelParams array of current panel parameters
   * @return string ASC or DESC
   */
  private function _getOrderDir( $panelParams, $currentPanel) {
    $order = 'ASC';
    $reverseOrderDisplay = array(JCL_LIST_PAST_EVENTS, JCL_LIST_YESTERDAY_EVENTS, JCL_LIST_LAST_WEEK_EVENTS, JCL_LIST_LAST_MONTH_EVENTS);
    if (in_array( $panelParams['date_range'.$currentPanel], $reverseOrderDisplay)) {
      $order = 'DESC';
    }

    return $order;
  }

} //end Modjcalpro_flexHelper
