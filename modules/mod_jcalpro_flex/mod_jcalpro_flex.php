<?php
/*
 **********************************************
 Copyright (c) 2006-2009 Anything-Digital.com
 **********************************************
 JCal Pro is a fork of the existing Extcalendar component for Joomla!
 (com_extcal_0_9_2_RC4.zip from mamboguru.com).
 Extcal (http://sourceforge.net/projects/extcal) was renamed
 and adapted to become a Mambo/Joomla! component by
 Matthew Friedman, and further modified by David McKinnis
 (mamboguru.com) to repair some security holes.

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This header must not be removed. Additional contributions/changes
 may be added to this header as long as no information is deleted.
 **********************************************

 $Id: mod_jcalpro_flex.php 565 2010-01-17 22:47:15Z shumisha $

 **********************************************
 Get the latest version of JCal Pro at:
 http://dev.anything-digital.com//
 **********************************************
 */

// No direct access
defined('_JEXEC') or die('=;)');

// include the helper file
require_once(dirname(__FILE__).DS.'helper.php');

// load common libs
$libsLoaded = Modjcalpro_flexHelper::loadLibs();
if (!$libsLoaded) {
  return;
}

// attach params to helper
Modjcalpro_flexHelper::setParams( $params);

// get the module parameters in a convenient structure, from the helper
$panelsParams = Modjcalpro_flexHelper::getPanelsParams( $module);

// get each panel data, using helper method
$panelsData = Modjcalpro_flexHelper::getPanelsData( $panelsParams);

// prepare a slider object for the slider we want to use
$displayType = $params->get( 'display_type', 'Vertical');
$sliderClass = 'Jcl' . $displayType . 'PaneSliders';
$sliderToDecorate = new $sliderClass($panelsParams['commonParams']);

// add an output filter using the decorator pattern
$filterClass = $params->get( 'microformat_type', JCL_MICROFORMAT_NONE);
// check class exists, use default one if not (default one has no effect)
$filterClass = class_exists( $filterClass) ? $filterClass : 'JclOutputFilter';
// instantiate pane
$pane =  new $filterClass( $sliderToDecorate);

// prepare the footer information
$footerData = Modjcalpro_flexHelper::getFooterData();

// include the template for display
require(JModuleHelper::getLayoutPath('mod_jcalpro_flex'));
