<?php
/**
@version 1.0: mod_s5_image_and_content_fader
Author: Shape 5 - Professional Template Community
Available for download at www.shape5.com
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

$background_s5_iacf		= $params->get( 'background' );
$pretext_s5_iacf		= $params->get( 'pretext' );
$tween_time_s5_iacf	    = $params->get( 'tweentime' );
$height_s5_iacf		    = $params->get( 'height' );
$width_s5_iacf   		= $params->get( 'width' );
$picture1_s5_iacf		= $params->get( 'picture1' );
$picture1link_s5_iacf		= $params->get( 'picture1link' );
$picture1target_s5_iacf		= $params->get( 'picture1target' );
$picture2_s5_iacf		= $params->get( 'picture2' );
$picture2link_s5_iacf		= $params->get( 'picture2link' );
$picture2target_s5_iacf		= $params->get( 'picture2target' );
$picture3_s5_iacf		= $params->get( 'picture3' );
$picture3link_s5_iacf		= $params->get( 'picture3link' );
$picture3target_s5_iacf		= $params->get( 'picture3target' );
$picture4_s5_iacf		= $params->get( 'picture4' );
$picture4link_s5_iacf		= $params->get( 'picture4link' );
$picture4target_s5_iacf		= $params->get( 'picture4target' );
$picture5_s5_iacf		= $params->get( 'picture5' );
$picture5link_s5_iacf		= $params->get( 'picture5link' );
$picture5target_s5_iacf		= $params->get( 'picture5target' );
$picture6_s5_iacf		= $params->get( 'picture6' );
$picture6link_s5_iacf		= $params->get( 'picture6link' );
$picture6target_s5_iacf		= $params->get( 'picture6target' );
$picture7_s5_iacf		= $params->get( 'picture7' );
$picture7link_s5_iacf		= $params->get( 'picture7link' );
$picture7target_s5_iacf		= $params->get( 'picture7target' );
$picture8_s5_iacf		= $params->get( 'picture8' );
$picture8link_s5_iacf		= $params->get( 'picture8link' );
$picture8target_s5_iacf		= $params->get( 'picture8target' );
$picture9_s5_iacf		= $params->get( 'picture9' );
$picture9link_s5_iacf		= $params->get( 'picture9link' );
$picture9target_s5_iacf		= $params->get( 'picture9target' );
$picture10_s5_iacf		= $params->get( 'picture10' );
$picture10link_s5_iacf		= $params->get( 'picture10link' );
$picture10target_s5_iacf	= $params->get( 'picture10target' );
$display_time_s5_iacf   	= $params->get( 'displaytime' );

$picture1text_s5_iacf   	= $params->get( 'picture1text' );
$picture1spacing_s5_iacf   	= $params->get( 'picture1spacing' );
$picture1textsize_s5_iacf   	= $params->get( 'picture1textsize' );
$picture1textcolor_s5_iacf   	= $params->get( 'picture1textcolor' );
$picture1textbg_s5_iacf   	= $params->get( 'picture1textbg' );
$picture2text_s5_iacf   	= $params->get( 'picture2text' );
$picture2spacing_s5_iacf   	= $params->get( 'picture2spacing' );
$picture2textsize_s5_iacf   	= $params->get( 'picture2textsize' );
$picture2textcolor_s5_iacf   	= $params->get( 'picture2textcolor' );
$picture2textbg_s5_iacf   	= $params->get( 'picture2textbg' );
$picture3text_s5_iacf   	= $params->get( 'picture3text' );
$picture3spacing_s5_iacf   	= $params->get( 'picture3spacing' );
$picture3textsize_s5_iacf   	= $params->get( 'picture3textsize' );
$picture3textcolor_s5_iacf   	= $params->get( 'picture3textcolor' );
$picture3textbg_s5_iacf   	= $params->get( 'picture3textbg' );
$picture4text_s5_iacf   	= $params->get( 'picture4text' );
$picture4spacing_s5_iacf   	= $params->get( 'picture4spacing' );
$picture4textsize_s5_iacf   	= $params->get( 'picture4textsize' );
$picture4textcolor_s5_iacf   	= $params->get( 'picture4textcolor' );
$picture4textbg_s5_iacf   	= $params->get( 'picture4textbg' );
$picture5text_s5_iacf   	= $params->get( 'picture5text' );
$picture5spacing_s5_iacf   	= $params->get( 'picture5spacing' );
$picture5textsize_s5_iacf   	= $params->get( 'picture5textsize' );
$picture5textcolor_s5_iacf   	= $params->get( 'picture5textcolor' );
$picture5textbg_s5_iacf   	= $params->get( 'picture5textbg' );
$picture6text_s5_iacf   	= $params->get( 'picture6text' );
$picture6spacing_s5_iacf   	= $params->get( 'picture6spacing' );
$picture6textsize_s5_iacf   	= $params->get( 'picture6textsize' );
$picture6textcolor_s5_iacf   	= $params->get( 'picture6textcolor' );
$picture6textbg_s5_iacf   	= $params->get( 'picture6textbg' );
$picture7text_s5_iacf   	= $params->get( 'picture7text' );
$picture7spacing_s5_iacf   	= $params->get( 'picture7spacing' );
$picture7textsize_s5_iacf   	= $params->get( 'picture7textsize' );
$picture7textcolor_s5_iacf   	= $params->get( 'picture7textcolor' );
$picture7textbg_s5_iacf   	= $params->get( 'picture7textbg' );
$picture8text_s5_iacf   	= $params->get( 'picture8text' );
$picture8spacing_s5_iacf   	= $params->get( 'picture8spacing' );
$picture8textsize_s5_iacf   	= $params->get( 'picture8textsize' );
$picture8textcolor_s5_iacf   	= $params->get( 'picture8textcolor' );
$picture8textbg_s5_iacf   	= $params->get( 'picture8textbg' );
$picture9text_s5_iacf   	= $params->get( 'picture9text' );
$picture9spacing_s5_iacf   	= $params->get( 'picture9spacing' );
$picture9textsize_s5_iacf   	= $params->get( 'picture9textsize' );
$picture9textcolor_s5_iacf   	= $params->get( 'picture9textcolor' );
$picture9textbg_s5_iacf   	= $params->get( 'picture9textbg' );
$picture10text_s5_iacf   	= $params->get( 'picture10text' );
$picture10spacing_s5_iacf   	= $params->get( 'picture10spacing' );
$picture10textsize_s5_iacf   	= $params->get( 'picture10textsize' );
$picture10textcolor_s5_iacf   	= $params->get( 'picture10textcolor' );
$picture10textbg_s5_iacf   	= $params->get( 'picture10textbg' );

$picture1textweight_s5_iacf   	= $params->get( 'picture1textweight' );
$picture2textweight_s5_iacf   	= $params->get( 'picture2textweight' );
$picture3textweight_s5_iacf   	= $params->get( 'picture3textweight' );
$picture4textweight_s5_iacf   	= $params->get( 'picture4textweight' );
$picture5textweight_s5_iacf   	= $params->get( 'picture5textweight' );
$picture6textweight_s5_iacf   	= $params->get( 'picture6textweight' );
$picture7textweight_s5_iacf   	= $params->get( 'picture7textweight' );
$picture8textweight_s5_iacf   	= $params->get( 'picture8textweight' );
$picture9textweight_s5_iacf   	= $params->get( 'picture9textweight' );
$picture10textweight_s5_iacf   	= $params->get( 'picture10textweight' );

$picture1textopac_s5_iacf   	= $params->get( 'picture1textopac' );
$picture2textopac_s5_iacf   	= $params->get( 'picture2textopac' );
$picture3textopac_s5_iacf   	= $params->get( 'picture3textopac' );
$picture4textopac_s5_iacf   	= $params->get( 'picture4textopac' );
$picture5textopac_s5_iacf   	= $params->get( 'picture5textopac' );
$picture6textopac_s5_iacf   	= $params->get( 'picture6textopac' );
$picture7textopac_s5_iacf   	= $params->get( 'picture7textopac' );
$picture8textopac_s5_iacf   	= $params->get( 'picture8textopac' );
$picture9textopac_s5_iacf   	= $params->get( 'picture9textopac' );
$picture10textopac_s5_iacf   	= $params->get( 'picture10textopac' );

$non_ie_picture1textopac_s5_iacf = $picture1textopac_s5_iacf / 100;
$non_ie_picture2textopac_s5_iacf = $picture2textopac_s5_iacf / 100;
$non_ie_picture3textopac_s5_iacf = $picture3textopac_s5_iacf / 100;
$non_ie_picture4textopac_s5_iacf = $picture4textopac_s5_iacf / 100;
$non_ie_picture5textopac_s5_iacf = $picture5textopac_s5_iacf / 100;
$non_ie_picture6textopac_s5_iacf = $picture6textopac_s5_iacf / 100;
$non_ie_picture7textopac_s5_iacf = $picture7textopac_s5_iacf / 100;
$non_ie_picture8textopac_s5_iacf = $picture8textopac_s5_iacf / 100;
$non_ie_picture9textopac_s5_iacf = $picture9textopac_s5_iacf / 100;
$non_ie_picture10textopac_s5_iacf = $picture10textopac_s5_iacf / 100;


$tween_time_s5_iacf = $tween_time_s5_iacf*1000;
$display_time_s5_iacf = $display_time_s5_iacf*1000;

$text_display_effect = $tween_time_s5_iacf * 0.75;
$text_display_time_s5_iacf = $display_time_s5_iacf - $text_display_effect;


if ($picture1target_s5_iacf == "1") {
$picture1target_s5_iacf = "_blank"; }
if ($picture1target_s5_iacf == "0") {
$picture1target_s5_iacf = "_top"; }
if ($picture2target_s5_iacf == "1") {
$picture2target_s5_iacf = "_blank"; }
if ($picture2target_s5_iacf == "0") {
$picture2target_s5_iacf = "_top"; }
if ($picture3target_s5_iacf == "1") {
$picture3target_s5_iacf = "_blank"; }
if ($picture3target_s5_iacf == "0") {
$picture3target_s5_iacf = "_top"; }
if ($picture4target_s5_iacf == "1") {
$picture4target_s5_iacf = "_blank"; }
if ($picture4target_s5_iacf == "0") {
$picture4target_s5_iacf = "_top"; }
if ($picture5target_s5_iacf == "1") {
$picture5target_s5_iacf = "_blank"; }
if ($picture5target_s5_iacf == "0") {
$picture5target_s5_iacf = "_top"; }
if ($picture6target_s5_iacf == "1") {
$picture6target_s5_iacf = "_blank"; }
if ($picture6target_s5_iacf == "0") {
$picture6target_s5_iacf = "_top"; }
if ($picture7target_s5_iacf == "1") {
$picture7target_s5_iacf = "_blank"; }
if ($picture7target_s5_iacf == "0") {
$picture7target_s5_iacf = "_top"; }
if ($picture8target_s5_iacf == "1") {
$picture8target_s5_iacf = "_blank"; }
if ($picture8target_s5_iacf == "0") {
$picture8target_s5_iacf = "_top"; }
if ($picture9target_s5_iacf == "1") {
$picture9target_s5_iacf = "_blank"; }
if ($picture9target_s5_iacf == "0") {
$picture9target_s5_iacf = "_top"; }
if ($picture10target_s5_iacf == "1") {
$picture10target_s5_iacf = "_blank"; }
if ($picture10target_s5_iacf == "0") {
$picture10target_s5_iacf = "_top"; }

?>

<?php if ($pretext_s5_iacf != "") { ?>
<br />
<?php echo $pretext_s5_iacf ?>
<br /><br />
<?php } ?>


<?php
$br = strtolower($_SERVER['HTTP_USER_AGENT']); // what browser.
if(strrpos($br,"msie 6") > 1) {
$iss_ie6_s5_iacf = "yes";
} 
else {
$iss_ie6_s5_iacf = "no";
}
?>


<?php
$br = strtolower($_SERVER['HTTP_USER_AGENT']); // what browser.
if(strrpos($br,"msie 7") > 1) {
$iss_ie7_s5_iacf = "yes";
} 
else {
$iss_ie7_s5_iacf = "no";
}
?>

<?php
$br = strtolower($_SERVER['HTTP_USER_AGENT']); // what browser.
if(strrpos($br,"msie 8") > 1) {
$iss_ie8_s5_iacf = "yes";
} 
else {
$iss_ie8_s5_iacf = "no";
}
?>

<div style="z-index:0;position: relative; overflow: hidden; height: <?php echo $height_s5_iacf ?>">

<div id="s5_iacf_outer" style="z-index:1;position: relative; max-height:<?php echo $height_s5_iacf ?>; max-width:<?php echo $width_s5_iacf ?>; overflow:hidden; background:#<?php echo $background_s5_iacf ?>">

<script type="text/javascript">
<?php if ($iss_ie6_s5_iacf == "yes" || $iss_ie7_s5_iacf == "yes" || $iss_ie8_s5_iacf == "yes") { ?>
var s5_iacf_inc = 12;
<?php } ?>
<?php if ($iss_ie6_s5_iacf == "no" && $iss_ie7_s5_iacf == "no" && $iss_ie8_s5_iacf == "no") { ?>
var s5_iacf_inc = 18;
<?php } ?>
</script>

<?php if ($picture1_s5_iacf != "") { ?>

<div id="picture1_s5_iacf" style="padding:0px; display:none; height:<?php echo $height_s5_iacf ?>; opacity:.0; <?php if ($iss_ie6_s5_iacf == "yes" || $iss_ie7_s5_iacf == "yes" || $iss_ie8_s5_iacf == "yes") { ?>filter: alpha(opacity=0); -moz-opacity: 0;<?php } ?> width:<?php echo $width_s5_iacf ?>; overflow:hidden; background-image: url(<?php echo $picture1_s5_iacf ?>); background-repeat: no-repeat">
<?php if ($picture1link_s5_iacf != "") { ?>
<a href="<?php echo $picture1link_s5_iacf ?>" target="<?php echo $picture1target_s5_iacf ?>">
<img alt="" id="picture1_blank_s5_iacf" style="border:none" src="modules/mod_s5_image_and_content_fader/blank.gif" height="<?php echo $height_s5_iacf ?>" width="<?php echo $width_s5_iacf ?>"></img>
</a>
<?php } ?>

<?php if ($picture1link_s5_iacf == "") { ?>
<img alt="" style="border:none" id="picture1_blank_s5_iacf" src="modules/mod_s5_image_and_content_fader/blank.gif" height="<?php echo $height_s5_iacf ?>" width="<?php echo $width_s5_iacf ?>"></img>
<?php } ?>

<?php if ($picture1text_s5_iacf != "") { ?>

<div id="picture1text_bg_s5_iacf" style="z-index:1;position: relative; opacity:<?php echo $non_ie_picture1textopac_s5_iacf ?>; <?php if ($iss_ie6_s5_iacf == "yes" || $iss_ie7_s5_iacf == "yes" || $iss_ie8_s5_iacf == "yes") { ?>filter: alpha(opacity=<?php echo $picture1textopac_s5_iacf ?>); <?php } ?> background:#<?php echo $picture1textbg_s5_iacf ?>">
</div>

<div id="picture1text_s5_iacf" style="z-index:1;height:auto; position: relative; padding:<?php echo $picture1spacing_s5_iacf ?>; color:#<?php echo $picture1textcolor_s5_iacf ?>; font-weight:<?php echo $picture1textweight_s5_iacf ?>; font-size:<?php echo $picture1textsize_s5_iacf ?>">
<?php echo $picture1text_s5_iacf ?>
</div>

<script type="text/javascript">
function set_picture1_loaders() {
document.getElementById("picture1_blank_s5_iacf").style.height = "<?php echo $height_s5_iacf ?>";
window.setTimeout('picture1text_load_bg_s5_iacf()',0);
window.setTimeout('picture1text_effect_big()',<?php echo $text_display_effect ?>);
window.setTimeout('picture1text_effect_small()',<?php echo $text_display_time_s5_iacf ?>);
}

function picture1text_load_bg_s5_iacf() {
document.getElementById("picture1text_s5_iacf").style.marginTop = (document.getElementById("picture1text_s5_iacf").offsetHeight * -1) + "px";
document.getElementById("picture1text_bg_s5_iacf").style.height = document.getElementById("picture1text_s5_iacf").offsetHeight + "px";
}

function picture1text_effect_big_timer() {
window.setTimeout('picture1text_effect_big()',10);
}

function picture1text_effect_big() {
var s5_outer_iacf = document.getElementById("s5_iacf_outer").offsetHeight;
if (document.getElementById("picture1_blank_s5_iacf").offsetHeight > s5_outer_iacf - document.getElementById("picture1text_s5_iacf").offsetHeight + 7) {
document.getElementById("picture1_blank_s5_iacf").style.height = document.getElementById("picture1_blank_s5_iacf").offsetHeight - s5_iacf_inc + "px";
picture1text_effect_big_timer();
}
else {
document.getElementById("picture1_blank_s5_iacf").style.height = document.getElementById("s5_iacf_outer").offsetHeight - document.getElementById("picture1text_s5_iacf").offsetHeight + "px";
}
}

function picture1text_effect_small_timer() {
window.setTimeout('picture1text_effect_small()',10);
}

</script>

<?php } ?>

</div>

<?php } ?>

<?php if ($picture2_s5_iacf != "") { ?>

<div id="picture2_s5_iacf" style="padding:0px; display:none; height:<?php echo $height_s5_iacf ?>; opacity:.0; <?php if ($iss_ie6_s5_iacf == "yes" || $iss_ie7_s5_iacf == "yes" || $iss_ie8_s5_iacf == "yes") { ?>filter: alpha(opacity=0); -moz-opacity: 0;<?php } ?> width:<?php echo $width_s5_iacf ?>; overflow:hidden; background-image: url(<?php echo $picture2_s5_iacf ?>); background-repeat: no-repeat">
<?php if ($picture2link_s5_iacf != "") { ?>
<a href="<?php echo $picture2link_s5_iacf ?>" target="<?php echo $picture2target_s5_iacf ?>">
<img alt="" style="border:none" id="picture2_blank_s5_iacf" src="modules/mod_s5_image_and_content_fader/blank.gif" height="<?php echo $height_s5_iacf ?>" width="<?php echo $width_s5_iacf ?>"></img>
</a>
<?php } ?>

<?php if ($picture2link_s5_iacf == "") { ?>
<img alt="" style="border:none" id="picture2_blank_s5_iacf" src="modules/mod_s5_image_and_content_fader/blank.gif" height="<?php echo $height_s5_iacf ?>" width="<?php echo $width_s5_iacf ?>"></img>
<?php } ?>

<?php if ($picture2text_s5_iacf != "") { ?>

<div id="picture2text_bg_s5_iacf" style="z-index:1;position: relative; opacity:<?php echo $non_ie_picture2textopac_s5_iacf ?>; <?php if ($iss_ie6_s5_iacf == "yes" || $iss_ie7_s5_iacf == "yes" || $iss_ie8_s5_iacf == "yes") { ?>filter: alpha(opacity=<?php echo $picture2textopac_s5_iacf ?>); <?php } ?> background:#<?php echo $picture2textbg_s5_iacf ?>">
</div>

<div id="picture2text_s5_iacf" style="z-index:1;height:auto; position: relative; padding:<?php echo $picture2spacing_s5_iacf ?>; color:#<?php echo $picture2textcolor_s5_iacf ?>; font-weight:<?php echo $picture2textweight_s5_iacf ?>; font-size:<?php echo $picture2textsize_s5_iacf ?>">
<?php echo $picture2text_s5_iacf ?>
</div>

<script type="text/javascript">
function set_picture2_loaders() {
document.getElementById("picture2_blank_s5_iacf").style.height = "<?php echo $height_s5_iacf ?>";
window.setTimeout('picture2text_load_bg_s5_iacf()',0);
window.setTimeout('picture2text_effect_big()',<?php echo $text_display_effect ?>);
window.setTimeout('picture2text_effect_small()',<?php echo $text_display_time_s5_iacf ?>);
}

function picture2text_load_bg_s5_iacf() {
document.getElementById("picture2text_s5_iacf").style.marginTop = (document.getElementById("picture2text_s5_iacf").offsetHeight * -1) + "px";
document.getElementById("picture2text_bg_s5_iacf").style.height = document.getElementById("picture2text_s5_iacf").offsetHeight + "px";
}

function picture2text_effect_big_timer() {
window.setTimeout('picture2text_effect_big()',10);
}

function picture2text_effect_big() {
var s5_outer_iacf = document.getElementById("s5_iacf_outer").offsetHeight;
if (document.getElementById("picture2_blank_s5_iacf").offsetHeight > s5_outer_iacf - document.getElementById("picture2text_s5_iacf").offsetHeight + 7) {
document.getElementById("picture2_blank_s5_iacf").style.height = document.getElementById("picture2_blank_s5_iacf").offsetHeight - s5_iacf_inc + "px";
picture2text_effect_big_timer();
}
else {
document.getElementById("picture2_blank_s5_iacf").style.height = document.getElementById("s5_iacf_outer").offsetHeight - document.getElementById("picture2text_s5_iacf").offsetHeight + "px";
}
}

function picture2text_effect_small_timer() {
window.setTimeout('picture2text_effect_small()',10);
}

</script>

<?php } ?>

</div>

<?php } ?>

<?php if ($picture3_s5_iacf != "") { ?>

<div id="picture3_s5_iacf" style="padding:0px; display:none; height:<?php echo $height_s5_iacf ?>; opacity:.0; <?php if ($iss_ie6_s5_iacf == "yes" || $iss_ie7_s5_iacf == "yes" || $iss_ie8_s5_iacf == "yes") { ?>filter: alpha(opacity=0); -moz-opacity: 0;<?php } ?> width:<?php echo $width_s5_iacf ?>; overflow:hidden; background-image: url(<?php echo $picture3_s5_iacf ?>); background-repeat: no-repeat">
<?php if ($picture3link_s5_iacf != "") { ?>
<a href="<?php echo $picture3link_s5_iacf ?>" target="<?php echo $picture3target_s5_iacf ?>">
<img alt="" style="border:none" id="picture3_blank_s5_iacf" src="modules/mod_s5_image_and_content_fader/blank.gif" height="<?php echo $height_s5_iacf ?>" width="<?php echo $width_s5_iacf ?>"></img>
</a>
<?php } ?>

<?php if ($picture3link_s5_iacf == "") { ?>
<img alt="" style="border:none" id="picture3_blank_s5_iacf" src="modules/mod_s5_image_and_content_fader/blank.gif" height="<?php echo $height_s5_iacf ?>" width="<?php echo $width_s5_iacf ?>"></img>
<?php } ?>

<?php if ($picture3text_s5_iacf != "") { ?>

<div id="picture3text_bg_s5_iacf" style="z-index:1;position: relative; opacity:<?php echo $non_ie_picture3textopac_s5_iacf ?>; <?php if ($iss_ie6_s5_iacf == "yes" || $iss_ie7_s5_iacf == "yes" || $iss_ie8_s5_iacf == "yes") { ?>filter: alpha(opacity=<?php echo $picture3textopac_s5_iacf ?>); <?php } ?> background:#<?php echo $picture3textbg_s5_iacf ?>">
</div>

<div id="picture3text_s5_iacf" style="z-index:1;height:auto; position: relative; padding:<?php echo $picture3spacing_s5_iacf ?>; color:#<?php echo $picture3textcolor_s5_iacf ?>; font-weight:<?php echo $picture3textweight_s5_iacf ?>; font-size:<?php echo $picture3textsize_s5_iacf ?>">
<?php echo $picture3text_s5_iacf ?>
</div>

<script type="text/javascript">
function set_picture3_loaders() {
document.getElementById("picture3_blank_s5_iacf").style.height = "<?php echo $height_s5_iacf ?>";
window.setTimeout('picture3text_load_bg_s5_iacf()',0);
window.setTimeout('picture3text_effect_big()',<?php echo $text_display_effect ?>);
window.setTimeout('picture3text_effect_small()',<?php echo $text_display_time_s5_iacf ?>);
}

function picture3text_load_bg_s5_iacf() {
document.getElementById("picture3text_s5_iacf").style.marginTop = (document.getElementById("picture3text_s5_iacf").offsetHeight * -1) + "px";
document.getElementById("picture3text_bg_s5_iacf").style.height = document.getElementById("picture3text_s5_iacf").offsetHeight + "px";
}

function picture3text_effect_big_timer() {
window.setTimeout('picture3text_effect_big()',10);
}

function picture3text_effect_big() {
var s5_outer_iacf = document.getElementById("s5_iacf_outer").offsetHeight;
if (document.getElementById("picture3_blank_s5_iacf").offsetHeight > s5_outer_iacf - document.getElementById("picture3text_s5_iacf").offsetHeight + 7) {
document.getElementById("picture3_blank_s5_iacf").style.height = document.getElementById("picture3_blank_s5_iacf").offsetHeight - s5_iacf_inc + "px";
picture3text_effect_big_timer();
}
else {
document.getElementById("picture3_blank_s5_iacf").style.height = document.getElementById("s5_iacf_outer").offsetHeight - document.getElementById("picture3text_s5_iacf").offsetHeight + "px";
}
}

function picture3text_effect_small_timer() {
window.setTimeout('picture3text_effect_small()',10);
}

</script>

<?php } ?>

</div>

<?php } ?>

<?php if ($picture4_s5_iacf != "") { ?>

<div id="picture4_s5_iacf" style="padding:0px; display:none; height:<?php echo $height_s5_iacf ?>; opacity:.0; <?php if ($iss_ie6_s5_iacf == "yes" || $iss_ie7_s5_iacf == "yes" || $iss_ie8_s5_iacf == "yes") { ?>filter: alpha(opacity=0); -moz-opacity: 0;<?php } ?> width:<?php echo $width_s5_iacf ?>; overflow:hidden; background-image: url(<?php echo $picture4_s5_iacf ?>); background-repeat: no-repeat">
<?php if ($picture4link_s5_iacf != "") { ?>
<a href="<?php echo $picture4link_s5_iacf ?>" target="<?php echo $picture4target_s5_iacf ?>">
<img alt="" style="border:none" id="picture4_blank_s5_iacf" src="modules/mod_s5_image_and_content_fader/blank.gif" height="<?php echo $height_s5_iacf ?>" width="<?php echo $width_s5_iacf ?>"></img>
</a>
<?php } ?>

<?php if ($picture4link_s5_iacf == "") { ?>
<img alt="" style="border:none" id="picture4_blank_s5_iacf" src="modules/mod_s5_image_and_content_fader/blank.gif" height="<?php echo $height_s5_iacf ?>" width="<?php echo $width_s5_iacf ?>"></img>
<?php } ?>

<?php if ($picture4text_s5_iacf != "") { ?>

<div id="picture4text_bg_s5_iacf" style="z-index:1;position: relative; opacity:<?php echo $non_ie_picture4textopac_s5_iacf ?>; <?php if ($iss_ie6_s5_iacf == "yes" || $iss_ie7_s5_iacf == "yes" || $iss_ie8_s5_iacf == "yes") { ?>filter: alpha(opacity=<?php echo $picture4textopac_s5_iacf ?>); <?php } ?> background:#<?php echo $picture4textbg_s5_iacf ?>">
</div>

<div id="picture4text_s5_iacf" style="z-index:1;height:auto; position: relative; padding:<?php echo $picture4spacing_s5_iacf ?>; color:#<?php echo $picture4textcolor_s5_iacf ?>; font-weight:<?php echo $picture4textweight_s5_iacf ?>; font-size:<?php echo $picture4textsize_s5_iacf ?>">
<?php echo $picture4text_s5_iacf ?>
</div>

<script type="text/javascript">
function set_picture4_loaders() {
document.getElementById("picture4_blank_s5_iacf").style.height = "<?php echo $height_s5_iacf ?>";
window.setTimeout('picture4text_load_bg_s5_iacf()',0);
window.setTimeout('picture4text_effect_big()',<?php echo $text_display_effect ?>);
window.setTimeout('picture4text_effect_small()',<?php echo $text_display_time_s5_iacf ?>);
}

function picture4text_load_bg_s5_iacf() {
document.getElementById("picture4text_s5_iacf").style.marginTop = (document.getElementById("picture4text_s5_iacf").offsetHeight * -1) + "px";
document.getElementById("picture4text_bg_s5_iacf").style.height = document.getElementById("picture4text_s5_iacf").offsetHeight + "px";
}

function picture4text_effect_big_timer() {
window.setTimeout('picture4text_effect_big()',10);
}

function picture4text_effect_big() {
var s5_outer_iacf = document.getElementById("s5_iacf_outer").offsetHeight;
if (document.getElementById("picture4_blank_s5_iacf").offsetHeight > s5_outer_iacf - document.getElementById("picture4text_s5_iacf").offsetHeight + 7) {
document.getElementById("picture4_blank_s5_iacf").style.height = document.getElementById("picture4_blank_s5_iacf").offsetHeight - s5_iacf_inc + "px";
picture4text_effect_big_timer();
}
else {
document.getElementById("picture4_blank_s5_iacf").style.height = document.getElementById("s5_iacf_outer").offsetHeight - document.getElementById("picture4text_s5_iacf").offsetHeight + "px";
}
}

function picture4text_effect_small_timer() {
window.setTimeout('picture4text_effect_small()',10);
}

</script>

<?php } ?>

</div>

<?php } ?>

<?php if ($picture5_s5_iacf != "") { ?>

<div id="picture5_s5_iacf" style="padding:0px; display:none; height:<?php echo $height_s5_iacf ?>; opacity:.0; <?php if ($iss_ie6_s5_iacf == "yes" || $iss_ie7_s5_iacf == "yes" || $iss_ie8_s5_iacf == "yes") { ?>filter: alpha(opacity=0); -moz-opacity: 0;<?php } ?> width:<?php echo $width_s5_iacf ?>; overflow:hidden; background-image: url(<?php echo $picture5_s5_iacf ?>); background-repeat: no-repeat">
<?php if ($picture5link_s5_iacf != "") { ?>
<a href="<?php echo $picture5link_s5_iacf ?>" target="<?php echo $picture5target_s5_iacf ?>">
<img alt="" style="border:none" id="picture5_blank_s5_iacf" src="modules/mod_s5_image_and_content_fader/blank.gif" height="<?php echo $height_s5_iacf ?>" width="<?php echo $width_s5_iacf ?>"></img>
</a>
<?php } ?>

<?php if ($picture5link_s5_iacf == "") { ?>
<img alt="" style="border:none" id="picture5_blank_s5_iacf" src="modules/mod_s5_image_and_content_fader/blank.gif" height="<?php echo $height_s5_iacf ?>" width="<?php echo $width_s5_iacf ?>"></img>
<?php } ?>

<?php if ($picture5text_s5_iacf != "") { ?>

<div id="picture5text_bg_s5_iacf" style="z-index:1;position: relative; opacity:<?php echo $non_ie_picture5textopac_s5_iacf ?>; <?php if ($iss_ie6_s5_iacf == "yes" || $iss_ie7_s5_iacf == "yes" || $iss_ie8_s5_iacf == "yes") { ?>filter: alpha(opacity=<?php echo $picture5textopac_s5_iacf ?>); <?php } ?> background:#<?php echo $picture5textbg_s5_iacf ?>">
</div>

<div id="picture5text_s5_iacf" style="z-index:1;height:auto; position: relative; padding:<?php echo $picture5spacing_s5_iacf ?>; color:#<?php echo $picture5textcolor_s5_iacf ?>; font-weight:<?php echo $picture5textweight_s5_iacf ?>; font-size:<?php echo $picture5textsize_s5_iacf ?>">
<?php echo $picture5text_s5_iacf ?>
</div>

<script type="text/javascript">
function set_picture5_loaders() {
document.getElementById("picture5_blank_s5_iacf").style.height = "<?php echo $height_s5_iacf ?>";
window.setTimeout('picture5text_load_bg_s5_iacf()',0);
window.setTimeout('picture5text_effect_big()',<?php echo $text_display_effect ?>);
window.setTimeout('picture5text_effect_small()',<?php echo $text_display_time_s5_iacf ?>);
}

function picture5text_load_bg_s5_iacf() {
document.getElementById("picture5text_s5_iacf").style.marginTop = (document.getElementById("picture5text_s5_iacf").offsetHeight * -1) + "px";
document.getElementById("picture5text_bg_s5_iacf").style.height = document.getElementById("picture5text_s5_iacf").offsetHeight + "px";
}

function picture5text_effect_big_timer() {
window.setTimeout('picture5text_effect_big()',10);
}

function picture5text_effect_big() {
var s5_outer_iacf = document.getElementById("s5_iacf_outer").offsetHeight;
if (document.getElementById("picture5_blank_s5_iacf").offsetHeight > s5_outer_iacf - document.getElementById("picture5text_s5_iacf").offsetHeight + 7) {
document.getElementById("picture5_blank_s5_iacf").style.height = document.getElementById("picture5_blank_s5_iacf").offsetHeight - s5_iacf_inc + "px";
picture5text_effect_big_timer();
}
else {
document.getElementById("picture5_blank_s5_iacf").style.height = document.getElementById("s5_iacf_outer").offsetHeight - document.getElementById("picture5text_s5_iacf").offsetHeight + "px";
}
}

function picture5text_effect_small_timer() {
window.setTimeout('picture5text_effect_small()',10);
}

</script>

<?php } ?>

</div>

<?php } ?>

<?php if ($picture6_s5_iacf != "") { ?>

<div id="picture6_s5_iacf" style="padding:0px; display:none; height:<?php echo $height_s5_iacf ?>; opacity:.0; <?php if ($iss_ie6_s5_iacf == "yes" || $iss_ie7_s5_iacf == "yes" || $iss_ie8_s5_iacf == "yes") { ?>filter: alpha(opacity=0); -moz-opacity: 0;<?php } ?> width:<?php echo $width_s5_iacf ?>; overflow:hidden; background-image: url(<?php echo $picture6_s5_iacf ?>); background-repeat: no-repeat">
<?php if ($picture6link_s5_iacf != "") { ?>
<a href="<?php echo $picture6link_s5_iacf ?>" target="<?php echo $picture6target_s5_iacf ?>">
<img alt="" style="border:none" id="picture6_blank_s5_iacf" src="modules/mod_s5_image_and_content_fader/blank.gif" height="<?php echo $height_s5_iacf ?>" width="<?php echo $width_s5_iacf ?>"></img>
</a>
<?php } ?>

<?php if ($picture6link_s5_iacf == "") { ?>
<img alt="" style="border:none" id="picture6_blank_s5_iacf" src="modules/mod_s5_image_and_content_fader/blank.gif" height="<?php echo $height_s5_iacf ?>" width="<?php echo $width_s5_iacf ?>"></img>
<?php } ?>

<?php if ($picture6text_s5_iacf != "") { ?>

<div id="picture6text_bg_s5_iacf" style="z-index:1;position: relative; opacity:<?php echo $non_ie_picture6textopac_s5_iacf ?>; <?php if ($iss_ie6_s5_iacf == "yes" || $iss_ie7_s5_iacf == "yes" || $iss_ie8_s5_iacf == "yes") { ?>filter: alpha(opacity=<?php echo $picture6textopac_s5_iacf ?>); <?php } ?> background:#<?php echo $picture6textbg_s5_iacf ?>">
</div>

<div id="picture6text_s5_iacf" style="z-index:1;height:auto; position: relative; padding:<?php echo $picture6spacing_s5_iacf ?>; color:#<?php echo $picture6textcolor_s5_iacf ?>; font-weight:<?php echo $picture6textweight_s5_iacf ?>; font-size:<?php echo $picture6textsize_s5_iacf ?>">
<?php echo $picture6text_s5_iacf ?>
</div>

<script type="text/javascript">
function set_picture6_loaders() {
document.getElementById("picture6_blank_s5_iacf").style.height = "<?php echo $height_s5_iacf ?>";
window.setTimeout('picture6text_load_bg_s5_iacf()',0);
window.setTimeout('picture6text_effect_big()',<?php echo $text_display_effect ?>);
window.setTimeout('picture6text_effect_small()',<?php echo $text_display_time_s5_iacf ?>);
}

function picture6text_load_bg_s5_iacf() {
document.getElementById("picture6text_s5_iacf").style.marginTop = (document.getElementById("picture6text_s5_iacf").offsetHeight * -1) + "px";
document.getElementById("picture6text_bg_s5_iacf").style.height = document.getElementById("picture6text_s5_iacf").offsetHeight + "px";
}

function picture6text_effect_big_timer() {
window.setTimeout('picture6text_effect_big()',10);
}

function picture6text_effect_big() {
var s5_outer_iacf = document.getElementById("s5_iacf_outer").offsetHeight;
if (document.getElementById("picture6_blank_s5_iacf").offsetHeight > s5_outer_iacf - document.getElementById("picture6text_s5_iacf").offsetHeight + 7) {
document.getElementById("picture6_blank_s5_iacf").style.height = document.getElementById("picture6_blank_s5_iacf").offsetHeight - s5_iacf_inc + "px";
picture6text_effect_big_timer();
}
else {
document.getElementById("picture6_blank_s5_iacf").style.height = document.getElementById("s5_iacf_outer").offsetHeight - document.getElementById("picture6text_s5_iacf").offsetHeight + "px";
}
}

function picture6text_effect_small_timer() {
window.setTimeout('picture6text_effect_small()',10);
}

</script>

<?php } ?>

</div>

<?php } ?>

<?php if ($picture7_s5_iacf != "") { ?>

<div id="picture7_s5_iacf" style="padding:0px; display:none; height:<?php echo $height_s5_iacf ?>; opacity:.0; <?php if ($iss_ie6_s5_iacf == "yes" || $iss_ie7_s5_iacf == "yes" || $iss_ie8_s5_iacf == "yes") { ?>filter: alpha(opacity=0); -moz-opacity: 0;<?php } ?> width:<?php echo $width_s5_iacf ?>; overflow:hidden; background-image: url(<?php echo $picture7_s5_iacf ?>); background-repeat: no-repeat">
<?php if ($picture7link_s5_iacf != "") { ?>
<a href="<?php echo $picture7link_s5_iacf ?>" target="<?php echo $picture7target_s5_iacf ?>">
<img alt="" style="border:none" id="picture7_blank_s5_iacf" src="modules/mod_s5_image_and_content_fader/blank.gif" height="<?php echo $height_s5_iacf ?>" width="<?php echo $width_s5_iacf ?>"></img>
</a>
<?php } ?>

<?php if ($picture7link_s5_iacf == "") { ?>
<img alt="" style="border:none" id="picture7_blank_s5_iacf" src="modules/mod_s5_image_and_content_fader/blank.gif" height="<?php echo $height_s5_iacf ?>" width="<?php echo $width_s5_iacf ?>"></img>
<?php } ?>

<?php if ($picture7text_s5_iacf != "") { ?>

<div id="picture7text_bg_s5_iacf" style="z-index:1;position: relative; opacity:<?php echo $non_ie_picture7textopac_s5_iacf ?>; <?php if ($iss_ie6_s5_iacf == "yes" || $iss_ie7_s5_iacf == "yes" || $iss_ie8_s5_iacf == "yes") { ?>filter: alpha(opacity=<?php echo $picture7textopac_s5_iacf ?>); <?php } ?> background:#<?php echo $picture7textbg_s5_iacf ?>">
</div>

<div id="picture7text_s5_iacf" style="z-index:1;height:auto; position: relative; padding:<?php echo $picture7spacing_s5_iacf ?>; color:#<?php echo $picture7textcolor_s5_iacf ?>; font-weight:<?php echo $picture7textweight_s5_iacf ?>; font-size:<?php echo $picture7textsize_s5_iacf ?>">
<?php echo $picture7text_s5_iacf ?>
</div>

<script type="text/javascript">
function set_picture7_loaders() {
document.getElementById("picture7_blank_s5_iacf").style.height = "<?php echo $height_s5_iacf ?>";
window.setTimeout('picture7text_load_bg_s5_iacf()',0);
window.setTimeout('picture7text_effect_big()',<?php echo $text_display_effect ?>);
window.setTimeout('picture7text_effect_small()',<?php echo $text_display_time_s5_iacf ?>);
}

function picture7text_load_bg_s5_iacf() {
document.getElementById("picture7text_s5_iacf").style.marginTop = (document.getElementById("picture7text_s5_iacf").offsetHeight * -1) + "px";
document.getElementById("picture7text_bg_s5_iacf").style.height = document.getElementById("picture7text_s5_iacf").offsetHeight + "px";
}

function picture7text_effect_big_timer() {
window.setTimeout('picture7text_effect_big()',10);
}

function picture7text_effect_big() {
var s5_outer_iacf = document.getElementById("s5_iacf_outer").offsetHeight;
if (document.getElementById("picture7_blank_s5_iacf").offsetHeight > s5_outer_iacf - document.getElementById("picture7text_s5_iacf").offsetHeight + 7) {
document.getElementById("picture7_blank_s5_iacf").style.height = document.getElementById("picture7_blank_s5_iacf").offsetHeight - s5_iacf_inc + "px";
picture7text_effect_big_timer();
}
else {
document.getElementById("picture7_blank_s5_iacf").style.height = document.getElementById("s5_iacf_outer").offsetHeight - document.getElementById("picture7text_s5_iacf").offsetHeight + "px";
}
}

function picture7text_effect_small_timer() {
window.setTimeout('picture7text_effect_small()',10);
}

</script>

<?php } ?>

</div>

<?php } ?>

<?php if ($picture8_s5_iacf != "") { ?>

<div id="picture8_s5_iacf" style="padding:0px; display:none; height:<?php echo $height_s5_iacf ?>; opacity:.0; <?php if ($iss_ie6_s5_iacf == "yes" || $iss_ie7_s5_iacf == "yes" || $iss_ie8_s5_iacf == "yes") { ?>filter: alpha(opacity=0); -moz-opacity: 0;<?php } ?> width:<?php echo $width_s5_iacf ?>; overflow:hidden; background-image: url(<?php echo $picture8_s5_iacf ?>); background-repeat: no-repeat">
<?php if ($picture8link_s5_iacf != "") { ?>
<a href="<?php echo $picture8link_s5_iacf ?>" target="<?php echo $picture8target_s5_iacf ?>">
<img alt="" style="border:none" id="picture8_blank_s5_iacf" src="modules/mod_s5_image_and_content_fader/blank.gif" height="<?php echo $height_s5_iacf ?>" width="<?php echo $width_s5_iacf ?>"></img>
</a>
<?php } ?>

<?php if ($picture8link_s5_iacf == "") { ?>
<img alt="" style="border:none" id="picture8_blank_s5_iacf" src="modules/mod_s5_image_and_content_fader/blank.gif" height="<?php echo $height_s5_iacf ?>" width="<?php echo $width_s5_iacf ?>"></img>
<?php } ?>

<?php if ($picture8text_s5_iacf != "") { ?>

<div id="picture8text_bg_s5_iacf" style="z-index:1;position: relative; opacity:<?php echo $non_ie_picture8textopac_s5_iacf ?>; <?php if ($iss_ie6_s5_iacf == "yes" || $iss_ie7_s5_iacf == "yes" || $iss_ie8_s5_iacf == "yes") { ?>filter: alpha(opacity=<?php echo $picture8textopac_s5_iacf ?>); <?php } ?> background:#<?php echo $picture8textbg_s5_iacf ?>">
</div>

<div id="picture8text_s5_iacf" style="z-index:1;height:auto; position: relative; padding:<?php echo $picture8spacing_s5_iacf ?>; color:#<?php echo $picture8textcolor_s5_iacf ?>; font-weight:<?php echo $picture8textweight_s5_iacf ?>; font-size:<?php echo $picture8textsize_s5_iacf ?>">
<?php echo $picture8text_s5_iacf ?>
</div>

<script type="text/javascript">
function set_picture8_loaders() {
document.getElementById("picture8_blank_s5_iacf").style.height = "<?php echo $height_s5_iacf ?>";
window.setTimeout('picture8text_load_bg_s5_iacf()',0);
window.setTimeout('picture8text_effect_big()',<?php echo $text_display_effect ?>);
window.setTimeout('picture8text_effect_small()',<?php echo $text_display_time_s5_iacf ?>);
}

function picture8text_load_bg_s5_iacf() {
document.getElementById("picture8text_s5_iacf").style.marginTop = (document.getElementById("picture8text_s5_iacf").offsetHeight * -1) + "px";
document.getElementById("picture8text_bg_s5_iacf").style.height = document.getElementById("picture8text_s5_iacf").offsetHeight + "px";
}

function picture8text_effect_big_timer() {
window.setTimeout('picture8text_effect_big()',10);
}

function picture8text_effect_big() {
var s5_outer_iacf = document.getElementById("s5_iacf_outer").offsetHeight;
if (document.getElementById("picture8_blank_s5_iacf").offsetHeight > s5_outer_iacf - document.getElementById("picture8text_s5_iacf").offsetHeight + 7) {
document.getElementById("picture8_blank_s5_iacf").style.height = document.getElementById("picture8_blank_s5_iacf").offsetHeight - s5_iacf_inc + "px";
picture8text_effect_big_timer();
}
else {
document.getElementById("picture8_blank_s5_iacf").style.height = document.getElementById("s5_iacf_outer").offsetHeight - document.getElementById("picture8text_s5_iacf").offsetHeight + "px";
}
}

function picture8text_effect_small_timer() {
window.setTimeout('picture8text_effect_small()',10);
}

</script>

<?php } ?>

</div>

<?php } ?>

<?php if ($picture9_s5_iacf != "") { ?>

<div id="picture9_s5_iacf" style="padding:0px; display:none; height:<?php echo $height_s5_iacf ?>; opacity:.0; <?php if ($iss_ie6_s5_iacf == "yes" || $iss_ie7_s5_iacf == "yes" || $iss_ie8_s5_iacf == "yes") { ?>filter: alpha(opacity=0); -moz-opacity: 0;<?php } ?> width:<?php echo $width_s5_iacf ?>; overflow:hidden; background-image: url(<?php echo $picture9_s5_iacf ?>); background-repeat: no-repeat">
<?php if ($picture9link_s5_iacf != "") { ?>
<a href="<?php echo $picture9link_s5_iacf ?>" target="<?php echo $picture9target_s5_iacf ?>">
<img alt="" style="border:none" id="picture9_blank_s5_iacf" src="modules/mod_s5_image_and_content_fader/blank.gif" height="<?php echo $height_s5_iacf ?>" width="<?php echo $width_s5_iacf ?>"></img>
</a>
<?php } ?>

<?php if ($picture9link_s5_iacf == "") { ?>
<img alt="" style="border:none" id="picture9_blank_s5_iacf" src="modules/mod_s5_image_and_content_fader/blank.gif" height="<?php echo $height_s5_iacf ?>" width="<?php echo $width_s5_iacf ?>"></img>
<?php } ?>

<?php if ($picture9text_s5_iacf != "") { ?>

<div id="picture9text_bg_s5_iacf" style="z-index:1;position: relative; opacity:<?php echo $non_ie_picture9textopac_s5_iacf ?>; <?php if ($iss_ie6_s5_iacf == "yes" || $iss_ie7_s5_iacf == "yes" || $iss_ie8_s5_iacf == "yes") { ?>filter: alpha(opacity=<?php echo $picture9textopac_s5_iacf ?>); <?php } ?> background:#<?php echo $picture9textbg_s5_iacf ?>">
</div>

<div id="picture9text_s5_iacf" style="z-index:1;height:auto; position: relative; padding:<?php echo $picture9spacing_s5_iacf ?>; color:#<?php echo $picture9textcolor_s5_iacf ?>; font-weight:<?php echo $picture9textweight_s5_iacf ?>; font-size:<?php echo $picture9textsize_s5_iacf ?>">
<?php echo $picture9text_s5_iacf ?>
</div>

<script type="text/javascript">
function set_picture9_loaders() {
document.getElementById("picture9_blank_s5_iacf").style.height = "<?php echo $height_s5_iacf ?>";
window.setTimeout('picture9text_load_bg_s5_iacf()',0);
window.setTimeout('picture9text_effect_big()',<?php echo $text_display_effect ?>);
window.setTimeout('picture9text_effect_small()',<?php echo $text_display_time_s5_iacf ?>);
}

function picture9text_load_bg_s5_iacf() {
document.getElementById("picture9text_s5_iacf").style.marginTop = (document.getElementById("picture9text_s5_iacf").offsetHeight * -1) + "px";
document.getElementById("picture9text_bg_s5_iacf").style.height = document.getElementById("picture9text_s5_iacf").offsetHeight + "px";
}

function picture9text_effect_big_timer() {
window.setTimeout('picture9text_effect_big()',10);
}

function picture9text_effect_big() {
var s5_outer_iacf = document.getElementById("s5_iacf_outer").offsetHeight;
if (document.getElementById("picture9_blank_s5_iacf").offsetHeight > s5_outer_iacf - document.getElementById("picture9text_s5_iacf").offsetHeight + 7) {
document.getElementById("picture9_blank_s5_iacf").style.height = document.getElementById("picture9_blank_s5_iacf").offsetHeight - s5_iacf_inc + "px";
picture9text_effect_big_timer();
}
else {
document.getElementById("picture9_blank_s5_iacf").style.height = document.getElementById("s5_iacf_outer").offsetHeight - document.getElementById("picture9text_s5_iacf").offsetHeight + "px";
}
}

function picture9text_effect_small_timer() {
window.setTimeout('picture9text_effect_small()',10);
}

</script>

<?php } ?>

</div>

<?php } ?>

<?php if ($picture10_s5_iacf != "") { ?>

<div id="picture10_s5_iacf" style="padding:0px; display:none; height:<?php echo $height_s5_iacf ?>; opacity:.0; <?php if ($iss_ie6_s5_iacf == "yes" || $iss_ie7_s5_iacf == "yes" || $iss_ie8_s5_iacf == "yes") { ?>filter: alpha(opacity=0); -moz-opacity: 0;<?php } ?> width:<?php echo $width_s5_iacf ?>; overflow:hidden; background-image: url(<?php echo $picture10_s5_iacf ?>); background-repeat: no-repeat">
<?php if ($picture10link_s5_iacf != "") { ?>
<a href="<?php echo $picture10link_s5_iacf ?>" target="<?php echo $picture10target_s5_iacf ?>">
<img alt="" style="border:none" id="picture10_blank_s5_iacf" src="modules/mod_s5_image_and_content_fader/blank.gif" height="<?php echo $height_s5_iacf ?>" width="<?php echo $width_s5_iacf ?>"></img>
</a>
<?php } ?>

<?php if ($picture10link_s5_iacf == "") { ?>
<img alt="" style="border:none" id="picture10_blank_s5_iacf" src="modules/mod_s5_image_and_content_fader/blank.gif" height="<?php echo $height_s5_iacf ?>" width="<?php echo $width_s5_iacf ?>"></img>
<?php } ?>

<?php if ($picture10text_s5_iacf != "") { ?>

<div id="picture10text_bg_s5_iacf" style="z-index:1;position: relative; opacity:<?php echo $non_ie_picture10textopac_s5_iacf ?>; <?php if ($iss_ie6_s5_iacf == "yes" || $iss_ie7_s5_iacf == "yes" || $iss_ie8_s5_iacf == "yes") { ?>filter: alpha(opacity=<?php echo $picture10textopac_s5_iacf ?>); <?php } ?> background:#<?php echo $picture10textbg_s5_iacf ?>">
</div>

<div id="picture10text_s5_iacf" style="z-index:1;height:auto; position: relative; padding:<?php echo $picture10spacing_s5_iacf ?>; color:#<?php echo $picture10textcolor_s5_iacf ?>; font-weight:<?php echo $picture10textweight_s5_iacf ?>; font-size:<?php echo $picture10textsize_s5_iacf ?>">
<?php echo $picture10text_s5_iacf ?>
</div>

<script type="text/javascript">
function set_picture10_loaders() {
document.getElementById("picture10_blank_s5_iacf").style.height = "<?php echo $height_s5_iacf ?>";
window.setTimeout('picture10text_load_bg_s5_iacf()',0);
window.setTimeout('picture10text_effect_big()',<?php echo $text_display_effect ?>);
window.setTimeout('picture10text_effect_small()',<?php echo $text_display_time_s5_iacf ?>);
}

function picture10text_load_bg_s5_iacf() {
document.getElementById("picture10text_s5_iacf").style.marginTop = (document.getElementById("picture10text_s5_iacf").offsetHeight * -1) + "px";
document.getElementById("picture10text_bg_s5_iacf").style.height = document.getElementById("picture10text_s5_iacf").offsetHeight + "px";
}

function picture10text_effect_big_timer() {
window.setTimeout('picture10text_effect_big()',10);
}

function picture10text_effect_big() {
var s5_outer_iacf = document.getElementById("s5_iacf_outer").offsetHeight;
if (document.getElementById("picture10_blank_s5_iacf").offsetHeight > s5_outer_iacf - document.getElementById("picture10text_s5_iacf").offsetHeight + 7) {
document.getElementById("picture10_blank_s5_iacf").style.height = document.getElementById("picture10_blank_s5_iacf").offsetHeight - s5_iacf_inc + "px";
picture10text_effect_big_timer();
}
else {
document.getElementById("picture10_blank_s5_iacf").style.height = document.getElementById("s5_iacf_outer").offsetHeight - document.getElementById("picture10text_s5_iacf").offsetHeight + "px";
}
}

function picture10text_effect_small_timer() {
window.setTimeout('picture10text_effect_small()',10);
}

</script>

<?php } ?>

</div>

<?php } ?>


</div>

</div>

<script language="javascript" type="text/javascript" src="modules/mod_s5_image_and_content_fader/fader.js"></script>

<script type="text/javascript">


function picture1_s5_iacf(id_s5_iacf) {
        document.getElementById(id_s5_iacf).style.display = "block";
	opacity_s5_iacf(id_s5_iacf, 0, 100, <?php echo $tween_time_s5_iacf ?>);
        window.setTimeout('picture1_done_s5_iacf()',<?php echo $display_time_s5_iacf ?>);
}

function picture1_done_s5_iacf(){
	picture1_doneload_s5_iacf('picture1_s5_iacf');
}

function picture1_doneload_s5_iacf(id_s5_iacf) {
	opacity_s5_iacf(id_s5_iacf, 100, 0, <?php echo $tween_time_s5_iacf ?>);
        window.setTimeout('picture1_next_s5_iacf()',<?php echo $tween_time_s5_iacf ?>);
}

function picture1_next_s5_iacf(id_s5_iacf) {
        document.getElementById('picture1_s5_iacf').style.display = "none";
	if (document.getElementById('picture2_s5_iacf')) {
		picture2_s5_iacf('picture2_s5_iacf');
		<?php if ($picture2text_s5_iacf != "") { ?>
		set_picture2_loaders();
		<?php } ?>
	}
	else {
		picture1_s5_iacf('picture1_s5_iacf');
		<?php if ($picture1text_s5_iacf != "") { ?>
		set_picture1_loaders();
		<?php } ?>
	}
}


function picture2_s5_iacf(id_s5_iacf) {
        document.getElementById(id_s5_iacf).style.display = "block";
	opacity_s5_iacf(id_s5_iacf, 0, 100, <?php echo $tween_time_s5_iacf ?>);
        window.setTimeout('picture2_done_s5_iacf()',<?php echo $display_time_s5_iacf ?>);
}

function picture2_done_s5_iacf(){
	picture2_doneload_s5_iacf('picture2_s5_iacf');
}

function picture2_doneload_s5_iacf(id_s5_iacf) {
	opacity_s5_iacf(id_s5_iacf, 100, 0, <?php echo $tween_time_s5_iacf ?>);
        window.setTimeout('picture2_next_s5_iacf()',<?php echo $tween_time_s5_iacf ?>);
}

function picture2_next_s5_iacf(id_s5_iacf) {
        document.getElementById('picture2_s5_iacf').style.display = "none";
	if (document.getElementById('picture3_s5_iacf')) {
		picture3_s5_iacf('picture3_s5_iacf');
		<?php if ($picture3text_s5_iacf != "") { ?>
		set_picture3_loaders();
		<?php } ?>
	}
	else {
		picture1_s5_iacf('picture1_s5_iacf');
		<?php if ($picture1text_s5_iacf != "") { ?>
		set_picture1_loaders();
		<?php } ?>
	}
}


function picture3_s5_iacf(id_s5_iacf) {
        document.getElementById(id_s5_iacf).style.display = "block";
	opacity_s5_iacf(id_s5_iacf, 0, 100, <?php echo $tween_time_s5_iacf ?>);
        window.setTimeout('picture3_done_s5_iacf()',<?php echo $display_time_s5_iacf ?>);
}

function picture3_done_s5_iacf(){
	picture3_doneload_s5_iacf('picture3_s5_iacf');
}

function picture3_doneload_s5_iacf(id_s5_iacf) {
	opacity_s5_iacf(id_s5_iacf, 100, 0, <?php echo $tween_time_s5_iacf ?>);
        window.setTimeout('picture3_next_s5_iacf()',<?php echo $tween_time_s5_iacf ?>);
}

function picture3_next_s5_iacf(id_s5_iacf) {
        document.getElementById('picture3_s5_iacf').style.display = "none";
	if (document.getElementById('picture4_s5_iacf')) {
		picture4_s5_iacf('picture4_s5_iacf');
		<?php if ($picture4text_s5_iacf != "") { ?>
		set_picture4_loaders();
		<?php } ?>
	}
	else {
		picture1_s5_iacf('picture1_s5_iacf');
		<?php if ($picture1text_s5_iacf != "") { ?>
		set_picture1_loaders();
		<?php } ?>
	}
}


function picture4_s5_iacf(id_s5_iacf) {
        document.getElementById(id_s5_iacf).style.display = "block";
	opacity_s5_iacf(id_s5_iacf, 0, 100, <?php echo $tween_time_s5_iacf ?>);
        window.setTimeout('picture4_done_s5_iacf()',<?php echo $display_time_s5_iacf ?>);
}

function picture4_done_s5_iacf(){
	picture4_doneload_s5_iacf('picture4_s5_iacf');
}

function picture4_doneload_s5_iacf(id_s5_iacf) {
	opacity_s5_iacf(id_s5_iacf, 100, 0, <?php echo $tween_time_s5_iacf ?>);
        window.setTimeout('picture4_next_s5_iacf()',<?php echo $tween_time_s5_iacf ?>);
}

function picture4_next_s5_iacf(id_s5_iacf) {
        document.getElementById('picture4_s5_iacf').style.display = "none";
	if (document.getElementById('picture5_s5_iacf')) {
		picture5_s5_iacf('picture5_s5_iacf');
		<?php if ($picture5text_s5_iacf != "") { ?>
		set_picture5_loaders();
		<?php } ?>
	}
	else {
		picture1_s5_iacf('picture1_s5_iacf');
		<?php if ($picture1text_s5_iacf != "") { ?>
		set_picture1_loaders();
		<?php } ?>
	}
}


function picture5_s5_iacf(id_s5_iacf) {
        document.getElementById(id_s5_iacf).style.display = "block";
	opacity_s5_iacf(id_s5_iacf, 0, 100, <?php echo $tween_time_s5_iacf ?>);
        window.setTimeout('picture5_done_s5_iacf()',<?php echo $display_time_s5_iacf ?>);
}

function picture5_done_s5_iacf(){
	picture5_doneload_s5_iacf('picture5_s5_iacf');
}

function picture5_doneload_s5_iacf(id_s5_iacf) {
	opacity_s5_iacf(id_s5_iacf, 100, 0, <?php echo $tween_time_s5_iacf ?>);
        window.setTimeout('picture5_next_s5_iacf()',<?php echo $tween_time_s5_iacf ?>);
}

function picture5_next_s5_iacf(id_s5_iacf) {
        document.getElementById('picture5_s5_iacf').style.display = "none";
	if (document.getElementById('picture6_s5_iacf')) {
		picture6_s5_iacf('picture6_s5_iacf');
		<?php if ($picture6text_s5_iacf != "") { ?>
		set_picture6_loaders();
		<?php } ?>
	}
	else {
		picture1_s5_iacf('picture1_s5_iacf');
		<?php if ($picture1text_s5_iacf != "") { ?>
		set_picture1_loaders();
		<?php } ?>
	}
}


function picture6_s5_iacf(id_s5_iacf) {
        document.getElementById(id_s5_iacf).style.display = "block";
	opacity_s5_iacf(id_s5_iacf, 0, 100, <?php echo $tween_time_s5_iacf ?>);
        window.setTimeout('picture6_done_s5_iacf()',<?php echo $display_time_s5_iacf ?>);
}

function picture6_done_s5_iacf(){
	picture6_doneload_s5_iacf('picture6_s5_iacf');
}

function picture6_doneload_s5_iacf(id_s5_iacf) {
	opacity_s5_iacf(id_s5_iacf, 100, 0, <?php echo $tween_time_s5_iacf ?>);
        window.setTimeout('picture6_next_s5_iacf()',<?php echo $tween_time_s5_iacf ?>);
}

function picture6_next_s5_iacf(id_s5_iacf) {
        document.getElementById('picture6_s5_iacf').style.display = "none";
	if (document.getElementById('picture7_s5_iacf')) {
		picture7_s5_iacf('picture7_s5_iacf');
		<?php if ($picture7text_s5_iacf != "") { ?>
		set_picture7_loaders();
		<?php } ?>
	}
	else {
		picture1_s5_iacf('picture1_s5_iacf');
		<?php if ($picture1text_s5_iacf != "") { ?>
		set_picture1_loaders();
		<?php } ?>
	}
}


function picture7_s5_iacf(id_s5_iacf) {
        document.getElementById(id_s5_iacf).style.display = "block";
	opacity_s5_iacf(id_s5_iacf, 0, 100, <?php echo $tween_time_s5_iacf ?>);
        window.setTimeout('picture7_done_s5_iacf()',<?php echo $display_time_s5_iacf ?>);
}

function picture7_done_s5_iacf(){
	picture7_doneload_s5_iacf('picture7_s5_iacf');
}

function picture7_doneload_s5_iacf(id_s5_iacf) {
	opacity_s5_iacf(id_s5_iacf, 100, 0, <?php echo $tween_time_s5_iacf ?>);
        window.setTimeout('picture7_next_s5_iacf()',<?php echo $tween_time_s5_iacf ?>);
}

function picture7_next_s5_iacf(id_s5_iacf) {
        document.getElementById('picture7_s5_iacf').style.display = "none";
	if (document.getElementById('picture8_s5_iacf')) {
		picture8_s5_iacf('picture8_s5_iacf');
		<?php if ($picture8text_s5_iacf != "") { ?>
		set_picture8_loaders();
		<?php } ?>
	}
	else {
		picture1_s5_iacf('picture1_s5_iacf');
		<?php if ($picture1text_s5_iacf != "") { ?>
		set_picture1_loaders();
		<?php } ?>
	}
}


function picture8_s5_iacf(id_s5_iacf) {
        document.getElementById(id_s5_iacf).style.display = "block";
	opacity_s5_iacf(id_s5_iacf, 0, 100, <?php echo $tween_time_s5_iacf ?>);
        window.setTimeout('picture8_done_s5_iacf()',<?php echo $display_time_s5_iacf ?>);
}

function picture8_done_s5_iacf(){
	picture8_doneload_s5_iacf('picture8_s5_iacf');
}

function picture8_doneload_s5_iacf(id_s5_iacf) {
	opacity_s5_iacf(id_s5_iacf, 100, 0, <?php echo $tween_time_s5_iacf ?>);
        window.setTimeout('picture8_next_s5_iacf()',<?php echo $tween_time_s5_iacf ?>);
}

function picture8_next_s5_iacf(id_s5_iacf) {
        document.getElementById('picture8_s5_iacf').style.display = "none";
	if (document.getElementById('picture9_s5_iacf')) {
		picture9_s5_iacf('picture9_s5_iacf');
		<?php if ($picture9text_s5_iacf != "") { ?>
		set_picture9_loaders();
		<?php } ?>
	}
	else {
		picture1_s5_iacf('picture1_s5_iacf');
		<?php if ($picture1text_s5_iacf != "") { ?>
		set_picture1_loaders();
		<?php } ?>
	}
}


function picture9_s5_iacf(id_s5_iacf) {
        document.getElementById(id_s5_iacf).style.display = "block";
	opacity_s5_iacf(id_s5_iacf, 0, 100, <?php echo $tween_time_s5_iacf ?>);
        window.setTimeout('picture9_done_s5_iacf()',<?php echo $display_time_s5_iacf ?>);
}

function picture9_done_s5_iacf(){
	picture9_doneload_s5_iacf('picture9_s5_iacf');
}

function picture9_doneload_s5_iacf(id_s5_iacf) {
	opacity_s5_iacf(id_s5_iacf, 100, 0, <?php echo $tween_time_s5_iacf ?>);
        window.setTimeout('picture9_next_s5_iacf()',<?php echo $tween_time_s5_iacf ?>);
}

function picture9_next_s5_iacf(id_s5_iacf) {
        document.getElementById('picture9_s5_iacf').style.display = "none";
	if (document.getElementById('picture10_s5_iacf')) {
		picture10_s5_iacf('picture10_s5_iacf');
		<?php if ($picture10text_s5_iacf != "") { ?>
		set_picture10_loaders();
		<?php } ?>
	}
	else {
		picture1_s5_iacf('picture1_s5_iacf');
		<?php if ($picture1text_s5_iacf != "") { ?>
		set_picture1_loaders();
		<?php } ?>
	}
}


function picture10_s5_iacf(id_s5_iacf) {
        document.getElementById(id_s5_iacf).style.display = "block";
	opacity_s5_iacf(id_s5_iacf, 0, 100, <?php echo $tween_time_s5_iacf ?>);
        window.setTimeout('picture10_done_s5_iacf()',<?php echo $display_time_s5_iacf ?>);
}

function picture10_done_s5_iacf(){
	picture10_doneload_s5_iacf('picture10_s5_iacf');
}

function picture10_doneload_s5_iacf(id_s5_iacf) {
	opacity_s5_iacf(id_s5_iacf, 100, 0, <?php echo $tween_time_s5_iacf ?>);
        window.setTimeout('picture10_next_s5_iacf()',<?php echo $tween_time_s5_iacf ?>);
}

function picture10_next_s5_iacf(id_s5_iacf) {
    document.getElementById('picture10_s5_iacf').style.display = "none";
	picture1_s5_iacf('picture1_s5_iacf');
	<?php if ($picture1text_s5_iacf != "") { ?>
	set_picture1_loaders();
	<?php } ?>
}

picture1_s5_iacf('picture1_s5_iacf');
<?php if ($picture1text_s5_iacf != "") { ?>
set_picture1_loaders();
<?php } ?>

</script>