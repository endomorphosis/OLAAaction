<?php
/**
 * @version		2.9.4
 * @package		Frontpage Slideshow
 * @author    JoomlaWorks - http://www.joomlaworks.gr
 * @copyright	Copyright (c) 2006 - 2011 JoomlaWorks Ltd. All rights reserved.
 * @license		Commercial - This code cannot be redistributed without permission from JoomlaWorks Ltd.
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

// JoomlaWorks reference parameters
$mod_name               = "mod_fpss";
$mod_copyrights_start   = "\n\n<!-- JoomlaWorks \"Frontpage Slideshow\" (v2.9.4) starts here -->\n";
$mod_copyrights_end     = "\n<!-- JoomlaWorks \"Frontpage Slideshow\" (v2.9.4) ends here -->\n\n";

// API
jimport('joomla.filesystem.file');
jimport('joomla.utilities.date');
$mainframe	= &JFactory::getApplication();
$document 	= &JFactory::getDocument();
$db 				= &JFactory::getDBO();
$user 			= &JFactory::getUser();
$aid 				= (int) $user->get('aid',0);
$menu				= &JSite::getMenu();
$nullDate		= $db->getNullDate();
$date 			= new JDate();
$now 				= $date->toMySQL();

// Assign paths
$sitePath 	= JPATH_SITE;
$siteUrl  	= JURI::base(true);

// Module parameters
$moduleclass_sfx 							= $params->get('moduleclass_sfx','');
$fpssCategory 								= (int) $params->get('fpssCategory',1);
$fpssTemplate 								= $params->get('fpssTemplate','Movies');
$loadJquery										= $params->get('loadJquery','1.6');
$width 												= (int) $params->get('width',500);
$height 											= (int) $params->get('height',308);
$sidebarWidth 								= (int) $params->get('sidebarWidth',200);
$hideNavigation 							= (int) $params->get('hideNavigation',0);
$delay 												= (int) $params->get('delay',6000);
$transition 									= (int) $params->get('transition',1000);
$loadingTime 									= (int) $params->get('loadingTime',800);
$autoStart 										= ($params->get('autoStart',1)) ? 'true' : 'false';
$rotateAction 								= $params->get('rotateAction','click');
$fpssOrdering 								= (int) $params->get('fpssOrdering',1);
$fpssSlideLimit 							= (int) $params->get('fpssSlideLimit');
$fpssSlideTitle								= $params->get('fpssSlideTitle','');
$fpssSlideSec     						= $params->get('fpssSlideSec','');
$fpssSlideCat     						= $params->get('fpssSlideCat','');
$fpssSlideSecCatSep 					= $params->get('fpssSlideSecCatSep','>>');
$fpssSlideText 								= $params->get('fpssSlideText','');
$fpssSlideTextWordLimit 			= (int) $params->get('fpssSlideTextWordLimit');
$fpssSlideTagline  						= $params->get('fpssSlideTagline','');
$fpssSlideReadMore   					= $params->get('fpssSlideReadMore','');
$fpssSlideLinksDisable				= (int) $params->get('fpssSlideLinksDisable',0);
// Module parameters (advanced)
$enableTinySrc								= (int) $params->get('enableTinySrc',0);
$fpssCssInclusionMethod				= (int) $params->get('fpssCssInclusionMethod',0);
$debugMode										= $params->get('debugMode',1);
if($debugMode==0) error_reporting(0); // Turn off all error reporting
$getModuleCacheState					= (int) $params->get('cache',1);

// Hide the navigation bar if requested
if($hideNavigation){
	$sidebarWidth = 0;
	$hideNavigationClass = ' class="fpssHideNavigation"';
} else {
	$hideNavigationClass = '';
}

// Quick check for the component
if(!JFolder::exists(JPATH_SITE.DS.'components'.DS.'com_fpss')){
	JError::raiseWarning('', JText::_("You need to install the Frontpage Slideshow component as well!"));
	return;
}

// Includes
require_once(dirname(__FILE__).DS.'helper.php');
require_once(JPATH_SITE.DS.'components'.DS.'com_content'.DS.'helpers'.DS.'route.php');

// Get slideshow contents from the database
$comContentParams = &JComponentHelper::getParams( 'com_content' );
$access = (!$comContentParams->get('shownoauth')) ? "AND ( (articles.access <= ".$aid." AND cc.access <= ".$aid." AND s.access <= ".$aid.") OR (articles.access IS NULL AND cc.access IS NULL AND s.access IS NULL) )" : "";

$query = "
	SELECT slides.*, articles.title as title, articles.introtext as introtext, articles.catid AS catid, articles.sectionid as sectionid, cc.title AS categoryTitle, s.title AS sectionTitle, categories.id AS slideCategory, categories.width AS slideWidth, categories.width_thumb AS slideThumbWidth,
	CASE WHEN CHAR_LENGTH(articles.alias) THEN CONCAT_WS(\":\", articles.id, articles.alias) ELSE articles.id END as slug,
	CASE WHEN CHAR_LENGTH(cc.alias) THEN CONCAT_WS(\":\", cc.id, cc.alias) ELSE cc.id END as catslug
	FROM #__fpss_slides as slides
	LEFT JOIN #__fpss_categories AS categories ON categories.id = slides.catid
	LEFT JOIN #__content AS articles ON articles.id = slides.itemlink
	LEFT JOIN #__categories AS cc ON cc.id = articles.catid
	LEFT JOIN #__sections AS s ON s.id = articles.sectionid
	WHERE categories.published=1
	AND slides.state=1
	AND slides.registers <= {$aid}
	AND (slides.publish_up = '{$nullDate}' OR slides.publish_up <= '{$now}')
	AND (slides.publish_down = '{$nullDate}' OR slides.publish_down >= '{$now}')
	AND slides.catid={$fpssCategory}
	AND (articles.state=1 OR articles.state IS NULL)
	AND (articles.publish_up = '{$nullDate}' OR articles.publish_up <= '{$now}' OR articles.publish_up IS NULL)
	AND (articles.publish_down = '{$nullDate}' OR articles.publish_down >= '{$now}' OR articles.publish_down IS NULL)
	AND (cc.published=1 OR cc.published IS NULL)
	AND (s.published=1 OR s.published IS NULL)
	{$access}
	ORDER BY ordering
";
$db->setQuery($query);
$rows = $db->loadObjectList();

// Prepare random ordering only
if($fpssOrdering==6){
	shuffle($rows);
}

// Loop through the slideshow contents
foreach($rows as $count=>$row){

	// Perform slide limit
	if($fpssSlideLimit && $count>=$fpssSlideLimit) continue;

	// Use slide IDs for indexing the array
	$key = $row->id;

  // Compare display options between component and module
	$showName           = ($fpssSlideTitle=='') ? $row->showtitle : $fpssSlideTitle;
	$showSectionTitle   = ($fpssSlideSec=='') ? $row->showseccat : $fpssSlideSec;
	$showCategoryTitle  = ($fpssSlideCat=='') ? $row->showseccat : $fpssSlideCat;
	$showText       		= ($fpssSlideText=='') ? $row->showcustomtext : $fpssSlideText;
	$showTagline        = ($fpssSlideTagline=='') ? $row->showplaintext : $fpssSlideTagline;
	$showReadMore       = ($fpssSlideReadMore=='') ? $row->showreadmore : $fpssSlideReadMore;

	// Get menu data
	if($row->menulink) $menuItem = $menu->getItem($row->menulink);

	// Slide name
	if($row->itemlink && $row->title && !$row->ctext){
		$output[$key]->name = $row->title;
	} elseif($row->menulink && $menuItem && !$row->ctext){
		$output[$key]->name = $menuItem->name;
	} else {
		$output[$key]->name = $row->name;
	}
	if(!$showName) $output[$key]->name = '';

	// Slide name used in alt attributes
	$output[$key]->altname = htmlentities($output[$key]->name, ENT_QUOTES, 'UTF-8');

	// Slide link
	if($row->itemlink && $row->slug){
		$output[$key]->link = JRoute::_(ContentHelperRoute::getArticleRoute($row->slug, $row->catslug, $row->sectionid));
	} elseif($row->menulink && $menuItem){
		$output[$key]->link = JRoute::_($menuItem->link.'&Itemid='.$menuItem->id);
	} elseif($row->customlink){
		$output[$key]->link = JFilterOutput::ampReplace($row->customlink);
	} else {
		$output[$key]->link = 'javascript:void(0);';
	}

	// Slide link target
	$output[$key]->target = ($row->target && $output[$key]->link != 'javascript:void(0);') ? ' target="_blank"' : '' ;

	// Slide text
	if($row->itemlink && $row->slug && !$row->ctext){
		$output[$key]->text = $row->introtext;
	} else {
		$output[$key]->text = $row->ctext;
	}
	if(!$showText) $output[$key]->text = '';

	$output[$key]->text = JWFrontpageSlideshowHelper::cleanPluginTags($output[$key]->text); // Cleanup plugin tags

	// Slide tagline
	if($showTagline) $output[$key]->tagline = strip_tags($row->plaintext); else $output[$key]->tagline = '';

	$output[$key]->tagline = JWFrontpageSlideshowHelper::cleanPluginTags($output[$key]->tagline); // Cleanup plugin tags

	// Slide "read more..." link
	$output[$key]->readMore = $showReadMore;

	// Build the Section >> Category path if it exists
	if($row->itemlink && $row->slug && ($showSectionTitle || $showCategoryTitle)){
		if($showSectionTitle){
			$output[$key]->secCatPath = $row->sectionTitle;
			if($showCategoryTitle) $output[$key]->secCatPath .= ' '.$fpssSlideSecCatSep.' '.$row->categoryTitle;
		} else {
			if($showCategoryTitle) $output[$key]->secCatPath = $row->categoryTitle;
		}
	} else {
		$output[$key]->secCatPath = '';
	}

	// Slide image (main)
	if($enableTinySrc){
		$output[$key]->mainImage = 'http://i.tinysrc.mobi/'.$row->slideWidth.'/'.JURI::root().$row->path;
	} else {
		$output[$key]->mainImage = $siteUrl.'/'.$row->path;
	}

	// Slide image (thumb)
	if($row->thumb){
		$output[$key]->thumbImage = $siteUrl.'/'.$row->thumb;
	} else {
		$output[$key]->thumbImage = $output[$key]->mainImage;
	}

	// Slide counter
	if(($count+1) < 10) $output[$key]->counter = "0".($count+1); else $output[$key]->counter = $count+1;

	// Slide rotate action
	if($rotateAction=='mouseover'){
		$output[$key]->rotateAction = ' onclick="parent.location=\''.$output[$key]->link.'\';return false;"';
	} else {
		$output[$key]->rotateAction = '';
	}

	// --------------- Content processing ---------------
	// Word limit on slide text
	if($fpssSlideTextWordLimit) $output[$key]->text = JWFrontpageSlideshowHelper::wordLimiter($output[$key]->text,$fpssSlideTextWordLimit);

	// Hide slide content completely if the each slide content element is hidden as well
	if(!$showName && !$showSectionTitle && !$showCategoryTitle && !$showText && !$showTagline && !$showReadMore){
		$output[$key]->content = false;
	} else {
		$output[$key]->content = true;
	}

	// Disable all slide links
	if($fpssSlideLinksDisable) $output[$key]->link = 'javascript:void(0);';

}

// Ordering
switch($fpssOrdering) {
	case 1: break;
	case 2: krsort($output); 	break;
	case 3: ksort($output); 	break;
	case 4: asort($output); 	break;
	case 5: arsort($output); 	break;
	case 6: break;
}

// Load FPSS head includes
$fpssTemplatePath = JWFrontpageSlideshowHelper::getTemplatePath($mod_name,$fpssTemplate);

// Define main CSS inclusion method
if($fpssCssInclusionMethod){
	ob_start();
	$fpssTemplateIncluded = true;
	include(JModuleHelper::getLayoutPath($mod_name,$fpssTemplate.DS.'css'.DS.'template_css'));
	$getFpssTemplate = ob_get_contents();
	ob_end_clean();
	$getFpssTemplate = "\n".str_replace('url(../images/','url(modules/mod_fpss/tmpl/'.$fpssTemplate.'/images/',$getFpssTemplate);
	$getFpssTemplateWithJs = preg_replace("/\t|\r|\n/"," ",$getFpssTemplate);
	$getFpssTemplateWithJs = str_replace('\'','\\\'',$getFpssTemplateWithJs);
} else {
	$getFpssTemplate = '
	<!--
	@import "'.$fpssTemplatePath.'/css/template_css.php?w='.$width.'&h='.$height.'&sw='.$sidebarWidth.'";
	//-->
	';
	$getFpssTemplateWithJs = '@import "'.$fpssTemplatePath.'/css/template_css.php?w='.$width.'&h='.$height.'&sw='.$sidebarWidth.'";';
}

if($mainframe->getCfg('caching') && $getModuleCacheState){
?>

	<?php echo $mod_copyrights_start; ?>
	<script type="text/javascript">
		//<![CDATA[
		document.write('<style type="text/css" media="all"><?php echo $getFpssTemplateWithJs; ?></style>');
		//]]>
	</script>
	<?php if($loadJquery): ?>
	<!-- Load jQuery remotely -->
	<script type="text/javascript" src="http://www.google.com/jsapi"></script>
	<script type="text/javascript">google.load("jquery", "<?php echo $loadJquery; ?>");</script>
	<?php endif; ?>
	<script type="text/javascript" src="<?php echo $siteUrl; ?>/modules/mod_fpss/includes/js/fpss.packed.js"></script>
	<script type="text/javascript">
		//<![CDATA[
		jQuery.noConflict();
		jQuery(document).ready(function($){
			$('.fpssModuleContainer').frontpageSlideshow({
				'crossFadeDelay':<?php echo $delay; ?>,
				'crossFadeSpeed':<?php echo $transition; ?>,
				'loaderDelay':<?php echo $loadingTime; ?>,
				'navEvent':'<?php echo $rotateAction; ?>',
				'autoslide':<?php echo $autoStart; ?>,
				'textPlay':'<?php echo JText::_('Play'); ?>',
				'textPause':'<?php echo JText::_('Pause'); ?>'
			});
		});
		//]]>
	</script>
	<?php echo $mod_copyrights_end; ?>

<?php } else { ?>

	<?php
	$headIncludesNoCache = JHTML::_('behavior.mootools');
	
	$headIncludesNoCache .= '
	<style type="text/css" media="all">
		'.$getFpssTemplate.'
	</style>
	';
	
	if($loadJquery){
	$headIncludesNoCache .= '
	<!-- Load jQuery remotely -->
	<script type="text/javascript" src="http://www.google.com/jsapi"></script>
	<script type="text/javascript">google.load("jquery", "'.$loadJquery.'");</script>
	';
	}

	$headIncludesNoCache .= '
	<script type="text/javascript" src="'.$siteUrl.'/modules/mod_fpss/includes/js/fpss.packed.js"></script>
	<script type="text/javascript">
		//<![CDATA[
		jQuery.noConflict();
		jQuery(document).ready(function($){
			$(\'.fpssModuleContainer\').frontpageSlideshow({
				\'crossFadeDelay\':'.$delay.',
				\'crossFadeSpeed\':'.$transition.',
				\'loaderDelay\':'.$loadingTime.',
				\'navEvent\':\''.$rotateAction.'\',
				\'autoslide\':'.$autoStart.',
				\'textPlay\':\''.JText::_('Play').'\',
				\'textPause\':\''.JText::_('Pause').'\'
			});
		});
		//]]>
	</script>
	';

	$document->addCustomTag($mod_copyrights_start.$headIncludesNoCache.$mod_copyrights_end);

}

// Output content with template
echo $mod_copyrights_start;
echo '<div class="fpssModuleContainer">';
require(JModuleHelper::getLayoutPath($mod_name,$fpssTemplate.DS.'default'));
echo '</div>';
echo JWFrontpageSlideshowHelper::setCrd();
echo $mod_copyrights_end;

// END
