<?php
/**
 * @version		2.9.4
 * @package		Frontpage Slideshow
 * @author    JoomlaWorks - http://www.joomlaworks.gr
 * @copyright	Copyright (c) 2006 - 2011 JoomlaWorks Ltd. All rights reserved.
 * @license		Commercial - This code cannot be redistributed without permission from JoomlaWorks Ltd.
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

class JElementCategories extends JElement {
	var	$_name = 'categories';
	
	function fetchElement($name, $value, &$node, $control_name){
	
		$db = &JFactory::getDBO();
		$query = 'SELECT id, name FROM #__fpss_categories WHERE published=1 ORDER BY name';
		$db->setQuery($query);
		$options = $db->loadObjectList();
		
		//array_unshift($options, JHTML::_('select.option', '0', JText::_('-- None selected --'), 'id', 'name'));

		// Output
		return JHTML::_('select.genericlist',  $options, ''.$control_name.'['.$name.']', 'class="inputbox"', 'id', 'name', $value, $control_name.$name );
	
	}
	
}