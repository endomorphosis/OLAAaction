<?php
if(!isset($fpssTemplateIncluded)){
	header("Content-type: text/css; charset: UTF-8");
	$width = (int) $_GET['w'];
	$height = (int) $_GET['h'];
	$sidebarWidth = (int) $_GET['sw'];
}
?>
/**
 * @version		2.9.4
 * @package		Frontpage Slideshow
 * @author    JoomlaWorks - http://www.joomlaworks.gr
 * @copyright	Copyright (c) 2006 - 2011 JoomlaWorks Ltd. All rights reserved.
 * @license		Commercial - This code cannot be redistributed without permission from JoomlaWorks Ltd.
 */

/* --- Slideshow Containers --- */
#fpss-outer-container {width:<?php echo $width; ?>px;padding:0;margin:4px auto;border:2px solid #ccc;}
#fpss-container {border:none;padding:0;margin:0;position:relative;width:<?php echo $width; ?>px;}
#fpss-slider {overflow:hidden;background:none;width:<?php echo $width; ?>px;height:<?php echo $height; ?>px;}
#slide-wrapper {font-size:11px;text-align:left;width:<?php echo $width; ?>px;height:<?php echo $height; ?>px;background:#000 url(../images/loading_black.gif) no-repeat center;}
#slide-wrapper #slide-outer {display:none;height:<?php echo $height; ?>px;}
#slide-wrapper #slide-outer .slide {position:absolute;right:0;overflow:hidden;width:<?php echo $width; ?>px;height:<?php echo $height; ?>px;}
#slide-wrapper #slide-outer .slide .slide-inner {margin:0;color:#fff;overflow:hidden;background:#000;height:<?php echo $height; ?>px;}
#slide-wrapper #slide-outer .slide .slide-inner a.fpss_img span span span {background:none;}

/* --- Content --- */
.fpss-introtext {margin:0;padding:0;position:absolute;bottom:25px;left:0;background:url(../images/transparent_bg.png);width:<?php echo $width-$sidebarWidth-30; ?>px;height:34px;overflow:hidden;}
.fpss-introtext .slidetext {margin:0;padding:0 8px;font-size:11px;}

/* Hide navigation if required */
.fpssHideNavigation #navi-outer {display:none;}

/* --- Navigation Buttons --- */
#pseudobox {position:absolute;top:7px;left:0;right:0;height:34px;margin:0;padding:0;background:#444;opacity:0.8;filter:alpha(opacity=80);width:<?php echo $sidebarWidth; ?>px;}
#navi-outer {position:absolute;right:0;z-index:9;bottom:16px;width:<?php echo $sidebarWidth; ?>px;}
#navi-outer ul {margin:0;padding:0;text-align:right;display:block;}
#navi-outer li {display:inline;background:none;padding:0;margin:0;}
#navi-outer li a,
#navi-outer li a:hover,
#navi-outer li a.navi-active {display:block;float:left;overflow:hidden;width:50px;height:50px;padding:0;margin:0 2px;text-decoration:none;position:relative;}
#navi-outer li a {background:none;}
#navi-outer li a:hover,
#navi-outer li a.navi-active {background:url(../images/nav-current.gif) no-repeat 49% 0;}
#navi-outer li a img,
#navi-outer li a:hover img,
#navi-outer li a.navi-active img {width:45px;height:30px;margin:8px 0 0 0;padding:0;}
#navi-outer li a img {opacity:0.7;filter:alpha(opacity=70);border:1px solid #aaa;}
#navi-outer li a:hover img,
#navi-outer li a.navi-active img {opacity:1.0;filter:alpha(opacity=100);border:1px solid #fff;width:48px;height:32px;margin:6px 0 0 0;padding:0;}
#navi-outer li a span.navbar-key {display:none;}
#navi-outer li a span.navbar-title {display:none;}
#navi-outer li a span.navbar-tagline {display:none;}
#navi-outer li a span.navbar-clr {display:none;}
#navi-outer li.noimages a {font-family:Tahoma, Arial, sans-serif;font-size:10px;border:none;text-align:center;width:auto;padding:0 4px;margin:0;background:none;}
#navi-outer li.noimages a:hover {font-family:Tahoma, Arial, sans-serif;font-size:10px;border:none;text-align:center;width:auto;padding:0 4px;margin:0;background:none;color:#FF9900;}
#navi-outer li.noimages a#fpss-container_prev {background:url(../images/nav-prev.gif) no-repeat center 50%;font-size:0;margin:0 2px;text-indent:-9999px;overflow:hidden;}
#navi-outer li.noimages a#fpss-container_playButton {display:none;}
#navi-outer li.noimages a#fpss-container_next {background:url(../images/nav-next.gif) no-repeat center 50%;font-size:0;margin:0 2px;text-indent:-9999px;overflow:hidden;}

/* Notice: Add custom text styling here to overwrite your template's CSS styles! */
.fpss-introtext .slidetext h1,
.fpss-introtext .slidetext h1 a {margin:0;padding:0;font-weight:bold;font-size:13px;color:#9c0;}
.fpss-introtext .slidetext h1 a:hover {color:#f90;text-decoration:none;}
.fpss-introtext .slidetext h2 {display:none;}
.fpss-introtext .slidetext h3 {margin:0;padding:0;color:#fff;font-size:11px;font-weight:normal;}
.fpss-introtext .slidetext p {display:none;}
.fpss-introtext .slidetext a.readon {display:none;}
.fpss-introtext .slidetext a.readon:hover {display:none;}

/* --- Generic Styling (highly recommended) --- */
#fpss-outer-container a:active,
#fpss-outer-container a:focus {outline:0;outline:expression(hideFocus='true');}
#fpss-container img {border:none;}
.fpss-introtext .slidetext img,
.fpss-introtext .slidetext p img {display:none;} /* this will hide images inside the introtext */
.fpss-clr {height:0;line-height:0;}

/* IE Specific Styling (use body.fpssIsIE6, body.fpssIsIE7, body.fpssIsIE8 to target specific IEs) */
body.fpssIsIE6 .fpss-introtext {background:#222;}
body.fpssIsIE6 .fpss-clr,
body.fpssIsIE7 .fpss-clr {display:none;}

/* --- End of stylesheet --- */