<?php
if(!isset($fpssTemplateIncluded)){
	header("Content-type: text/css; charset: UTF-8");
	$width = (int) $_GET['w'];
	$height = (int) $_GET['h'];
	$sidebarWidth = (int) $_GET['sw'];
}
?>
/**
 * @version		2.9.4
 * @package		Frontpage Slideshow
 * @author    JoomlaWorks - http://www.joomlaworks.gr
 * @copyright	Copyright (c) 2006 - 2011 JoomlaWorks Ltd. All rights reserved.
 * @license		Commercial - This code cannot be redistributed without permission from JoomlaWorks Ltd.
 */

/* --- Slideshow Containers --- */
#fpss-outer-container {width:<?php echo $width+$sidebarWidth; ?>px;margin:4px auto;padding:0 16px;border:1px solid #b0b0b0;background:#fff;}
#fpss-container {margin:0;padding:0;position:relative;width:<?php echo $width+$sidebarWidth; ?>px;}
#fpss-slider {overflow:hidden;background:none;width:<?php echo $width+$sidebarWidth; ?>px;height:<?php echo $height; ?>px;}

/* --- Slideshow Block --- */
.slide {position:absolute;right:0;width:<?php echo $width+$sidebarWidth; ?>px;}
#slide-wrapper {font-size:11px;text-align:left;width:<?php echo $width+$sidebarWidth; ?>px;height:<?php echo $height; ?>px;background:#fff url(../images/loading.gif) no-repeat 50% 50%;}
#slide-wrapper #slide-outer {display:none;height:<?php echo $height; ?>px;}
#slide-wrapper #slide-outer .slide-inner {margin:0;color:#fff;overflow:hidden;height:<?php echo $height; ?>px;}
#slide-wrapper #slide-outer .slide-inner a.fpss_img {display:block;margin:0 0 0 <?php echo $sidebarWidth; ?>px;padding:16px 0;border-left:1px solid #7b7b7b;overflow:hidden;}
#slide-wrapper #slide-outer .slide-inner a.fpss_img span {display:block;margin:0 0;overflow:hidden;height:<?php echo $height-32; ?>px;position:relative;}
#slide-wrapper #slide-outer .slide-inner a.fpss_img span span {margin:0;}
#slide-wrapper #slide-outer .slide-inner a.fpss_img span span span {background:url(../images/readmore.png) no-repeat right bottom;}
#slide-wrapper #slide-outer .slide-inner a.fpss_img span span span img {display:none;}

/* --- Content --- */
.fpss-introtext {margin:0;padding:0;position:absolute;top:0;left:0;overflow:hidden;background:#fff;width:<?php echo $sidebarWidth; ?>px;height:<?php echo $height-80; ?>px;}
.fpss-introtext .slidetext {padding:16px 8px 4px 2px;}

/* Hide navigation if required */
.fpssHideNavigation #navi-outer {display:none;}

/* --- Navigation Buttons --- */
#navi-outer {position:absolute;bottom:0;left:0;margin:8px 0 16px 0;padding:0;width:<?php echo $sidebarWidth; ?>px;overflow:hidden;display:block;}
#navi-outer ul {margin:0;padding:0 16px 0 0;text-align:right;}
#navi-outer li {display:inline;background:none;padding:0;margin:0;}
#navi-outer li a,#navi-outer li a:hover,#navi-outer li a.navi-active {display:block;float:left;overflow:hidden;width:30px;height:30px;padding:0;margin:2px;text-decoration:none;line-height:40px;background:#404040;}
#navi-outer li a {border:1px solid #bbb;}
#navi-outer li a:hover {border:1px solid #ff9900;}
#navi-outer li a.navi-active {border:1px solid #ff9900;}
#navi-outer li a img,#navi-outer li a:hover img,#navi-outer li a.navi-active img {height:80px;width:auto;display:block;margin:-5px 0 0 -55px;}
#navi-outer li a img {opacity:0.6;filter:alpha(opacity=60);}
#navi-outer li a:hover img {opacity:1.0;filter:alpha(opacity=100);}
#navi-outer li a.navi-active img {opacity:1.0;filter:alpha(opacity=100);}
#navi-outer li a span.navbar-img {}
#navi-outer li a span.navbar-key {padding:0 2px;}
#navi-outer li a span.navbar-title {display:none;}
#navi-outer li a span.navbar-tagline {display:none;}
#navi-outer li a span.navbar-clr {display:none;}
#navi-outer li.noimages {display:none;}

/* --- Notice: Add custom text styling here to overwrite your template's CSS styles! --- */
.fpss-introtext .slidetext h1,
.fpss-introtext .slidetext h1 a {font-family:Georgia, "Times New Roman", Times, serif;font-size:18px;margin:0;padding:0;color:#0088bf;line-height:120%;}
.fpss-introtext .slidetext h1 a:hover {color:#cc3300;text-decoration:none;}
.fpss-introtext .slidetext h2 {font-size:10px;margin:0;padding:0;color:#999;}
.fpss-introtext .slidetext h3 {font-size:13px;margin:0;padding:2px 0;color:#555;font-weight:bold;}
.fpss-introtext .slidetext p {margin:0;padding:0;color:#333;}
.fpss-introtext .slidetext a.readon {display:none;}
.fpss-introtext .slidetext a.readon:hover {display:none;}

/* --- Generic Styling (highly recommended) --- */
#fpss-outer-container a:active,
#fpss-outer-container a:focus {outline:0;outline:expression(hideFocus='true');}
#fpss-container img {border:none;}
.fpss-introtext .slidetext img,
.fpss-introtext .slidetext p img {display:none;} /* this will hide images inside the introtext */
.fpss-clr {height:0;line-height:0;}

/* IE Specific Styling (use body.fpssIsIE6, body.fpssIsIE7, body.fpssIsIE8 to target specific IEs) */
body.fpssIsIE6 #slide-outer .slide-inner a.fpss_img span span span {background:url(../images/readmore_ie.png) no-repeat right bottom;}
body.fpssIsIE6 #slide-outer .slide-inner a.fpss_img span span span,
body.fpssIsIE7 #slide-outer .slide-inner a.fpss_img span span span {cursor:pointer;}

/* --- End of stylesheet --- */