<!-- Newsscroller Self DHTML J1.6 by Kubik-Rubik.de - Version 1.6-1-1 -->
<?php
/**
 * @Copyright
 *
 * @package		Newsscroller Self DHTML for Joomla 1.6
 * @author		Viktor Vogel {@link http://joomla-extensions.kubik-rubik.de/}
 * @version		Version: 1.6-1 - 24-Jan-2011
 * @link		Project Site {@link http://joomla-extensions.kubik-rubik.de/ns-newsscroller-self-dhtml}
 *
 *	@license GNU/GPL
 *	This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

defined('_JEXEC') or die('Restricted access');
?>
<div id="marqueecontainer" onmouseover="copyspeed=pausespeed" onmouseout="copyspeed=marqueespeed">
<div id="vmarquee" class="vmarquee">
<?php echo $html_content; ?>
</div>
</div>
<?php if ($copy) : ?>
	<br />
	<div id="vmarqueesmall">
		NS-DHTML by <a title="NS-DHTML - Joomla! 1.6 Erweiterung by Kubik-Rubik.de - Viktor Vogel" target="_blank" href="http://joomla-extensions.kubik-rubik.de/">Kubik-Rubik.de</a>
	</div>
<?php endif; ?>