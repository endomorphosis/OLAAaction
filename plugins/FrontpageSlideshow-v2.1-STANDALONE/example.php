<?php
/**
 * @version		$Id: example.php 200 2010-12-29 20:00:05Z joomlaworks $
 * @package		Frontpage Slideshow (standalone)
 * @author		JoomlaWorks http://www.joomlaworks.gr
 * @copyright	Copyright (c) 2006 - 2010 JoomlaWorks, a business unit of Nuevvo Webware Ltd. All rights reserved.
 * @license		Commercial - This code cannot be redistributed without permission from Nuevvo Webware Ltd.
 */

/* Example php file showcasing how to implement Frontpage Slideshow (standalone) on any .php page */

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Frontpage Slideshow (standalone) | Example/demo page</title>
		<style type="text/css">
			body {font-family:Arial,Verdana,Sans-serif;font-size:12px;color:#444;padding:0;margin:0;background:#eee;}
			a {color:#0073b9;font-weight:bold;text-decoration:none;}
			a:active,a:focus {outline:0;}
			a:hover {color:#c00;font-weight:bold;text-decoration:none;}
			#container {width:720px;padding:16px;margin:16px auto;background:#fff;}
			span#notice {font-style:italic;color:#999;}
			p#credits {font-size:11px;color:#999;margin:16px 0 0 0;padding:4px 0 0 0;border-top:1px dotted #ccc;text-align:right;}
			p#credits a {color:#777;}
			p#credits a:hover {color:#444;}
		</style>
	</head>
	<body>
		<div id="container">
		
			<h1>Frontpage Slideshow (standalone)</h1>
			
			<?php
			/* Frontpage Slideshow: Setup Steps */
			/* Please refer first to the online documentation here: http://www.joomlaworks.gr/content/view/74/41/ */
			
			// 1. Enter the name of your slideshow
			$slideshow = "demo";
			
			// 2. Enter your site's URL (without trailing slash)
			$siteURL = ""; // e.g. http://www.site.com or http://www.site.com/folder if your site is in a subfolder
			
			// 3. Enter the absolute path of your site on the server (without trailing slash)
			$sitePath = ""; // e.g. /home/user/public_html or /home/user/public_html/folder if your site is located in a subfolder
			
			// Include the slideshow
			include_once($sitePath."/fpss/fpss.php"); // DO NO EDIT THIS LINE
			
			?>
			
			<span id="notice">This demo uses the "Movies" template and the sample data that come bundled with the purchase package</span>

			<h2>About</h2>
			<p>Frontpage Slideshow is a simple to use yet powerful content slideshow for your PHP based website. Built in such a way that it's easy to deploy and customize on any PHP website (including CMSs like WordPress, Drupal etc.), making it one of the most flexible slideshow systems in the world. Use Frontpage Slideshow to display your featured articles, stories or products in the most eye-catching way.</p>
			<p>Frontpage Slideshow is used in websites visited by millions of people everyday around the world:</p>
			<ul>
				<li><a href="http://www.gazzetta.gr" target="_blank">Gazzetta.gr</a> (more than 5 million visitors per month)</li>
				<li><a href="http://www.tnawrestling.com" target="_blank">TNA Wrestling</a> (more than 3 million visitors per month)</li>
				<li><a href="http://www.linux.com" target="_blank">Linux.com</a></li>
				<li><a href="http://www.afro.who.int" target="_blank">World Health Organization</a></li>
			</ul>
			<p>...and thousands more websites based on content management systems like Joomla!, Drupal, WordPress or even static PHP based websites.</p>
			<p>Frontpage Slideshow comes in one package and contains a Joomla! extension and this "standalone" distribution for use on any PHP based website!</p>
			<p>To learn more, please check out the following resources on the <b>JoomlaWorks</b> website:</p>
			<ul>
				<li><a href="http://www.joomlaworks.gr/content/view/24/42/" target="_blank">Frontpage Slideshow product page</a></li>
				<li><a href="http://www.joomlaworks.gr/content/view/74/41/" target="_blank">Frontpage Slideshow documentation (for PHP websites)</a></li>
				<li><a href="http://www.joomlaworks.gr/content/view/71/41/" target="_blank">Frontpage Slideshow documentation (for Joomla!)</a></li>
			</ul>
			<p id="credits">Copyright &copy; 2006-<?php echo date('Y'); ?> <a target="_blank" href="http://www.joomlaworks.gr">JoomlaWorks</a>, a business unit of <a target="_blank" href="http://nuevvo.com">Nuevvo Webware Ltd.</a></p>
		</div>
	</body>
</html>
