<?php
/**
 * @version		$Id: data.php 125 2010-09-28 21:22:02Z joomlaworks $
 * @package		Frontpage Slideshow (standalone)
 * @author		JoomlaWorks http://www.joomlaworks.gr
 * @copyright	Copyright (c) 2006 - 2010 JoomlaWorks, a business unit of Nuevvo Webware Ltd. All rights reserved.
 * @license		Commercial - This code cannot be redistributed without permission from Nuevvo Webware Ltd.
 */

/* -------------------------------------------------------------------------- */
/* ------------------------- Your slideshow content ------------------------- */
/* -------------------------------------------------------------------------- */

/*
*** TIP ***
-----------
To add more slides to your FPSS slideshow, simple copy/paste and then change per your needs the data blocks marked with "slide elements".

The order of the blocks defines the default slide order.
*/

$slides = (object)array(
// --- Start slide list ---



				// slide elements
				(object)array(
					"title" => "Frontpage Slideshow",
					"category" => "About Frontpage Slideshow",
					"tagline" => "Eye-candy for any PHP based website",
					"text" => "Frontpage SlideShow is the most eye-catching way to display your featured articles, stories or even products in your PHP based website, including CMSs like WordPress or Drupal.",
					"image" => "knightandday.jpg",
					"thumbImage" => "",
					"link" => "http://www.frontpageslideshow.net",
					"linkOpensInNewWindow" => 0,
					"showReadMore" => 1,
				),
				
				// slide elements
				(object)array(
					"title" => "Frontpage Slideshow is Search Engine Friendly!",
					"category" => "About Frontpage Slideshow",
					"tagline" => "Makes it iPhone compatible too!",
					"text" => "Unlike Flash based slideshows, Frontpage Slideshow uses CSS and Javascript wizardry only. The content of the slides is laid out as html code, which means it can be crawled by search engines. The proper usage (and order) of h1, h2, p (and more) tags will make sure Google (or any other search engine) regularly indexes your latest/featured items.",
					"image" => "theinvasion.jpg",
					"thumbImage" => "",
					"link" => "http://www.frontpageslideshow.net",
					"linkOpensInNewWindow" => 0,
					"showReadMore" => 1,
				),				
				
				// slide elements
				(object)array(
					"linkOpensInNewWindow" => 0,
					"title" => "Easy to embed on any PHP site",
					"category" => "About Frontpage Slideshow",
					"tagline" => "Your slideshow will be ready in minutes",
					"text" => "Just upload the /fpss folder on your server, add a small PHP snippet on your site or CMS template, edit a php file with your slideshow data and you're done!",
					"image" => "ateam.jpg",
					"thumbImage" => "",
					"link" => "http://www.joomlaworks.gr/fpss",
					"linkOpensInNewWindow" => 0,
					"showReadMore" => 1,
				),				
				
				// slide elements (***TIP***: copy this data block and paste it right below to add more slides)
				(object)array(
					"title" => "Powers thousands of sites worldwide",
					"category" => "About Frontpage Slideshow",
					"tagline" => "From corporate sites to \"big league\" magazines & portals",
					"text" => "Frontpage Slideshow is used in websites visited by millions of people everyday around the world like Linux.com, BettyConfidential.com, the World Health Organization and many more.",
					"image" => "theotherguys.jpg",
					"thumbImage" => "",
					"link" => "http://www.joomlaworks.gr",
					"linkOpensInNewWindow" => 0,
					"showReadMore" => 1,
				),



// --- End slide list ---
);
