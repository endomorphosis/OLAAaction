<?php
/**
 * @version		2.9.1
 * @package		Frontpage Slideshow
 * @author    JoomlaWorks - http://www.joomlaworks.gr
 * @copyright	Copyright (c) 2006 - 2011 JoomlaWorks, a business unit of Nuevvo Webware Ltd. All rights reserved.
 * @license		Commercial - This code cannot be redistributed without permission from Nuevvo Webware Ltd.
 */

defined('_JEXEC') or die('Restricted access');

class TableCategory extends JTable
{
	var $id 						= null;
	var $name 					= null;
	var $width 					= null;
	var $quality 				= null;
	var $width_thumb 		= null;
	var $quality_thumb 	= null;
	var $published 			= null;
	
	function __construct(&$db)
	{
		parent::__construct( _FPSS_TABLE_CATEGORIES, 'id', $db );
	}
}
