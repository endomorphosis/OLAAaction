CREATE TABLE IF NOT EXISTS `#__fpss_categories` (
		`id` int(3) NOT NULL auto_increment,
		`name` varchar(225) NOT NULL,
		`width` int(11) NOT NULL,
		`quality` int(11) NOT NULL,
		`width_thumb` int(11) NOT NULL,
		`quality_thumb` int(11) NOT NULL,
		`published` tinyint(1) NOT NULL,
		PRIMARY KEY (`id`)
) TYPE=MyISAM;

CREATE TABLE IF NOT EXISTS `#__fpss_slides` (
		`id` int(11) unsigned NOT NULL auto_increment,
		`catid` int(11) NOT NULL,
		`name` varchar(225) NOT NULL default '',
		`path` varchar(225) NOT NULL default '',
		`path_type` varchar(110) NOT NULL default '',
		`thumb` varchar(225) NOT NULL,
		`state` tinyint(3) NOT NULL default '0',
		`publish_up` datetime NOT NULL default '0000-00-00 00:00:00',
		`publish_down` datetime NOT NULL default '0000-00-00 00:00:00',
		`itemlink` int(11) NOT NULL default '0',
		`menulink` int(11) NOT NULL default '0',
		`target` tinyint(3) NOT NULL default '0',
		`customlink` varchar(225) default NULL,
		`nolink` tinyint(1) NOT NULL,
		`ctext` text NOT NULL,
		`plaintext` text NOT NULL,
		`registers` tinyint(3) NOT NULL default '0',
		`showtitle` tinyint(3) NOT NULL default '0',
		`showseccat` tinyint(3) NOT NULL default '0',
		`showcustomtext` tinyint(3) NOT NULL default '0',
		`showplaintext` tinyint(3) NOT NULL default '0',
		`showreadmore` tinyint(3) NOT NULL default '0',  
		`ordering` int(11) NOT NULL default '0',
		PRIMARY KEY (`id`)
) TYPE=MyISAM;
