<?php 
/**
 * @version		2.9.1
 * @package		Frontpage Slideshow
 * @author    JoomlaWorks - http://www.joomlaworks.gr
 * @copyright	Copyright (c) 2006 - 2011 JoomlaWorks, a business unit of Nuevvo Webware Ltd. All rights reserved.
 * @license		Commercial - This code cannot be redistributed without permission from Nuevvo Webware Ltd.
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

class JWFrontpageSlideshowHelper {

	function getTemplatePath($mod_name,$fpssTemplate){
		$find = array(JPATH_SITE,DS.'default.php',DS);
		$replace = array('','','/');
		return JURI::base(true).str_replace($find,$replace,JModuleHelper::getLayoutPath($mod_name,$fpssTemplate.DS.'default'));
	}
	
	function wordLimiter($str, $limit = 100, $end_char = '&#8230;') {
		if (trim($str) == '') return $str;
		preg_match('/\s*(?:\S*\s*){'. (int) $limit .'}/', $str, $matches);
		if (strlen($matches[0]) == strlen($str)) $end_char = '';
		return rtrim(strip_tags($matches[0])).$end_char;
	}
	
	function cleanPluginTags($str){
		$str = preg_replace('/{([a-zA-Z0-9\-_]*)\s*(.*?)}/i','', $str);
		return $str;
	}
		
	function setCrd(){
		return base64_decode("PGRpdiBzdHlsZT0iZGlzcGxheTpub25lOyI+RnJvbnRwYWdlIFNsaWRlc2hvdyB8IENvcHlyaWdodCAmY29weTsgMjAwNi0yMDExIEpvb21sYVdvcmtzLCBhIGJ1c2luZXNzIHVuaXQgb2YgTnVldnZvIFdlYndhcmUgTHRkLjwvZGl2Pg==");
	}
}
