<?php
/**
 * @version		2.9.1
 * @package		Frontpage Slideshow
 * @author    JoomlaWorks - http://www.joomlaworks.gr
 * @copyright	Copyright (c) 2006 - 2011 JoomlaWorks, a business unit of Nuevvo Webware Ltd. All rights reserved.
 * @license		Commercial - This code cannot be redistributed without permission from Nuevvo Webware Ltd.
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

// hide default Joomla! errors
error_reporting(0);

jimport('joomla.installer.installer');
$db = & JFactory::getDBO();
$status = new JObject();
$status->modules = array();
$status->plugins = array();
$src = $this->parent->getPath('source');

$modules = &$this->manifest->getElementByPath('modules');
if (is_a($modules, 'JSimpleXMLElement') && count($modules->children())) {
	foreach ($modules->children() as $module) {
		$mname = $module->attributes('module');
		$client = $module->attributes('client');
		if(is_null($client)) $client = 'site';
		($client=='administrator')? $path=$src.DS.'administrator'.DS.'modules'.DS.$mname: $path = $src.DS.'modules'.DS.$mname;
		$installer = new JInstaller;
		$result = $installer->install($path);
		$status->modules[] = array('name'=>$mname,'client'=>$client, 'result'=>$result);
	}
}

$plugins = &$this->manifest->getElementByPath('plugins');
if (is_a($plugins, 'JSimpleXMLElement') && count($plugins->children())) {
	foreach ($plugins->children() as $plugin) {
		$pname = $plugin->attributes('plugin');
		$pgroup = $plugin->attributes('group');
		$path = $src.DS.'plugins'.DS.$pgroup;
		$installer = new JInstaller;
		$result = $installer->install($path);
		$status->plugins[] = array('name'=>$pname,'group'=>$pgroup, 'result'=>$result);

		$query = "UPDATE #__plugins SET published=1 WHERE element='{$pname}' AND folder='{$pgroup}'";
		$db->setQuery($query);
		$db->query();
	}
}

if (JFolder::exists(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_joomfish'.DS.'contentelements')){
	$elements = &$this->manifest->getElementByPath('joomfish');
	if (is_a($elements, 'JSimpleXMLElement') && count($elements->children())) {
		foreach ($elements->children() as $element) {
			JFile::copy($src.DS.'administrator'.DS.'components'.DS.'com_joomfish'.DS.'contentelements'.DS.$element->data(),JPATH_ADMINISTRATOR.DS.'components'.DS.'com_joomfish'.DS.'contentelements'.DS.$element->data());
		}

	}
}
else {
	$mainframe = &JFactory::getApplication();
	$mainframe->enqueueMessage(JText::_('Notice: Content elements for Joom!Fish were not copied to the related folder, because Joom!Fish was not found on your system.'));
}

// Configuration file
if(!JFile::exists(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_fpss'.DS.'configuration.php')){
	$buffer = "
<?php
class FPSSConfig {
	var \$editor = '1';
	var \$articlelist = '1';
	var \$septhumb = '0';
	var \$show_width = '0';
	var \$show_quality = '0';
	var \$basepath = 'images/stories';
}
?>
";
	JFile::write(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_fpss'.DS.'configuration.php',trim($buffer));
}

// Sample data
$query = "SELECT COUNT(*) FROM #__fpss_slides";
$db->setQuery($query);
$numOfSlides=$db->loadResult();

$query = "SELECT COUNT(*) FROM #__fpss_categories";
$db->setQuery($query);
$numOfCategories=$db->loadResult();

if($numOfSlides==0 AND $numOfCategories==0){
	$query = "INSERT INTO `#__fpss_categories` (`id`, `name`, `width`, `quality`, `width_thumb`, `quality_thumb`, `published`) VALUES (1, 'Demo category', 400, 80, 100, 75, 1)";
	$db->setQuery($query);
	$db->query();

	$query = "INSERT INTO `#__fpss_slides` (`id`, `catid`, `name`, `path`, `path_type`, `thumb`, `state`, `publish_up`, `publish_down`, `itemlink`, `menulink`, `target`, `customlink`, `nolink`, `ctext`, `plaintext`, `registers`, `showtitle`, `showseccat`, `showcustomtext`, `showplaintext`, `showreadmore`, `ordering`) VALUES 
	(1, 1, 'Powers thousands of sites worldwide', 'components/com_fpss/images/theotherguys.jpg', '1', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 1, 0, '', 0, 'Frontpage Slideshow is used in websites visited by millions of people everyday around the world like Linux.com, BettyConfidential.com, the World Health Organization and many more. This slide is linked by default to a menu item, which is part of the default Joomla! sample data.', 'From corporate sites to \"big league\" magazines & portals', 0, 1, 1, 1, 1, 1, 4), 
	(2, 1, 'Unlimited slideshows, MVC templating', 'components/com_fpss/images/ateam.jpg', '1', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 0, 1, '', 0, 'Create unlimited slideshows within your Joomla! site. Use the power of MVC templating to create new slideshow templates or modify the pre-packaged ones. This slide is linked by default to an article, which is part of the default Joomla! sample data.', 'Powerful, yet simple to use', 0, 1, 1, 1, 1, 1, 3), 
	(3, 1, 'Frontpage Slideshow is Search Engine Friendly!', 'components/com_fpss/images/theinvasion.jpg', '1', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 'http://www.joomlaworks.gr/fpss', 0, 'Unlike Flash based slideshows, Frontpage Slideshow uses CSS and Javascript wizardry only. The content of the slides is laid out as html code, which means it can be crawled by search engines. The proper usage (and order) of h1, h2, p (and more) tags will make sure Google (or any other search engine) regularly indexes your latest/featured items.', '...And iPhone &amp; iPad friendly too!', 0, 1, 0, 1, 1, 1, 2), 
	(4, 1, 'Frontpage Slideshow', 'components/com_fpss/images/knightandday.jpg', '1', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 'http://www.frontpageslideshow.net/', 0, 'Frontpage SlideShow is the most eye-catching way to display your featured articles, stories or even products in your Joomla! based website.', 'Eye-candy for any Joomla! based website', 0, 1, 1, 1, 1, 1, 1)
	";
	$db->setQuery($query);
	$db->query();
}

?>

<?php $rows = 0;?>
<img src="components/com_fpss/images/fpss_logo_460x60.png" width="460" height="60" alt="Frontpage Slideshow" align="right" />
<h2><?php echo JText::_('Frontpage Slideshow Installation Status'); ?></h2>
<table class="adminlist">
	<thead>
		<tr>
			<th class="title" colspan="2"><?php echo JText::_('Extension'); ?></th>
			<th width="30%"><?php echo JText::_('Status'); ?></th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td colspan="3"></td>
		</tr>
	</tfoot>
	<tbody>
		<tr class="row0">
			<td class="key" colspan="2"><?php echo JText::_('Component'); ?></td>
			<td><strong><?php echo JText::_('Installed'); ?></strong></td>
		</tr>
		<?php if (count($status->modules)) : ?>
		<tr>
			<th><?php echo JText::_('Module'); ?></th>
			<th><?php echo JText::_('Client'); ?></th>
			<th></th>
		</tr>
		<?php foreach ($status->modules as $module) : ?>
		<tr class="row<?php echo (++ $rows % 2); ?>">
			<td class="key"><?php echo $module['name']; ?></td>
			<td class="key"><?php echo ucfirst($module['client']); ?></td>
			<td><strong><?php echo ($module['result'])?JText::_('Installed'):JText::_('Not installed'); ?></strong></td>
		</tr>
		<?php endforeach;?>
		<?php endif;?>
		<?php if (count($status->plugins)) : ?>
		<tr>
			<th><?php echo JText::_('Plugin'); ?></th>
			<th><?php echo JText::_('Group'); ?></th>
			<th></th>
		</tr>
		<?php foreach ($status->plugins as $plugin) : ?>
		<tr class="row<?php echo (++ $rows % 2); ?>">
			<td class="key"><?php echo ucfirst($plugin['name']); ?></td>
			<td class="key"><?php echo ucfirst($plugin['group']); ?></td>
			<td><strong><?php echo ($plugin['result'])?JText::_('Installed'):JText::_('Not installed'); ?></strong></td>
		</tr>
		<?php endforeach; ?>
		<?php endif; ?>
	</tbody>
</table>
