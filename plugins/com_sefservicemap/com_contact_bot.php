<?php
//*******************************************************
//* SEF Service Map Component
//* http://www.sefservicemap.com
//* (C) Radoslaw Kubera
//* license http://www.gnu.org/copyleft/gpl.html GNU/GPL
//*******************************************************

/** ensure this file is being included by a parent file */
defined( '_JEXEC' ) or die( 'Restricted access' );

function Contact_items ($level, $id,$Itemid,$params) {
	$priority=$params->get( 'priority', '' ) ;
	$changefreq=$params->get( 'changefreq', '' );
	$show_item_icons=intval($params->get( 'show_item_icons', '1' )) ;
	$db = JFactory::getDBO();

	$user	= &JFactory::getUser();
	$aid	= (int) $user->get('aid', 0);

	if ($id) $cat_sel = " and a.catid='".$id."'"; else $cat_sel='';
	$db->setQuery("SELECT a.*, b.alias as 'catalias' FROM #__contact_details AS a LEFT JOIN #__categories AS b ON a.catid = b.id where a.published >='1' ".$cat_sel." and a.access<='$aid' order by a.ordering");
	$items=$db->loadObjectList();
	$lev=$level+1;
	if (count($items!=0)) {
		foreach ($items as $item) {
			$link="index.php?option=com_contact&amp;view=contact&amp;id=".$item->id.":".$item->alias."&amp;catid=".$item->catid.";".$item->catalias."&amp;Itemid=".$Itemid;
			$element->name = $item->name;
			$element->link = $link;
			$element->level = $lev;
			$element->priority = $priority;
			$element->changefreq= $changefreq;
			if ($show_item_icons) $element->image = 'contact_people.gif'; else $element->image='none';
			$element->icon_path='/plugins/com_sefservicemap/';
			AddExtMenuItem ($element);
		}
	}
}

function com_contact_bot ($level,$link,$Itemid,$params) {
	$catid = smGetParam($link,'catid',0);
	$view = smGetParam($link,'view','');
	if ($view=='category') Contact_items ($level,$catid,$Itemid,$params);
}
?>