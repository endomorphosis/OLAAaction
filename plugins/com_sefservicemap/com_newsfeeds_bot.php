<?php
//*******************************************************
//* SEF Service Map Component
//* http://www.sefservicemap.com
//* (C) Radoslaw Kubera
//* license http://www.gnu.org/copyleft/gpl.html GNU/GPL
//*******************************************************

/** ensure this file is being included by a parent file */
defined( '_JEXEC' ) or die( 'Restricted access' );

function Newsfeeds_items ($level, $id,$Itemid,$params,$alias) {
	$db = JFactory::getDBO();
	if ($alias=='') {
		$query = "select alias from #__category where id='$id'";
		$db->setQuery($query);
		$alias = $db->loadResult();
	}
	$priority=$params->get( 'priority', '' ) ;
	$changefreq=$params->get( 'changefreq', '' );
	$show_item_icons=intval($params->get( 'show_item_icons', '1' )) ;
	$db->setQuery("SELECT * FROM #__newsfeeds where catid='$id' and published >='1' order by ordering");
	$items=$db->loadObjectList();
	$lev=$level+1;
	if (count($items!=0)) {
		foreach ($items as $item) {
			$link="index.php?view=newsfeed&amp;catid=".$id.":".$alias."&amp;id=".$item->id."-".$item->alias."&amp;option=com_newsfeeds&amp;Itemid=".$Itemid;
			$element->name = $item->name;
			$element->link = $link;
			$element->level = $lev;
			$element->priority = $priority;
			$element->changefreq= $changefreq;
			if ($show_item_icons) $element->image = 'newsfeeds_rss.gif'; else $element->image='none';
			$element->icon_path='/plugins/com_sefservicemap/';
			AddExtMenuItem ($element);
		}
	}
}

function Newsfeeds_category ($level, $id,$Itemid,$params) {
	$priority=$params->get( 'priority', '' ) ;
	$changefreq=$params->get( 'changefreq', '' );
	$show_cat_icons=intval($params->get( 'show_cat_icons', '1' )) ;
	$show_cat_desc=intval($params->get( 'show_content_desc', '1' )) ;
	$show_mode=intval($params->get( 'show_mode', '1' )) ;
	$db = JFactory::getDBO();

	$user	= &JFactory::getUser();
	$aid	= (int) $user->get('aid', 0);

	$db->setQuery("SELECT * FROM #__categories where parent_id='$id' and section='com_newsfeeds' and published >='1' and access<='$aid'"
	." order by ordering");
	$categories=$db->loadObjectList();
	$lev=$level+1;
	if (count($categories!=0)) {
		foreach ($categories as $category) {
			$description = '';
			if ($show_cat_desc) $description = $category->name;
			$element->description = $description;

			$link="index.php?view=category&amp;id=".$category->id.":".$category->alias."&amp;option=com_newsfeeds&amp;Itemid=".$Itemid;
			$element->name = $category->title;
			$element->link = $link;
			$element->level = $lev;
			$element->priority = $priority;
			$element->changefreq= $changefreq;
			if ($show_cat_icons) $element->image = 'newsfeeds_rssdir.gif'; else $element->image='none';
			$element->icon_path='/plugins/com_sefservicemap/';
			AddExtMenuItem ($element);
			if ($show_mode) Newsfeeds_items ($lev, $category->id,$Itemid,$params,$category->alias);
			Newsfeeds_category($lev, $category->id,$Itemid,$params);
		}
	}
}


function com_newsfeeds_bot ($level,$link,$Itemid,$params) {
	$view = smGetParam($link,'view','');
	$catid = smGetParam($link,'id',0);

	if ($view=='categories') Newsfeeds_category($level,$catid,$Itemid,$params);
	else if ($view=='category') Newsfeeds_items ($level, $catid,$Itemid,$params,'');
}

?>