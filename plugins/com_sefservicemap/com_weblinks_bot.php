<?php
//*******************************************************
//* SEF Service Map Component
//* http://www.sefservicemap.com
//* (C) Radoslaw Kubera
//* license http://www.gnu.org/copyleft/gpl.html GNU/GPL
//*******************************************************

/** ensure this file is being included by a parent file */
defined( '_JEXEC' ) or die( 'Restricted access' );

function Weblinks_category ($level, $id,$Itemid,$params) {
	$priority=$params->get( 'priority', '' ) ;
	$changefreq=$params->get( 'changefreq', '' );
	$show_cat_icons=intval($params->get( 'show_cat_icons', '1' )) ;
	$show_cat_desc=intval($params->get( 'show_cat_desc', '1' )) ;

	$db = JFactory::getDBO();

	$user	= &JFactory::getUser();
	$aid	= (int) $user->get('aid', 0);

	$db->setQuery("SELECT * FROM #__categories where parent_id='$id' and section='com_weblinks' and published >='1' and access <='$aid' order by ordering");
	$categories=$db->loadObjectList();
	$lev=$level+1;
	if (count($categories!=0)) {
	if ($categories)
		foreach ($categories as $category) {
			$description = '';
			if ($show_cat_desc) $description = $category->name;
			$element->description = $description;
			$link="index.php?option=com_weblinks&amp;view=category&amp;id=".$category->id.":".$category->alias."&amp;Itemid=".$Itemid;
			$element->name = $category->title;
			$element->link = $link;
			$element->level = $lev;
			$element->priority = $priority;
			$element->changefreq= $changefreq;
			if ($show_cat_icons) $element->image = 'weblinks_world.gif'; else $element->image='none';
			$element->icon_path='/plugins/com_sefservicemap/';
			AddExtMenuItem($element);
			Weblinks_category($lev, $category->id,$Itemid,$params);
		}
	}
}


function com_weblinks_bot ($level,$link,$Itemid,$params) {
 	$catid = smGetParam($link,'catid',0);
	$view = smGetParam($link,'view','');
	if ($view=='categories') Weblinks_category($level,$catid,$Itemid,$params);
}
?>