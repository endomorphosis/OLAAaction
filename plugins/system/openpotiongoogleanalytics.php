<?php
######################################################################
# OPENPOTION Google Analytics          	          	          	     #
# Copyright (C) 2010 by OPENPOTION  	   	   	   	   	   	   	   	 #
# Homepage   : www.openpotion.com		   	   	   	   	   	   		 #
# Author     : Jason Hull      		   	   	   	   	   	   	   	   	 #
# Email      : jason@openpotion.com 	   	   	   	   	   	   	     #
# Version    : 1.0                        	   	    	   	   	   	 #
# License    : http://www.gnu.org/copyleft/gpl.html GNU/GPL          #
######################################################################

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.plugin.plugin');

class plgSystemOpenPotiongoogleanalytics extends JPlugin
{
	function plgSystemOpenPotiongoogleanalytics(&$subject, $config)
	{
		parent::__construct($subject, $config);
		
    $this->_plugin = JPluginHelper::getPlugin( 'system', 'openpotiongoogleanalytics' );
    $this->_params = new JParameter( $this->_plugin->params );
	}
	
	function onAfterRender()
	{
		global $mainframe;
		
		$web_property_id = $this->params->get('web_property_id', '');
		
		if($web_property_id == '' || $mainframe->isAdmin() || strpos($_SERVER["PHP_SELF"], "index.php") === false)
		{
			return;
		}

    $buffer = JResponse::getBody();

		$google_analytics_javascript = '
			
			<script type="text/javascript">
			  var _gaq = _gaq || [];
			  _gaq.push([\'_setAccount\', \''.$web_property_id.'\']);
			  _gaq.push([\'_trackPageview\']);

			  (function() {
			    var ga = document.createElement(\'script\'); ga.type = \'text/javascript\'; ga.async = true;
		    	ga.src = (\'https:\' == document.location.protocol ? \'https://ssl\' : \'http://www\') + \'.google-analytics.com/ga.js\';
    			(document.getElementsByTagName(\'head\')[0] || document.getElementsByTagName(\'body\')[0]).appendChild(ga);
			  })();

			</script>
			';

		$pos = strrpos($buffer, "<body");
		
		if($pos > 0)
		{
			$pos = strpos($buffer,">",$pos) + 1;
			$buffer = substr($buffer, 0, $pos).$google_analytics_javascript.substr($buffer, $pos);

			JResponse::setBody($buffer);
		}
		
		return true;
	}
}
?>