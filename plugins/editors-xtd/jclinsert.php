<?php
/*
 **********************************************
 JCal Pro event insert plugin
 Copyright (c) 2009-2010 Anything-Digital.com
 **********************************************
 JCal Pro is a fork of the existing Extcalendar component for Joomla!
 (com_extcal_0_9_2_RC4.zip from mamboguru.com).
 Extcal (http://sourceforge.net/projects/extcal) was renamed
 and adapted to become a Mambo/Joomla! component by
 Matthew Friedman, and further modified by David McKinnis
 (mamboguru.com) to repair some security holes.

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This header must not be removed. Additional contributions/changes
 may be added to this header as long as no information is deleted.
 **********************************************

 * JCal pro event insert plugin
 *
 * $Id: jclinsert.php 661 2010-08-17 16:49:47Z shumisha $
 *
 * Plugin for inserting events or links to events in content
 * using Joomla editor

 **********************************************
 Get the latest version of JCal Pro at:
 http://dev.anything-digital.com//
 **********************************************
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.plugin.plugin' );

/**
 * Editor Image buton
 *
 * @package Editors-xtd
 * @since 1.5
 */
class plgButtonJclinsert extends JPlugin
{
	/**
	 * Constructor
	 *
	 * For php4 compatability we must not use the __constructor as a constructor for plugins
	 * because func_get_args ( void ) returns a copy of all passed arguments NOT references.
	 * This causes problems with cross-referencing necessary for the observer design pattern.
	 *
	 * @param 	object $subject The object to observe
	 * @param 	array  $config  An array that holds the plugin configuration
	 * @since 1.5
	 */
	function plgButtonJclinsert(& $subject, $config)
	{
		parent::__construct($subject, $config);
	}

	/**
	 * Display the button
	 *
	 * @return array A two element array of ( imageName, textToInsert )
	 */
	function onDisplay($name)
	{

	  global $mainframe;
	  
	  // load our language file
	  JPlugin::loadLanguage( 'plg_editors-xtd_jclinsert', JPATH_ADMINISTRATOR);
	  
	  // insert additionnal css for our button
	  $css = '.button2-left .plg_editors-xtd_jclinsert {
              background: url(' . JURI::base() . ($mainframe->isAdmin() ? '../' : '') .  'components/com_jcalpro/images/jcl_editor_insert_button.png) 100% 0 no-repeat;
           }';
    $document = &JFactory::getDocument();
    $document->addStyleDeclaration( $css);
	  
		$link = 'index.php?option=com_jcalpro&amp;task=editorinsert&amp;tmpl=component&amp;e_name='.$name;

		JHTML::_('behavior.modal');

		$button = new JObject();
		$button->set('modal', true);
		$button->set('link', $link);
		$button->set('text', JText::_('PLG_JCLINSERT_BUTTON_TITLE'));
		$button->set('name', 'plg_editors-xtd_jclinsert');
		//$button->set('name', 'blank');
		$button->set('options', "{handler: 'iframe', size:{x:window.getSize().scrollSize.x*0.9, y: window.getSize().size.y*0.9}}");

		return $button;
	}
  
}
