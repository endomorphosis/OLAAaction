<?php die("Access Denied"); ?>
a:4:{s:4:"body";s:31725:"<div class="componentheading">
	Recent News</div>
<table class="blog" cellpadding="0" cellspacing="0">

<tr>
	<td valign="top">
		<table width="100%"  cellpadding="0" cellspacing="0">
		<tr>
												<td valign="top"
						width="100%"
						class="article_column">
						
<table class="contentpaneopen">
<tr>
		<td class="contentheading" width="100%">
				<a href="/en/news/144-2012-a-beyond-latino-health-equity-conference" class="contentpagetitle">
			2012 &amp; Beyond: Latino Health Equity Conference</a>
			</td>
	
		<td align="right" width="100%" class="buttonheading">
	<a href="/en/news/144-2012-a-beyond-latino-health-equity-conference?format=pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/new_architect/images/pdf_button.png" alt="PDF"  /></a>	</td>
	
		<td align="right" width="100%" class="buttonheading">
	<a href="/en/news/144-2012-a-beyond-latino-health-equity-conference?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Print" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/new_architect/images/printButton.png" alt="Print"  /></a>	</td>
	
		<td align="right" width="100%" class="buttonheading">
	<a href="/en/component/mailto/?tmpl=component&amp;link=9674e5c5f772c38246ee11e3a1f0f061a9953222" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/new_architect/images/emailButton.png" alt="E-mail"  /></a>	</td>
		   </tr>
</table>
<table class="contentpaneopen">




<tr>
<td valign="top" colspan="2">
<strong> </strong>
<p> </p>
<p>The Latino Health Equity Conference will provide a forum to focus on individual and community pathways to health through research, programs and policies for Latinos.</p>
<p>This interactive summit will build the path for a Latino Health Initiative with strategies to eliminate health disparities.</p>
<p>The goals will be to:</p>
<ol start="1">
<li>Enhance participants’      understanding of social, economic and political factors that impact health      for Latinos.</li>
<li>Understand major disease      prevention and health promotion challenges facing Latinos locally,      nationally and globally and potential solutions to those challenges.</li>
<li>Build understanding of      culturally appropriate public health practice and research to promote      health for Latinos.</li>
<li>Develop partnerships between      Latino communities and key stakeholders in building community capacity for      health through research, education and advocacy.</li>
</ol>
<p>Conference proceedings will be developed into a white paper which will assist local and national policy and decision makers in establishing funding priorities for Latino health.</p>
<p>Participants will include:</p>
<ul>
<li>Community members and community      organizations</li>
<li>Health professionals</li>
<li>Students</li>
<li>Educators and researchers</li>
<li>Local, national and global      organizations dedicated to community health</li>
<li>City, county and state      administrators</li>
</ul>
<p> </p>
<p> </p>
<p> </p>
<p><strong>Keynote Speaker</strong></p>
<p><strong> </strong>Dr. Pastor is Professor of American Studies & Ethnicity at the  University of Southern California. Founding director of the Center for  Justice, Tolerance, and Community at the University of California, Santa  Cruz, Pastor currently directs the Program for Environmental and  Regional Equity at USC and is co-director of USC’s Center for the Study  of Immigrant Integration. Dr. Pastor’s research has generally focused on  issues of environmental justice, regional inclusion, and the economic  and social conditions facing low-income urban communities. His most  recent book, Uncommon Common Ground: Race and America’s Future (W.W.  Norton 2010; co-authored with Angela Glover Blackwell and Stewart Kwoh),  documents the gap between progress in racial attitudes and racial  realities, and offers a new set of strategies for both talking about  race and achieving racial equity.</p>
<p> </p>
<p> </p>
<p> </p>
<p><strong>About Familia En Accion</strong></p>
<br />
<p style="margin: 0.1pt 0in;">Familias en Acción was founded in 1998 in response to a growing need by the Latino community in the Portland Metro area for a Latino culturally specific community based organization focused on family well-being.</p>
<p style="margin: 0.1pt 0in;"> </p>
<p style="margin: 0.1pt 0in;"> </p>
<p style="margin: 0.1pt 0in;">Familias en Acción is a health promotion and advocacy organization dedicated to health equity for Latinos in Oregon and SW Washington. Leadership development and family empowerment are central to the mission and purpose of Familias en Acción. Training is provided to community members and promotores/community health workers building leadership and knowledge of health, family wellness, mental health, health equity and other topics important to healthy family functioning.</p>
<p style="margin: 0.1pt 0in;"> </p>
<p style="margin: 0.1pt 0in;"> </p>
<p style="margin: 0.1pt 0in;">In addition to our health promotion programs we work in research partnership with the University of Portland and Oregon Health and Science University (OHSU). All staff and promotores/community health workers are trained in Community Based Participatory Research.</p>
<p style="margin: 0.1pt 0in;"> </p>
<p style="margin: 0.1pt 0in;"> </p>
<p style="margin: 0.1pt 0in;"> </p>
<p style="margin: 0.1pt 0in;"> </p>
<hr /></td>
</tr>



</table>
<span class="article_separator">&nbsp;</span>

<table class="contentpaneopen">
<tr>
		<td class="contentheading" width="100%">
				<a href="/en/news/143-alabamas-message-for-oregon-self-defeat" class="contentpagetitle">
			Alabama's message for Oregon: self-defeat</a>
			</td>
	
		<td align="right" width="100%" class="buttonheading">
	<a href="/en/news/143-alabamas-message-for-oregon-self-defeat?format=pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/new_architect/images/pdf_button.png" alt="PDF"  /></a>	</td>
	
		<td align="right" width="100%" class="buttonheading">
	<a href="/en/news/143-alabamas-message-for-oregon-self-defeat?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Print" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/new_architect/images/printButton.png" alt="Print"  /></a>	</td>
	
		<td align="right" width="100%" class="buttonheading">
	<a href="/en/component/mailto/?tmpl=component&amp;link=bcb269716bb82edd77fadf22c043ab08cead2122" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/new_architect/images/emailButton.png" alt="E-mail"  /></a>	</td>
		   </tr>
</table>
<table class="contentpaneopen">




<tr>
<td valign="top" colspan="2">
<div id="PrintContainer">
<h5 class="updated" title="2011-10-07T02:33:45Z">Published: Thursday, October 06, 2011,  7:33 PM By  	 	 	 	 		 			 	 		 			<strong> The Oregonian Editorial Board </strong> The Oregonian</h5>
<p> </p>
<h5 class="updated" title="2011-10-07T02:33:45Z"></h5>
<p>You'd think a killer virus was set loose upon northeast Alabama over the past week. That's how quickly people rushed into panic, some of them packing up and feeding pets for the last time before driving over the state line. And among those who stayed, thousands went into seclusion: Children avoided school and pregnant women stayed home, afraid to go to the hospital.</p>
<p> </p>
<p> </p>
<p><br /> This is the price of having a last name like Gonzalez or Rodriguez and being undocumented in a state that fears Latino people are ruining the party.</p>
<p> </p>
<p><br /> <br /> This is not new. But Alabama's new immigration law -- patterned after efforts in Arizona and Georgia and upheld by a judge last week -- is peculiarly narrow-minded in its belief that jobs will somehow be saved for upright Alabamans struggling to find employment, so much of it in the agriculture and poultry processing sectors. The idea behind the law is to root out those unable to prove they're there legally, hold employers who hire them to account, question the right of American-born children of undocumented workers to attend public school, and otherwise strike fear into the heart of melting pot America.</p>
<p> </p>
<p> </p>
<p><br /> For the moment and on legal grounds only, Alabama wins. But for months and years ahead, Alabama shows itself to be an epic loser that Oregon and every other state needs to watch as Exhibit A in how to blow holes in the feet: economically, politically, socially. What the law does besides scare people off, as tracked by The New York Times, is to create among those who stay an uneducated and needy underclass.</p>
<p> </p>
<p> </p>
<p><br /> From our country's inception, immigrants have been our lifeblood. In recent decades, however, illegal immigration has posed challenge: At last count, there were more than 11 million undocumented folks embedded across the country. Yet in recent years, with the sputtering economy and high unemployment, some states desperate for jobs retrench, lay blame and think in protectionist ways. And that's where the self-defeating myopia sets in.</p>
<p><br /> <br /> Georgia passed its anti-immigration law in April yet already watches thousands of acres of agricultural crops go to ruin, unharvested. A report released Tuesday by the Center for American Progress estimates Georgia's crop losses this year alone will hit $300 million while the law's broader damage to the state's economy this year could reach $1 billion.</p>
<p> </p>
<p> </p>
<p> </p>
<p>This, too, is not new. The Oregon Center for Public Policy in 2007 calculated that unauthorized workers in Oregon paid $134 million to $187 million in combined state and federal taxes on wages in 2005. For those who would view their Oregon employers as scofflaws, consider that $97 million to $136 million in taxes were paid by employers on behalf of unauthorized workers in the same year.</p>
<p> </p>
<p> </p>
<p><br /> Separately, the Pew Hispanic Center estimated well more than 5 percent of Oregon's workforce in 2008 was unauthorized immigrants, while 2010 U.S. Census data showed Hispanics overall making up nearly 12 percent of Oregon's population, up from 8 percent a decade earlier.</p>
<p> </p>
<p> </p>
<p><br /> <br /> There's no going back, here or in Alabama, which has only a fraction of Oregon's immigrants. Long-overdue immigration reform calls upon states and the Congress not only to restate our best purposes in fostering a diverse society but to view all measures taken in dollars and cents.</p>
<p> </p>
<p> </p>
<p><br /> The so-called reforms we're seeing in Arizona, Georgia and now Alabama only hurt regional economies, undercutting our best odds of finding national prosperity again. Treating these initiatives as viruses that do real harm would be a step in the right direction.</p>
<p> </p>
<p> </p>
<p>© 2011 OregonLive.com. All rights reserved.</p>
<p> </p>
<p> </p>
</div>
<hr /></td>
</tr>



</table>
<span class="article_separator">&nbsp;</span>

<table class="contentpaneopen">
<tr>
		<td class="contentheading" width="100%">
				<a href="/en/news/142-breaking-business-barriers" class="contentpagetitle">
			Breaking business barriers</a>
			</td>
	
		<td align="right" width="100%" class="buttonheading">
	<a href="/en/news/142-breaking-business-barriers?format=pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/new_architect/images/pdf_button.png" alt="PDF"  /></a>	</td>
	
		<td align="right" width="100%" class="buttonheading">
	<a href="/en/news/142-breaking-business-barriers?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Print" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/new_architect/images/printButton.png" alt="Print"  /></a>	</td>
	
		<td align="right" width="100%" class="buttonheading">
	<a href="/en/component/mailto/?tmpl=component&amp;link=0b01ea7eccf5565dc2f03e2da0144104ab0f8e81" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/new_architect/images/emailButton.png" alt="E-mail"  /></a>	</td>
		   </tr>
</table>
<table class="contentpaneopen">




<tr>
<td valign="top" colspan="2">
<div><img src="http://theworldlink.com/app/images/site/logo.gif" alt="The World" /></div>
<h1></h1>
<p><strong>By Gail Elber, The World |  Posted: Friday, October 28, 2011 10:24 am</strong></p>
<p> </p>
<p><strong><br /></strong></p>
<p>NORTH BEND — Starting and running a business is hard for anyone. But if English isn’t your first language, it’s even harder.</p>
<p>“There’s a language barrier and cultural differences,” said Rene Quintana. “We see it, and sometimes we feel it.”</p>
<p>Quintana is the founder of Manos Unidos, an organization based in Crescent City, Calif., whose purpose is “to inform, improve, assist, encourage and strengthen the cultural roots, traditions and values of the Latino community.”</p>
<p> </p>
<p> </p>
<p>At the group’s second Latino Business Summit in North Bend Wednesday, elected officials, government agency representatives and business managers praised the help they’ve gotten from Manos Unidos in reaching out to Latino customers, clients and constituents.</p>
<p>Group makes connections</p>
<p> </p>
<p> </p>
<p> </p>
<div class="jce_caption" style="width: 300px; margin: 15px; float: left; display: inline-block;"><img style="float: left;" alt="4eaae5f9966a5.preview-300" src="images/stories/4eaae5f9966a5.preview-300.jpg" height="200" width="300" />
<div style="text-align: center; clear: both;">Benjamin Brayfield</div>
</div>
The name —&nbsp;“Manos Unidos,” rather than the grammatically correct “Unidas” —&nbsp;is a play on words. “That’s what makes us different from everybody else,”&nbsp; Quintana said. “The wording came from the farm workers. When we were discussing the name, someone said ‘Manos Unidas,’ and then they said it should be ‘Unidos,’ like ‘Estados Unidos (United States).’ It’s a play on words to show that we’re different. A lot of agencies are called Manos Unidas, but we wanted to make sure that the word ‘united’ is clear to our participants.”
<p>Quintana didn’t originally set out to be a community organizer. After stints at the Harvard Business School and Oxford University, he landed at the University of California at Berkeley to pursue a BA.</p>
<p> </p>
<p> </p>
<p>“At Berkeley, there was so much going on on campus, and that exposed me to what was going on in the world,” he said. “I started getting involved with the neighborhood outreach programs.”</p>
<p>He moved to Crescent City and continued his work there, eventually organizing a school for farm workers in Smith River. “We taught English, computer training, civics —&nbsp;all those good things,” he said.</p>
<p> </p>
<p> </p>
<p>Ten years ago, he helped start Manos Unidos. Since then, the organization has worked in Humboldt and Del Norte counties and in the Brookings, Medford and Coos Bay areas. In October, the group got a $10,000 grant from the U.S. Department of Agriculture to facilitate participation by Latinos in the agency’s agricultural and business assistance programs.</p>
<p>Quintana said he works with people from Mexico, Argentina, Peru and other countries who often are wary of applying for a grant, joining a chamber of commerce or volunteering for a city advisory commission because they have experience with corrupt agencies in their native country.</p>
<p> </p>
<p> </p>
<p>Also, “there’s a digital divide,” he said. “Latinos are not using computers, which are a primary source of information.”</p>
<p> </p>
<p> </p>
<p><strong>Becoming more accessible</strong></p>
<p> </p>
<p> </p>
<p>Wednesday’s gathering showed that he’s been working not only with Latinos but also with government agencies and companies to eliminate misconceptions on both sides.</p>
<p>With Quintana translating, Coos Bay Mayor Crystal Shoji explained how her city’s departments are organized. She encouraged attendees to apply for a current opening on the Parks Commission, emphasizing that those commissioners don’t have to speak in public very much.</p>
<p> </p>
<p> </p>
<p>“I don’t think it would be a scary thing, whether you have good English or bad English,” she said.</p>
<p> </p>
<p> </p>
<p>About one-third of her audience of 50 consisted of business people and other interested guests from the Latino community. The other two-thirds was representatives, Latino and otherwise, from agencies and companies that have worked with Manos Unidos.</p>
<p> </p>
<p> </p>
<p>Some of them, including Shoji, North Bend Mayor Rick Wetherell, and representatives of Bank of America and Northwest Community Credit Union, got awards honoring their efforts to make their organizations more accessible to Spanish speakers, by hiring bilingual employees and providing bilingual materials.</p>
<p> </p>
<p> </p>
<p>Representatives of the Bay Area Chamber of Commerce, Oregon Employment Department, USDA Rural Programs and Southwestern Oregon Community College’s Business Development Center spoke about their organizations.</p>
<p> </p>
<p> </p>
<p>After the meeting, Celso Ledesma, a business advisor at the Business Development Center, said he encounters many Latino entrepreneurs who have the drive to start businesses but don’t understand the rules about taxes, accounting, and payroll.</p>
<p> </p>
<p> </p>
<p>“They don’t understand why they can’t just take money out of the cash register,” he said. “They have to put themselves on the payroll.”</p>
<p>Ledesma, who worked in banking for 35 years in his native Peru, can help them figure out planning and licensing requirements and can connect them with Spanish-speaking accountants and tax preparers.</p>
<p> </p>
<p><strong><br /></strong></p>
<p><strong>Connections help everyone</strong></p>
<p> </p>
<p> </p>
<p>Maria Diaz, owner of the Latino grocery store Mi Ranchito, hosted Wednesday’s gathering in her store’s party room. A native of Puerto Rico who lived for years in California, she recently moved to North Bend with her family, seeking better schools and a safer environment for her teenage children. Now she’s interested in getting involved in the community. “I’m going to some meetings and hoping to join the Chamber of Commerce,” she said after the meeting.</p>
<p> </p>
<p> </p>
<p>Norma Peña Salinas, a Manos Unidos board member and editor of the Spanish newspaper La Bandera, told the group that when people reach out in that way, everyone benefits. With Quintana translating, she said, “People who know the Hispanic community, the Latinos, know that we have a big heart.</p>
<p> </p>
<p> </p>
<p>“Enjoy and learn if you have a Hispanic neighbor or coworker. You will have a sincere friend in the good times and the bad times.”</p></td>
</tr>



</table>
<span class="article_separator">&nbsp;</span>

<table class="contentpaneopen">
<tr>
		<td class="contentheading" width="100%">
				<a href="/en/news/141-in-oregon-latino-homeowners-surges-as-black-homeownership-plummets" class="contentpagetitle">
			In Oregon, Latino homeowners surges as black homeownership plummets</a>
			</td>
	
		<td align="right" width="100%" class="buttonheading">
	<a href="/en/news/141-in-oregon-latino-homeowners-surges-as-black-homeownership-plummets?format=pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/new_architect/images/pdf_button.png" alt="PDF"  /></a>	</td>
	
		<td align="right" width="100%" class="buttonheading">
	<a href="/en/news/141-in-oregon-latino-homeowners-surges-as-black-homeownership-plummets?tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Print" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/new_architect/images/printButton.png" alt="Print"  /></a>	</td>
	
		<td align="right" width="100%" class="buttonheading">
	<a href="/en/component/mailto/?tmpl=component&amp;link=f8fca71a64c88fa0d8542c9518099f15f73a3c05" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/new_architect/images/emailButton.png" alt="E-mail"  /></a>	</td>
		   </tr>
</table>
<table class="contentpaneopen">




<tr>
<td valign="top" colspan="2">
<h5>Published: Saturday, September 03, 2011,  9:55 PM &nbsp;&nbsp;&nbsp; Updated: Sunday, September 04, 2011, 11:17 AM</h5>
<p> </p>
<p> </p>
<div class="jce_caption" style="width: 380px; margin: 15px; float: left; display: inline-block;"><img style="float: left;" alt="Latino_homeowners" src="images/stories/Latino_homeowners.jpg" height="252" width="380" />
<div style="text-align: center; clear: both;">Dania Maxwell/The Oregonian</div>
</div>
A decade ago, Oregon's black and Latino populations shared a bleak distinction: They had the lowest homeownership rates in the state.<br /><br />But the decade that saw real estate undergo a dizzying boom and then a devastating bust bore decidedly different outcomes for the two communities. <br /><br />Latinos saw rates surge 9 percent, from 37 percent to 40.2 percent, even as overall Oregon homeownership rates dipped from 64 percent to 62 percent, recently released data from the 2010 Census show. <br /><br />But black Oregonians saw their rates slide by 12 percent, from 37.4 percent to 32.9 percent -- the biggest decline among all the state's racial and ethnic groups. The fall was so steep that Portland, the heart of Oregon's black community, had 482 fewer homeowners in 2010 than in 2000, despite gains in population.
<p> </p>
<p> </p>
<p>Experts say the numbers mark the coming of age of one community, and the washing away of hard-fought gains for another. <br /><br />"The Latino story is an immigration story. Immigrants are incredibly upwardly mobile; they start low, and they end up high," said Dowell Myers, director of the Population Dynamics Research Group at the University of Southern California. "But cities with lots of black residents have all suffered these types of declines in homeowners. African Americans just don't have the immigrant trajectory -- immigrants outpace everyone." <br /><br /><br />"They were newcomers, they were younger, and they were renters," Myers said. Fast-forward a decade, he said, and you find a community that is more established, more educated and more financially stable. <br /><br />As more Latinos became ready to buy, organizations sprouted to make owning a reality. Javier Alomia joined Portland's fledgling Latino Home Initiative, aimed at moving Latinos from renters to buyers through education and down-payment assistance, in 2005 shortly after becoming a real estate agent. <br /><br />"I saw that there was a need in the Hispanic community for homeownership to be done the right way," said Alomia, who emigrated from Ecuador 10 years ago and bought his first home in Northeast Portland in 2006. "When I started in the industry, I saw there was a lot of abuse from unscrupulous lenders."</p>
<p> </p>
<p> </p>
<p><br /><strong>"Families pull together</strong>"</p>
<p> </p>
<p><br />Today, about half of Alomia's clients are Latino, and about half of those are second-generation Americans. He said efforts to assist Latinos contributed to the increase in homeownership, but culture also played a role. <br /><br />That's how Adan Lucatero's family became homeowners. Lucatero, 32, came to the United States from Mexico 27 years ago, landing in Oregon after his mom found work in nurseries. <br /><br />The family of nine rented until his mother helped his brother buy a home a few years ago. One of Lucatero's sisters later bought the house. And this year, she helped Lucatero become a homeowner himself. <br /><br />Lucatero, who works at a Hillsboro company that makes ceramic vases and cups, said buying his Beaverton condo took three years of saving and cleaning up his credit. To him, getting the key was the final step in becoming American. <br /><br />"When I first came here, I wanted something to be proud of, that you worked hard to get," he said. "It was a family effort."</p>
<p> </p>
<p> </p>
<p>Patricia Tardiff, 54, bought her Hillsboro home in April with a $3,000 down payment, and shares expenses with her grown daughter. <br /><br />"My dream came true, and I was able to buy a place for me and my daughter," said Tardiff, a school secretary who emigrated from Colombia 12 years ago. <br /><br />It's a common story, Myers said. "You see them pooling incomes together, and so they tend to have a lot of income when buying," he said. <br /><br />That and youth -- more than half of Latino owners are 44 or younger, compared with a quarter statewide -- may have buffered Latinos against the worst of the housing crisis in Oregon. Many had less home equity to tap and avoided the kind of loans that spiraled into foreclosure for others. More earners meant the family could still make the mortgage if one lost a job. And, Myers said, many Latinos were inaccessible to predatory lenders, who in Oregon tended to speak only English.</p>
<p> </p>
<p> </p>
<p><br /><strong>"We saw it coming" </strong></p>
<p><strong><br /></strong></p>
<p><br /><br />Homeownership for black Oregonians looks very different. <br /><br />Nationwide, African Americans saw their ownership rates hit a record 50 percent in 2006 and then fall back to 45 percent in 2010, according to census data. In Oregon, the tumble to 32.9 percent made African Americans the least likely racial or ethnic group to own their homes. In Portland, black homeownership rates are the lowest in at least 20 years. <br /><br />Decline in black homeownership Doris and Joe Hawkins are losing the house Doris bought in 1964 after they took out home equity loans that they can no longer afford. The couple are part of a major decline in black homeownership rates across Oregon. Watch video<br />"We saw it coming," said Cheryl Roberts, executive director of the African American Alliance for Homeownership in Portland, "because of the economic breakdown that we've had, and the subprime lending that targeted us the most."</p>
<p> </p>
<p><img style="margin: 15px; float: left;" alt="black_homeowners" src="images/stories/black_homeowners.jpg" height="252" width="380" /><br /><br />Unlike Latino homeowners, more than 70 percent of African American homeowners in Oregon are 45 and older. Myers said many Africans Americans bought homes shortly after state and federal laws began outlawing housing discrimination. <br /><br />"The older generation was denied the right to buy homes, and once they got the right, they went crazy and bought homes as fast as they could," Myers said. African Americans saw another wave of homebuying early in the past decade, with a federal push to bring homeownership to underserved communities. <br /><br />But by the end of the decade, many of the gains had been swept away. <br /><br />In 2008, the Oregon Center for Public Policy released a study showing that black and Latino borrowers in Oregon were twice as likely to get a subprime loan as whites of the same income level. Several states and cities have sued lenders such as Wells Fargo and now-defunct Countrywide for discriminating against black and Latino borrowers by charging higher interest rates.</p>
<p> </p>
<p><br /><br /><strong>Many will lose homes </strong></p>
<p><strong><br /></strong></p>
<p><br /><br />But in some ways, African Americans were especially vulnerable, with the nation's highest unemployment rates and often only one earner in the household. <br /><br />The Center for Responsible Lending in Durham, N.C., calculates that about 11 percent of black homeowners are in some stage of foreclosure and that 1.1 million black families will lose their homes by 2012. <br /><br />Portland in 2004 also set a goal to close the homeownership gap between white residents and people of color by 2015. <br /><br />Since then, the gap between whites and Asians has closed, and the Latino gap has narrowed. But the outlook for African Americans -- with jobless rates in Oregon of 12.8 percent in 2010, compared with 11.0 percent overall-- is dire.</p>
<p> </p>
<p><br /><br />"Many fear the whole process of buying and being discriminated against, and they worry that they'll lose their jobs," said Felicia Tripp Folsom, deputy director of the Portland Housing Center, a nonprofit that helps low- and moderate-income residents of all races become homeowners. "They are afraid to buy." <br /><br />She finds the trends highlighted by the census both heartening and troubling. <br /><br />"I think it's great that Latinos are able to buy at levels they are, but my concern is why African Americans are seeing the decline," Tripp Folsom said. "Whether people want to admit it or not, something is wrong."</p>
<p> </p>
<p><br /><br />-- Nikole Hannah-Jones</p></td>
</tr>



</table>
<span class="article_separator">&nbsp;</span>
</td>
				 
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td valign="top">
		<div class="blog_more">
			<div>
	<strong>More Articles...</strong>
</div>
<ul>
	<li>
			<a class="blogsection" href="/en/news/139-national-latino-aids-awareness-day-conzientizate">
			National Latino AIDS Awareness Day: Conzientizate!</a>
	</li>
	<li>
			<a class="blogsection" href="/en/news/133-one-year-later">
			OLAA Summit: One Year Later</a>
	</li>
	<li>
			<a class="blogsection" href="/en/news/132-miracle-theaters-hispanic-heritage-month">
			Miracle Theater's Hispanic Heritage Month</a>
	</li>
</ul>
		</div>
	</td>
</tr>
<tr>
	<td valign="top" align="center">
		&lt;&lt; <span class="pagenav">Start</span> &lt; <span class="pagenav">Prev</span> <span class="pagenav">1</span> <a title="2" href="/en/news?start=4" class="pagenav">2</a> <a title="3" href="/en/news?start=8" class="pagenav">3</a> <a title="4" href="/en/news?start=12" class="pagenav">4</a> <a title="Next" href="/en/news?start=4" class="pagenav">Next</a> &gt; <a title="End" href="/en/news?start=12" class="pagenav">End</a> &gt;&gt;		<br /><br />
	</td>
</tr>
<tr>
	<td valign="top" align="center">
		Page 1 of 4	</td>
</tr>
</table>
";s:4:"head";a:10:{s:5:"title";s:11:"Recent News";s:11:"description";s:285:"The Oregon Latino Agenda for Action (OLAA) is an effort convened by Oregon Consensus of Portland State University to bring together a broad range of representatives from the Latino community throughout Oregon to collaborate in developing an &quot;Oregon Latino Agenda for Action.&quot;";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:24:"text/html; charset=utf-8";}s:8:"standard";a:2:{s:6:"robots";s:13:"index, follow";s:8:"keywords";s:53:"Latino, Oregon, action, agenda, summit, Latino summit";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:54:"/plugins/system/jcemediabox/css/jcemediabox.css?v=1017";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:69:"/plugins/system/jcemediabox/themes/squeeze/css/style.css?version=1017";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:52:"/plugins/system/jcemediabox/js/jcemediabox.js?v=1017";s:15:"text/javascript";s:52:"/plugins/system/jcemediabox/js/mediaobject.js?v=1017";s:15:"text/javascript";s:52:"/plugins/system/jcemediabox/addons/default.js?v=1017";s:15:"text/javascript";s:28:"/media/system/js/mootools.js";s:15:"text/javascript";s:27:"/media/system/js/caption.js";s:15:"text/javascript";}s:6:"script";a:1:{s:15:"text/javascript";s:680:"	JCEMediaObject.init('/', {flash:"10,0,22,87",windowmedia:"5,1,52,701",quicktime:"6,0,2,0",realmedia:"7,0,0,0",shockwave:"8,5,1,0"});JCEMediaBox.init({popup:{width:"",height:"",legacy:0,lightbox:0,shadowbox:0,resize:1,icons:1,overlay:1,overlayopacity:0.8,overlaycolor:"#000000",fadespeed:500,scalespeed:500,hideobjects:0,scrolling:"fixed",close:2,labels:{'close':'Close','next':'Next','previous':'Previous','cancel':'Cancel','numbers':'{$current} of {$total}'}},tooltip:{className:"tooltip",opacity:0.8,speed:150,position:"br",offsets:{x: 16, y: 16}},base:"/",imgpath:"plugins/system/jcemediabox/img",theme:"squeeze",themecustom:"",themepath:"plugins/system/jcemediabox/themes"});";}s:6:"custom";a:0:{}}s:7:"pathway";a:1:{i:0;O:8:"stdClass":2:{s:4:"name";s:4:"News";s:4:"link";s:19:"index.php?Itemid=15";}}s:6:"module";a:0:{}}