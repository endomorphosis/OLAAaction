<?php die("Access Denied"); ?>
a:4:{s:4:"body";s:4392:"<table class="contentpaneopen">
<tr>
		<td class="contentheading" width="100%">
				<a href="/en/get-involved/resources-and-links" class="contentpagetitle">
			Resources</a>
			</td>
				
		
					</tr>
</table>

<table class="contentpaneopen">



<tr>
<td valign="top">
<p><strong>Arts &amp; Culture</strong></p>
<ul>
<li><a target="_blank" href="http://www.milagro.org/">Miracle Theatre Group</a></li>
</ul>
<p> </p>
<p><strong>Community</strong></p>
<ul>
<li><a href="http://www.manoamanofc.org/about.html">Mano a Mano Family Center</a></li>
<li><a href="http://www.latinocommunityassociation.org/">Latino Community Association</a></li>
<li><a href="http://centrocultural.org/english.htm">Centro Cultural</a></li>
</ul>
<p> </p>
<p><strong>Education</strong></p>
<ul>
<li><a target="_blank" href="http://www.educateya.org/">Edúcate Ya</a></li>
<li><a href="http://www.haciendacdc.org/">Hacienda CDC</a></li>
<li><a target="_blank" href="http://www.latnet.org/">Latino Network</a> </li>
<li><a href="http://www.oala.info/index.asp?C=126&amp;P=27&amp;N=Home">Oregon Association of Latino Administrators </a></li>
<li><a target="_blank" href="http://www.pdx.edu/news/portland-state-commits-to-improving-latino-student-success">PSU Latino Taskforce for Student Success</a></li>
</ul>
<p> </p>
<p><strong>Economic</strong></p>
<ul>
<li><a href="http://www.hmccoregon.com/">Hispanic Metropolitan Chamber of Commerce</a></li>
<li><a target="_blank" href="http://www.rdiinc.org/">Rural Development Initiative</a></li>
</ul>
<p> </p>
<p><strong>Environmental</strong></p>
<ul>
<li><a href="http://www.verdenw.org/">Verde</a></li>
<li><a href="http://www.adelantemujeres.org/index.html">Adelante Mujeres</a></li>
</ul>
<p> </p>
<p><strong>Government</strong></p>
<ul>
<li><a href="http://www.sre.gob.mx/portland/">Consulate of Mexico</a></li>
<li><a target="_blank" href="http://www.oregon.gov/Hispanic/index.shtml">Oregon Commission on Hispanic Affairs</a></li>
</ul>
<p> </p>
<span style="text-decoration: underline;"><a target="_blank" href="http://www.sre.gob.mx/portland/"></a></span>
<p><strong>Health</strong></p>
<ul>
<li><a target="_blank" href="http://www.mchealth.org/">Multnomah County Health Department</a><br /><a target="_blank" href="http://www.oregon.gov/DHS/ph/omh/">State of Oregon Office of Multicultural Health</a></li>
</ul>
<p><strong><br /></strong></p>
<p><strong>Housing</strong></p>
<ul>
<li><a href="http://www.casaoforegon.org/">Community And Shelter Assistance (CASA)</a></li>
</ul>
<p> </p>
<p><strong>Human Rights</strong></p>
<ul>
<li><a href="http://www.causaoregon.org/">CAUSA </a></li>
<li><a href="http://www.safecommunitiesproject.com/">Safe Communities</a></li>
<li><a href="http://www.pcasc.net/">PCASC</a></li>
</ul>
<p> </p>
<p><strong>Labor</strong></p>
<ul>
<li><a href="http://www.jwjpdx.org/">Jobs with Justice</a></li>
<li><a href="http://www.pcun.org/">Pineros y Campesinos Unidos del Noroeste (PCUN)</a></li>
<li><a href="http://portlandvoz.org/">VOZ</a></li>
</ul>
<p> </p>
<p><strong>Media</strong></p>
<ul>
<li><a href="http://www.elhispanicnews.com/">El Hispanic News </a> </li>
<li><a target="_blank" href="http://www.laramedia.com/">Lara Media Services</a></li>
</ul>
<p> </p>
<p><strong>Networks</strong></p>
<ul>
<li><a target="_blank" href="http://www.tacs.org/">TACS/NAO</a></li>
</ul>
<p> </p>
<p><strong>Policy and Research Institutes</strong></p>
<ul>
<li><a target="_blank" href="http://www.pewhispanic.org/">Pew Hispanic Center</a></li>
</ul>
<p> </p>
<p><strong>Religious/Faith Community</strong></p>
<p> </p>
<p><strong>Student Groups</strong> <a href="http://latinosunidossiempre.org/"></a></p>
<ul>
<li><a href="http://latinosunidossiempre.org/">Latinos Unidos Siempre (LUS)</a></li>
<li>Movimiento Estudiatil Chicano de Aztlan (MEChA)</li>
</ul>
<ul>
</ul>
<p> </p>
<p><strong>National Organizations</strong></p>
<ul>
<li><a href="http://www.nclr.org/">National Council of La Raza</a></li>
<li><a href="http://lulac.org/">League of United Latino American Citizens (LULAC)</a></li>
<li><a href="http://www.maldef.org/index.html">Mexican American Legal Defense &amp; Education Fund (MALDEF)</a></li>
</ul>
<p> </p>
<p> </p>
<ul>
</ul>
<ul>
</ul></td>
</tr>

</table>
<span class="article_separator">&nbsp;</span>
";s:4:"head";a:10:{s:5:"title";s:9:"Resources";s:11:"description";s:285:"The Oregon Latino Agenda for Action (OLAA) is an effort convened by Oregon Consensus of Portland State University to bring together a broad range of representatives from the Latino community throughout Oregon to collaborate in developing an &quot;Oregon Latino Agenda for Action.&quot;";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:24:"text/html; charset=utf-8";}s:8:"standard";a:4:{s:6:"robots";s:13:"index, follow";s:8:"keywords";s:53:"Latino, Oregon, action, agenda, summit, Latino summit";s:5:"title";s:9:"Resources";s:6:"author";s:12:"Ash Shepherd";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:54:"/plugins/system/jcemediabox/css/jcemediabox.css?v=1017";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:69:"/plugins/system/jcemediabox/themes/squeeze/css/style.css?version=1017";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:52:"/plugins/system/jcemediabox/js/jcemediabox.js?v=1017";s:15:"text/javascript";s:52:"/plugins/system/jcemediabox/js/mediaobject.js?v=1017";s:15:"text/javascript";s:52:"/plugins/system/jcemediabox/addons/default.js?v=1017";s:15:"text/javascript";s:28:"/media/system/js/mootools.js";s:15:"text/javascript";s:27:"/media/system/js/caption.js";s:15:"text/javascript";}s:6:"script";a:1:{s:15:"text/javascript";s:680:"	JCEMediaObject.init('/', {flash:"10,0,22,87",windowmedia:"5,1,52,701",quicktime:"6,0,2,0",realmedia:"7,0,0,0",shockwave:"8,5,1,0"});JCEMediaBox.init({popup:{width:"",height:"",legacy:0,lightbox:0,shadowbox:0,resize:1,icons:1,overlay:1,overlayopacity:0.8,overlaycolor:"#000000",fadespeed:500,scalespeed:500,hideobjects:0,scrolling:"fixed",close:2,labels:{'close':'Close','next':'Next','previous':'Previous','cancel':'Cancel','numbers':'{$current} of {$total}'}},tooltip:{className:"tooltip",opacity:0.8,speed:150,position:"br",offsets:{x: 16, y: 16}},base:"/",imgpath:"plugins/system/jcemediabox/img",theme:"squeeze",themecustom:"",themepath:"plugins/system/jcemediabox/themes"});";}s:6:"custom";a:0:{}}s:7:"pathway";a:2:{i:0;O:8:"stdClass":2:{s:4:"name";s:12:"Get Involved";s:4:"link";s:18:"index.php?Itemid=9";}i:1;O:8:"stdClass":2:{s:4:"name";s:9:"Resources";s:4:"link";s:19:"index.php?Itemid=27";}}s:6:"module";a:0:{}}