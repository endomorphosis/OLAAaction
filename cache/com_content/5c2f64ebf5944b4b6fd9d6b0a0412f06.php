<?php die("Access Denied"); ?>
a:4:{s:4:"body";s:14709:"<table class="contentpaneopen">
<tr>
		<td class="contentheading" width="100%">
				<a href="/en/about-olaa/staffcommittees-and-chairs" class="contentpagetitle">
			Committees and Chairs</a>
			</td>
				
		
					</tr>
</table>

<table class="contentpaneopen">



<tr>
<td valign="top">
<p>Numerous committees have been established over the course of the planning period for the OLAA Summit.&nbsp; The work of these committees is informed by a larger OLAA Summit Committee,&nbsp; the members of which meet monthly to allow for an opportunity for participants to check in and make decisions requiring greater input for the October Summit.&nbsp; Primary among these committees are the following:</p>
<p> </p>
<p><strong>Executive Committee </strong></p>
<p>This committee is responsible for fundraising, developing political protocols, providing the overall spirit and guidelines for the Summit, guidance in future thinking, help in designating spokespersons, and overseeing documentation.</p>
<p> </p>
<p><strong>Program Committee </strong></p>
<p>This committee is responsible for the flow activities during the Summit, organizing volunteer staffing during the Summit, the production of plenary sessions, the community fair, organization of the artists' showcase, and cultural aspects of the Summit.</p>
<p> </p>
<p><strong>Outreach Committee</strong></p>
<p>This committee is responsible for ensuring regional engagement, gathering contact information, developing lists of potential participants, and conducting outreach efforts for content and input to inform the issues and activities of the Summit.</p>
<p> </p>
<p><strong>Media/PR Function Committee</strong></p>
<p>This committee has a consultative role about the overall media strategy for managing on-site press and media, and the OLAA Newsroom.</p>
<p> </p>
<p><strong>Summit</strong><strong> Venue &amp; Facilities Committee </strong></p>
<p>This committee oversees the contractual agreements and arrangements with the Salem Convention Center, and coordinates all space usage and ambiance for Summit activities.</p>
<p> </p>
<p><strong>Documentation of the Summit Committee</strong></p>
<p>This committee is charged with all cablecast/broadcast efforts, as well as any interactive communications components on the day of the Summit.&nbsp; Members will also coordinate photo, video, and audio documentation and archiving.</p>
<p> </p>
<p>For more information on past meetings please visit our <a href="index.php?option=com_content&amp;view=article&amp;id=29&amp;Itemid=36">Meeting Archive</a></p>
<p> </p>
<p>A great many people have volunteered their time and efforts to OLAA's development and planning.</p>
<p> </p>
<p><strong>Core steering committee members include:</strong></p>
<br /> 
<table border="0" style="width: 98%;">
<tbody>
<tr>
<td width="50%" valign="top">
<p><strong>Consuelo Saragoza, Co-Founder</strong></p>
<p>Senior Advisor of Public Health &amp; Community Initiatives</p>
<p>Multnomah County Health Department</p>
<p> </p>
<p><strong>Alberto Moreno, M.S.W., Executive Committee</strong></p>
<p>Migrant Health Coordinator</p>
<p>State of Oregon Office of Multicultural Health</p>
<p> </p>
<p><strong>Carmen Rubio, Fiscal Sponsor</strong></p>
<p>Executive Director</p>
<p>Latino Network</p>
<p> </p>
<p><strong>Julie Esparza-Brown</strong></p>
<p>Director</p>
<p>Portland State University Bilingual Teachers Pathway</p>
<p> </p>
<p><strong>Leticia Hernandez</strong></p>
<p>Financial Aid &amp; Scholarship Outreach Counselor</p>
<p>Linfield College, Portland Campus</p>
<p> </p>
<p><strong>Rev. Miriam Méndez</strong></p>
<p>Primera Iglesia Bautista (First Baptist Church) Portland, OR</p>
<p>George Fox Evangelical Seminary</p>
<p> </p>
<p><strong>Jessica Rodriguez Montegna</strong></p>
<p>Latino Program Manager</p>
<p>Rural Development Initiatives</p>
<p> </p>
<p><strong>Maria Rubio</strong></p>
<p>Proprietor/CFO</p>
<p>M.C.Resources Consulting</p>
<p> </p>
</td>
<td width="50%" valign="top">
<p><strong>Carlos Crespo, Co-Founder</strong></p>
<p>Director of the School of Community Health</p>
<p>Portland State University</p>
<p> </p>
<h3>Guadalupe Guajardo, Facilitator</h3>
<p>Senior Consultant</p>
<p>NAO/TACS</p>
<p> </p>
<p><strong>Olga Sanchez</strong></p>
<p>Artistic Director</p>
<p>Miracle MainStage &amp; Community Artes</p>
<p>Miracle Theatre Group</p>
<p> </p>
<p><em> </em></p>
<p><strong>Ursula Rojas Weiser</strong></p>
<p>Cónsul de Asuntos Comunitarios/Community Affairs</p>
<p>Consulate of Mexico in Portland</p>
<p> </p>
<p><strong>Victoria Lara</strong></p>
<p>Founder and Chief Marketing Officer</p>
<p>Lara Media Services</p>
<p> </p>
<p><strong>Veronica Valenzuela</strong></p>
<p>Policy Advisor</p>
<p>Multnomah County Commissioner Deborah Kafoury's Office</p>
<p> </p>
<p><strong>Jose Ibarra</strong></p>
<p>Chair</p>
<p>Oregon Commission on Hispanic Affairs</p>
</td>
</tr>
</tbody>
</table>
<div id="_mcePaste" style="position: absolute; left: -10000px; top: 0px; width: 1px; height: 1px; overflow: hidden;">Normal 0   false false false        MicrosoftInternetExplorer4     <img src="plugins/editors/jce/tiny_mce/plugins/media/img/trans.gif" width="100" height="100" id="ieooui" title="title" />
<p class="MsoNormal">Numerous committees have been established over the course of the planning period for the OLAA Summit.&nbsp; The work of these committees is informed by a larger OLAA Summit Committee <em><span style="background: none repeat scroll 0% 0% yellow;">(hyperlinked)</span></em>, the members of which meet monthly to allow for an opportunity for participants to check in and make decisions requiring greater input for the October Summit.&nbsp; Primary among these committees are the following:</p>
<p class="MsoNormal"> </p>
<p class="MsoNormal"><strong>Executive Committee </strong></p>
<p class="MsoNormal">This committee is responsible for fundraising, developing political protocols, providing the overall spirit and guidelines for the Summit, guidance in future thinking, help in designating spokespersons, and overseeing documentation.</p>
<p class="MsoNormal"><strong>Program Committee </strong></p>
<p class="MsoNormal">This committee is responsible for the flow activities during the Summit, organizing volunteer staffing during the Summit, the production of plenary sessions, the community fair, organization of the artists' showcase, and cultural aspects of the Summit.&nbsp;</p>
<p class="MsoNormal"><strong>Outreach Committee</strong></p>
<p class="MsoNormal">This committee is responsible for ensuring regional engagement, gathering contact information, developing lists of potential participants, and conducting outreach efforts for content and input to inform the issues and activities of the Summit.&nbsp;&nbsp;&nbsp;&nbsp;</p>
<p class="MsoNormal"><strong><span style="color: black;">Media/PR Function Committee</span></strong></p>
<p class="MsoNormal"><span style="color: black;">This committee has a consultative role about the overall media strategy for managing on-site press and media, and the OLAA Newsroom.</span></p>
<p class="MsoNormal"><strong>Summit</strong><strong> Venue &amp; Facilities Committee </strong></p>
<p class="MsoNormal">This committee oversees the contractual agreements and arrangements with the Salem Convention Center, and coordinates all space usage and ambiance for Summit activities.&nbsp;</p>
<p class="MsoNormal"><strong>Documentation of the Summit Committee</strong></p>
<p class="MsoNormal">This committee is charged with all cablecast/broadcast efforts, as well as any interactive communications components on the day of the Summit.&nbsp; Members will also coordinate photo, video, and audio documentation and archiving.</p>
<p class="MsoNormal"> </p>
<p class="MsoNormal">A great many people have volunteered their time and efforts to OLAA's development and planning.&nbsp; Current participants include:</p>
<p class="MsoNormal"> </p>
<p class="MsoNormal"><span lang="ES-MX" style="color: black;">Carlos Crespo</span></p>
<p class="MsoNormal"><span lang="ES-MX" style="color: black;">Director of the School of Community Health&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></p>
<p class="MsoNormal"><span lang="ES-MX" style="color: black;">Portland State University&nbsp; </span><span lang="ES-MX" style="color: blue;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</span></p>
<p class="MsoNormal"><a href="mailto:
 <script language='JavaScript' type='text/javascript'>
 <!--
 var prefix = 'm&#97;&#105;lt&#111;:';
 var suffix = '';
 var attribs = '';
 var path = 'hr' + 'ef' + '=';
 var addy10944 = 'c&#111;ns&#117;&#101;l&#111;.s&#97;r&#97;g&#111;z&#97;' + '&#64;';
 addy10944 = addy10944 + 'c&#111;' + '&#46;' + 'm&#117;ltn&#111;m&#97;h' + '&#46;' + '&#111;r' + '&#46;' + '&#117;s';
 document.write( '<a ' + path + '\'' + prefix + addy10944 + suffix + '\'' + attribs + '>' );
 document.write( addy10944 );
 document.write( '<\/a>' );
 //-->
 </script><script language='JavaScript' type='text/javascript'>
 <!--
 document.write( '<span style=\'display: none;\'>' );
 //-->
 </script>This e-mail address is being protected from spambots. You need JavaScript enabled to view it
 <script language='JavaScript' type='text/javascript'>
 <!--
 document.write( '</' );
 document.write( 'span>' );
 //-->
 </script>"></a></p>
<p class="MsoNormal"><span lang="ES-MX" style="color: black;">Consuelo Saragoza</span></p>
<p class="MsoNormal"><span style="color: black;">Senior Advisor of Public Health &amp; Community Initiatives</span></p>
<p class="MsoNormal"><span style="color: black;">Multnomah County Health Department</span></p>
<p class="MsoNormal"> </p>
<p class="MsoNormal"><span style="color: black;">Alberto Moreno, M.S.W.</span></p>
<p class="MsoNormal"><span style="color: black;">Migrant Health Coordinator</span></p>
<p class="MsoNormal"><span style="color: black;">State of Oregon Office of Multicultural Health</span></p>
<p class="MsoNormal"><span style="color: black;">&nbsp;</span></p>
<p class="MsoNormal"><span style="color: black;">Julie Esparza-Brown</span></p>
<p class="MsoNormal"><span style="color: black;">Director</span></p>
<p class="MsoNormal"><span style="color: black;">Portland</span><span style="color: black;"> State University</span><span style="color: black;"> Bilingual Teachers Pathway </span></p>
<p class="MsoNormal"><span style="color: black;">&nbsp;</span></p>
<p class="MsoNormal"><span style="color: black;">Leticia Hernandez</span></p>
<p class="MsoNormal"><span style="color: black;">Financial Aid &amp; Scholarship Outreach Counselor</span></p>
<p class="MsoNormal"><span style="color: black;">Linfield</span><span style="color: black;"> College, Portland Campus </span></p>
<p class="MsoNormal"> </p>
<p class="MsoNormal"><span style="color: black;">Rev. Miriam Méndez</span></p>
<p class="MsoNormal"><span style="color: black;">Primera Iglesia Bautista (First Baptist Church) Portland, OR</span></p>
<p class="MsoNormal"><span style="color: black;">George Fox Evangelical Seminary </span></p>
<p class="MsoNormal"> </p>
<p class="MsoNormal"><span lang="ES-MX" style="color: black;">Jessica Rodriguez Montegna</span></p>
<p class="MsoNormal"><span style="color: black;">Latino Program Manager</span></p>
<p class="MsoNormal"><span style="color: black;">Rural Development Initiatives </span></p>
<p class="MsoNormal"> </p>
<p class="MsoNormal"><span style="color: black;">Maria Rubio </span></p>
<p class="MsoNormal">Proprietor/CFO</p>
<p class="MsoNormal">M.C.Resources Consulting</p>
<p class="MsoNormal"> </p>
<p class="MsoNormal"><span style="color: black;">Olga Sanchez</span></p>
<p class="MsoNormal"><span style="color: black;">Artistic Director </span></p>
<p class="MsoNormal"><span lang="ES-MX" style="color: black;">Miracle MainStage &amp; Community Artes</span></p>
<p class="MsoNormal"><span lang="ES-MX" style="color: black;">Miracle Theatre Group </span></p>
<p class="MsoNormal"><span lang="ES-MX" style="color: black;">&nbsp;</span></p>
<p class="MsoNormal"><span lang="ES-MX" style="color: black;">Rafael Arellano</span></p>
<p class="MsoNormal"><span lang="ES-MX" style="color: black;">Executive Director </span></p>
<p class="MsoNormal"><span lang="ES-MX" style="color: black;">Educate Ya</span><span lang="ES-MX" style="color: #343434;"> </span><span lang="ES-MX" style="font-size: 13pt; font-family: Tahoma; color: black;">&nbsp;</span></p>
<p class="MsoNormal"><em><span lang="ES-MX">&nbsp;</span></em></p>
<p class="MsoNormal">Ursula Rojas Weiser</p>
<p class="MsoNormal">Cónsul de Asuntos Comunitarios/Community Affairs</p>
<p class="MsoNormal">Consulate of Mexico in Portland</p>
<p class="MsoNormal"> </p>
<p class="MsoNormal"><span lang="ES-MX" style="color: black;">Carmen Rubio </span></p>
<p class="MsoNormal"><span lang="ES-MX" style="color: black;">Executive Director </span></p>
<p class="MsoNormal"><span lang="ES-MX" style="color: black;">Latino Network </span></p>
<p class="MsoNormal"><span lang="ES-MX" style="color: black;">&nbsp;</span></p>
<p class="MsoNormal"><span lang="ES-MX" style="color: black;">Victoria Lara</span></p>
<p class="MsoNormal"><span lang="ES-MX" style="color: black;">Founder and Chief Marketing Officer</span></p>
<p class="MsoNormal"><span lang="ES-MX" style="color: black;">Lara Media Services</span></p>
<p class="MsoNormal"><span lang="ES-MX" style="color: black;">&nbsp;</span></p>
<p class="MsoNormal"><span lang="ES-MX" style="color: black;">Veronica Valenzuela</span></p>
<p class="MsoNormal"><span lang="ES-MX" style="color: black;">Policy Advisor</span></p>
<p class="MsoNormal"><span style="color: black;">Multnomah</span><span style="color: black;"> County</span><span style="color: black;"> Commissioner Deborah Kafoury's Office&nbsp; </span><span style="font-size: 13pt; font-family: Tahoma; color: black;">&nbsp;</span></p>
<p class="MsoNormal"> </p>
<p class="MsoNormal"><span style="color: black;">Jose Ibarra</span></p>
<p class="MsoNormal"><span style="color: black;">Chair</span></p>
<p class="MsoNormal"><span style="color: black;">Oregon</span><span style="color: black;"> Commission on Hispanic Affairs</span></p>
</div></td>
</tr>

</table>
<span class="article_separator">&nbsp;</span>
";s:4:"head";a:10:{s:5:"title";s:21:"Committees and Chairs";s:11:"description";s:285:"The Oregon Latino Agenda for Action (OLAA) is an effort convened by Oregon Consensus of Portland State University to bring together a broad range of representatives from the Latino community throughout Oregon to collaborate in developing an &quot;Oregon Latino Agenda for Action.&quot;";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:24:"text/html; charset=utf-8";}s:8:"standard";a:4:{s:6:"robots";s:13:"index, follow";s:8:"keywords";s:53:"Latino, Oregon, action, agenda, summit, Latino summit";s:5:"title";s:21:"Committees and Chairs";s:6:"author";s:12:"Ash Shepherd";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:54:"/plugins/system/jcemediabox/css/jcemediabox.css?v=1017";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:69:"/plugins/system/jcemediabox/themes/squeeze/css/style.css?version=1017";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:52:"/plugins/system/jcemediabox/js/jcemediabox.js?v=1017";s:15:"text/javascript";s:52:"/plugins/system/jcemediabox/js/mediaobject.js?v=1017";s:15:"text/javascript";s:52:"/plugins/system/jcemediabox/addons/default.js?v=1017";s:15:"text/javascript";s:28:"/media/system/js/mootools.js";s:15:"text/javascript";s:27:"/media/system/js/caption.js";s:15:"text/javascript";}s:6:"script";a:1:{s:15:"text/javascript";s:680:"	JCEMediaObject.init('/', {flash:"10,0,22,87",windowmedia:"5,1,52,701",quicktime:"6,0,2,0",realmedia:"7,0,0,0",shockwave:"8,5,1,0"});JCEMediaBox.init({popup:{width:"",height:"",legacy:0,lightbox:0,shadowbox:0,resize:1,icons:1,overlay:1,overlayopacity:0.8,overlaycolor:"#000000",fadespeed:500,scalespeed:500,hideobjects:0,scrolling:"fixed",close:2,labels:{'close':'Close','next':'Next','previous':'Previous','cancel':'Cancel','numbers':'{$current} of {$total}'}},tooltip:{className:"tooltip",opacity:0.8,speed:150,position:"br",offsets:{x: 16, y: 16}},base:"/",imgpath:"plugins/system/jcemediabox/img",theme:"squeeze",themecustom:"",themepath:"plugins/system/jcemediabox/themes"});";}s:6:"custom";a:0:{}}s:7:"pathway";a:2:{i:0;O:8:"stdClass":2:{s:4:"name";s:10:"About OLAA";s:4:"link";s:18:"index.php?Itemid=2";}i:1;O:8:"stdClass":2:{s:4:"name";s:27:"Staff/Committees and Chairs";s:4:"link";s:18:"index.php?Itemid=4";}}s:6:"module";a:0:{}}