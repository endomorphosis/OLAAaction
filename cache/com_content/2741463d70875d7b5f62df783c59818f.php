<?php die("Access Denied"); ?>
a:4:{s:4:"body";s:2988:"<table class="contentpaneopen">
<tr>
		<td class="contentheading" width="100%">
				<a href="/en/summit" class="contentpagetitle">
			First OLAA Summit a Success!</a>
			</td>
				
		
					</tr>
</table>

<table class="contentpaneopen">



<tr>
<td valign="top">
<p> </p>
On the 17th and 18th of October, 170 participants from around the state, representing many sectors of the Latino community, met in Salem at the Salem Conference Center for the first ever OLAA Summit.  A goal of the summit was to identify and prioritize issues and proactively work together to work toward short term and long term solutions.  <br />
<p> </p>
<p>The  facilitated process included small group interaction generating recommendations and priorities using polling tool technology to vote on  issues pertaining to health, education, immigration, and the economic  well-being of Latinos. The combination of the facilitated process, networking, partnership forming and the  commitment by Oregon Solutions surpassed all of the expected outcomes of the  summit.</p>
<p> </p>
<p>Both the organizers and the attendees left the two day summit uplifted by the feeling that all voices were heard. Many in attendance made a commitment to move an action-oriented agenda forward regardless of geographic representation, background or politics.  In summation, several participants stated that "it was awesome!".</p>
<p> </p>
<p> </p>
<hr />
<p> </p>
<p><strong>Previous Summit information</strong></p>
<p> </p>
<h2></h2>
<h2>When:</h2>
<p>Sunday, October 17, 2010 ~ 1:00pm-7:00pm</p>
<p>Monday, October 18, 2010 ~ 8:00am-4:00pm</p>
<p> </p>
<h2>Where:</h2>
<p>Salem Conference Center</p>
<p>200   Commercial Street SE</p>
<p>Salem,  Oregon 97301</p>
<p><a href="http://www.salemconferencecenter.org/directions.php" target="_blank">Get Directions</a></p>
<p> </p>
<p>This historic statewide Latino summit will identify and prioritize policy recommendations on critical issues of concern to the Latino community.  We are inviting Latino organizational and community representatives from across the state to ensure regional engagement and support to help frame the issues and courses of response and action.  We are also partnering with existing organizations and other influential and respected groups devoted to education, public health, and community development, among other areas of interest, to contribute to the content of the Summit.</p>
<p> </p>
<p>Titled “One United Voice – Una Voz Unida”, the Summit will offer plenaries, keynote speakers, and group sessions. On the first day, the Summit will focus on the reception of community input and the research papers.  On the second day participants will prioritize finding into a strategic agenda for action.  The event will also feature a community fair with local Latino businesses and sponsors, and an art exhibit to showcase Latino artists.</p></td>
</tr>

</table>
<span class="article_separator">&nbsp;</span>
";s:4:"head";a:10:{s:5:"title";s:28:"First OLAA Summit a Success!";s:11:"description";s:285:"The Oregon Latino Agenda for Action (OLAA) is an effort convened by Oregon Consensus of Portland State University to bring together a broad range of representatives from the Latino community throughout Oregon to collaborate in developing an &quot;Oregon Latino Agenda for Action.&quot;";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:24:"text/html; charset=utf-8";}s:8:"standard";a:4:{s:6:"robots";s:13:"index, follow";s:8:"keywords";s:53:"Latino, Oregon, action, agenda, summit, Latino summit";s:5:"title";s:28:"First OLAA Summit a Success!";s:6:"author";s:12:"Ash Shepherd";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:54:"/plugins/system/jcemediabox/css/jcemediabox.css?v=1017";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:69:"/plugins/system/jcemediabox/themes/squeeze/css/style.css?version=1017";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:52:"/plugins/system/jcemediabox/js/jcemediabox.js?v=1017";s:15:"text/javascript";s:52:"/plugins/system/jcemediabox/js/mediaobject.js?v=1017";s:15:"text/javascript";s:52:"/plugins/system/jcemediabox/addons/default.js?v=1017";s:15:"text/javascript";s:28:"/media/system/js/mootools.js";s:15:"text/javascript";s:27:"/media/system/js/caption.js";s:15:"text/javascript";}s:6:"script";a:1:{s:15:"text/javascript";s:680:"	JCEMediaObject.init('/', {flash:"10,0,22,87",windowmedia:"5,1,52,701",quicktime:"6,0,2,0",realmedia:"7,0,0,0",shockwave:"8,5,1,0"});JCEMediaBox.init({popup:{width:"",height:"",legacy:0,lightbox:0,shadowbox:0,resize:1,icons:1,overlay:1,overlayopacity:0.8,overlaycolor:"#000000",fadespeed:500,scalespeed:500,hideobjects:0,scrolling:"fixed",close:2,labels:{'close':'Close','next':'Next','previous':'Previous','cancel':'Cancel','numbers':'{$current} of {$total}'}},tooltip:{className:"tooltip",opacity:0.8,speed:150,position:"br",offsets:{x: 16, y: 16}},base:"/",imgpath:"plugins/system/jcemediabox/img",theme:"squeeze",themecustom:"",themepath:"plugins/system/jcemediabox/themes"});";}s:6:"custom";a:0:{}}s:7:"pathway";a:1:{i:0;O:8:"stdClass":2:{s:4:"name";s:11:"Summit 2010";s:4:"link";s:18:"index.php?Itemid=8";}}s:6:"module";a:0:{}}