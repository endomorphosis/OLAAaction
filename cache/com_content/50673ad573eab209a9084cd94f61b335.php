<?php die("Access Denied"); ?>
a:4:{s:4:"body";s:3299:"<table class="contentpaneopen">
<tr>
		<td class="contentheading" width="100%">
				<a href="/en/on-the-issues/immigration" class="contentpagetitle">
			Immigration Reform</a>
			</td>
				
		
					</tr>
</table>

<table class="contentpaneopen">



<tr>
<td valign="top">
<p> </p>
<p> </p>
<p><span style="font-size: 12pt;"><strong><img src="images/stories/slideshows/left/latino-on-bus.jpg" alt="latino-on-bus" style="margin: 15px; float: left;" height="240" width="240" /></strong></span></p>
<p><span style="font-size: 12pt;"><strong>Immigration Reform</strong></span></p>
<br />
<p>Our current immigration system is broken and needs fundamental reform.  OLAA endorses immigration reform that will:</p>
<p> </p>
<ul>
<li>Bring the 12 million undocumented people out of the shadows through a pathway to legalization in a manner that is streamlined and just.</li>
</ul>
<p> </p>
<ul>
<li>Prosecute unethical employers who undermine the work conditions for all workers.</li>
</ul>
<p> </p>
<ul>
<li>Create and enforce worker protections before any discussions or “guest worker” provisions take place.</li>
</ul>
<p> </p>
<ul>
<li>Refine our enforcement laws in a manner that is consistent and fair with civil rights laws and due process.</li>
</ul>
<p> </p>
<p><strong>1. Licenses to All Citizens </strong></p>
<p> </p>
<p>Put pressure on legislators to give a driver’s license to every  undocumented a tax paying citizen. Match an ITIN to your license.  (Taxation Vs. representation).</p>
<p> </p>
<p><strong>2. Balance Immigration Discourse </strong></p>
<p> </p>
<p>Solicit financial support to use the media to bring International  attention and balance to the immigration discourse; emphasize the fact  that this is a human rights issue and the enforcement of immigration  policies is having negative effects on schools, families, and  businesses.</p>
<p> </p>
<p><strong>3.Educate the Broader Community </strong></p>
<p> </p>
<p>Emphasize that as a community that we need to educate the broader  community, not just Latinos, about the impact of the immigration and the  fact that it affects all the citizens in the society.</p>
<strong> </strong>
<p> </p>
<strong> </strong>
<p> </p>
<p> </p>
<table style="width: 523px; height: 259px;" class="article_seperator" border="2">
<tbody style="text-align: left;">
<tr style="text-align: left;">
<td style="text-align: left;" class="article_seperator">
<p> </p>
<p style="text-align: center;"><a target="_self" href="en/news/143-alabamas-message-for-oregon-self-defeat"><img alt="Screen_shot_2011-10-31_at_4.11.06_PM" src="images/stories/Screen_shot_2011-10-31_at_4.11.06_PM.png" height="267" width="163" /></a></p>
</td>
<td valign="top"><br /></td>
<td style="text-align: left;">
<p style="text-align: center;"> </p>
<p style="text-align: center;"><img alt="Screen_shot_2011-10-31_at_4.11.06_PM" src="images/stories/Screen_shot_2011-10-31_at_4.11.06_PM.png" height="267" width="163" /></p>
</td>
</tr>
</tbody>
</table>
<br />
<p> </p>
<p> </p>
<p> </p>
<p> </p>
<p> </p>
<p> </p>
<p> </p>
<p> </p>
<p> </p>
<p> </p>
<p> </p>
<p> </p>
<p> </p>
<br />
<p> </p>
<p> </p></td>
</tr>

</table>
<span class="article_separator">&nbsp;</span>
";s:4:"head";a:10:{s:5:"title";s:18:"Immigration Reform";s:11:"description";s:285:"The Oregon Latino Agenda for Action (OLAA) is an effort convened by Oregon Consensus of Portland State University to bring together a broad range of representatives from the Latino community throughout Oregon to collaborate in developing an &quot;Oregon Latino Agenda for Action.&quot;";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:24:"text/html; charset=utf-8";}s:8:"standard";a:4:{s:6:"robots";s:13:"index, follow";s:8:"keywords";s:53:"Latino, Oregon, action, agenda, summit, Latino summit";s:5:"title";s:18:"Immigration Reform";s:6:"author";s:13:"Rolando Avila";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:54:"/plugins/system/jcemediabox/css/jcemediabox.css?v=1017";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:69:"/plugins/system/jcemediabox/themes/squeeze/css/style.css?version=1017";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:52:"/plugins/system/jcemediabox/js/jcemediabox.js?v=1017";s:15:"text/javascript";s:52:"/plugins/system/jcemediabox/js/mediaobject.js?v=1017";s:15:"text/javascript";s:52:"/plugins/system/jcemediabox/addons/default.js?v=1017";s:15:"text/javascript";s:28:"/media/system/js/mootools.js";s:15:"text/javascript";s:27:"/media/system/js/caption.js";s:15:"text/javascript";}s:6:"script";a:1:{s:15:"text/javascript";s:680:"	JCEMediaObject.init('/', {flash:"10,0,22,87",windowmedia:"5,1,52,701",quicktime:"6,0,2,0",realmedia:"7,0,0,0",shockwave:"8,5,1,0"});JCEMediaBox.init({popup:{width:"",height:"",legacy:0,lightbox:0,shadowbox:0,resize:1,icons:1,overlay:1,overlayopacity:0.8,overlaycolor:"#000000",fadespeed:500,scalespeed:500,hideobjects:0,scrolling:"fixed",close:2,labels:{'close':'Close','next':'Next','previous':'Previous','cancel':'Cancel','numbers':'{$current} of {$total}'}},tooltip:{className:"tooltip",opacity:0.8,speed:150,position:"br",offsets:{x: 16, y: 16}},base:"/",imgpath:"plugins/system/jcemediabox/img",theme:"squeeze",themecustom:"",themepath:"plugins/system/jcemediabox/themes"});";}s:6:"custom";a:0:{}}s:7:"pathway";a:2:{i:0;O:8:"stdClass":2:{s:4:"name";s:13:"On the Issues";s:4:"link";s:19:"index.php?Itemid=68";}i:1;O:8:"stdClass":2:{s:4:"name";s:11:"Immigration";s:4:"link";s:19:"index.php?Itemid=69";}}s:6:"module";a:0:{}}