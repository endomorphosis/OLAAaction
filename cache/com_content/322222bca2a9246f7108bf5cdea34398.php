<?php die("Access Denied"); ?>
a:4:{s:4:"body";s:2037:"<table class="contentpaneopen">
<tr>
		<td class="contentheading" width="100%">
				<a href="/en/about-olaa" class="contentpagetitle">
			About OLAA</a>
			</td>
				
		
					</tr>
</table>

<table class="contentpaneopen">



<tr>
<td valign="top">
<h2>Background on Latinos in Oregon</h2>
<p> </p>
<p>With original settlers arriving in Oregon in the mid 1800's, Latinos are now the largest, fastest growing, and most ethnically and racially diverse community in Oregon.  While we reside in all parts of the state including the Portland Metropolitan area, Eastern, Central, Southern and Coastal Oregon, comprehensive information about the critical social, political, cultural, and economic issues faced by Latinos in Oregon has yet to be collated.  There is no one ongoing research source where      policymakers, educators, human service agencies, and businesses can go to access information for  crafting public policy, proposing new legislation, or identifying needs, contributions, and resources.  As the population has increased, especially in the last 25 years, Latino Oregonians from various government and civil sectors have frequently discussed but never succeeded in coming together with a single, unified voice to articulate their perspectives, concerns, challenges, and solutions - until now.</p>
<p> </p>
<h2>OLAA's formation</h2>
<p> </p>
<p>The impetus for OLAA came from the echoes of those discussions and three gatherings (Salons) convened by Oregon Consensus of Portland State University with key Latino leadership in 2009 and 2010.  From these gatherings and a series of committee meetings emerged the Oregon Latino Agenda for Action (OLAA).  OLAA is governed by an executive committee and is staffed in large part by volunteers and a facilitation team.  Initial funding for the effort was provided by the Oregon Consensus Program and Northwest Health Foundation.  The Latino Network is currently serving as the fiscal agent for OLAA.</p></td>
</tr>

</table>
<span class="article_separator">&nbsp;</span>
";s:4:"head";a:10:{s:5:"title";s:10:"About OLAA";s:11:"description";s:285:"The Oregon Latino Agenda for Action (OLAA) is an effort convened by Oregon Consensus of Portland State University to bring together a broad range of representatives from the Latino community throughout Oregon to collaborate in developing an &quot;Oregon Latino Agenda for Action.&quot;";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:24:"text/html; charset=utf-8";}s:8:"standard";a:4:{s:6:"robots";s:13:"index, follow";s:8:"keywords";s:53:"Latino, Oregon, action, agenda, summit, Latino summit";s:5:"title";s:10:"About OLAA";s:6:"author";s:12:"Ash Shepherd";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:54:"/plugins/system/jcemediabox/css/jcemediabox.css?v=1017";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:69:"/plugins/system/jcemediabox/themes/squeeze/css/style.css?version=1017";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:52:"/plugins/system/jcemediabox/js/jcemediabox.js?v=1017";s:15:"text/javascript";s:52:"/plugins/system/jcemediabox/js/mediaobject.js?v=1017";s:15:"text/javascript";s:52:"/plugins/system/jcemediabox/addons/default.js?v=1017";s:15:"text/javascript";s:28:"/media/system/js/mootools.js";s:15:"text/javascript";s:27:"/media/system/js/caption.js";s:15:"text/javascript";}s:6:"script";a:1:{s:15:"text/javascript";s:680:"	JCEMediaObject.init('/', {flash:"10,0,22,87",windowmedia:"5,1,52,701",quicktime:"6,0,2,0",realmedia:"7,0,0,0",shockwave:"8,5,1,0"});JCEMediaBox.init({popup:{width:"",height:"",legacy:0,lightbox:0,shadowbox:0,resize:1,icons:1,overlay:1,overlayopacity:0.8,overlaycolor:"#000000",fadespeed:500,scalespeed:500,hideobjects:0,scrolling:"fixed",close:2,labels:{'close':'Close','next':'Next','previous':'Previous','cancel':'Cancel','numbers':'{$current} of {$total}'}},tooltip:{className:"tooltip",opacity:0.8,speed:150,position:"br",offsets:{x: 16, y: 16}},base:"/",imgpath:"plugins/system/jcemediabox/img",theme:"squeeze",themecustom:"",themepath:"plugins/system/jcemediabox/themes"});";}s:6:"custom";a:0:{}}s:7:"pathway";a:1:{i:0;O:8:"stdClass":2:{s:4:"name";s:10:"About OLAA";s:4:"link";s:18:"index.php?Itemid=2";}}s:6:"module";a:0:{}}