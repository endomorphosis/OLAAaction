<?php
/**
 * @version		2.9.4
 * @package		Frontpage Slideshow
 * @author    JoomlaWorks - http://www.joomlaworks.gr
 * @copyright	Copyright (c) 2006 - 2011 JoomlaWorks Ltd. All rights reserved.
 * @license		Commercial - This code cannot be redistributed without permission from JoomlaWorks Ltd.
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

class HTML_FPSlideShow {

	function showHeaderIM($option, $task) {
		global $mainframe, $option, $Itemid;
		
		$document = &JFactory::getDocument();
		$document->addStyleSheet(JURI::root(true).'/administator/components/'.$option.'/css/fpss.css');

		echo '
<div id="jwfpss" class="jwfpss-fp">
<h2 id="jwfpss-logo"></h2>
		';
	}

	function showFooter() {
?>

	<div id="jwfpss-footer">
		<a href="http://www.frontpageslideshow.net" target="_blank">Frontpage Slideshow v2.9.4</a> | Copyright &copy; 2006-<?php echo date('Y'); ?> <a href="http://www.joomlaworks.gr/" target="_blank">JoomlaWorks Ltd.</a>
	</div>	
</div>

<?php
	}
	// END CLASS
}

?>