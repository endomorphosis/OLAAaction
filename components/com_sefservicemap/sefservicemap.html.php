<?php
//*******************************************************
//* SEF Service Map Component
//* http://www.sefservicemap.com
//* (C) Radoslaw Kubera
//* license http://www.gnu.org/copyleft/gpl.html GNU/GPL
//*******************************************************

/** ensure this file is being included by a parent file */
defined( '_JEXEC' ) or die( 'Restricted access' );

function show_element ($element, $start=-1, $end=-1, &$counter) {

	global $mainframe;
	check_for_new_column($start, $end, $counter);
	if ($start>=0 && ($counter<=$start || $counter>$end)) return;
	// $point = '<img hspace="5" src="/components/com_sefservicemap/images/point.gif" border="0" alt="" />'; 
	$point = 'point';
	$icons = $mainframe->ParamsArray->get( 'show_icons', '1' ) ;
	$max_length=intval($mainframe->ParamsArray->get( 'max_length', '0' )); 
	$max_desc_length=intval($mainframe->ParamsArray->get( 'max_desc_length', '50' )); 
	$sublevels=$mainframe->ParamsArray->get( 'sublevels', '0' ) ;  
	$level0=$mainframe->ParamsArray->get( 'level0','level0');
	$level1=$mainframe->ParamsArray->get( 'level1','level1');
	$level2=$mainframe->ParamsArray->get( 'level2','level2');
	$level3=$mainframe->ParamsArray->get( 'level3','level3');
	$level4=$mainframe->ParamsArray->get( 'level4','level4');
	$level5=$mainframe->ParamsArray->get( 'level5','level5');
	$level6=$mainframe->ParamsArray->get( 'level6','level6');
	$level7=$mainframe->ParamsArray->get( 'level7','level7');
	$level8=$mainframe->ParamsArray->get( 'level8','level8');
	$level9=$mainframe->ParamsArray->get( 'level9','level9');
	$levelx=$mainframe->ParamsArray->get( 'levelx','levelx');
	$desc = $mainframe->ParamsArray->get( 'desc','description');
	$show_desc = intval($mainframe->ParamsArray->get( 'show_desc','1'));
	$icon_path=$mainframe->ParamsArray->get( 'icon_path','');

	$description='';
	if ($show_desc) {  
		if (isset($element->description) && $element->description!='') { 
			$short_desc=SEF_SM_HTML_To_Plaintext ($element->description);
			if ($max_desc_length!=0) {
				$sub_desc = substr ($short_desc,0,$max_desc_length); 
				if (strlen($sub_desc)==strlen($short_desc)) $short_desc=$sub_desc; else $short_desc=$sub_desc.'...';
			}
			$description='<div class="'.$desc.'">'.$short_desc.'</div>'; 
		}
	}

	$name = $element->name;
	if ($max_length!=0) {
		$subname = substr ($name,0,$max_length); 
		if (strlen($subname)==strlen($name)) $name=$subname; else $name=$subname.'...';
	}
	if ($icons) {
		if (eregi('mambots',$element->icon_path)) $element->icon_path = str_replace ('mambots','plugins',$element->icon_path);
		if (!$icon_path) $icon_path = $element->icon_path;
		if ($element->image=='none' ) $image='';
		else {
			if (!$element->image) $image='<img hspace="5" src="/components/com_sefservicemap/images/directory.gif" border="0" alt="" />';
			else  $image='<img border="0" hspace="5" src="'.$icon_path.'/'.$element->image.'" alt="" />';
		}
	} 
	else {
		if ($name!='' && $name!="&nbsp;") $image=$point;
		else $image='';
	}

	switch ($element->level) {
		case 0: $class = 'class="'.$level0.'"';break;
		case 1: $class = 'class="'.$level1.'"';break;
		case 2: $class = 'class="'.$level2.'"';break;
		case 3: $class = 'class="'.$level3.'"';break;
		case 4: $class = 'class="'.$level4.'"';break;
		case 5: $class = 'class="'.$level5.'"';break;
		case 6: $class = 'class="'.$level6.'"';break;
		case 7: $class = 'class="'.$level7.'"';break;
		case 8: $class = 'class="'.$level8.'"';break;
		case 9: $class = 'class="'.$level9.'"';break;
		default: $class = 'class="'.$levelx.'"';break;       
	}

	if ($element->link!='') {  
		if ($image=='point') echo '<div '.$class.'><ul><li><a href="'.JROUTE::_($element->link).'">'.$name.'</a></li></ul>'.$description."</div>\n";
		else echo '<div '.$class.'><a href="'.JROUTE::_($element->link).'">'.$image.$name.'</a>'.$description."</div>\n";
	}
	else  {
		if ($image=='point') echo '<div '.$class.'><ul><li>'.$name.$description."</li></ul></div>\n";
		else echo '<div '.$class.'>'.$image.$name.$description."</div>\n";
	}
}

function show_xml_index_element ($link,$lastmod='') {
	echo '<sitemap>'."\n";
	echo '  <loc>'.$link.'</loc>'."\n";
	if ($lastmod) echo '  <lastmod>'.$lastmod.'</lastmod>'."\n";
	echo '</sitemap>'."\n";

}

function show_xml_element($element, $start=-1, $end=-1, &$counter) {

	if ($start>=0 && ($counter<=$start || $counter>$end)) return;

	if (eregi('Itemid=',$element->link) && !eregi('index.php',$element->link)) return;
	if (!isset($element->link) || $element->link=='') return;
	$live_site = 'http://'.$_SERVER['SERVER_NAME'];
	$link = $element->link;
	if (isset($element->lastmod) && $element->lastmod!='') $lastmod = $element->lastmod; else $lastmod='';

	if ($lastmod) {
   		$time = strtotime($lastmod);
		$lastmod= date('Y-m-d',$time);
	}

	if (isset($element->changefreq) && $element->changefreq!='') $changefreq = $element->changefreq; else $changefreq='';
	if (isset($element->priority) && $element->priority!='') $priority = $element->priority; else $priority='';
	$link = JRoute::_($link);
	$index = substr($link,0,4);
	$first = substr($link,0,1);
	$sep='/';
	if ($first=='/') $sep = '';
	if ($index!='http') {  
		$link = $live_site.$sep.$link;
	}
	else {
		if (substr ($link,0,strlen($live_site)) !=$live_site) $link='';
	}

	$link= utf8_encode($link);
	$link = str_replace('&amp;','&',$link);
	$link = str_replace('&apos;',"'",$link);
	$link = str_replace('&quot;','"',$link);
	$link = str_replace('&gt;','>',$link);
	$link = str_replace('&lt;','<',$link);
	$link = str_replace('&','&amp;',$link);
	$link = str_replace("'",'&apos;',$link);
	$link = str_replace('"','&quot;',$link);
	$link = str_replace('>','&gt;',$link);
	$link = str_replace('<','&lt;',$link);
	echo '<url>'."\n";
	echo '  <loc>'.$link.'</loc>'."\n";
	if ($priority) echo '  <priority>'.$priority.'</priority>'."\n";
	if ($changefreq) echo '  <changefreq>'.$changefreq.'</changefreq>'."\n";
	if ($lastmod) echo '  <lastmod>'.$lastmod.'</lastmod>'."\n";
	echo '</url>'."\n";
}

function show_txt_element($element) {
	if (eregi('Itemid=',$element->link) && !eregi('index.php',$element->link)) return;
	if (!isset($element->link) || $element->link=='') return;
	$live_site = 'http://'.$_SERVER['SERVER_NAME'];

	$link = $element->link;
	$link = JRoute::_($link);
	$index = substr($link,0,4);
	$first = substr($link,0,1);
	$sep='/';
	if ($first=='/') $sep = '';
	if ($index!='http') {  
		$link = $live_site.$sep.$link;
	}
	else {
		if (substr ($link,0,strlen($live_site)) !=$live_site) $link='';
	}
	$link= utf8_encode($link);
	$link = str_replace('&amp;','&',$link);
	$link = str_replace('&apos;',"'",$link);
	$link = str_replace('&quot;','"',$link);
	$link = str_replace('&gt;','>',$link);
	$link = str_replace('&lt;','<',$link);
	$link = str_replace('&','&amp;',$link);
	$link = str_replace("'",'&apos;',$link);
	$link = str_replace('"','&quot;',$link);
	$link = str_replace('>','&gt;',$link);
	$link = str_replace('<','&lt;',$link);
	echo $link."\r\n";
}

class HTML_sefservicemap {
	var $database;
	var $ComponentParams='';
	var $utils;

	function HTML_sefservicemap() {
		global $mainframe;
		$this->database = JFactory::getDBO();
		$this->ComponentParams = $mainframe->ParamsArray;
	}

	function RenderMapXMLHeader() {
		header('Content-type: application/xml');
		echo '<?xml version="1.0" encoding="UTF-8"?>'."\r\n";
		echo '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'."\n";

	}

	function RenderMapHTMLHeader() {
		$id = intval(JRequest::getVar('Itemid',0));
		$query = "SELECT id,name from #__menu where id='$id'";
		$this->database->setQuery($query); 
		$tmp = $this->database->loadObject();
		if ($tmp) {
			$name=$tmp->name;
		} 
		else {
			$query = "SELECT id,name from #__menu where published='1' and link like '%com_sefservicemap%' and type='component'";
			$this->database->setQuery($query); 
			$tmp = $this->database->loadObject();
			$name=$tmp->name;
		}
		?><div class="componentheading"><?php echo $name?></div><br/><?php
		$css= '<link href="/components/com_sefservicemap/css/template_css.css" rel="stylesheet" type="text/css" />';
		$document =& JFactory::getDocument();
		$document->addCustomTag($css);		

	}

	function RenderMapXMLFooter() {
		echo '</urlset>';
		//workground for JoomArtio SEF
		exit;
	}

	function RenderMapXMLIndexHeader() {
		header('Content-type: application/xml');
		echo '<?xml version="1.0" encoding="UTF-8"?>'."\r\n";
		echo '<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'."\n";
	}

	function RenderMapXMLIndexFooter() {
		echo '</sitemapindex>';
		//workground for JoomArtio SEF
		exit;
	}

	function RenderMapHTMLFooter() {

		$lang =& JFactory::getLanguage();
		$mylang = $lang->_lang;
		$mylang = JRequest::getVar('lang',$mylang);

		$mylang = '&amp;lang='.$mylang;

		$root = $_SERVER['DOCUMENT_ROOT'];
		$rootarr = explode ('/',$root);
		$rootarray = array();
		if ($rootarr) {
			foreach ($rootarr as $r) {
				if ($r!='') $rootarray[]=$r;
			}
		}

		$xmllink = SEFSMRoute::_('index.php?option=com_sefservicemap&amp;task=xmlmapindex&amp;no_html=1');

		$txtlink = SEFSMRoute::_('index.php?option=com_sefservicemap&amp;task=txtmap&amp;no_html=1'.$mylang);

		$complink = $this->utils->getlink(); if ($complink!='l') $complink = 'by '.$complink; else $complink='';
		?>
		<table align="center" width="100%">
			<tr>
				<td align="center">
					<a href="<?php echo $xmllink ?>" title="<?php echo JText::_('Sitemap XML') ?>"><img src="/components/com_sefservicemap/images/xml.png" border="0" alt="<?php echo JText::_('Sitemap XML') ?>"/>&nbsp;<?php echo JText::_('Sitemap XML') ?></a>
				</td>
				<td align="center">
					<a href="<?php echo $txtlink ?>" title="<?php echo JText::_('Sitemap TXT') ?>"><img src="/components/com_sefservicemap/images/txt.png" border="0" alt="<?php echo JText::_('Sitemap TXT') ?>"/>&nbsp;<?php echo JText::_('Sitemap TXT') ?></a>
				</td>
			</tr>
		</table>
		<?php if ($complink) { 
			?>
			<table width="100%" align="center">
				<tr align ="center">
					<td align="center" class="sponsor">
						<a href="<?php echo _SEF_SM_HOME_PAGE ?>">SEF Service Map v.<?php echo _SEF_SM_VERSION ?></a> <?php echo $complink; ?>
					</td>
				</tr>
			</table>
			<?php
		}
	}
}

?>