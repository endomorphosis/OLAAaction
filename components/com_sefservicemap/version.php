<?php
//*******************************************************
//* SEF Service Map Component
//* http://www.sefservicemap.com
//* (C) Radoslaw Kubera
//* license http://www.gnu.org/copyleft/gpl.html GNU/GPL
//*******************************************************

DEFINE ('_SEF_SM_VERSION','2.0');
DEFINE ('_SEF_SM_HOME_PAGE','http://www.sefservicemap.com');
?>