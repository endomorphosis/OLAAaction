<?php
//*******************************************************
//* SEF Service Map Component
//* http://www.sefservicemap.com
//* (C) Radoslaw Kubera
//* license http://www.gnu.org/copyleft/gpl.html GNU/GPL
//*******************************************************

$a  =dirname(__FILE__);
/** ensure this file is being included by a parent file */
defined( '_JEXEC' ) or die( 'Restricted access' );

ini_set('max_execution_time', 0);

include JPATH_COMPONENT.DS.'version.php';
include JPATH_COMPONENT.DS."sefservicemap.html.php";
include_once (JPATH_SITE.DS."administrator".DS."components".DS."com_sefservicemap".DS."include".DS."sefservicemap.util.php");

class sefservicemap {
	var $database = '';
	var $utils;

	function sefservicemap() {
		global $mainframe;
		$mainframe->maptype='';
		$mainframe->sef_sm_cache_file='';
	       $mainframe->queue_elements = 0;


		$this->utils = new SEF_SM_Utils_class;
		$this->utils->smGetComponentParams();
		$this->utils->get_pingback();

		$this->database = JFactory::getDBO();

		$cache_dir = JPATH_SITE.DS.'components'.DS.'com_sefservicemap'.DS.'cache'.DS;
		@mkdir ($cache_dir);

		$this->utils->smSynchronize();
		$this->utils->cache_clear_Check();

		$task = JRequest::getVar('task','');

		$mainframe->maptype = 'html';
		switch ($task) { 
			case 'googlemap':
			case 'xmlmap': $mainframe->maptype='xml';  break;
			case 'txtmap': $mainframe->maptype='txt';  break;
			case 'xmlmapindex': $mainframe->maptype='xml'; break;
		}

		$mainframe->elements_count=0;

		$query = "select id,name as title,ordering AS menu_ordering ,published AS menu_published, params FROM #__sef_sm_menus WHERE published='1' ORDER BY `ordering` ASC";
		$this->database->setQuery($query);

		$menus=$this->database->LoadObjectList();
		$menu_titles=$mainframe->ParamsArray->get( 'menu_titles', '1' ); 
		$show_menu_icons= intval($mainframe->ParamsArray->get('show_menu_icons','0'));
		$icon_path = $mainframe->ParamsArray->get('icon_path','');
		$show_module_icons = intval($mainframe->ParamsArray->get('show_module_icons','1'));
		if ($icon_path=='') $icon_path =DS.'components'.DS.'com_sefservicemap'.DS.'images';
		global $mainframe;

		foreach ($menus as $menu) { 
			$level=0;
			$pars = new JParameter($menu->params);
			$title = $menu->title;
			$menutype=$menu->title;
			$menu_images = intval($pars->get('menu_images','0'));
			if ($menu_titles) {
				$level=1;
				$mainframe->sm_menuitem_id=$menu->id;
				$element='';
				$element->link='';
				$element->name=$menu->title;
				$element->level=0;
				if ($show_module_icons) $element->image='menu.gif'; else $element->image='none';
				$element->icon_path=$icon_path;
				$element->description = '';
				AddExtMenuItem($element);
			}
			$this->showMenuMap($title,$menutype,$menu_images,$show_menu_icons,$level,$icon_path);
			if ($menu_titles) { 
				$mainframe->sm_menuitem_id=$menu->id;
				$element='';
				$element->name= '&nbsp;';
				$element->link='';
				$element->level=10;
				$element->image='none';
				$element->icon_path=$icon_path;
				$element->description = '';
				AddExtMenuItem($element);
			}
		}
		if ($task=='xmlmapindex') {
			$this->create_index(); 
		}
		else if ($mainframe->maptype == 'html' || $mainframe->maptype =='xml') $this->print_map();
		else exit;
	}
    
	function showMenuMap($title,$menutype,$menu_images,$show_menu_icons,$level,$icon_path) { 
		$this->showMenu($title,$menutype,0,$level,$menu_images,$show_menu_icons,$icon_path);
	}

	function showMenu($title,$menutype,$parent,$level,$menu_images,$show_menu_icons,$icon_path) {
		global $mainframe;
		$user = &JFactory::getUser();
		$gid = (int) $user->get('gid', 0);

		$query = "SELECT a.*,b.ping_enabled, b.published AS menu_published,b.integrator AS menu_integrator, b.integrator_id as integrator_id, b.ordering AS menu_ordering, b.params AS menu_params"
		."\n FROM #__menu AS a LEFT JOIN #__sef_sm_menu as b ON b.menu_id = a.id "
		."\n WHERE a.menutype='$menutype' and a.published >'0' and a.parent='$parent' and b.published = '1' and a.access<='$gid' ORDER BY `ordering` ASC";
		$this->database->setQuery($query);

		$submenus = $this->database->loadObjectList();
		if ($submenus)
		foreach ($submenus as $submenu) {
			$pars = new JParameter($submenu->params);
			$image='directory.gif';
			if ($show_menu_icons==1){
				$icon = $pars->get('menu_image','-1');
				if ($icon!='-1') {
					$image=$icon;
					$icon_path = DS.'images'.DS.'stories'.DS;       
				}
			}
	
			if ($show_menu_icons==2) {
				$image='none';
			}
	
			$Itid=smGetParam($submenu->link,'Itemid','');
			if (!$Itid) {  
				if ($submenu->type!='url') {
					$option = smGetParam($submenu->link,'option','');
					$link = 'index.php?Itemid='.$submenu->id.'&option='.$option;
				} else $link=$submenu->link;
	
				$Itid=$submenu->id;
			} 
			else {
				$link = $submenu->link;
			}
	
			$element->name = $submenu->name;
			if ($submenu->type=='component' || $submenu->type=='components') $element->link = $link; else $element->link='';
			$element->level = $level;
			$element->image = $image;
			$element->icon_path=$icon_path;
			$element->description='';
	
			AddExtMenuItem($element);

			$integrator = $this->utils->get_menuitem_integtator($submenu);
			if ($integrator) {
				$this->utils->integrator_call($integrator,$submenu,$level);
			}
			
			$lev=$level+1;
			$this->showMenu($submenu->name,$menutype,$submenu->id,$lev,$menu_images,$show_menu_icons,$icon_path);
		}
	}

	function print_menu($title, $menutype, $parent,$level, $menu_images, $show_menu_icons, $icon_path, $start, $end, &$counter) {

		if ($start>=0 && $counter>$end) return;

		global $mainframe;
    
		$user = &JFactory::getUser();
		$gid = (int) $user->get('gid', 0);

		$query = "SELECT a.*,b.ping_enabled, b.published AS menu_published,b.integrator AS menu_integrator, b.integrator_id as integrator_id, b.ordering AS menu_ordering, b.params AS menu_params"
		."\n FROM #__menu AS a LEFT JOIN #__sef_sm_menu as b ON b.menu_id = a.id "
		."\n WHERE a.menutype='$menutype' and a.published >'0' and a.parent='$parent' and b.published = '1' and a.access<='$gid' ORDER BY `ordering` ASC";
		$this->database->setQuery($query);

		$submenus = $this->database->loadObjectList();
		if ($submenus)
		foreach ($submenus as $submenu) {
			$pars = new JParameter($submenu->params);
			$image='directory.gif';
			if ($show_menu_icons==1){
				$icon = $pars->get('menu_image','-1');
				if ($icon!='-1') {
					$image=$icon;
					$icon_path = DS.'images'.DS.'stories'.DS;       
				}
			}
	
			if ($show_menu_icons==2) {
				$image='none';
			}
	
			$Itid=smGetParam($submenu->link,'Itemid','');
			if (!$Itid) {  
				if ($submenu->type!='url') {
					$option = smGetParam($submenu->link,'option','');
					$link = 'index.php?Itemid='.$submenu->id.'&option='.$option;
				} else $link=$submenu->link;
	
				$Itid=$submenu->id;
			} 
			else {
				$link = $submenu->link;
			}
	
			$element->name = $submenu->name;
			if ($submenu->type=='component' || $submenu->type=='components') $element->link = $link; else $element->link='';
//			$element->link = $link;
			$element->level = $level;
			$element->image = $image;
			$element->icon_path=$icon_path;
			$element->description='';
	
			$counter++;
			switch ($mainframe->maptype) {
				case 'html' : show_element ($element, $start, $end, $counter); break;
				case 'xml' : show_xml_element ($element, $start, $end, $counter); break;
				case 'txt' : show_txt_element ($element, $start, $end, $counter); break;
			}

			$this->utils->get_from_cache ($submenu->id,1, $start, $end, $counter);
			$lev=$level+1;
			$this->print_menu($submenu->name, $menutype, $submenu->id, $lev, $menu_images, $show_menu_icons, $icon_path, $start, $end, $counter);
		}
	}


	function create_index() {
		$max = 1000;
		global $mainframe;
		$mainframe->elements_count = $mainframe->elements_count-1;
		$count = $mainframe->elements_count;

		$MyHTML_sefservicemap = new HTML_sefservicemap ();
		$MyHTML_sefservicemap->utils = $this->utils;

		$MyHTML_sefservicemap->RenderMapXMLIndexHeader();

		$db = JFactory::getDBO();
		$query = "select * from #__languages where active='1'";
		$db->setQuery($query);
		$languages = $db->loadObjectList();

		$root = $_SERVER['DOCUMENT_ROOT'];
		$rootarr = explode ('/',$root);
		$rootarray = array();
		if ($rootarr) {
			foreach ($rootarr as $r) {
				if ($r!='') $rootarray[]=$r;
			}
		}

		$path = JPATH_SITE;
		$patharr = explode ('/',$path);
		$patharray = array();
		if ($patharr) {
			foreach ($patharr as $r) {
				if ($r!='') $patharray[]=$r;
			}
		}

		$segments = count($patharray) - count($rootarray);
		if ($segments !=0) {
			$folder = '/';
			for ($i = count($rootarray); $i<count($patharray); $i++) {
				$folder .= $patharray[$i].'/';
			}
		} else $folder = '/';

		if ($languages) {
			foreach ($languages as $language) {
				if ($count>$max) {
					$start = 0;
					while ($start<$count) {
						$link = SEFSMRoute::_('index.php?option=com_sefservicemap&amp;task=xmlmap&amp;no_html=1&amp;limitstart='.$start.'&amp;limit='.$max.'&amp;lang='.$language->shortcode);

						show_xml_index_element ($link);
						$start= $start+$max;
					}
				}
				else {
					$link = SEFSMRoute::_('index.php?option=com_sefservicemap&amp;task=xmlmap&amp;no_html=1&amp;lang='.$language->shortcode);

					show_xml_index_element ($link);
				}
			}
		}
		else {
			$lang =& JFactory::getLanguage();
			$mylang = $lang->_lang;
			if ($count>$max) {
				$start = 0;
				while ($start<$count) {
					$link = SEFSMRoute::_('index.php?option=com_sefservicemap&amp;task=xmlmap&amp;no_html=1&amp;limitstart='.$start.'&amp;limit='.$max.'&amp;lang='.$mylang);

					show_xml_index_element ($link);
					$start= $start+$max;
				}
			}
			else {
				$link = SEFSMRoute::_('index.php?option=com_sefservicemap&amp;task=xmlmap&amp;no_html=1&amp;lang='.$mylang);

				show_xml_index_element ($link);
			}
		}

		$MyHTML_sefservicemap->RenderMapXMLIndexFooter();

	}

	function print_map() {

		$counter = -1;

		global $mainframe;
		if ($mainframe->maptype == 'html') $items_pagination = $mainframe->ParamsArray->get( 'items_pagination', '40' ) ;
		else $items_pagination = intval(JRequest::getVar('limit',-1));

		$limit = intval (JRequest::getVar('limit',$items_pagination));
		if ($limit>=0) $limitstart = intval(JRequest::getVar('limitstart',0)); else {
			$limitstart = -1;
			$limit=0;
		}
		$start = $limitstart;
		$end = $start+$limit;

		if ($start>=0) $counter=0;

		$mainframe->elements_count = $mainframe->elements_count-1;
		$count = $mainframe->elements_count;

		if ($items_pagination && $mainframe->maptype == 'html') {
			jimport('joomla.html.pagination');
			$pagenav = new JPagination($mainframe->elements_count, $limitstart, $limit);
		}
		

		$this->database = JFactory::getDBO();

		$MyHTML_sefservicemap = new HTML_sefservicemap ();
		$MyHTML_sefservicemap->utils = $this->utils;

		if ($mainframe->maptype == 'html') $MyHTML_sefservicemap->RenderMapHTMLHeader();
		else $MyHTML_sefservicemap->RenderMapXMLHeader();

		if ($mainframe->maptype == 'html') {

			echo '<table class="sitemap" width="100%"><tr><td valign="top">';
		}

		$query = "select id,name,ordering AS menu_ordering ,published AS menu_published, params FROM #__sef_sm_menus WHERE published='1' ORDER BY `ordering` ASC";
		$this->database->setQuery($query);
		$menus=$this->database->LoadObjectList();

		$titlearr = array();
		$query = "select * FROM #__menu_types";
		$this->database->setQuery($query);
		$menutp = $this->database->loadObjectList();
		if ($menutp) {
			foreach ($menutp as $menut) {
				$titlearr[$menut->menutype] = $menut->title;
			}
		}

		$menu_titles=$mainframe->ParamsArray->get( 'menu_titles', '1' ); 
		$show_menu_icons= intval($mainframe->ParamsArray->get('show_menu_icons','0'));
		$icon_path = $mainframe->ParamsArray->get('icon_path','');
		$show_module_icons = intval($mainframe->ParamsArray->get('show_module_icons','1'));
		if ($icon_path=='') $icon_path =DS.'components'.DS.'com_sefservicemap'.DS.'images';
		global $mainframe;

		foreach ($menus as $menu) { 
			$menu->title = $titlearr[$menu->name];
			$level=0;
			$pars = new JParameter($menu->params);
			$title = $menu->name;
			$menutype=$menu->name;
			$menu_images = intval($pars->get('menu_images','0'));
			if ($menu_titles) {
				$level=1;
				$mainframe->sm_menuitem_id=$menu->id;
				$element='';
				$element->link='';
				$element->name=$menu->title;
				$element->level=0;
				if ($show_module_icons) $element->image='menu.gif'; else $element->image='none';
				$element->icon_path=$icon_path;
				$element->description = '';
				$counter++;
				switch ($mainframe->maptype) {
					case 'html' : show_element ($element, $start, $end, $counter); break;
					case 'xml' : show_xml_element ($element, $start, $end, $counter); break;
					case 'txt' : show_txt_element ($element, $start, $end, $counter); break;
				}
				
			}
			$this->print_menu($title,$menutype,0,$level,$menu_images,$show_menu_icons,$icon_path, $start, $end, $counter);
			if ($menu_titles) { 
				$mainframe->sm_menuitem_id=$menu->id;
				$element='';
				$element->name= '&nbsp;';
				$element->link='';
				$element->level=10;
				$element->image='none';
				$element->icon_path=$icon_path;
				$element->description = '';
				$counter++;
				switch ($mainframe->maptype) {
					case 'html' : show_element ($element, $start, $end, $counter); break;
					case 'xml' : show_xml_element ($element, $start, $end, $counter); break;
					case 'txt' : show_txt_element ($element, $start, $end, $counter); break;
				}
			}
		}

		if ($mainframe->maptype == 'html') {
			echo '</td></tr></table>';	

			if ($items_pagination) {
				echo '<table width="100%"><tr><td height="20px"></td></tr><tr><td align="center">'.$pagenav->getPagesLinks( ).'</td></tr><tr><td height="30px"></td></tr></table>';
			}
			$MyHTML_sefservicemap->RenderMapHTMLFooter();

		}
		else if ($mainframe->maptype=='xml') $MyHTML_sefservicemap->RenderMapXMLFooter();
	}

}
$mysefservicemap = new sefservicemap();
?>