<?php header("Content-type: text/css"); ?>
/*======================================================================*\
|| #################################################################### ||
|| # Copyright (C) 2006-2009 Youjoomla LLC. All Rights Reserved.        ||
|| # This file may not be redistributed in whole or significant part. # ||
|| # ---------------- THIS IS NOT FREE SOFTWARE ---------------- #      ||
|| # http://www.youjoomla.com | http://www.youjoomla.com/license.html # ||
|| #################################################################### ||
\*======================================================================*/
body#color{}
#centerbottom{}
#logo{background:url(../images/mint/logo.png) no-repeat 0px 15px}
#banner{}
#advholder, 
#topshelf, 
#topmodule .yjnewsflash{border-bottom:1px solid #e4e4e4}

.search{border:1px solid #e4e4e4; background:#f8f9f6}
.search .inputbox, 
.search .inputbox:hover, 
.search .inputbox:focus{color:#8c857d; background:url(../images/search_bg.gif) repeat-x left bottom}
.search .button{background:#c0cf8e; color:#fff}

#modlgn_passwd, #modlgn_username{color:#c0cf8e; border:1px solid #e4e4e4}
#modlgn_passwd{background:#fff}
#modlgn_username{background:#fff}
#mod_login_remember{border:none; padding:0!important}

a:link, a:visited{text-decoration:none; color:#c0cf8e}
a:focus, a:hover{color:#78980d; text-decoration:none}
h1, h3, h4, 
h5, h6, h2{color:#78980d}
.small, .createdate, .modifydate, .mosimage_caption, .createby{text-transform:uppercase; color:#ccc; font:10px Tahoma,Verdana,"Lucida Sans"}
.componentheading, 
.contentheading, 
.contentheading a:link, 
.contentheading a:visited, 
.contentheading a:link, 
.contentheading a:visited{color:#c0cf8e; border-bottom:1px solid #e4e4e4}
#pathway{border-bottom:1px solid #e4e4e4}
.toclink{border:1px solid #e4e4e4}
fieldset{border:1px solid #d6d4d4}
.yjsquare_in fieldset{border:none}
.inputbox{background:#FFF; border:1px solid #e4e4e4}
.inputbox:hover, .inputbox:focus{background:#e4e4e4; border:1px solid #e4e4e4}

.button{color:#fff}
.button, .validate{background:#c0cf8e; color:#fff}
.pollbuttons .vote, 
.pollbuttons .results, 
.borderspan{background:#f8f9f6; border:1px solid #e4e4e4}
.pollbuttons .results .button{background:#c0cf8e}
.pollbuttons .vote .button{background:#1c140d}
a.button:link, 
a.button:visited, 
a.button:focus{color:#fff; padding:3px}
.back_button a:hover, .back_button a:active{color:#000; text-decoration:none}
a.pagenav, 
.pagenav_prev a, 
.pagenav_next a, 
.pagenavbar a, .back_button a{background:#c0cf8e; color:#fff}
a.readon:link, a.readon:visited{background:#c0cf8e; border:1px solid #e4e4e4; color:#fff}

.readon span{background:#c0cf8e; border:5px solid #f8f9f6}
a.readon:hover, .pagenavbar a:hover{color:#18110b}
.yjns_rm:link, 
.yjns_rm:visited{color:#fff; background:#c0cf8e; padding:0 5px 0 5px; height:17px}
.yjns_rm:hover{color:#18110b}
.yjnsreadon{background:#f8f9f6; border:1px solid #e4e4e4; padding:5px 5px 3px 5px}

.yjsquare h3{color:#c0cf8e; border-bottom:1px solid #e4e4e4}
.yjsquare h3 span{color:#c0cf8e}
.yjsquare{color:#555}

.yjsquare_yj1 h3{color:#18110b; border-bottom:1px solid #ccc}
.yjsquare_yj1 h3 span{color:#18110b}
.yjsquare_yj1{background:#fefeec; color:#555; border:1px solid #ccc}

.yjsquare_yj2 h3{color:#78980d; border-bottom:1px solid #78980d}
.yjsquare_yj2 h3 span{color:#78980d}
.yjsquare_yj2{background:#F8F9F6; color:#555}

#horiznav ul li a{color:#c0cf8e; background:none}

#horiznav ul li a:hover, 
#horiznav ul li:hover a, 
#horiznav ul li.sfHover a{color:#18110b; background:none}
#horiznav li#current a, 
#horiznav li#current a:hover, 
#horiznav li#current:hover a, 
#horiznav li#current.sfHover a, 
#horiznav li.active a{color:#18110b; background:none}
#horiznav li#current, 
#horiznav li#current:hover, 
#horiznav li#current.sfHover, 
#horiznav li.active{background:none}
#horiznav ul li:hover li a, 
#horiznav ul li.sfHover li a, 
#horiznav ul li#current:hover li a, 
#horiznav ul li#current.sfHover li a, 
#horiznav ul li:hover ul ul li a, 
#horiznav ul ul li.active{color:#18110b; background-image:none}

#horiznav li:hover ul, 
#horiznav li li:hover ul, 
#horiznav li li li:hover ul, 
#horiznav li li li li:hover ul, 
#horiznav li.sfHover ul, 
#horiznav li li.sfHover ul, 
#horiznav li li li.sfHover ul, 
#horiznav li li li li.sfHover ul, 
#horiznav ul li:hover li a, 
#horiznav ul li.sfHover li a, 
#horiznav ul li:hover ul ul li a, 
#horiznav ul li.sfHover ul ul li a{background-color:#c0cf8e}

#horiznav li ul li a:hover, 
#horiznav li ul li ul li a:hover, 
#horiznav li li:hover, 
#horiznav ul li li.sfHover, 
#horiznav li li.sfHover a, 
#horiznav ul li li:hover a, 
#horiznav ul li li:hover, 
#horiznav ul li li.sfHover, 
#horiznav ul li li#current a:hover, 
#horiznav li.haschild ul li#current.sfHover, 
#horiznav li.haschild ul li#current:hover, 
#horiznav ul li.haschild li#current a:hover{background-color:#c0cf8e; color:#fff; background-image:none}

#horiznav ul li#current ul li a, 
#horiznav ul li#current ul li, 
#horiznav ul li#current ul li a:hover, 
#horiznav ul li#current ul li.hover
#horiznav ul li#current ul li.sfHover, 
#horiznav ul li.haschild li#current a{color:#fff}

#horiznav li li{border-bottom:1px solid #b4c47d}

#horiznav li ul{}

#horiznav li, #horiznav{font-weight:bold; background:none}
#horiznav li{background:none}
#horiznav ul li:hover, #horiznav ul li.sfHover{background:none}

#horiznav ul li ul li.haschild a.child, 
#horiznav ul li ul li a.child:hover{background:url(../images/mint/topmenu/arrow_right.gif) 95% 50% no-repeat}

#horiznav ul li.active ul li a.child{background:url(../images/mint/topmenu/arrow_right.gif) 95% 50% no-repeat}

#horiznav_d{font-weight:bold; background:none}
#horiznav_d li{background:none}
#horiznav_d li a, #horiznav_d li .separator{color:#c0cf8e; background:none}
#horiznav_d ul li ul{background:#fff}
#horiznav_d li.active, #horiznav_d li.active:hover, 
#horiznav_d li.active.sfHover, 
#horiznav_d li:hover, #horiznav_d li.sfHover{background:none}
#horiznav_d li.active a, #horiznav_d li.active .separator, 
#horiznav_d li.active a:hover, #horiznav_d li.active .separator:hover, 
#horiznav_d li:hover a, #horiznav_d li:hover .separator, 
#horiznav_d li.sfHover a, #horiznav_d li.sfHover .separator{background:none; color:#18110b}
#horiznav_d li.haschild li a:hover, 
#horiznav_d li.haschild li.active a{text-decoration:underline; background:none}
#horiznav_d li.haschild li a{color:#c0cf8e; font-weight:bold; background:none}

#mainlevel li a, 
.menu li a, 
a.mainlevel{background:#fff url(../images/mint/mainlevel.jpg) no-repeat left top}
#mainlevel li a:hover, 
#mainlevel a#active_menu:link, #mainlevel a#active_menu:visited, 
a.mainlevel:hover, 
a.mainlevel#active_menu, 
#mainlevel li a:hover, 
#mainlevel a#active_menu:link, #mainlevel a#active_menu:visited, 
.menu li a:hover, .menu li.active a{background:#fff url(../images/mint/mainlevel.jpg) no-repeat left bottom}
a.sublevel{background:url(../images/mint/bodyli.gif) no-repeat 2px 7px}
a.sublevel:hover{text-decoration:underline}
#footmod a.mainlevel:hover, 
#footmod a.mainlevel:active, 
#footmod a.mainlevel:focus, 
#mainlevel li a:hover, 
#mainlevel a#active_menu:link, 
#mainlevel a#active_menu:visited, 
a.mainlevel#active_menu, 
a.sublevel, a.sublevel:hover, 
a.sublevel:active, a.sublevel:focus, 
#active_menu, a.mainlevel:hover, 
a.mainlevel:active, 
a.mainlevel:focus, 
ul li a:hover, 
li.active a, 
li.parent a{color:#78980d}
a.mainlevel, 
#mainlevel li a{color:#c0cf8e}
.yjsquare li.active ul li a, 
a.sublevel#active_menu{color:#c0cf8e}
.yjsquare li.active ul li a:hover, 
.yjsquare li.active ul li.active a{color:#78980d; background:#fff url(../images/mint/mainlevel.jpg) no-repeat left top}
body li{padding-left:15px; background:url(../images/mint/bodyli.gif) no-repeat 0px 5px; font-family:Verdana,Arial,Helvetica, sans-serif; font-size:11px; text-decoration:none}
#topshelf h3, #bottomshelf h3, 
#topshelf h3, #bottomshelf span{color:#78980D}
#user1, #user2, #user3, 
#user4, #user5, #user6, 
#bottomshelf{background:#f8f9f6}
#topshelf li, 
#bottomshelf li{background:url(../images/mint/shelf_li.gif) no-repeat 0px 6px}
#topshelf .yjsquare h3{border-bottom:1px solid #c0cf8e}
#cp{color:#18110b}
#cp a:link, 
#cp a:visited{color:#78980D}
#cp a:hover{color:#18110b}
#topshelf img.border, 
#bottomshelf img.border{border:1px solid #bdbcba; background:#e4e6e0; margin:5px 3px 0 0}
#navigator10 li.selected_right div.inner span.title{color:#c0cf8e}
