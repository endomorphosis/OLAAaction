<?php
/*======================================================================*\
|| #################################################################### ||
|| # Youjoomla LLC - YJ- Licence Number 456BT510
|| # Licensed to - Ash Shepherd
|| # ---------------------------------------------------------------- # ||
|| # Copyright (C) 2006-2009 Youjoomla LLC. All Rights Reserved.        ||
|| # This file may not be redistributed in whole or significant part. # ||
|| # ---------------- THIS IS NOT FREE SOFTWARE ---------------- #      ||
|| # http://www.youjoomla.com | http://www.youjoomla.com/license.html # ||
|| #################################################################### ||
\*======================================================================*/

defined( '_JEXEC' ) or die( 'Restricted index access' );

define( 'TEMPLATEPATH', dirname(__FILE__) );
require( TEMPLATEPATH.DS."settings.php");



?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" >
<head>
<jdoc:include type="head" />
<?php JHTML::_('behavior.mootools'); ?>
<link href="<?php echo $yj_site ?>/css/template.<?php echo $cssextens; ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo $yj_site ?>/css/<?php echo $css_file; ?>.<?php echo $cssextens; ?>" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="<?php echo $yj_site ?>/favicon.ico" />
<?php require( TEMPLATEPATH.DS."head.php");?>
</head>
<body id="color" style="background:url(<?php echo $yj_site ?>/images/bgs/<?php echo $bgimage ?>) no-repeat center top;">
<div id="centertop" style="font-size:<?php echo $css_font; ?>; width:<?php echo $css_width; ?>;">
    <?php if ($ie6notice == 1){ ?>
    <!-- notices -->
    <!--[if lte IE 6]>
<p class="clip" style="text-align:center" >Hello visitor.You are using IE6 , an outdated version of Internet Explorer. Please consider upgrading. Click <a href="http://www.webstandards.org/action/previous-campaigns/buc/" target="_blank" >here</a> for more info .</p>
<![endif]-->
    <?php } ?>
    <?php if($nonscript == 1 ){?>
    <noscript>
    <p class="error" style="text-align:center" >This Joomla Template is equiped with JavaScript. Your browser does not support JavaScript! Please enable it for maximum experience. Thank you.</p>
    </noscript>
    <?php } ?>
    <!--end  notices -->
    <!--header-->
    <div id="header">
        <div id="logo" class="png">
            <div id="tags">
                <h1> <a href="index.php" title="<?php echo $tags?>"><?php echo $seo ?></a> </h1>
            </div>
            <!-- end tags -->
        </div>
        <!-- end logo -->
        <?php if ($this->countModules('banner')) {?>
        <div id="banner">
            <jdoc:include type="modules" name="banner" style="raw" />
        </div>
        <!-- end banner -->
        <?php } ?>
    </div>
    <!-- end header -->
    <!--top menu-->
    <div id="<?php echo $topmenuclass ?>" style="font-size:<?php echo $css_font; ?>;">
        <div id="<?php echo $menuclass ?>"> <?php echo $topmenu; ?> </div>
    </div>
    <!-- end top menu -->
    <!-- advert 1 -->
    <?php if ($this->countModules('advert1')) {?>
    <div id="advholder">
    	<div id="advert1"><jdoc:include type="modules" name="advert1" style="yjsquare" /></div>
    
    </div><?php } ?>
    <!-- end -->
    <?php if ($this->countModules('user1') || $this->countModules('user2') || $this->countModules('user3')) {?>
    <!-- top shelf -->
    <div id="topshelf">
    <?php if ($this->countModules('user1')) {?>
    <div id="user1" style="width:<?php echo $topswidth ?>;"><div class="topsin"><jdoc:include type="modules" name="user1" style="yjsquare" /></div></div><?php } ?>
     <?php if ($this->countModules('user2')) {?>
    <div id="user2" style="width:<?php echo $topswidth ?>;"><div class="topsin"><jdoc:include type="modules" name="user2" style="yjsquare" /></div></div><?php } ?>
    <?php if ($this->countModules('user3')) {?>
    <div id="user3" style="width:<?php echo $topswidth ?>;"><div class="topsin"><jdoc:include type="modules" name="user3" style="yjsquare" /></div></div><?php } ?>
    
    </div>
    <!-- end --><?php } ?>
    
    
</div>
<!-- end centartop-->
<?php require( TEMPLATEPATH.DS."layouts/layout".$site_layout.".php");?>
    <?php if ($this->countModules('user4') || $this->countModules('user5') || $this->countModules('user6')) {?>
<!-- bottom shelf-->
<div id="bottomshelf">
	 <?php if ($this->countModules('user4')) {?>
    <div id="user4" style="width:<?php echo $bottomsswidth ?>;"><div class="bottomsin"><jdoc:include type="modules" name="user4" style="yjsquare" /></div></div><?php } ?>
     <?php if ($this->countModules('user5')) {?>
    <div id="user5" style="width:<?php echo $bottomsswidth ?>;"><div class="bottomsin"><jdoc:include type="modules" name="user5" style="yjsquare" /></div></div><?php } ?>
    <?php if ($this->countModules('user6')) {?>
    <div id="user6" style="width:<?php echo $bottomsswidth ?>;"><div class="bottomsin"><jdoc:include type="modules" name="user6" style="yjsquare" /></div></div><?php } ?>
</div>
<!-- end --><?php } ?>
<!-- footer -->
<div id="footer"  style="font-size:<?php echo $css_font; ?>; width:<?php echo $css_width; ?>;">
    <div id="youjoomla">
               <?php
# FOR YOUJOOMLA LLC COPYRIGHT REMOVAL VISIT THIS PAGE 
# http://www.youjoomla.com/faq/view/you-joomla-templates-club/can-i-remove-youjoomla.com-copyright/78
?>
        <div id="cp"> <?php echo getYJLINKS()  ?> </div>
    </div>
</div>
<!-- end footer -->
</div>

<!-- end centerbottom-->
</body>
</html>
